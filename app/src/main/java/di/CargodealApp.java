package di;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import features.common.crash.CrashHandler;
import features.log.LogWriter;

/**
 * Created by Eugene on 23.02.2017.
 */

public class CargodealApp extends Application {
    private final Thread.UncaughtExceptionHandler handler = new CrashHandler(Thread.getDefaultUncaughtExceptionHandler());
    private DependencyContainer dc;
    @Override
    public void onCreate() {
        super.onCreate();
        dc = new DependencyContainer(this);
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        LogWriter.log("onTrimMemory: "+level);
        LogWriter.getLogStorageManager().checkNeedToStoreLog();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        LogWriter.log("onLowMemory");
        LogWriter.getLogStorageManager().checkNeedToStoreLog();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public DependencyContainer getDc() {
        return dc;
    }
}
