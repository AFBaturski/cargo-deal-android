package di;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import androidx.lifecycle.ProcessLifecycleOwner;
import api.ApiHelper;
import api.TaskScheduler;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import common.AppLifecycleListener;
import common.CargoDealSystem;
import storage.SQLiteHelper;
import features.log.LogWriter;
import location.LocationManager;
import location.LocationReceiver;
import model.Model;
import notification.NotificationHelper;
import trips.TripRecorder;

public class DependencyContainer {
  public static DependencyContainer getInstance(Activity activity) {
    return ((CargodealApp) activity.getApplication()).getDc();
  }
  public static DependencyContainer getInstance(Service service) {
    return ((CargodealApp) service.getApplication()).getDc();
  }
  private final SQLiteDatabase storage;
  private final Model model;
  private final ApiHelper api;
  private final TaskScheduler scheduler;
  private final NotificationHelper notificationHelper;
  private final TripRecorder tripRecorder;
  private final LocationManager locationManager;
  private final LocationReceiver locationReceiver;
  private final CargoDealSystem cargodealSystem;
  private final Context context;

  public DependencyContainer(CargodealApp app) {
    this.context = app.getApplicationContext();
    SQLiteHelper.createInstance(app.getApplicationContext());
    storage = SQLiteHelper.instance().getReadableDatabase();
    scheduler = TaskScheduler.createInstance();
    model = Model.createInstance(this);
    api = ApiHelper.createInstance(model, app.getApplicationContext());
    notificationHelper = new NotificationHelper(app.getApplicationContext(), model);
    tripRecorder = new TripRecorder(model);
    locationManager = new LocationManager(this);
    cargodealSystem = CargoDealSystem.createInstance(this);
    locationReceiver = LocationReceiver.createInstance(this);
    ProcessLifecycleOwner.get().getLifecycle().addObserver(AppLifecycleListener.getInstance());
    cargodealSystem.executeStartupTasks();
    LogWriter.createInstance(
        context,
        throwable -> FirebaseCrashlytics.getInstance().recordException(throwable),
        message -> FirebaseCrashlytics.getInstance().log(message)
    );
  }

  public SQLiteDatabase getStorage() {
    return storage;
  }

  public Model getModel() {
    return model;
  }

  public ApiHelper getApi() {
    return api;
  }

  public TaskScheduler getScheduler() {
    return scheduler;
  }

  public NotificationHelper getNotificationHelper() {
    return notificationHelper;
  }

  public TripRecorder getTripRecorder() {
    return tripRecorder;
  }

  public LocationReceiver getLocationReceiver() {
    return locationReceiver;
  }

  public LocationManager getLocationManager() {
    return locationManager;
  }

  public Context getContext() {
    return context;
  }
}
