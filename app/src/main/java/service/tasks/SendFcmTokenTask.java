package service.tasks;

import android.content.Context;
import api.ApiHelper;
import api.TaskScheduler;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import features.log.LogWriter;
import model.Model;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendFcmTokenTask implements Runnable {
    private final TaskScheduler scheduler;
    private final Context context;
    private final ApiHelper api;
    private final Model model;

    public SendFcmTokenTask(Context context, TaskScheduler scheduler, ApiHelper api, Model model) {
        this.context = context;
        this.scheduler = scheduler;
        this.api = api;
        this.model = model;
    }

    @Override
    public void run() {
        sendFCMTokenToServer();
    }

    private void sendFCMTokenToServer(){
        final int RETRY_SEND_FCM_TOKEN_PERIOD = 10*1000;
        final String token = FirebaseInstanceId.getInstance().getToken();
        if(token == null || model.getUserInfo() == null)
            return;

        JsonArray channels = new JsonArray();
        channels.add("");

        JsonObject request = new JsonObject();
        request.addProperty("deviceType", "android");
        request.addProperty("pushType", "gcm");
        request.addProperty("deviceToken", token);
        request.add("channels", channels);
        JsonObject currentUser = new JsonObject();
        currentUser.addProperty("__type","Pointer");
        currentUser.addProperty("className","_User");
        currentUser.addProperty("objectId", model.getUserInfo().user_id);
        request.add("user", currentUser);

        LogWriter.log(this.getClass().getSimpleName()+" : createInstallation");
        Call<JsonObject> sendFCMTokenCall = api.getServerAPI().createInstallation(request);
        sendFCMTokenCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogWriter.logServerResponse(response);
                if(!response.isSuccessful()){
                    LogWriter.log("Can't send FCM token");
                    scheduler.schedule(new SendFcmTokenTask(context, scheduler, api, model), RETRY_SEND_FCM_TOKEN_PERIOD);
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                LogWriter.log("Failure to send FCM token: " + t.getLocalizedMessage());
                scheduler.schedule(new SendFcmTokenTask(context, scheduler, api, model), RETRY_SEND_FCM_TOKEN_PERIOD);
            }
        });
    }
}
