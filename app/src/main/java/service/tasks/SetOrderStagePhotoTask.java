package service.tasks;

import android.content.Context;
import api.ApiHelper;
import api.TaskScheduler;
import api.requests.OrderStagePhotoRequest;
import di.CargodealApp;
import common.Utils;
import di.DependencyContainer;
import features.log.LogWriter;
import model.Model;

public class SetOrderStagePhotoTask implements Runnable {
    private final ApiHelper api;
    private final Context context;
    private final TaskScheduler taskScheduler;
    private final Model model;
    private final DependencyContainer dependencyContext;

    public SetOrderStagePhotoTask(DependencyContainer dependencyContext) {
        this.dependencyContext = dependencyContext;
        this.context = dependencyContext.getContext();
        this.api = dependencyContext.getApi();
        this.model = dependencyContext.getModel();
        this.taskScheduler = dependencyContext.getScheduler();
    }

    @Override
    public void run() {
        final int PENDING_ORDER_STAGE_PHOTO_CHECK_PERIOD = 10 * 1000;
        final int RETRY_ORDER_STAGE_PHOTO_CHECK_PERIOD = 5 * 1000;
        if (!Utils.checkInternetConnection(context)) {
            taskScheduler.schedule(new SetOrderStagePhotoTask(dependencyContext), PENDING_ORDER_STAGE_PHOTO_CHECK_PERIOD);
            return;
        }
        OrderStagePhotoRequest request = new OrderStagePhotoRequest(context, api, model, result -> {
            if (model.getMyOrderStagePhoto() != null || model.isMyOrderStatusNeedToSubmit()) {
                LogWriter.log("SendOrderStagePhoto scheduled");
                if (result.isSuccess()) {
                    taskScheduler.execute(new SetOrderStagePhotoTask(dependencyContext));
                } else {
                    taskScheduler.schedule(new SetOrderStagePhotoTask(dependencyContext), RETRY_ORDER_STAGE_PHOTO_CHECK_PERIOD);
                }
            }
        });
        request.submit();
    }
}
