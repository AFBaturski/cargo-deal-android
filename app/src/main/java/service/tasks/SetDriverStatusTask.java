package service.tasks;

import android.content.Context;
import api.ApiHelper;
import api.DriverStatusTask;
import api.TaskScheduler;
import common.*;
import di.CargodealApp;
import di.DependencyContainer;
import features.log.LogWriter;
import model.Model;

import java.util.Date;

public class SetDriverStatusTask implements Runnable {
    private final ApiHelper api;
    private final Context context;
    private final TaskScheduler taskScheduler;
    private final Model model;
    private final DependencyContainer dependencyContext;


    public SetDriverStatusTask(DependencyContainer dependencyContext) {
        this.dependencyContext = dependencyContext;
        this.context = dependencyContext.getContext();
        this.api = dependencyContext.getApi();
        this.taskScheduler = dependencyContext.getScheduler();
        this.model = dependencyContext.getModel();
    }

    @Override
    public void run() {
        final int PENDING_DRIVER_STATUS_RETRY_PERIOD = 3*1000;

        if (!Utils.checkInternetConnection(context) && !Model.getInstance().isCrewStatusUpdatedOnServer()) {
            taskScheduler.schedule(new SetDriverStatusTask(dependencyContext), PENDING_DRIVER_STATUS_RETRY_PERIOD);
        } else if (!Model.getInstance().isCrewStatusUpdatedOnServer()) {
            taskScheduler.execute(new SendCoordsTask(dependencyContext));
            LogWriter.log(String.format("Смена статуса водителя %s", Utils.sdf.format(new Date())));
            new DriverStatusTask(dependencyContext, result -> {
                if (!result) {
                    taskScheduler.schedule(new SetDriverStatusTask(dependencyContext), PENDING_DRIVER_STATUS_RETRY_PERIOD);
                }
            }).submit();
        }
    }
}
