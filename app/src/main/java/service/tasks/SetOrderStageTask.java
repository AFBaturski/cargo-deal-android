package service.tasks;

import android.content.Context;
import api.TaskScheduler;
import api.requests.OrderStageRequest;
import common.*;
import di.DependencyContainer;
import features.log.LogWriter;
import features.order.stage.model.OrderStage;
import model.Model;

public class SetOrderStageTask implements Runnable {
    private final Context context;
    private final TaskScheduler scheduler;
    private final OnCompletion callback;
    private final DependencyContainer dependencyContext;

    public SetOrderStageTask(DependencyContainer dependencyContext, OnCompletion callback) {
        this.dependencyContext = dependencyContext;
        this.context = dependencyContext.getContext();
        this.scheduler = dependencyContext.getScheduler();
        this.callback = callback;
    }

    @Override
    public void run() {
        final int PENDING_ORDER_STATUS_CHECK_PERIOD = 10 * 1000;
        final int RETRY_ORDER_STATUS_CHECK_PERIOD = 5 * 1000;
        if (!Utils.checkInternetConnection(context)) {
            scheduler.schedule(new SetOrderStageTask(dependencyContext, new EmptyCompletion()), PENDING_ORDER_STATUS_CHECK_PERIOD);
            return;
        }
        scheduler.execute(new SendCoordsTask(dependencyContext));
        OrderStage orderStage = Model.getInstance().getMyOrderStage();
        OrderStageRequest request = new OrderStageRequest(dependencyContext, orderStage, result -> {
            if (Model.getInstance().isMyOrderStatusNeedToSubmit()) {
                LogWriter.log("SendMyOrderStatus scheduled");
                if (result.isSuccess()) {
                    scheduler.execute(new SetOrderStageTask(dependencyContext, callback));
                } else {
                    scheduler.schedule(new SetOrderStageTask(dependencyContext, callback), RETRY_ORDER_STATUS_CHECK_PERIOD);
                }
            } else {
                callback.onComplete(result.isSuccess());
            }
        });
        request.submit();
    }
}
