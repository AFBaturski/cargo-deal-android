package service.tasks;

import android.content.Context;
import api.ApiHelper;
import api.TaskScheduler;
import api.requests.UserPositionRequest;
import com.macsoftex.cargodeal.driver.R;
import common.*;
import di.DependencyContainer;
import features.log.LogWriter;
import location.LocationManager;
import model.Model;
import features.order.OrderManager;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class SendCoordsTask implements Runnable {
    private final TaskScheduler scheduler;
    private final Context context;
    private final ApiHelper api;
    private final Model model;
    private final DependencyContainer dependencyContext;

    public SendCoordsTask(DependencyContainer dependencyContext) {
        this.dependencyContext = dependencyContext;
        this.context = dependencyContext.getContext();
        this.scheduler = dependencyContext.getScheduler();
        this.api = dependencyContext.getApi();
        this.model = dependencyContext.getModel();
    }

    @Override
    public void run() {
        if (!Utils.checkInternetConnection(context)) {
            scheduler.schedule(this, OrderManager.getRequestPeriodDependsOnOrderStatus());
            return;
        }

        final List<Model.UserPosition> positions = model.getLastUserPosition(ApiHelper.MAX_POSITION_REQUEST_COUNT);
        final int requestPositionsCount = positions.size();

        if (requestPositionsCount == 0 && LocationManager.getLastDriverLocation() != null) {
            positions.add(Model.UserPosition.fromLocation(LocationManager.getLastDriverLocation()));
        }

        SendCoordsTask sendCoordsTask = new SendCoordsTask(dependencyContext);
        if (positions.isEmpty() || requestPositionsCount == 0) {
            scheduler.schedule(sendCoordsTask, OrderManager.getRequestPeriodDependsOnOrderStatus());
            return;
        }

        final long timeFrom = positions.get(0).timestampSec;
        final long timeTo = positions.get(requestPositionsCount - 1).timestampSec;

        Collection<Model.UserPosition> simplifiedPositions = PositionsSimplifier.simplify(positions);
        UserPositionRequest sendUserPositionTask = new UserPositionRequest(api, result -> {
            if (result) {
                LogWriter.log(context.getString(R.string.last_connect) + " (" + simplifiedPositions.size() + ") " + (Utils.sdf.format(new Date())));
                model.deleteUserPositionRange(timeFrom, timeTo);

                if (isNeedToImmediatelySendNextRequest(requestPositionsCount) && scheduler != null) {
                    scheduler.execute(sendCoordsTask);
                } else if (scheduler != null){
                    scheduler.schedule(sendCoordsTask, OrderManager.getRequestPeriodDependsOnOrderStatus());
                }
            }
        });
        sendUserPositionTask.submit(simplifiedPositions);

    }

    private boolean isNeedToImmediatelySendNextRequest(int requestPositionsCount) {
        return (requestPositionsCount == ApiHelper.NEED_NEXT_POSITION_REQUEST);
    }
}
