package service.tasks;

import android.content.Context;
import android.location.LocationManager;
import common.MessageEvent;
import org.greenrobot.eventbus.EventBus;

public class CheckGpsStateTask implements Runnable {
    private Context context;

    public CheckGpsStateTask(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );
        if (!(manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER))){
            EventBus.getDefault().post(new MessageEvent(null, MessageEvent.GPS_DISABLED, null));
        }
    }
}
