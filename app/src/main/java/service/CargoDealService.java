package service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import di.DependencyContainer;
import features.log.LogWriter;
import location.LocationManager;
import notification.NotificationHelper;
import features.main.HomeActivity;
import activities.ViewOrderActivity;
import model.Model;
import api.*;

public class CargoDealService extends Service {
  public static final String TAG = CargoDealService.class.getSimpleName();
  public static final String ACTION_START_SERVICE = "cargodeal.start_service";
  public static final String ACTION_STOP_SERVICE = "cargodeal.stop_service";
  private final IBinder binder = new LocalBinder();
  private TaskScheduler scheduler;
  private Model model;
  private NotificationHelper notificationHelper;
  private boolean isInitComplete = false;
  private LocationManager locationManager;

  public static void startService(Context context) {
    Intent intent = new Intent(context, CargoDealService.class);
    intent.setAction(ACTION_START_SERVICE);
    context.startService(intent);
  }

  public void killApp() {
    LogWriter.log("User initiated exit from app");
    LogWriter.getLogStorageManager().checkNeedToStoreLog();
    deInit();
    stopSelf();
    scheduler.shutdownNow();
    android.os.Process.killProcess(android.os.Process.myPid());
    System.exit(0);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    if (intent != null && intent.getAction() != null) {
      switch (intent.getAction()) {
        case ACTION_START_SERVICE:
          checkIsInitComplete();
          break;
        case ACTION_STOP_SERVICE:
          deInit();
          stopSelf();
          break;
      }
    } else {
      LogWriter.log("CargodealService restarted by system");
      init(); //Пересоздание сервиса системой
    }
    return START_STICKY;
  }

  private void checkIsInitComplete() {
    if (!isInitComplete) {
      init();
    }
  }

  @Override
  public void onCreate() {
    super.onCreate();
    DependencyContainer dependencyContext = DependencyContainer.getInstance(this);
    model = dependencyContext.getModel();
    scheduler = dependencyContext.getScheduler();
    notificationHelper = dependencyContext.getNotificationHelper();
    locationManager = dependencyContext.getLocationManager();
  }

  private void init() {
    locationManager.init(this);
    startServiceInForeground();
    isInitComplete = true;
  }

  @Override
  public IBinder onBind(Intent intent) {
    return binder;
  }

  @Override
  public boolean onUnbind(Intent intent) {
    return true;
  }

  @Override
  public void onRebind(Intent intent) {
    super.onRebind(intent);
  }

  private void startServiceInForeground() {
    Model.MyOrderInfo activeOrderInfo = model.getMyOrderInfo();
    PendingIntent pendingIntent;

    if (activeOrderInfo != null) {
      pendingIntent = ViewOrderActivity.newPendingIntent(getBaseContext(), activeOrderInfo.getAsOrder());
    } else {
      pendingIntent = HomeActivity.newPendingIntent(getBaseContext());
    }

    Notification notification = notificationHelper.prepareNotification(pendingIntent);
    startForeground(NotificationHelper.NOTIFICATION_ID, notification);
  }

  private void deInit() {
    isInitComplete = false;
    stopForeground(true);
  }

  @Override
  public void onDestroy() {
    deInit();
    super.onDestroy();
  }

  public class LocalBinder extends Binder {
    public CargoDealService getService() {
      return CargoDealService.this;
    }
  }
}
