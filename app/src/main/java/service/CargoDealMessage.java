package service;

import activities.*;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import android.os.Handler;
import android.os.Looper;
import com.google.gson.JsonObject;

import common.*;
import di.DependencyContainer;
import features.log.LogWriter;
import features.main.HomeActivity;
import features.order.model.Order;
import features.order.OrderFactory;
import location.LocationManager;
import model.*;
import notification.NotificationHelper;
import org.greenrobot.eventbus.EventBus;

import java.security.InvalidParameterException;
import java.util.*;

import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 06.05.2019.
 * Macsoftex
 */
public class CargoDealMessage {
    private enum MessageType {
        NOTIFICATION_NEW_ORDER,
        NOTIFICATION_CANCEL_ORDER,
        NOTIFICATION_CHANGE_ORDER
    }
    private final Model model;

    private Context context;
    private ApiHelper api;
    private final JsonObject messageData;
    private boolean isNeedNotification;
    private NotificationHelper notificationHelper;
    private LocationManager locationManager;

    public CargoDealMessage(DependencyContainer dependencyContext, JsonObject messageData) {
        context = dependencyContext.getContext();
        api = dependencyContext.getApi();
        model = dependencyContext.getModel();
        notificationHelper = dependencyContext.getNotificationHelper();
        locationManager = dependencyContext.getLocationManager();
        this.messageData = messageData;
        isNeedNotification = true;
    }

    void handleMessage() {
        final String TYPE_NEW_ORDER = "newOrder";
        final String TYPE_CHANGE_ORDER = "changeOrder";
        final String TYPE_CANCEL_ORDER = "cancelOrder";
        final String TYPE_DISBAND_TRANSPORT_UNIT = "disband";

        String messageType = messageData.get("type").getAsString();
        switch (messageType){
            case TYPE_NEW_ORDER:
                handleNewOrderMessage(messageData);
                break;
            case TYPE_CHANGE_ORDER:
                handleChangeOrderMessage(messageData);
                break;
            case TYPE_CANCEL_ORDER:
                handleCancelOrderMessage(messageData);
                break;
            case TYPE_DISBAND_TRANSPORT_UNIT:
                handleDisbandTransportUnitMessage(messageData);
                break;
            default:
                throw new InvalidParameterException("Unknown message type: "+messageType);
        }
    }

    private void handleDisbandTransportUnitMessage(JsonObject messageData) {
        Model.getInstance().clearTransportTable();
        LogWriter.log("Transport unit disbanded");
        EventBus.getDefault().post(new MessageEvent(null, MessageEvent.TRANSPORT_UNIT_DISBANDED, null));
    }

    private void handleCancelOrderMessage(JsonObject messageData) {
        Order order = OrderFactory.fromJsonObject(messageData.get("object").getAsJsonObject());
        LogWriter.log("Order canceled: "+order);
        Intent intent = ActivityCancelOrder.newIntent(context, order);
        context.startActivity(intent);

        model.cancelCurrentOrderAndAwaitNewOne();

        PendingIntent pendingIntent = HomeActivity.newPendingIntent(context);
        notificationHelper.updateNotification(pendingIntent);
        notificationHelper.updateNotification(notificationHelper.getForegroundNotificationText());

        String notificationContentText = String.format(Locale.getDefault(), context.getString(R.string.order_declined), order.getNumber());
        postLocalNotification(intent, notificationContentText, MessageType.NOTIFICATION_CANCEL_ORDER);
    }

    private void handleNewOrderMessage(JsonObject messageData) {
        if (model.getDriverStatus() == DriverStatus.CREW_STATUS_NOT_READY
                || model.getDriverStatus() == DriverStatus.CREW_STATUS_WORK) {
            new Handler(Looper.getMainLooper()).post(() -> {
                Utils.showToast("Поступил новый заказ. Принять его можно после завершения текущего заказа", context);
            });
            return;
        }

        model.setInProgress(true);

        List<Order> ordersList = OrderFactory.fromJsonArray(messageData.get("objects").getAsJsonArray());
        for (Order order : ordersList) {
            api.confirmOrderReceived(order);
        }

        Intent intent = ActivityIncomingOrder.newIntent(context, ordersList);
        context.startActivity(intent);

        String notificationContentText = context.getString(R.string.new_order_arrived);
        postLocalNotification(intent, notificationContentText, MessageType.NOTIFICATION_NEW_ORDER);
    }

    private void handleChangeOrderMessage(JsonObject messageData) {
        Model.MyOrderInfo currentOrder = model.getMyOrderInfo();
        Model.MyOrderInfo changedOrder = Model.MyOrderInfo.fromJson(messageData.get("object").getAsJsonObject());
        String authorId = messageData.get("authorId").getAsString();
        LogWriter.log("Current order changed. Order: "+changedOrder.orderNumber + ". AuthorId: "+authorId);

        MyOrderChangesInfo myOrderChangesInfo = MyOrderChangesInfo.copyFrom(model.getMyOrderChangesInfo(changedOrder.orderNumber));
        myOrderChangesInfo.applyOrderChanges(currentOrder, changedOrder, authorId);
        model.saveMyOrderChanges(myOrderChangesInfo, changedOrder.orderNumber);

        currentOrder.merge(messageData.get("object").getAsJsonObject());
        model.saveMyOrderInfo(currentOrder);

        Intent intent = ActivityOrderChanges.newIntent(context);
        context.startActivity(intent);

        PendingIntent pendingIntent = ViewOrderActivity.newPendingIntent(context, currentOrder.getAsOrder());
        notificationHelper.updateNotification(pendingIntent);
        notificationHelper.updateNotification(notificationHelper.getForegroundNotificationText());

        String notificationContentText = String.format(Locale.getDefault(), context.getString(R.string.order_changed), changedOrder.orderNumber);
        postLocalNotification(intent, notificationContentText, MessageType.NOTIFICATION_CHANGE_ORDER);
    }

    private void postLocalNotification(Intent notificationIntent, String notificationContentText, MessageType notificationId) {
        if (!isNeedNotification)
            return;
        LocalNotification notification = new LocalNotification(context, notificationContentText, notificationIntent, notificationId.ordinal());
        notification.postNotification();
    }

    public void setNeedNotification(boolean needNotification) {
        isNeedNotification = needNotification;
    }
}
