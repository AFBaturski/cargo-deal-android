package service;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import di.CargodealApp;
import di.DependencyContainer;
import location.LocationManager;
import model.Model;

/**
 * Created by Игорь on 14.03.2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when eventType is received.
     *
     * @param remoteMessage Object representing the eventType received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (Model.getInstance().getInProgress() || Model.getInstance().getUserInfo() == null)
            return;

        if(LocationManager.isMockGps()) //Не принимать заказы, если координаты подставные
            return;

        if (remoteMessage.getData().size() > 0) {
            String dataString = remoteMessage.getData().get("data");
            processMessage(dataString);
        }
    }

    private void processMessage(String dataString) {
        JsonObject messageData = new JsonParser().parse(dataString).getAsJsonObject();
        if (!messageData.has("type"))
            return;
        CargoDealMessage cargoDealMessage = new CargoDealMessage(DependencyContainer.getInstance(this), messageData);
        cargoDealMessage.handleMessage();
    }
}