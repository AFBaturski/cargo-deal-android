package notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import com.macsoftex.cargodeal.driver.R;
import model.DriverStatus;
import model.Model;

public class NotificationHelper {
    public static final int NOTIFICATION_ID = 1338;
    private final Context context;
    private final Model model;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private String channelID = "";

    public NotificationHelper(Context context, Model model) {
        this.context = context;
        this.model = model;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        initNotificationChannel();
    }

    public Notification prepareNotification(PendingIntent pendingIntent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            notificationBuilder = new NotificationCompat.Builder(context, channelID);
        }
        else {
            notificationBuilder = new NotificationCompat.Builder(context);
        }
        return notificationBuilder
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(getForegroundNotificationText())
                .setContentIntent(pendingIntent)
                .setColor(context.getResources().getColor(R.color.green_500))
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();
    }

    private void initNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelID = context.getPackageName() + ".foreground";
            String channelName = context.getString(R.string.app_name);
            NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_MIN);
            channel.setLightColor(Color.BLUE);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }
    }

    public String getForegroundNotificationText() {
        Model.MyOrderInfo orderInfo = model.getMyOrderInfo();
        DriverStatus driverStatus = model.getDriverStatus();
        String statusString = "Статус: " + context.getString(driverStatus.titleId());
        if (orderInfo != null) {
            return statusString + ", Заказ № " + orderInfo.orderNumber;
        }
        return statusString;
    }

    public void updateNotification(PendingIntent pendingIntent){
        if (notificationBuilder == null)
            return;
        notificationBuilder.setContentIntent(pendingIntent);
        updateNotificationWithNewData();
    }

    public void updateNotification(String notificationText){
        if (notificationBuilder == null)
            return;
        notificationBuilder.setContentText(notificationText);
        notificationBuilder.setSound(null);
        updateNotificationWithNewData();
    }

    public void updateNotification(Uri notificationSound){
        notificationBuilder.setSound(notificationSound);
        updateNotificationWithNewData();
    }

    private void updateNotificationWithNewData(){
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }
}
