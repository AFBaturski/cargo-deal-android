package common;

import android.graphics.PointF;
import model.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import static common.Utils.filter;

public class PositionsSimplifier {
    public static Collection<Model.UserPosition> simplify(List<Model.UserPosition> positions) {
        int totalWaypointCount = 0;
        int deletedWaypointCount = 0;
        Model.UserPosition[] arrayTemplate = new Model.UserPosition[0];
        Vector<Model.UserPosition> lastWaypoints = new Vector<>();
        for (int index = 0, positionsSize = positions.size(); index < positionsSize; index++) {
            Model.UserPosition position = positions.get(index);
            lastWaypoints.add(position);
            ++totalWaypointCount;

            if (lastWaypoints.size() < 3) {
                continue;
            }

            if (!shouldDelete(lastWaypoints.toArray(arrayTemplate))) {
                lastWaypoints.remove(0);
                continue;
            }

            ++deletedWaypointCount;
            Model.UserPosition base = lastWaypoints.get(1);
            lastWaypoints.remove(1);
            positions.set(index, null);
        }
        return filter(positions, new NotNullPredicate<Model.UserPosition>());
    }

    private static boolean shouldDelete(Model.UserPosition[] waypointsTriangle) {
        PointF base = toPointF(waypointsTriangle[1]);
        PointF one = subtract(toPointF(waypointsTriangle[0]), base);
        PointF other = subtract(toPointF(waypointsTriangle[2]), base);
        double area = getTriangleArea(one, other);
        return area < 0.00000001;
    }

    private static double getTriangleArea(PointF one, PointF other) {
        return Math.abs(getPseudoDotProduct(one, other)) / 2.0;
    }

    private static double getPseudoDotProduct(PointF one, PointF other) {
        return one.x * other.y - one.y * other.x;
    }

    private static PointF subtract(PointF one, PointF other) {
        return new PointF(one.x - other.x, one.y - other.y);
    }

    private static PointF toPointF(Model.UserPosition waypoint) {
        return new PointF((float) waypoint.lon, (float) waypoint.lat);
    }
}
