package common;

public enum OperationState {
    IDLE,
    LOADING,
    SUCCESS,
    ERROR,
}
