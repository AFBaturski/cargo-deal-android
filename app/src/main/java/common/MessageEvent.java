package common;

import android.app.Activity;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by Alex Baturski on 17.08.2018.
 */

public class MessageEvent {
    public static final int DRIVER_STATUS_CHANGED = 0;
    public static final int GPS_DATA_CHANGED = 1;
    public static final int ORDER_STATUS_CHANGED = 2;
    public static final int NEED_RELOGIN = 3;
    public static final int NEED_ORDER_STATUS_SUBMIT = 4;
    public static final int ORDER_COMPLETE = 5;
    public static final int DRIVER_STATUS_SETTING = 6;
    public static final int CHECK_TRANSPORT_UNIT_FINISHED = 7;
    public static final int CHECK_TRANSPORT_UNIT_STARTED = 8;
    public static final int CANT_SET_STATUS_READY = 9;
    public static final int CANT_SET_STATUS_BECAUSE_MOCK = 10;
    public static final int NEED_DRIVER_STATUS_SUBMIT = 11;
    public static final int LOADER_EXCEPTION = 12;
    public static final int NEED_UPDATE_STATUS = 13;
    public static final int ORDER_CANCELED = 14;
    public static final int CONNECTING_TO_SERVER = 15;
    public static final int LOADER_FINISHED = 16;
    public static final int GPS_DISABLED = 17;
    public static final int TRANSPORT_UNIT_DISBANDED = 18;
    public static final int NEED_UPDATE_MAP = 19;
    public static final int INVALID_REQUEST = 20;

    private int mEventType;
    private String mMessage;
    private Activity mActivity;

    public MessageEvent(String message, int eventType, Activity activity) {
        mEventType = eventType;
        mMessage = message;
        mActivity = activity;
    }

    public static void postNeedReloginEvent() {
        MessageEvent needReloginEvent = new MessageEvent(null, MessageEvent.NEED_RELOGIN, null);
        EventBus.getDefault().post(needReloginEvent);
    }

    public static void postInvalidRequestEvent() {
        MessageEvent invalidRequestEvent = new MessageEvent(null, MessageEvent.INVALID_REQUEST, null);
        EventBus.getDefault().post(invalidRequestEvent);
    }


    public MessageEvent(){}

    public int getEventType() {
        return mEventType;
    }

    public String getMessage() {
        return mMessage;
    }

    public Activity getActivity() {
        return mActivity;
    }
}
