package common;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 05.05.2019.
 * Macsoftex
 */
public class LocalNotification {
    Context mContext;
    String mContentText;
    Intent mIntent;
    Notification mNotification;
    int mNotificationId;

    public LocalNotification(Context context, String contentText, Intent intent, int notificationId){
        mContext = context;
        mContentText = contentText;
        mIntent = intent;
        mNotificationId = notificationId;
        mNotification = makeDefaultNotification();
    }

    private Notification makeDefaultNotification(){
        LocalNotificationChannel notificationChannel = new LocalNotificationChannel(mContext);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0 , mIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationCompat.Builder notificationBuilder;
        notificationBuilder = createNotificationBuilder(notificationChannel);

        notificationBuilder.setPriority(Notification.PRIORITY_MAX)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(mContext.getString(R.string.app_name))
                    .setContentText(mContentText)
                    .setAutoCancel(true)
                    .setSound(NotificationSound.getNotificationSound())
                    .setContentIntent(pendingIntent);
        return notificationBuilder.build();
        }

    private NotificationCompat.Builder createNotificationBuilder(LocalNotificationChannel notificationChannel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            return new NotificationCompat.Builder(mContext, notificationChannel.getChannelId());
        } else {
            return new NotificationCompat.Builder(mContext);
        }
    }

    public void postNotification(){
        final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                notificationManager.notify(mNotificationId, mNotification);
            }
        }, 4000);
    }

}
