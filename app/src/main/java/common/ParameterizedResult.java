package common;

public class ParameterizedResult<T> {
  private T result;
  private Throwable exception;
  private ParameterizedResult(T result) {
    this.result = result;
  }

  private ParameterizedResult(Throwable exception) {
    this.exception = exception;
  }

  static class Success<T> extends ParameterizedResult<T> {
    public Success(T result) {
      super(result);
  }
}
  static class Failure extends ParameterizedResult<Void> {
    public Failure(Throwable exception) {
      super(exception);
    }
  }

  public boolean isSuccess() {
    return this instanceof Success;
  }

  public boolean isFailure() {
    return this instanceof Failure;
  }

  public static <T> ParameterizedResult<T> success(T value) {
    return new Success<>(value);
  }

  public static <T> ParameterizedResult<T> failure(Throwable exception) {
    return (ParameterizedResult<T>) new Failure(exception);
  }

  public T getOrNull() {
    if (this.isSuccess())
      return this.result;
    else
      return null;
  }

  public Throwable exceptionOrNull() {
    if (this.isFailure())
      return this.exception;
    else
      return null;
  }

  public String errorMessageOrNull() {
    if (this.isFailure())
      return this.exception.getLocalizedMessage();
    else
      return null;
  }
}
