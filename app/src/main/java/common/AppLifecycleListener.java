package common;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * Created by BAFF on 17.02.2020.
 * Macsoftex
 */
public class AppLifecycleListener implements LifecycleObserver {
    private static AppLifecycleListener instance = new AppLifecycleListener();

    public static AppLifecycleListener getInstance() {
        return instance;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onMoveBackground() {
        CargoDealSystem.getInstance().setNeedShowMyOrder(true);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onAppCreated() {
        CargoDealSystem.getInstance().setNeedShowMyOrder(true);
    }
}
