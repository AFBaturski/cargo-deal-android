package common;

public interface Predicate<T> {
    boolean apply(T object);
}
