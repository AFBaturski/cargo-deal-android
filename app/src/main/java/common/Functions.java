package common;

/**
 * Created by alex-v on 04.03.16.
 */
public class Functions {
    public static double headingInRadians(double lat1, double lon1, double lat2, double lon2) {
        double dLon = lon2 - lon1;
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

        double heading = Math.atan2(y, x);
        if (heading < 0)
            return 2 * Math.PI + heading;
        return heading;
    }

    public static double headingInDegrees(double lat1, double lon1, double lat2, double lon2) {
        return Math.toDegrees(headingInRadians(Math.toRadians(lat1), Math.toRadians(lon1), Math.toRadians(lat2), Math.toRadians(lon2)));
    }

    public static double normalize180(double heading) {
        while (true) {
            if (heading <= -180)
                heading += 360;
            else if (heading > 180)
                heading -= 360;
            else
                return heading;
        }
    }

    public static double normalize360(double heading) {
        while (true) {
            if (heading < 0)
                heading += 360;
            else if (heading >= 360)
                heading -= 360;
            else
                return heading;
        }
    }

    public static double angleDivergence(double one, double other) {
        double divergence = Math.max(one, other) - Math.min(one, other);
        if (divergence <= 180)
            return divergence;
        return 360 - divergence;
    }

    public static double reverseAngle(double angle) {
        if (angle > 180)
            return angle - 180;
        else
            return angle + 180;
    }
}
