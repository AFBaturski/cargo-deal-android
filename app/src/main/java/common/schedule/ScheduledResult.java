package common.schedule;

import java.util.concurrent.ScheduledFuture;

/**
 * Created by Alex Baturski on 12.12.2019.
 * Macsoftex
 */
public class ScheduledResult {
    private final ScheduledFuture mFuture;

    public ScheduledResult(ScheduledFuture future) {
        mFuture = future;
    }

    public boolean cancel() {
        return mFuture.cancel(true);
    }
}
