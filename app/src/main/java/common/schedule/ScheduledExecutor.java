package common.schedule;

import android.os.Build;

import java.util.Objects;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * Created by Alex Baturski on 12.12.2019.
 * Macsoftex
 */

public class ScheduledExecutor{
    private final ScheduledThreadPoolExecutor mExecutor;
    private final static ScheduledExecutor sInstance = new ScheduledExecutor();

    public static ScheduledExecutor getInstance() {
            return Objects.requireNonNull(sInstance);
    }

    private ScheduledExecutor() {
        int corePoolSize = Math.min(8, Runtime.getRuntime().availableProcessors()*2);
        mExecutor = new ScheduledThreadPoolExecutor(corePoolSize, new LowPriorityThreadFactory());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mExecutor.setRemoveOnCancelPolicy(true);
        }
    }

    public static ScheduledFuture<?> schedule(Runnable r, long delay){
        return getInstance().mExecutor.schedule(r, delay, TimeUnit.MILLISECONDS);
    }

    public static ScheduledFuture<?> schedule(Runnable r, long delay, long period){
        return getInstance().mExecutor.scheduleAtFixedRate(r, delay, period, TimeUnit.MILLISECONDS);
    }

    public static ScheduledFuture<?> submit(Runnable r){
        return getInstance().mExecutor.schedule(r, 0, TimeUnit.MILLISECONDS);
    }

    public static void purge() {
        getInstance().mExecutor.purge();
    }
}
