package common;

/**
 * Created by alex-v on 17.03.17.
 */
public interface OnCompletion {
    void onComplete(boolean result);
}
