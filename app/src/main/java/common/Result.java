package common;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import common.exceptions.EmptyResultException;

import java.util.Optional;

public class Result {
  private Object data;
  private Throwable exception;

  private Result(Object data) { this.data = data; }

  private Result() { this.data = null; }

  private Result(@NonNull Throwable exception) {
    this.exception = exception;
  }

  static class Success extends Result {
    public Success() {
      super();
  }
    public Success(Object data) {
      super(data);
  }  
}
  static class Failure extends Result {
    public Failure(@NonNull Throwable exception) {
      super(exception);
    }
  }

  public static Result success() {
    return new Success();
  }

  public static Result failure(@NonNull Throwable exception) {
    return new Failure(exception);
  }

  public static Result failure() {
    return new Failure(new EmptyResultException());
  }

  public boolean isSuccess() {
    return this instanceof Success;
  }

  public boolean isFailure() {
    return this instanceof Failure;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public Optional<Object> getData() {
    return Optional.of(data);
  }

  public Throwable exceptionOrNull() {
    if (this.isFailure())
      return this.exception;
    else
      return null;
  }

  public String errorMessageOrNull() {
    if (this.isFailure())
      return this.exception.getLocalizedMessage();
    else
      return null;
  }
}
