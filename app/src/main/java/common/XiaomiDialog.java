package common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.PreferenceManager;

import com.macsoftex.cargodeal.driver.R;


/**
 * Created by alex on 13.12.17.
 */

public class XiaomiDialog {
    private static final String SHOW_DIALOG_KEY = "XiaomiDialog";

    public static boolean isNeedToShow(Context context) {
        boolean show = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SHOW_DIALOG_KEY, true);
        return show && XiaomiTools.isMiuiOs();
    }

    public static void show(final Activity activity, final OnCompletion onCompletion) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(activity);
        dlgAlert.setTitle(R.string.xiaomi_dialog_title);
        dlgAlert.setMessage(R.string.xiaomi_dialog_text);
        dlgAlert.setCancelable(false);
        dlgAlert.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                resetNeedToShow(activity);
                boolean res = XiaomiTools.showHiddenAppsConfigActivity(activity, RequestCodeList.MIUI_POWER_HIDE_MODE_SETTINGS_REQUEST_CODE);

                if (onCompletion != null)
                    onCompletion.onComplete(res);
            }
        });

        try {
            dlgAlert.show();
        } catch (Exception e) {
            resetNeedToShow(activity);

            if (onCompletion != null)
                onCompletion.onComplete(false);
        }
    }

    private static void resetNeedToShow(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(SHOW_DIALOG_KEY, false).apply();
    }
}
