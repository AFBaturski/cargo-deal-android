package common;

/**
 * Created by alex-v on 20.04.17.
 */
public class RequestCodeList {
    public static final int PERMISSION_REQUEST_CODE = 500;
    public static final int DRAW_OVERLAYS_SETTINGS_REQUEST_CODE = 501;
    public static final int MAP_REQUEST_CODE = 502;
    public static final int DANGER_EDIT_PARAMETERS_REQUEST_CODE = 503;
    public static final int ADD_DANGER_REQUEST_CODE = 504;
    public static final int GOOGLE_PLAY_IN_APP_REQUEST_CODE = 505;
    public static final int MIUI_POWER_HIDE_MODE_SETTINGS_REQUEST_CODE = 506;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;
}
