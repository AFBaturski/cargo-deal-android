package common;

import android.view.View;

/**
 * Created by Alex Baturski on 01.04.2020.
 * Macsoftex
 */
public class TariffAccess {
    private static final int ACCESS_DENIED = 0;
    public static boolean isTariffAccessEnabled(double orderCostVal) {
        return ((int)orderCostVal != ACCESS_DENIED);
    }

}
