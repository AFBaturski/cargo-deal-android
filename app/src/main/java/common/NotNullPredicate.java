package common;

public class NotNullPredicate<T> implements Predicate<T>{
    @Override
    public boolean apply(T object) {
        return object != null;
    }
}
