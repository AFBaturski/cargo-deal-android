package common;

import features.order.model.Order;

public interface OrderActionsListener {
    void declineOrder(Order order);
}
