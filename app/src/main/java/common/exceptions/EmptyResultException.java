package common.exceptions;

public class EmptyResultException extends Exception {
  public EmptyResultException() {
    super("Empty result");
  }
}
