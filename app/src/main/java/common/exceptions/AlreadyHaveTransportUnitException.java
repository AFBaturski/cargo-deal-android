package common.exceptions;

public class AlreadyHaveTransportUnitException extends Exception {

    public AlreadyHaveTransportUnitException(String message) {
        super(message);
    }
}
