package common.exceptions;

public class HasActiveTripException extends Exception {

    public HasActiveTripException(String message) {
        super(message);
    }
}
