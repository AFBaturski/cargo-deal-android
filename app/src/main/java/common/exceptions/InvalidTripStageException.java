package common.exceptions;

import android.content.Context;
import com.macsoftex.cargodeal.driver.R;

public class InvalidTripStageException extends Exception {

    public InvalidTripStageException(String message) {
        super(message);
    }

    public InvalidTripStageException(Context context) {
        super(context.getString(R.string.InvalidTripStage));
    }
}
