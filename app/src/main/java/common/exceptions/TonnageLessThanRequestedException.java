package common.exceptions;

public class TonnageLessThanRequestedException extends Exception {
    public TonnageLessThanRequestedException(String message) {
        super(message);
    }
}
