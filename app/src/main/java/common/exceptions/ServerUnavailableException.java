package common.exceptions;

public class ServerUnavailableException extends Exception {
    public ServerUnavailableException(String message) {
        super(message);
    }
}
