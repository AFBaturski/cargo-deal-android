package common.exceptions;

import android.content.Context;
import com.macsoftex.cargodeal.driver.R;

public class ExceptionHelper {
    public static Exception checkExceptionForNoInternetConnection(Context context, Exception e) {
        if (e.getClass().getName().equals("java.net.UnknownHostException"))
            return new ServerUnavailableException(context.getString(R.string.no_server_access));
        else if (e.getClass().getName().equals("java.net.SocketTimeoutException")){
            return new ServerUnavailableException(context.getString(R.string.no_server_access));
        }
        return e;
    }
}
