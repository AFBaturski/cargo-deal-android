package common.exceptions;

public class CarrierMismatchException extends Exception {

    public CarrierMismatchException(String message) {
        super(message);
    }
}
