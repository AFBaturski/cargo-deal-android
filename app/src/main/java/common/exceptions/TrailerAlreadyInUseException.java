package common.exceptions;

public class TrailerAlreadyInUseException extends Exception {

    public TrailerAlreadyInUseException(String message) {
        super(message);
    }
}
