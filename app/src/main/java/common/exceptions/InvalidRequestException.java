package common.exceptions;

import android.content.Context;
import com.macsoftex.cargodeal.driver.R;

public class InvalidRequestException extends Exception {

    public InvalidRequestException(String message) {
        super(message);
    }

    public InvalidRequestException(Context context) {
        super(context.getString(R.string.InvalidRequest));
    }
}
