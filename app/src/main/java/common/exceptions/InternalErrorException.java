package common.exceptions;

import android.content.Context;
import com.macsoftex.cargodeal.driver.R;

public class InternalErrorException extends Exception {

    public InternalErrorException(String message) {
        super(message);
    }

    public InternalErrorException(Context context) {
        super(context.getString(R.string.InternalError));
    }
}
