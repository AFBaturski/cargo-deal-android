package common.exceptions;

public class NoOfferException extends Exception {

    public NoOfferException(String message) {
        super(message);
    }
}
