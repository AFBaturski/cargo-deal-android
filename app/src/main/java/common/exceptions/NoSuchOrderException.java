package common.exceptions;

/**
 * Created by Alex Baturski on 06.05.2019.
 * Macsoftex
 */
public class NoSuchOrderException extends Exception {
    public NoSuchOrderException(String message) {
        super(message);
    }
}
