package common.exceptions;

import android.content.Context;
import com.macsoftex.cargodeal.driver.R;

public class NoActiveTripException extends Exception {
    public NoActiveTripException(String message) {
        super(message);
    }

    public NoActiveTripException(Context context) {
        super(context.getString(R.string.NoActiveTrip));
    }
}
