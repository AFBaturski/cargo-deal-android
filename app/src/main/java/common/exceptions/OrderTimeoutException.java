package common.exceptions;

public class OrderTimeoutException extends Exception {

    public OrderTimeoutException(String message) {
        super(message);
    }
}
