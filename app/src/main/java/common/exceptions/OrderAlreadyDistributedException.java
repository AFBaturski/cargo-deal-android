package common.exceptions;

public class OrderAlreadyDistributedException extends Exception {

    public OrderAlreadyDistributedException(String message) {
        super(message);
    }
}
