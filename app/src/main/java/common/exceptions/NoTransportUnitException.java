package common.exceptions;

import android.content.Context;
import com.macsoftex.cargodeal.driver.R;

public class NoTransportUnitException extends Exception {

    public NoTransportUnitException(String message) {
        super(message);
    }

    public NoTransportUnitException(Context context) {
        super(context.getString(R.string.NoTransportUnit));
    }
}
