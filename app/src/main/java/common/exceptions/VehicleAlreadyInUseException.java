package common.exceptions;

public class VehicleAlreadyInUseException extends Exception {

    public VehicleAlreadyInUseException(String message) {
        super(message);
    }
}
