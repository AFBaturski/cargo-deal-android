package common;

import android.app.Activity;

/**
 * Created by Alex Baturski on 17.08.2018.
 */

public class ExceptionEvent {
    public static final int NEED_GPS_ACCESS = 1;
    private int mEventType;
    private Exception mException;

    public ExceptionEvent(int eventType, Exception e) {
        mEventType = eventType;
        mException = e;
    }

    public int getEventType() {
        return mEventType;
    }

    public Exception getException() {
        return mException;
    }
}