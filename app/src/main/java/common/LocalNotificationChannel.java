package common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Build;

import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 05.05.2019.
 * Macsoftex
 */
public class LocalNotificationChannel {
    private String mChannelId;
    private String mChannelName;
    private Context mContext;
    private NotificationChannel mChannel;

    public LocalNotificationChannel(Context context) {
        mContext = context;
        mChannelId = mContext.getPackageName()+".server_notifications";
        mChannelName = mContext.getString(R.string.app_name);
        makeHighPriorityChannel();
    }

    public void makeHighPriorityChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            mChannel = new NotificationChannel(mChannelId, mChannelName, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setLightColor(Color.BLUE);
            mChannel.setSound(NotificationSound.getNotificationSound(), new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build());
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(mChannel);
        }
    }

    public String getChannelId() {
        return mChannelId;
    }
}
