package common;

import android.media.RingtoneManager;
import android.net.Uri;

/**
 * Created by Alex Baturski on 05.05.2019.
 * Macsoftex
 */
public class NotificationSound {
    public NotificationSound() {
    }

    public static Uri getNotificationSound(){
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }
}
