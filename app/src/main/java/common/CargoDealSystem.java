package common;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;

import api.TaskScheduler;
import com.google.gson.JsonArray;
import com.macsoftex.cargodeal.driver.R;

import di.DependencyContainer;
import location.LocationManager;
import model.DriverStatus;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import activities.MainActivity;
import features.transport_unit.ui.TransportChooserActivity;
import model.Model;
import service.CargoDealService;
import service.tasks.*;

/**
 * Created by Alex Baturski on 17.08.2018.
 */

public class CargoDealSystem {
    public static final int MAX_ALLOWED_SESSIONS = 1;
    public static final double MIN_SPEED_FOR_COURSE_DETECTION = 10;
    private static CargoDealSystem sInstance;
    private final Model model;
    private final TaskScheduler scheduler;
    private final DependencyContainer dependencyContext;
    private HandlerThread mHandlerThread;
    private Handler mHandler;
    private boolean isMyPositionFound;
    private boolean isMyOrderStatusSubmitting;
    private boolean isDriverStatusSubmitting;
    public static final int ORDERS_LIST = 0;
    public static final int FINISHED_ORDERS_LIST = 1;
    private Context context = null;
    private JsonArray userSessions;
    private boolean needShowMyOrder = true;
    private LocationManager locationManager;

    public static CargoDealSystem createInstance(DependencyContainer dependencyContext){
        if(sInstance == null){
            sInstance = new CargoDealSystem(dependencyContext);
            EventBus.getDefault().register(sInstance);
        }
        return sInstance;
    }

    public boolean isMyPositionFound() {
        return isMyPositionFound;
    }

    public void setMyPositionFound(boolean myPositionFound) {
        isMyPositionFound = myPositionFound;
    }

    public static CargoDealSystem getInstance() {
        return sInstance;
    }

    private CargoDealSystem(DependencyContainer dependencyContext) {
        this.dependencyContext = dependencyContext;
        this.context = dependencyContext.getContext();
        this.model = dependencyContext.getModel();
        this.scheduler = dependencyContext.getScheduler();
        this.locationManager = dependencyContext.getLocationManager();
    }

    @Subscribe (threadMode = ThreadMode.MAIN)
    public void handleMessageEvent(MessageEvent event){
        switch (event.getEventType()){
            case MessageEvent.DRIVER_STATUS_CHANGED:
                Utils.dismissSnackBar();
                if (event.getMessage() != null)
                    Utils.showToast(event.getMessage(), context);
                break;
            case MessageEvent.DRIVER_STATUS_SETTING:
                Utils.showSnackBar(event.getMessage(), event.getActivity());
                break;
            case MessageEvent.CHECK_TRANSPORT_UNIT_FINISHED:
                Utils.dismissSnackBar();
                Utils.showToast(event.getMessage(), context);
                break;
            case MessageEvent.CHECK_TRANSPORT_UNIT_STARTED:
                Utils.showSnackBar(event.getMessage(), event.getActivity());
                break;
            case MessageEvent.CANT_SET_STATUS_READY:
                Utils.dismissSnackBar();
                Utils.showToast(event.getMessage(), context);
                break;
            case MessageEvent.CANT_SET_STATUS_BECAUSE_MOCK:
                Utils.dismissSnackBar();
                Utils.showToast(event.getMessage(), context);
                break;
            case MessageEvent.NEED_DRIVER_STATUS_SUBMIT:
                Utils.dismissSnackBar();
                Utils.showToast(event.getMessage(), context);
                break;
            case MessageEvent.LOADER_EXCEPTION:
                Utils.dismissSnackBar();
                Utils.showToast(event.getMessage(), context);
                break;
            case MessageEvent.CONNECTING_TO_SERVER:
                Utils.showSnackBar(event.getMessage(), event.getActivity());
                break;
            case MessageEvent.LOADER_FINISHED:
                Utils.dismissSnackBar();
                break;
            case MessageEvent.TRANSPORT_UNIT_DISBANDED:
                Utils.showToast(context.getString(R.string.NoTransportUnit), context);
                showTransportUnitChooser();
                break;
            case MessageEvent.INVALID_REQUEST:
                Utils.showToast(context.getString(R.string.InvalidRequest), context);
                break;
            case MessageEvent.NEED_RELOGIN:
                Utils.showToast(context.getString(R.string.need_relogin), context);
                clearSession();
                logout();
                break;
        }
    }

    private void showTransportUnitChooser() {
        if (!model.isTransportUnitAvailable()) {
            Intent intent = new Intent(context, TransportChooserActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public boolean isDriverStatusSubmitting() {
        return isDriverStatusSubmitting;
    }

    public void setDriverStatusSubmitting(boolean driverStatusSubmitting) {
        isDriverStatusSubmitting = driverStatusSubmitting;
    }

    public Context getContext() {
        return context;
    }

    public JsonArray getUserSessions() {
        return userSessions;
    }

    public void setUserSessions(JsonArray userSessions) {
        this.userSessions = userSessions;
    }

    public void logout() {
        stopService();
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction(MainActivity.LOGOUT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void stopService() {
        Intent intent = new Intent(context, CargoDealService.class);
        intent.setAction(CargoDealService.ACTION_STOP_SERVICE);
        context.startService(intent);
    }

    public void clearSession() {
        Model.getInstance().deleteUserInfo();
        Model.getInstance().setUserInfo(null);
        Model.getInstance().saveCrewStatus(DriverStatus.CREW_STATUS_NOT_READY);
        Model.getInstance().saveCrewStatusUpdatedOnServer();
        Model.getInstance().setLogoutState(false);
    }

    public boolean isNeedShowMyOrder() {
        return needShowMyOrder;
    }

    public void setNeedShowMyOrder(boolean needShowMyOrder) {
        this.needShowMyOrder = needShowMyOrder;
    }

    public void executeStartupTasks() {
        scheduler.schedule(new CheckGpsStateTask(context), 0, LocationManager.CHECK_GPS_PERIOD);

        scheduler.execute(new SendCoordsTask(dependencyContext));
        scheduler.execute(new SetOrderStageTask(dependencyContext, new EmptyCompletion()));
        scheduler.execute(new SetOrderStagePhotoTask(dependencyContext));

        if (model.isCrewStatusUpdatedOnServer())
            scheduler.execute(new SetDriverStatusTask(dependencyContext));
    }
}
