package common;

public enum StageOperationState {
  LOADING,
  ERROR,
  STAGE_SET,
  ORDER_COMPLETED
}
