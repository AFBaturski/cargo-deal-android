package common;

public class Event<T> {
  private boolean hasBeenHandled;
  private T content;

  public Event(T content) {
    this.content = content;
  }

  public T getContentIfNotHandled() {
    if (hasBeenHandled) {
      return null;
    } else {
      hasBeenHandled = true;
      return content;
    }
  }
}
