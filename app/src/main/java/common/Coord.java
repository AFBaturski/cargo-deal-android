package common;

import android.location.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex-v on 01.03.16.
 */
public class Coord implements Serializable {
    private static final double EARTH_RADIUS = 6372797.6;

    private double latitude, longitude;

    public static Coord coord(double latitude, double longitude) {
        return new Coord(latitude, longitude);
    }

    public static Coord zeroCoord() {
        return new Coord(0, 0);
    }

    public Coord(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public float distanceTo(Coord coord) {
        Location locationA = new Location("point A");
        locationA.setLatitude(latitude);
        locationA.setLongitude(longitude);

        Location locationB = new Location("point B");
        locationB.setLatitude(coord.getLatitude());
        locationB.setLongitude(coord.getLongitude());

        return locationA.distanceTo(locationB);
    }

    public double headingTo(Coord coord) {
        return Functions.headingInDegrees(latitude, longitude, coord.getLatitude(), coord.getLongitude());
    }

    public Coord coordWithBearing(double bearing, double distance) {
        final double earthRadius = 6372797.6;
        double bearingInRadians = Math.toRadians(bearing);
        double distanceInRadians = distance / earthRadius;

        double latitudeInRadians = Math.toRadians(latitude);
        double longitudeInRadians = Math.toRadians(longitude);

        double resultLatitudeInRadians = Math.asin(Math.sin(latitudeInRadians) * Math.cos(distanceInRadians) + Math.cos(latitudeInRadians) * Math.sin(distanceInRadians) * Math.cos(bearingInRadians));
        double resultLongitudeInRadians = longitudeInRadians + Math.atan2(Math.sin(bearingInRadians) * Math.sin(distanceInRadians) * Math.cos(latitudeInRadians),
                Math.cos(distanceInRadians) - Math.sin(latitudeInRadians) * Math.sin(resultLatitudeInRadians));

        return new Coord(Math.toDegrees(resultLatitudeInRadians),  Math.toDegrees(resultLongitudeInRadians));
    }

    public List<Coord> bboxWithRadius(double radius) {
        double distanceInRadians = radius / EARTH_RADIUS;

        double latitudeInRadians = Math.toRadians(latitude);
        double longitudeInRadians = Math.toRadians(longitude);

        double sinLatitude = Math.sin(latitudeInRadians);
        double cosLatitude = Math.cos(latitudeInRadians);
        double sinDistance = Math.sin(distanceInRadians);
        double cosDistance = Math.cos(distanceInRadians);

        double sinDistance_cosLatitude = sinDistance * cosLatitude;

        ArrayList<Coord> result = new ArrayList<>();

        double bearing = 0.0;

        for (int i=0; i<4; i++) {
            double bearingInRadians = Math.toRadians(bearing);

            double resultLatitudeInRadians = Math.asin(sinLatitude * cosDistance + sinDistance_cosLatitude * Math.cos(bearingInRadians));
            double resultLongitudeInRadians = longitudeInRadians + Math.atan2(Math.sin(bearingInRadians) * sinDistance_cosLatitude,
                    cosDistance - sinLatitude * Math.sin(resultLatitudeInRadians));

            bearing += 90.0;

            Coord coord = new Coord(Math.toDegrees(resultLatitudeInRadians), Math.toDegrees(resultLongitudeInRadians));
            result.add(coord);
        }

        return result;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o))
            return true;

        if (!(o instanceof Coord))
            return false;

        Coord other = (Coord)o;

        final double epsilon = 1e-8;
        double latitudeDelta = Math.abs(getLatitude() - other.getLatitude());
        double longitudeDelta = Math.abs(getLongitude() - other.getLongitude());

        return latitudeDelta < epsilon && longitudeDelta < epsilon;
    }

    public boolean equals(Coord otherCoord, double epsilonInMeters) {
        if (equals(otherCoord))
            return true;

        final double distance = distanceTo(otherCoord);
        return distance < epsilonInMeters;
    }
}
