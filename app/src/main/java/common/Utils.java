package common;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import android.widget.FrameLayout;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import androidx.core.content.ContextCompat;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.macsoftex.cargodeal.driver.R;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import api.ServerApiManager;
import features.log.LogWriter;
import id.zelory.compressor.Compressor;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by Eugene on 23.02.2017.
 */

public class Utils {
    public static final int DISMISS = 0;
    public static final int SHOW_LOCATION_SETTINGS = 1;
    private static Toast mToast;
    private static com.google.android.material.snackbar.Snackbar mSnackBar;
    public static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.US);


    public static JsonObject createRPC(String method){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("jsonrpc", "2.0");
        jsonObject.addProperty("id", 1234);
        jsonObject.addProperty("method", method);
        return jsonObject;
    }

    public static String convertUnixTimeToDate(Long unixMilliseconds){
        if (unixMilliseconds == null)
            return "";

        Date date = new java.util.Date(unixMilliseconds);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        return sdf.format(date);
    }

    public static String convertUnixTimeToTimeShort(Long unixMilliseconds){
        if (unixMilliseconds == null)
            return "";

        Date date = new java.util.Date(unixMilliseconds);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm", Locale.getDefault());
        return sdf.format(date);
    }

    public static String convertUnixTimeToTime(long unixMilliseconds){
        Date date;
        date = new java.util.Date(unixMilliseconds);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        return sdf.format(date);
    }


    public static String convertUnixTimeToDateTime(Long unixMilliseconds){
        if (unixMilliseconds == null)
            return "";

        Date date = new java.util.Date(unixMilliseconds);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        return sdf.format(date);
    }


    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void showToast(String message, Context context) {
        if (message == null) return;

        switch (message) {
            case ServerApiManager.BAD_REQUEST:
                message = context.getString(R.string.need_relogin);
                break;
            case ServerApiManager.INTERNAL_ERROR:
                message = context.getString(R.string.InternalError);
                break;
        }

        if (mToast != null && mToast.getView() != null && mToast.getView().isShown())
            mToast.cancel();

        mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        initToast();
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mToast.show();
                }
            });
        } catch (Exception e){
            LogWriter.log(e.getMessage());
        }
    }

    private static void initToast() {
        View toastView = mToast.getView();
        TextView toastMessage = (TextView) toastView.findViewById(android.R.id.message);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParams.setMargins(16, 16, 16, 16);
        toastMessage.setLayoutParams(lParams);
        toastMessage.setPadding(1,1,1,1);
        toastMessage.setTextColor(Color.WHITE);
        toastMessage.setGravity(Gravity.CENTER);
        toastMessage.setTextSize(16);
        toastView.setBackgroundResource(R.drawable.toast);
        mToast.setGravity(Gravity.CENTER,0, 0);
    }

    public static boolean checkInternetConnection(Context context){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        try {
            boolean isNetworkAvailable = cm.getActiveNetworkInfo() != null;
            return (isNetworkAvailable &&
                    cm.getActiveNetworkInfo().isConnected());
        } catch (NullPointerException e) {
            return false;
        }
    }

    public static void showAlertSnackBar(String message, final Activity activity, int resId, View.OnClickListener action) {
        if (message == null) return;

        hideSnackBar();

        mSnackBar = com.google.android.material.snackbar.Snackbar.make(getRootView(activity), message+"\n", com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE);
        mSnackBar.setActionTextColor(Color.WHITE);
        mSnackBar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.red_500));
        mSnackBar.setAction(resId, action);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mSnackBar.show();
            }
        });
    }

    public static void hideSnackBar(){
        if (mSnackBar != null && mSnackBar.getView().isShown())
            mSnackBar.dismiss();
    }

    public static void showSnackBar(String message, Activity activity) {
        if (message == null) return;

        if (mSnackBar != null && mSnackBar.getView().isShown())
            mSnackBar.dismiss();

        mSnackBar = com.google.android.material.snackbar.Snackbar.make(getRootView(activity), message, com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE);
        mSnackBar.setActionTextColor(Color.WHITE);
        mSnackBar.setAction(R.string.hide, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSnackBar.dismiss();
            }
        });
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mSnackBar.show();
            }
        });
    }

    private static View getRootView(Activity activity) {
        final ViewGroup contentViewGroup = (ViewGroup) activity.findViewById(android.R.id.content);
        View rootView = null;

        if(contentViewGroup != null)
            rootView = contentViewGroup.getChildAt(0);

        if(rootView == null)
            rootView = activity.getWindow().getDecorView().getRootView();

        return rootView;
    }

    public static void dismissSnackBar() {
        if (mSnackBar != null)
            mSnackBar.dismiss();
    }

    public static String getFileAsBase64(Context context, File file){
        String base64 = null;
        if(file != null) {
            try {
                if(file.exists()) {
                    FileInputStream fis = context.openFileInput(file.getName());
                    byte [] imageBytes = new byte[(int) fis.getChannel().size()];
                    fis.read(imageBytes);
                    base64 = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
                }
            } catch (Exception e){
                LogWriter.log(e.getMessage());
                return null;
            }
        }
    return base64;
    }

    public static File compressImage(Context context, String imageName) {
        File fileToCompress = new File(context.getFilesDir(), imageName);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileToCompress.getAbsolutePath(), options);
        Compressor compressor = new Compressor(context)
                .setQuality(30)
                .setMaxHeight(options.outHeight)
                .setMaxWidth(options.outWidth)
                .setDestinationDirectoryPath(context.getFilesDir().getAbsolutePath());

        try {
            return compressor.compressToFile(fileToCompress, "compressed_"+fileToCompress.getName());
        } catch (IOException e) {
            LogWriter.log(e.getMessage());
            return null;
        }
    }

    public static String getThreadSignature()
    {
        Thread t = Thread.currentThread();
        long l = t.getId();
        String name = t.getName();
        long p = t.getPriority();
        String gname = t.getThreadGroup().getName();
        return (name + ":(id)" + l);
    }

    public static String convertUnixTimeToDateTimeWithoutYear(Long unixMilliseconds) {
        if (unixMilliseconds == null)
            return "";

        Date date = new java.util.Date(unixMilliseconds);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd.MM HH:mm", Locale.getDefault());
        return sdf.format(date);
    }

    public static Map<String, String> bundleToMap(Bundle extras) {
        Map<String, String> map = new HashMap<String, String>();
        Set<String> ks = extras.keySet();
        for (String key : ks) {
            map.put(key, extras.getString(key));
        }
        return map;
    }

//    public static
//        String destinationImagePath = tripId + "_" + stage + ".jpg";
//        if (copy(imagePath, destinationImagePath))
//            imagePath = destinationImagePath;
//    } catch (
//    IOException e) {
//        LogWriter.log(e.getMessage());
//    }
public static boolean copy(Context context, Uri srcFileUri, String dstFileName) throws IOException {
    File dst = new File(context.getFilesDir(), dstFileName);
    InputStream in = context.getContentResolver().openInputStream(srcFileUri);
    try {
        OutputStream out = new FileOutputStream(dst);
        try {
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } finally {
            out.close();
        }
    } finally {
        in.close();
    }
    return dst.length() != 0;
}

    public static <T> Collection<T> filter(Collection<T> target, Predicate<T> predicate) {
        Collection<T> result = new ArrayList<>();
        for (T element: target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static int getCorePoolSize() {
        return Math.min(8, Runtime.getRuntime().availableProcessors()*2);
    }

    public static class Snackbar {
        public static com.google.android.material.snackbar.Snackbar showSnackbarAtTop(View rootContainer, String message, int length) {
            return showSnackBar(rootContainer, message, Gravity.TOP, length);
        }

        public static com.google.android.material.snackbar.Snackbar showSnackbarAtBottom(View rootContainer, String message, int length) {
            return showSnackBar(rootContainer, message, Gravity.BOTTOM, length);
        }

        public static com.google.android.material.snackbar.Snackbar showSnackbarAtCenter(View rootContainer, String message, int length) {
            return showSnackBar(rootContainer, message, Gravity.CENTER, length);
        }

        private static com.google.android.material.snackbar.Snackbar showSnackBar(View rootContainer, String message, int gravity, int length) {
            com.google.android.material.snackbar.Snackbar snackBarView = com.google.android.material.snackbar.Snackbar.make(rootContainer, message, length);
            View view = snackBarView.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.gravity = gravity;
            view.setLayoutParams(params);
            view.setBackground(ContextCompat.getDrawable(rootContainer.getContext(), R.drawable.toast));
            snackBarView.setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_FADE);
            snackBarView.show();
            return snackBarView;
        }
    }
}
