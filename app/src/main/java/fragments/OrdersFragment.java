package fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import common.*;
import di.DependencyContainer;
import features.log.LogWriter;
import features.order.OrderManager;
import org.greenrobot.eventbus.EventBus;

import java.util.*;

import activities.OrdersActivity;
import loaders.FinishedOrdersLoader;
import loaders.OrdersLoader;
import model.FinishedOrdersList;
import features.order.model.Order;
import features.order.OrderManager;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 25.07.2018.
 */

public class OrdersFragment extends Fragment implements LoaderManager.LoaderCallbacks {
    public static final String TAG = OrdersFragment.class.getSimpleName();
    private static final String EXTRA_ORDERS_LIST_TYPE = "com.macsoftex.cargodeal.driver.order_list_type";
    private static final String EXTRA_ORDERS = "com.macsoftex.cargodeal.driver.orders";
    private ApiHelper mAPI;
    private DependencyContainer dependencyContext;

    public static OrdersFragment newInstance(List<Order> orders,  int orderListType){
        OrdersFragment fragment = new OrdersFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_ORDERS_LIST_TYPE, orderListType);
        args.putSerializable(EXTRA_ORDERS, (ArrayList<Order>) orders);
        fragment.setArguments(args);
        return fragment;
    }

    private RecyclerView recycler_view;
    private TextView empty_view;
    private DatePickerDialog mDatePickerDialog;
    private OrderAdapter mOrderAdapter;
    private List<Order> mOrders;
    private int orderListType;
    private Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_orders_fragment,container,false);

        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_view.getContext(),
                layoutManager.getOrientation());
        recycler_view.addItemDecoration(dividerItemDecoration);

        empty_view = view.findViewById(R.id.empty_view);
        empty_view.setText(R.string.refreshing_info);
        updateUI();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int direction;
        switch (item.getItemId()){
            case R.id.sortByNumber:
                direction = (FinishedOrdersList.getInstance().getOrderBy() == FinishedOrdersList.ORDER_BY_DATE)
                        ? FinishedOrdersList.DESCENDING : (FinishedOrdersList.getInstance().getDirection() == FinishedOrdersList.ASCENDING)
                        ? FinishedOrdersList.DESCENDING : FinishedOrdersList.ASCENDING;
                FinishedOrdersList.getInstance().setDirection(direction);
                FinishedOrdersList.getInstance().setOrderBy(FinishedOrdersList.ORDER_BY_NUMBER);
                mOrders = FinishedOrdersList.getInstance().getFinishedOrders();
                updateUI();
                return true;
            case R.id.sortByDate:
                direction = (FinishedOrdersList.getInstance().getOrderBy() == FinishedOrdersList.ORDER_BY_NUMBER)
                        ? FinishedOrdersList.DESCENDING : (FinishedOrdersList.getInstance().getDirection() == FinishedOrdersList.ASCENDING)
                        ? FinishedOrdersList.DESCENDING : FinishedOrdersList.ASCENDING;
                FinishedOrdersList.getInstance().setDirection(direction);
                FinishedOrdersList.getInstance().setOrderBy(FinishedOrdersList.ORDER_BY_DATE);
                mOrders = FinishedOrdersList.getInstance().getFinishedOrders();
                updateUI();
                return true;
            case R.id.filterButton:
                mDatePickerDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (orderListType == CargoDealSystem.FINISHED_ORDERS_LIST)
            inflater.inflate(R.menu.orders_history, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void updateUI() {
        if (mOrders.size() == 0) {
            empty_view.setText(R.string.no_orders_available);
            empty_view.setVisibility(View.VISIBLE);
        }
        else
            empty_view.setVisibility(View.GONE);

        mOrderAdapter = new OrderAdapter(mOrders);
        recycler_view.setAdapter(mOrderAdapter);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAPI = ApiHelper.getInstance();

        orderListType = (int)getArguments().getSerializable(EXTRA_ORDERS_LIST_TYPE);
        mOrders = (List<Order>) getArguments().getSerializable(EXTRA_ORDERS);

        int title = (orderListType == CargoDealSystem.ORDERS_LIST)? R.string.menu_orders :R.string.menu_finished_orders;
        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } catch (Exception e){
            LogWriter.log(e.getMessage());
        }
        //Забираем тип списка: Мои заказы или История заказов

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        mDatePickerDialog = new DatePickerDialog(getContext(), datePickerListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.setCancelable(true);
        mDatePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.clearFilter), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    FinishedOrdersList.getInstance().setFiltered(false);
                    mOrders = (orderListType == CargoDealSystem.ORDERS_LIST) ? OrderManager.getInstance().getOrders()
                            : FinishedOrdersList.getInstance().getFinishedOrders();
                    updateUI();
                }
            }
        });
        mDatePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.applyFilter), mDatePickerDialog);

        final Bundle args = new Bundle();
        args.putLong("timeInterval", ApiHelper.ORDERS_LIST_PERIOD);//забираем поступившие заказы за последние X часов
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isDetached()) {
                    getLoaderManager().destroyLoader(orderListType);
                    getLoaderManager().initLoader(orderListType, args, OrdersFragment.this).forceLoad();
                }
            }
        },500);
        setHasOptionsMenu(true);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(selectedYear, selectedMonth, selectedDay, 0, 0);
            FinishedOrdersList.getInstance().setFiltered(true);
            FinishedOrdersList.getInstance().setFilteredDate(calendar.getTime());
            mOrders = FinishedOrdersList.getInstance().getFinishedOrders();
            updateUI();
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(Objects.requireNonNull(getActivity()));
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if(id == CargoDealSystem.ORDERS_LIST){
            return new OrdersLoader(dependencyContext, args);
        } else if (id == CargoDealSystem.FINISHED_ORDERS_LIST) {
            return new FinishedOrdersLoader(mAPI, getActivity(), args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if(loader instanceof OrdersLoader){
            OrdersLoader ordersLoader = (OrdersLoader) loader;
            //Обновляем список в главном потоке
            handler.post(new Runnable() {
                @Override
                public void run() {
                    updateUI();
                }
            });

            if (ordersLoader.getException() != null) {
                String message = (ordersLoader.getException() != null)? ordersLoader.getException().getMessage(): null;
                EventBus.getDefault().post(new MessageEvent(message, MessageEvent.LOADER_EXCEPTION, getActivity()));
            }
        }

        if(loader instanceof FinishedOrdersLoader){
            FinishedOrdersLoader finishedOrdersLoader = (FinishedOrdersLoader) loader;
            //Обновляем список в главном потоке
            handler.post(new Runnable() {
                @Override
                public void run() {
                    updateUI();
                }
            });

            if(finishedOrdersLoader.getException() != null){
                String message = (finishedOrdersLoader.getException() != null)? finishedOrdersLoader.getException().getMessage(): null;
                EventBus.getDefault().post(new MessageEvent(message, MessageEvent.LOADER_EXCEPTION, getActivity()));
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView additionalDescriptionText;
        private TextView createdAtText;
        private TextView orderNumberText;
        private TextView tonnageText;
        private TextView descriptionText;
        private ImageView statusIndicator;
        private TextView orderCost;
        private TextView orderCostLabel;
        private ImageView orderCostImageView;
        private Order mOrder;

        public OrderHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.orders_list_item, parent, false));
            orderNumberText = itemView.findViewById(R.id.orderNumberText);
            createdAtText = itemView.findViewById(R.id.createdAtText);
            tonnageText = itemView.findViewById(R.id.tonnageText);
            descriptionText = itemView.findViewById(R.id.descriptionText);
            additionalDescriptionText = itemView.findViewById(R.id.additionalDescriptionText);
            statusIndicator = itemView.findViewById(R.id.statusText);
            orderCost = itemView.findViewById(R.id.orderCost);
            orderCostLabel = itemView.findViewById(R.id.orderCostLabel);
            orderCostImageView = itemView.findViewById(R.id.orderCostImageView);

            itemView.setOnClickListener(this);
        }

        private void bind(Order order){
            mOrder = order;
            orderNumberText.setText(String.valueOf(this.mOrder.getNumber()));
            createdAtText.setText(Utils.convertUnixTimeToDateTimeWithoutYear(this.mOrder.getCreatedAt()));
            tonnageText.setText(String.valueOf(this.mOrder.getTonnage()));

            if (orderListType == CargoDealSystem.FINISHED_ORDERS_LIST){
                statusIndicator.setVisibility(View.GONE);
                //statusIndicator.setImageDrawable(getResources().getDrawable(R.drawable.status_finished));
                orderCost.setTextColor(getResources().getColor(R.color.colorBlack));
            } else if (orderListType == CargoDealSystem.ORDERS_LIST
                    && order.equals(OrderManager.getInstance().getActiveOrder())) {
                statusIndicator.setImageDrawable(getResources().getDrawable(R.drawable.status_active));
                orderCost.setTextColor(getResources().getColor(R.color.green_800));
            }
            else {
                statusIndicator.setImageDrawable(getResources().getDrawable(R.drawable.status_passive));
                orderCost.setTextColor(getResources().getColor(R.color.amber_900));
            }

            descriptionText.setText(this.mOrder.getLoadingPointAddress());
            additionalDescriptionText.setText(this.mOrder.getUnloadingPointAddress());

            double orderCostVal;
            if (orderListType == CargoDealSystem.ORDERS_LIST)
                orderCostVal = this.mOrder.getTariff()*this.mOrder.getTonnage();
            else
                orderCostVal = this.mOrder.getTariff()*this.mOrder.getUnloadedTonnage();

            orderCost.setText(String.format(Locale.getDefault(), "%.0f руб.", orderCostVal));
            handleTariffAccessForDriver(orderCostVal);
        }

        private void handleTariffAccessForDriver(double orderCostVal) {
            int visibility = TariffAccess.isTariffAccessEnabled(orderCostVal) ? View.VISIBLE : View.GONE;
            orderCost.setVisibility(visibility);
            orderCostLabel.setVisibility(visibility);
            orderCostImageView.setVisibility(visibility);
        }

        @Override
        public void onClick(View view) {
            Intent intent = OrdersActivity.newIntent(getActivity(),mOrders, mOrder.getNumber(), orderListType);
            startActivity(intent);
        }
    }

    private class OrderAdapter extends RecyclerView.Adapter<OrderHolder>{
        private List<Order> orders;

        public OrderAdapter(List<Order> orders){
            this.orders = orders;
        }

        @Override
        public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new OrderHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(OrderHolder holder, int position) {
        if (orders.size() == 0)
            return;
        try {
            Order order = orders.get(position);
            holder.bind(order);
        } catch (Exception e){
            LogWriter.log(e.getMessage());
        }
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }
    }

    Runnable showAlertDialog = new Runnable() {
        @Override
        public void run() {
            String title = getString(R.string.my_orders_load_failed);
            String body = getString(R.string.my_orders_load_failed_msg);
            AlertDialogFragment alertDialogFragment;
            alertDialogFragment = AlertDialogFragment.instance(title, body, true);
            alertDialogFragment.show(getFragmentManager(),"orders_loading_alert");
        }
    };

    private void returnToHome(){
        final OrdersActivity ordersActivity = (OrdersActivity) getActivity();
    }
}
