package fragments;

import android.content.*;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import api.TaskScheduler;
import common.*;
import common.exceptions.NoOfferException;
import storage.TransportContract;
import di.DependencyContainer;
import features.order.model.Order;
import features.order.OrderFactory;
import features.order.OrderManager;
import location.LocationManager;
import model.*;
import org.greenrobot.eventbus.EventBus;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import activities.PhotoCommentActivity;
import common.OrderActionsListener;
import common.TariffAccess;
import loaders.CancelIncomingOrderLoader;
import loaders.ConfirmIncomingOrderLoader;
import api.ApiHelper;
import api.ServerApiManager;
import com.macsoftex.cargodeal.driver.R;
import viewmodels.MyOrderFragmentViewModel;

/**
 * Created by Eugene on 24.02.2017.
 */

public class MyOrderFragment extends Fragment implements LoaderManager.LoaderCallbacks  {
    public static final String TAG = MyOrderFragment.class.getSimpleName();
    private static final boolean SILENT = true;
    private static final Integer NO_STATUS = -1;
    private String title;
    private String body;
    private Handler handler = new Handler();
    private View cancelButton;
    private TextView createdAt;
    private TextView totalCost;
    private TextView totalCostLabel;
    private TextView currentStatusText;
    private TextView currentStatusLabel;
    private Button declineButton;
    private ServerApiManager mServerAPIManager;
    private TextView supplier;
    private TextView customer;
    private TextView unloadingTime;
    private ApiHelper mAPI;

    public static final String UPDATE_MY_ORDER = "update my order";
    public static final String ARG_ORDER = "order";
    public static final String ARG_ORDER_LIST_TYPE = "order_list_type";
    private Order mOrder;
    private int mOrderListType;

    private Button setStatusButton, route_button_to, route_button_from, acceptButton;
    private Button dialUnloadingContactPhoneButton;
    private TextView weight,
            form,
            loadingAddress,
            loadingContactName,
            loadingDate,
            unloadingAddress,
            unloadingContactName,
            unloadingContactPhone,
            unloadingDate,
            comment, acceptOrderText,
            selectStatusText, cannotAcceptOrder;

    private Model.MyOrderInfo myOrderInfo;
    TaskScheduler scheduler;
    private DependencyContainer dependencyContext;
    private MyOrderFragmentViewModel viewModel;

    public static MyOrderFragment newInstance(Order order, int orderListType) {
        MyOrderFragment fragment = new MyOrderFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ORDER, order);
        args.putSerializable(ARG_ORDER_LIST_TYPE, orderListType);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mServerAPIManager = new ServerApiManager((AppCompatActivity) getActivity(), getContext(), SILENT);
        assert getArguments() != null;
        mOrder = (Order)getArguments().getSerializable(ARG_ORDER);
        mOrderListType = (int)getArguments().getSerializable(ARG_ORDER_LIST_TYPE);
    }

    private TextView articleType;
    private TextView articleBrand;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.my_order_layout, container, false);
        articleType = v.findViewById(R.id.articleType);
        articleBrand = v.findViewById(R.id.articleBrand);
        createdAt = (TextView) v.findViewById(R.id.createdAt);
        totalCost = (TextView) v.findViewById(R.id.totalCost);
        totalCostLabel = (TextView) v.findViewById(R.id.totalCostLabel);
        comment = (TextView) v.findViewById(R.id.Comment);
        form = (TextView) v.findViewById(R.id.Form);
        weight = (TextView) v.findViewById(R.id.Weight);
        loadingAddress = (TextView) v.findViewById(R.id.loading_address);
        loadingContactName = (TextView) v.findViewById(R.id.loading_contact_name);
        loadingDate = (TextView) v.findViewById(R.id.loading_date);
        supplier = (TextView) v.findViewById(R.id.supplier);
        customer = (TextView) v.findViewById(R.id.customer);
        unloadingAddress = (TextView) v.findViewById(R.id.unloading_address);
        unloadingContactName = (TextView) v.findViewById(R.id.unloading_contact_name);
        unloadingContactPhone = (TextView) v.findViewById(R.id.unloading_contact_phone);
        unloadingDate = (TextView) v.findViewById(R.id.unloading_date);
        unloadingTime = (TextView) v.findViewById(R.id.unloading_time);
        acceptOrderText = v.findViewById(R.id.acceptOrderText);
        acceptButton = v.findViewById(R.id.acceptButton);
        cancelButton = v.findViewById(R.id.cancelButton);
        declineButton = v.findViewById(R.id.declineButton);
        selectStatusText = v.findViewById(R.id.selectStatusText);
        currentStatusLabel = v.findViewById(R.id.currentStatusLabel);
        currentStatusText = v.findViewById(R.id.currentStatusText);
        setStatusButton = v.findViewById(R.id.setStatusButton);
        route_button_to = (Button) v.findViewById(R.id.route_button_to);
        route_button_from = (Button) v.findViewById(R.id.route_button_from);
        dialUnloadingContactPhoneButton = v.findViewById(R.id.dial_unloading_contact_phone_button);
        cannotAcceptOrder = v.findViewById(R.id.cannotAcceptOrder);

        updateUIforFinishedOrders();
        updateUIforActualOrders();

        setStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Model.OrderNextStage> orderNextStages = Model.getInstance().getOrderNextStages();
                Integer orderStatusId = (orderNextStages != null && orderNextStages.size() > 0) ? orderNextStages.get(0).id : null;
                String orderStatusCaption = (orderNextStages != null && orderNextStages.size() > 0) ? orderNextStages.get(0).caption: getString(R.string.no_status);

                Intent intent = new Intent(getActivity(), PhotoCommentActivity.class);
                intent.putExtra("StageID", orderStatusId);
                intent.putExtra("caption", orderStatusCaption);
                startActivity(intent);
            }
        });

        route_button_to.setOnClickListener(routeButtonOnClickListener);
        route_button_from.setOnClickListener(routeButtonOnClickListener);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new MessageEvent(getString(R.string.connecting_to_server), MessageEvent.CONNECTING_TO_SERVER, getActivity()));
                Bundle requestArgs = new Bundle();
                requestArgs.putInt("number",mOrder.getNumber());
                requestArgs.putDouble("tonnage",mOrder.getTonnage());
                requestArgs.putString("comment",mOrder.getComment());

                getActivity().getSupportLoaderManager().destroyLoader(ConfirmIncomingOrderLoader.ID);
                getActivity().getSupportLoaderManager().initLoader(ConfirmIncomingOrderLoader.ID, requestArgs, MyOrderFragment.this).forceLoad();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new MessageEvent(getString(R.string.connecting_to_server), MessageEvent.CONNECTING_TO_SERVER, getActivity()));
                Bundle requestArgs = new Bundle();
                requestArgs.putInt("number",mOrder.getNumber());
                if (mOrder != null && !mOrder.isActive())
                    requestArgs.putBoolean("hideOffer",true);
                getActivity().getSupportLoaderManager().destroyLoader(CancelIncomingOrderLoader.ID);
                getActivity().getSupportLoaderManager().initLoader(CancelIncomingOrderLoader.ID, requestArgs, MyOrderFragment.this).forceLoad();
            }
        });

        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new MessageEvent(getString(R.string.connecting_to_server), MessageEvent.CONNECTING_TO_SERVER, getActivity()));
                Bundle requestArgs = new Bundle();
                requestArgs.putInt("number",mOrder.getNumber());
                getActivity().getSupportLoaderManager().destroyLoader(CancelIncomingOrderLoader.ID);
                getActivity().getSupportLoaderManager().initLoader(CancelIncomingOrderLoader.ID, requestArgs, MyOrderFragment.this).forceLoad();
            }
        });

        return v;
    }

    private void updateUIforActualOrders() {
        if (mOrderListType != CargoDealSystem.ORDERS_LIST || mOrder == null)
            return;

        if (mOrder.isActive()){
            acceptOrderText.setVisibility(View.GONE);
            acceptButton.setVisibility(View.GONE);
            cancelButton.setVisibility(View.GONE);
            cannotAcceptOrder.setVisibility(View.GONE);
            selectStatusText.setVisibility(View.VISIBLE);
            setStatusButton.setVisibility(View.VISIBLE);
            currentStatusLabel.setVisibility(View.VISIBLE);
            currentStatusText.setVisibility(View.VISIBLE);
            updateDeclineButton();
        } else { //Если отображаемый заказ неактивный
            selectStatusText.setVisibility(View.GONE);
            setStatusButton.setVisibility(View.GONE);
            currentStatusLabel.setVisibility(View.GONE);
            currentStatusText.setVisibility(View.GONE);

            route_button_from.setVisibility(View.GONE);
            route_button_to.setVisibility(View.GONE);
            declineButton.setVisibility(View.GONE);
        }

        if (OrderManager.getInstance().getActiveOrder() == null) { //Если нет активного заказа
            cannotAcceptOrder.setVisibility(View.GONE);
            acceptButton.setEnabled(true);
            cancelButton.setEnabled(true);
            declineButton.setVisibility(View.GONE);
        }
    }

    private void updateDeclineButton() {
        if (mOrder == null)
            return;
        Integer orderStatusId = mOrder.getOrderStatusId();
        if (orderStatusId != null && !orderStatusId.equals(NO_STATUS))
            declineButton.setVisibility(View.GONE);
        else
            declineButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(Objects.requireNonNull(getActivity()));
    }

    private void updateUIforFinishedOrders() {
        if (mOrderListType != CargoDealSystem.FINISHED_ORDERS_LIST)
        return;

        acceptOrderText.setVisibility(View.GONE);
        acceptButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
        declineButton.setVisibility(View.GONE);
        cannotAcceptOrder.setVisibility(View.GONE);

        selectStatusText.setVisibility(View.GONE);
        setStatusButton.setVisibility(View.GONE);
        currentStatusLabel.setVisibility(View.GONE);
        currentStatusText.setVisibility(View.GONE);

        route_button_from.setVisibility(View.GONE);
        route_button_to.setVisibility(View.GONE);
    }

    private View.OnClickListener routeButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Model.getInstance().getOrderNextStages().size() > 0
                    && (Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_ACCEPTED
                    || Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_GET_TO_LOADING)) //Заказ только принят
                tryToStartYandexNavigator(mOrder.getLoadingPointLat(), mOrder.getLoadingPointLon());
            else if (Model.getInstance().getOrderNextStages().size() > 0
                    && (Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_LOADING_COMPLETE
                    || Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_GET_TO_UNLOADING)){ //Заказ только принят
                tryToStartYandexNavigator(mOrder.getUnloadingPointLat(), mOrder.getUnloadingPointLon());
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        updateUIforActualOrders();
        showMyOrderDescription();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(MyOrderFragmentViewModel.class);
        viewModel.checkForOrderChanges();
        observeOrderChangesWithViewUpdate();
    }

    private void observeOrderChangesWithViewUpdate() {
        if (viewModel.orderChanges != null) {
            viewModel.orderChanges.observe(getViewLifecycleOwner(), this::updateChangesView);
        }
    }

    private void updateChangesView(Map<Integer, ChangeData> changes) {
        ViewGroup rootView = (ViewGroup) getView();
        if (rootView == null) {
            return;
        }
        recursiveLoopChildren(rootView,
                changes.keySet().stream()
                        .map(this::getString)
                        .collect(Collectors.toList()));
    }

    public void recursiveLoopChildren(ViewGroup parent, List<String> changesTags) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                recursiveLoopChildren((ViewGroup) child, changesTags);
            } else {
                if (child instanceof TextView && changesTags.stream().anyMatch(item -> item.equals(child.getTag()))) {
                    ((TextView) child).setTextColor(getResources().getColor(R.color.red_500));
                }
            }
        }
    }

    private void showMyOrderDescription(){
        if (mOrder != null) {
            articleType.setText(mOrder.getArticleType());
            articleBrand.setText(mOrder.getArticleBrand());
            createdAt.setText(Utils.convertUnixTimeToDateTime(mOrder.getCreatedAt()));
            totalCost.setText(String.format(Locale.getDefault(),"%.0f руб.", mOrder.getTariff()*mOrder.getTonnage()));

            handleTariffAccessForDriver(mOrder.getTariff());

            comment.setText(mOrder.getComment());
            double yourTonnage = Model.getInstance().getSelectedTransport(TransportContract.TYPE_VEHICLE).getTonnage();
            if (Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER) != null)
                yourTonnage += Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER).getTonnage();

            if (mOrderListType == CargoDealSystem.ORDERS_LIST)
                weight.setText(String.format(Locale.getDefault(),"%.0f %s. Ваша грузоподъемность: " +
                    "%.0f", mOrder.getTonnage(), getString(R.string.weight_units), yourTonnage));
            else
                weight.setText(String.format(Locale.getDefault(),"%.0f %s", mOrder.getTonnage(), getString(R.string.weight_units)));

            form.setText(mOrder.getArticleShape());
            loadingAddress.setText(mOrder.getLoadingPointAddress());
            loadingContactName.setText(mOrder.getLoadingContactName());

            loadingDate.setText(Utils.convertUnixTimeToDate(mOrder.getLoadingDate()));
            supplier.setText(mOrder.getSupplier());
            customer.setText(mOrder.getCustomer());
            unloadingAddress.setText(mOrder.getUnloadingPointAddress());
            unloadingContactName.setText(mOrder.getUnloadingContactName());
            unloadingContactPhone.setText(mOrder.getUnloadingContactPhone());
            if (mOrder.getUnloadingContactPhone() != null && !mOrder.getUnloadingContactPhone().isEmpty()) {
                View.OnClickListener dialClick = view -> {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + mOrder.getUnloadingContactPhone()));
                    startActivity(callIntent);
                };
                dialUnloadingContactPhoneButton.setOnClickListener(dialClick);
                dialUnloadingContactPhoneButton.setVisibility(View.VISIBLE);
            }
            unloadingDate.setText(Utils.convertUnixTimeToDate(mOrder.getUnloadingBeginDate()));
            String unloadTime = String.format("c %s до %s", Utils.convertUnixTimeToTimeShort(mOrder.getUnloadingBeginDate()), Utils.convertUnixTimeToTimeShort(mOrder.getUnloadingEndDate()));
            unloadingTime.setText(unloadTime);
            updateCurrentStatus();
            updateNewStatus();

            if (mOrderListType == CargoDealSystem.FINISHED_ORDERS_LIST)
                return;

            //Отображаем кнопки Проложить маршрут
            synchronized (this) {
                if (Model.getInstance().getOrderNextStages().size() > 0
                        && (Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_LOADING_COMPLETE
                        || Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_GET_TO_UNLOADING)) {
                    route_button_from.setVisibility(View.GONE);
                    route_button_to.setVisibility(View.VISIBLE);
                } else if (Model.getInstance().getOrderNextStages().size() > 0
                        && (Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_ACCEPTED
                        || Model.getInstance().getOrderNextStages().get(0).id == Model.ORDER_STAGE_GET_TO_LOADING)) {
                    route_button_from.setVisibility(View.VISIBLE);
                    route_button_to.setVisibility(View.GONE);
                } else {
                    route_button_from.setVisibility(View.GONE);
                    route_button_to.setVisibility(View.GONE);
                }
            }
        }
    }

    private void handleTariffAccessForDriver(double orderTariff) {
        int visibility = TariffAccess.isTariffAccessEnabled(orderTariff) ? View.VISIBLE : View.GONE;
        totalCost.setVisibility(visibility);
        totalCostLabel.setVisibility(visibility);
    }

    private void updateCurrentStatus() {
        Model.MyOrderInfo orderInfo = Model.getInstance().getMyOrderInfo();
        if (orderInfo == null || orderInfo.orderStatusTitle.equals("")
                || !mOrder.isActive()) {
            hideOrderStatusUI();
            return;
        }
        showOrderStatusUI();
        String orderStatusDateTime = Utils.convertUnixTimeToTimeShort(orderInfo.orderStatusDateTime);
        String orderStatusTitle = orderInfo.orderStatusTitle;
        mOrder = OrderFactory.newBuilder(mOrder)
            .orderStatusId(orderInfo.orderStatusId)
            .build();
        currentStatusText.setText(String.format("%s %s", orderStatusDateTime, orderStatusTitle));
        updateDeclineButton();
    }

    private void showOrderStatusUI() {
        currentStatusLabel.setVisibility(View.VISIBLE);
        currentStatusText.setVisibility(View.VISIBLE);
    }

    private void hideOrderStatusUI() {
        currentStatusText.setVisibility(View.GONE);
        currentStatusLabel.setVisibility(View.GONE);
    }

    private void updateNewStatus(){
        List<Model.OrderNextStage> stages = Model.getInstance().getOrderNextStages();
        String newStatus = (stages != null && stages.size() > 0) ? stages.get(0).caption : getString(R.string.no_status);
        boolean needFinishActivity = (newStatus.equals(getString(R.string.no_status)) && setStatusButton.getVisibility() == View.VISIBLE);
        if (needFinishActivity)
            getActivity().finish();

        setStatusButton.setText(newStatus);
    }

    private void tryToStartYandexNavigator(double latitude, double longitude){
        // Создаем интент для построения маршрута
        Intent intent = new Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP");
        intent.setPackage("ru.yandex.yandexnavi");

        PackageManager pm = getActivity().getPackageManager();
        List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);

        // Проверяем, установлен ли Яндекс.Навигатор
        if (infos == null || infos.size() == 0) {
            // Если нет - будем открывать страничку Навигатора в Google Play
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=ru.yandex.yandexnavi"));
        } else {
            if (LocationManager.getLastDriverLocation() != null){
                intent.putExtra("lat_from", LocationManager.getLastDriverLocation().getCoordinate().getLatitude());
                intent.putExtra("lon_from", LocationManager.getLastDriverLocation().getCoordinate().getLongitude());
            }
            intent.putExtra("lat_to", latitude);
            intent.putExtra("lon_to", longitude);
        }

        // Запускаем нужную Activity
        startActivity(intent);
    }

    @NonNull
    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if(id == ConfirmIncomingOrderLoader.ID)
            return new ConfirmIncomingOrderLoader(dependencyContext, args);
        else if(id == CancelIncomingOrderLoader.ID)
            return new CancelIncomingOrderLoader(dependencyContext, args);
        else
            throw new InvalidParameterException("Specified loader not found.");
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object data) {
        EventBus.getDefault().post(new MessageEvent(getString(R.string.connecting_to_server), MessageEvent.LOADER_FINISHED, getActivity()));
        if(loader instanceof ConfirmIncomingOrderLoader){
            ConfirmIncomingOrderLoader confirmIncomingOrderLoader = (ConfirmIncomingOrderLoader) loader;

            Fragment prev = getChildFragmentManager().findFragmentByTag("incoming_order_confirmation");
            if (prev != null) {
                DialogFragment df = (DialogFragment) prev;
                df.dismiss();
            }
            if (confirmIncomingOrderLoader.getException() != null) {
                confirmIncomingOrderLoader.handleException(getContext(), getChildFragmentManager());
            } else {
                mOrder = OrderFactory.newBuilder(mOrder)
                    .active(true)
                    .build();
                updateUIforActualOrders();
                showMyOrderDescription();
                Utils.showToast(getString(R.string.accept_complete), getContext());
            }
        }

        if (loader instanceof CancelIncomingOrderLoader) {
            CancelIncomingOrderLoader cancelIncomingOrderLoader = (CancelIncomingOrderLoader)loader;
            if(cancelIncomingOrderLoader.getException() != null &&
                    !(cancelIncomingOrderLoader.getException() instanceof NoOfferException)){
                cancelIncomingOrderLoader.handleException(getContext(), getChildFragmentManager());
            } else if (cancelIncomingOrderLoader.getException() instanceof NoOfferException) {
                OrderManager.getInstance().removeOrderByNumber(mOrder.getNumber());
                removeOrder();
            } else if (cancelIncomingOrderLoader.getException() == null) {
                if (OrderManager.getInstance().getActiveOrder() != null &&
                        mOrder.getNumber().equals(OrderManager.getInstance().getActiveOrder().getNumber())) {
                    Model.getInstance().deleteCurrentOrder();
                    OrderManager.getInstance().removeActiveOrder();
                } else {
                    OrderManager.getInstance().removeOrderByNumber(mOrder.getNumber());
                }
                removeOrder();
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_READY);
            }
        }
    }

    @Override
    public void onLoaderReset( Loader loader) {

    }

    public static class StatusListEntry {
        String caption;
        public int id;
        StatusListEntry(String caption, int id) {
            this.caption = caption;
            this.id = id;
        }
        @Override
        public String toString() {
            return caption;
        }
    }

    private void removeOrder(){
        final OrderActionsListener orderActions = (OrderActionsListener) getActivity();
        orderActions.declineOrder(mOrder);
    }

    public void showAlertDialog(String titleMsg, String bodyMsg, final boolean needFinishActivity){
        this.title = titleMsg;
        this.body = bodyMsg;

        Runnable showAlertDialog = new Runnable() {
            @Override
            public void run() {
                AlertDialogFragment alertDialogFragment;
                alertDialogFragment = AlertDialogFragment.instance(title, body, needFinishActivity);
                alertDialogFragment.show(getChildFragmentManager(),"incoming_order_confirmation_alert");
            }
        };
        handler.post(showAlertDialog);
    }
}
