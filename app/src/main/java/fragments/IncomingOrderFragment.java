package fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import common.*;
import common.exceptions.NoOfferException;
import di.DependencyContainer;
import location.LocationManager;
import org.greenrobot.eventbus.EventBus;

import java.util.Locale;
import java.util.Objects;

import activities.ActivityIncomingOrder;
import activities.GoogleMapActivity;
import features.main.HomeActivity;
import storage.TransportContract;
import loaders.CancelIncomingOrderLoader;
import loaders.ConfirmIncomingOrderLoader;
import model.Model;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

public class IncomingOrderFragment extends Fragment implements LoaderManager.LoaderCallbacks  {
    static final String PAGE_NUMBER = "arg_page_number";
    static final String PAGE_COUNT= "arg_page_count";
    static final String ALLOWED_TONNAGE= "arg_allowed_tonnage";

    int pageNumber;
    int pageCount;
    private TextView orderId;
    private TextView loadingPointAddress;
    private TextView unloadingPointAddress;
    private DependencyContainer dependencyContext;
    private LocationManager locationManager;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(Objects.requireNonNull(getActivity()));
        locationManager = dependencyContext.getLocationManager();
    }

    private TextView tonnage;
    private TextView comment;
    private ImageView entryPointImg, conclusionPointImg;
    private Button accept, cancel;
    private ImageButton mLoadingPointButton;
    private ImageButton mUnloadingPointButton;
    private ProgressDialog progressDialog;
    private AlertDialog dialog;
    private String progressDialogTitle;
    private String title;
    private String body;
    private String dialog_button_text;
    private Handler handler = new Handler();
    private Bundle requestArgs;
    private Bundle order;
    private double allowedTonnage;
    private TextView tariff;
    private TextView tariffLabel;
    private ApiHelper api;
    private Model model;

    public static IncomingOrderFragment newInstance(Bundle ordersBundle, Integer page_number, Integer page_count, double allowedTonnage) {
        IncomingOrderFragment pageFragment = new IncomingOrderFragment();
        Bundle arguments = new Bundle();
        arguments.putBundle("order",ordersBundle.getBundle(page_number.toString()));
        arguments.putInt(PAGE_NUMBER, page_number);
        arguments.putInt(PAGE_COUNT, page_count);
        arguments.putDouble(ALLOWED_TONNAGE, allowedTonnage);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = ApiHelper.getInstance();
        model = Model.getInstance();
        pageNumber = getArguments().getInt(PAGE_NUMBER);
        pageCount = getArguments().getInt(PAGE_COUNT);
        allowedTonnage = getArguments().getDouble(ALLOWED_TONNAGE);
        order = getArguments().getBundle("order");
    }

    TextView articleType;
    TextView articleBrand;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incoming_order, null);

        articleType = (TextView) view.findViewById(R.id.articleType);
        articleBrand = (TextView) view.findViewById(R.id.articleBrand);
        comment = (TextView) view.findViewById(R.id.Comment);
        orderId = (TextView) view.findViewById(R.id.orderId);
        unloadingPointAddress = (TextView) view.findViewById(R.id.ConclusionPointAddress);
        loadingPointAddress = (TextView) view.findViewById(R.id.EntryPointAddress);
        tonnage = (TextView) view.findViewById(R.id.Weight);
        tariff = (TextView) view.findViewById(R.id.Tariff);
        tariffLabel = (TextView) view.findViewById(R.id.tariffLabel);
        accept = (Button) view.findViewById(R.id.accept);
        cancel = (Button) view.findViewById(R.id.cancel);
        mLoadingPointButton = view.findViewById(R.id.LoadingPointButton);
        mLoadingPointButton = view.findViewById(R.id.LoadingPointButton);
        mLoadingPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), GoogleMapActivity.class);
                intent.putExtra("longitude", order.getFloat("loadingPoint.longitude"));
                intent.putExtra("latitude", order.getFloat("loadingPoint.latitude"));
                intent.putExtra("title", order.getString("loadingPoint.address"));
                startActivity(intent);
            }
        });

        mUnloadingPointButton = view.findViewById(R.id.UnloadingPointButton);
        mUnloadingPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), GoogleMapActivity.class);
                intent.putExtra("longitude", order.getFloat("unloadingPoint.longitude"));
                intent.putExtra("latitude", order.getFloat("unloadingPoint.latitude"));
                intent.putExtra("title", order.getString("unloadingPoint.address"));
                startActivity(intent);
            }
        });

        TextView orderId = (TextView) view.findViewById(R.id.orderId);

        accept.setOnClickListener(onClickListener);
        cancel.setOnClickListener(onClickListener);

        updateText();
        return view;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle requestArgs = new Bundle();
            requestArgs.putInt("number",order.getInt("number"));
            requestArgs.putString("comment",order.getString("comment"));
            if (allowedTonnage <= 0)
                requestArgs.putDouble("tonnage",order.getDouble("tonnage"));
            else
                requestArgs.putDouble("tonnage", allowedTonnage);

            EventBus.getDefault().post(new MessageEvent(getString(R.string.connecting_to_server), MessageEvent.CONNECTING_TO_SERVER, getActivity()));
            if(view.getId() == R.id.accept){
               accept.setEnabled(false);
               getActivity().getSupportLoaderManager().destroyLoader(ConfirmIncomingOrderLoader.ID);
               getActivity().getSupportLoaderManager().initLoader(ConfirmIncomingOrderLoader.ID, requestArgs, IncomingOrderFragment.this).forceLoad();
            } else if(view.getId() == R.id.cancel){
                cancel.setEnabled(false);
                getActivity().getSupportLoaderManager().destroyLoader(CancelIncomingOrderLoader.ID);
                getActivity().getSupportLoaderManager().initLoader(CancelIncomingOrderLoader.ID, requestArgs, IncomingOrderFragment.this).forceLoad();
            }
        }
    };

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if(id == ConfirmIncomingOrderLoader.ID){
            return new ConfirmIncomingOrderLoader(dependencyContext, args);
        }
        if(id == CancelIncomingOrderLoader.ID){
            return new CancelIncomingOrderLoader(dependencyContext, args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        EventBus.getDefault().post(new MessageEvent(null, MessageEvent.LOADER_FINISHED, getActivity()));

        if (loader instanceof ConfirmIncomingOrderLoader) {
            ConfirmIncomingOrderLoader confirmIncomingOrderLoader = (ConfirmIncomingOrderLoader) loader;

            Fragment prev = getChildFragmentManager().findFragmentByTag("incoming_order_confirmation");
            if (prev != null) {
                DialogFragment df = (DialogFragment) prev;
                df.dismiss();
            }
            accept.setEnabled(true);
            //Если запрос завершился неудачно
            if (confirmIncomingOrderLoader.getException() != null) {
                confirmIncomingOrderLoader.handleException(getContext(), getChildFragmentManager());
            } else {
                Model.getInstance().setInProgress(false);
                acceptOrder();
            }
        }

        if (loader instanceof CancelIncomingOrderLoader) {
            CancelIncomingOrderLoader cancelIncomingOrderLoader = (CancelIncomingOrderLoader) loader;
            cancel.setEnabled(true);

            if (cancelIncomingOrderLoader.getException() != null &&
                    !(cancelIncomingOrderLoader.getException() instanceof NoOfferException)) {
                cancelIncomingOrderLoader.handleException(getContext(), getChildFragmentManager());
            } else {
                Model.getInstance().setInProgress(false);
                removeOrder();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private void acceptOrder(){
        accept.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        final ActivityIncomingOrder activityIncomingOrder = (ActivityIncomingOrder) getActivity();
            Intent intent = HomeActivity.newIntent(getContext());
            Bundle bundle = new Bundle();
                bundle.putString("fragmentName", "my_orders");
            intent.putExtras(bundle);
            startActivity(intent);
            locationManager.changeLocationUpdatesInterval();
            Utils.showToast(getString(R.string.accept_complete), getContext());
            getActivity().finish();
    }

    private void removeOrder(){
        final ActivityIncomingOrder activityIncomingOrder = (ActivityIncomingOrder) getActivity();
        int delay=0;
        if (activityIncomingOrder.getOrdersCount() == 0)
            return;
        accept.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        if (activityIncomingOrder.getOrdersCount() == 1){
            delay = 500;
            Intent intent = new Intent(getContext(), HomeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("fragmentName", "map");
            intent.putExtras(bundle);
            startActivity(intent);
            getActivity().finish();
        }
            Handler uiHandler = new Handler();
            uiHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    activityIncomingOrder.removeCurrentItem();
                }
            },delay);
    }

        public void hideProgressDialog(){
        Fragment prev = getFragmentManager().findFragmentByTag("incoming_order_confirmation");
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }
    }

    public void hideAlertDialog(){
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void updateText(){
        if(order == null)
            return;

        articleType.setText(order.getString("articleType"));
        articleBrand.setText(order.getString("articleBrand"));
        comment.setText(order.getString("comment"));
        tariff.setText(String.format(Locale.getDefault(), "%.0f руб.", order.getDouble("tariff")));
        handleTariffAccessForDriver(order.getDouble("tariff"));

        double yourTonnage = Model.getInstance().getSelectedTransport(TransportContract.TYPE_VEHICLE).getTonnage();
        if (Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER) != null)
               yourTonnage += Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER).getTonnage();

        orderId.setText(String.format(Locale.getDefault(),"%s %d (%d из %d)", getString(R.string.new_order_num), order.getInt("number"), pageNumber + 1, pageCount));
        if (allowedTonnage == -1)
            tonnage.setText(String.format(Locale.getDefault(),"%.0f %s. Ваша грузоподъемность: %.0f", order.getDouble("tonnage"), getString(R.string.weight_units), yourTonnage));
        else
            tonnage.setText(String.format(Locale.getDefault(),"%.0f %s. Ваша грузоподъемность: %.0f", allowedTonnage, getString(R.string.weight_units), yourTonnage));
        loadingPointAddress.setText(order.getString("loadingPoint.address"));
        unloadingPointAddress.setText(order.getString("unloadingPoint.address"));
    }

    private void handleTariffAccessForDriver(double orderTariff) {
        int visibility = TariffAccess.isTariffAccessEnabled(orderTariff) ? View.VISIBLE : View.GONE;
        tariff.setVisibility(visibility);
        tariffLabel.setVisibility(visibility);
    }
}
