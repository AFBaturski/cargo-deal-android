package fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.bumptech.glide.Glide;
import com.macsoftex.cargodeal.driver.R;
import di.CargodealApp;
import common.StageOperationState;
import common.Utils;
import features.log.LogWriter;
import features.order.model.Order;
import model.CarriageOrder;
import model.Model;
import viewmodels.PhotoCommentViewModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class PhotoCommentFragment extends Fragment {
    public static final String TAG = PhotoCommentFragment.class.getSimpleName();
    public static final int CAMERA_PERMISSION_REQ = 101;
    private Context context;
    private Uri imageUri;

    public static PhotoCommentFragment newInstance(){
        return new PhotoCommentFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    private PhotoCommentViewModel viewModel;
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(PhotoCommentViewModel.class);
        initObservers();
    }

    private void initObservers() {
        viewModel.error.observe(getViewLifecycleOwner(), event -> {
            String message = event.getContentIfNotHandled();
            if (message != null) {
                hideProgressDialog();
                Utils.showToast(message, getContext());
            }
        });

        viewModel.state.observe(getViewLifecycleOwner(), event -> {
            StageOperationState state = event.getContentIfNotHandled();
            if (state == null)
                return;
            if (state == StageOperationState.LOADING) {
                ProgressDialogFragment.instance(getString(R.string.order_status_progress)).show(
                        getChildFragmentManager(), "order_status_progress");
            } else {
                hideProgressDialog();
                Objects.requireNonNull(getActivity()).finish();
                if (state == StageOperationState.STAGE_SET) {
                    Utils.showToast(getString(R.string.order_status_success_msg), getContext());
                } else if (state == StageOperationState.ORDER_COMPLETED) {
                    Utils.showToast(getString(R.string.order_status_final_msg), getContext());
                }
            }
        });
    }

    private void hideProgressDialog() {
        Fragment progress = getChildFragmentManager().findFragmentByTag("order_status_progress");
        if (progress != null) {
            DialogFragment dialogFragment = (DialogFragment) progress;
            dialogFragment.dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.set_status_fragment, container, false);
        TextView newStatus = root.findViewById(R.id.new_status_textview);
        root.findViewById(R.id.takePhoto).setOnClickListener(onClickListener);
        root.findViewById(R.id.deletePhoto).setOnClickListener(onClickListener);
        root.findViewById(R.id.sendButton).setOnClickListener(onClickListener);
        root.findViewById(R.id.sendButtonMultiLine).setOnClickListener(onClickListener);

        newStatus.setText(Objects.requireNonNull(getActivity()).getIntent().getStringExtra("caption"));

        if (getActivity().getIntent().getIntExtra("StageID", -1) == Model.ORDER_STAGE_LOADING_COMPLETE)
            ((TextView)root.findViewById(R.id.tonnageLabel)).setText(R.string.tonnageLoading_hint);
        else if (getActivity().getIntent().getIntExtra("StageID", -1) == Model.ORDER_STAGE_FINAL)
            ((TextView)root.findViewById(R.id.tonnageLabel)).setText(R.string.tonnageUnloading_hint);
        else {
            root.findViewById(R.id.sendButtonMultiLine).setVisibility(View.GONE);
            root.findViewById(R.id.sendButton).setVisibility(View.VISIBLE);
            root.findViewById(R.id.tonnageFact).setVisibility(View.GONE);
            root.findViewById(R.id.takePhoto).setVisibility(View.GONE);
            root.findViewById(R.id.tonnageLabel).setVisibility(View.GONE);
        }
        Order order = Model.getInstance().getMyOrderInfo().getAsOrder();
        if (order instanceof CarriageOrder
                && getActivity().getIntent().getIntExtra("StageID", -1) == Model.ORDER_STAGE_LOADING_COMPLETE) {
            root.findViewById(R.id.mileageLabel).setVisibility(View.VISIBLE);
            root.findViewById(R.id.mileageEdit).setVisibility(View.VISIBLE);
            ((EditText)root.findViewById(R.id.mileageEdit)).setText(String.valueOf(order.getDistance()));
            root.findViewById(R.id.mileageEdit).setEnabled(order.getDistance() == 0);

            root.findViewById(R.id.invoiceNumberLabel).setVisibility(View.VISIBLE);
            root.findViewById(R.id.waybillNumberEdit).setVisibility(View.VISIBLE);
        }
            return root;
    }

    private final View.OnClickListener onClickListener = view -> {
        if(view.getId() == R.id.takePhoto){
            takePhotoClick();
        } else if(view.getId() == R.id.sendButton || view.getId() == R.id.sendButtonMultiLine){
            sendButtonClick();
        } else if(view.getId() == R.id.deletePhoto){
            deletePhotoClick();
        }
    };

    private void deletePhotoClick() {
        if (imageUri != null) {
            Objects.requireNonNull(getContext()).getContentResolver().delete(imageUri, null, null);
        }
        imageUri = null;
        ((ImageView)(Objects.requireNonNull(this.getView()).findViewById(R.id.photo))).setImageDrawable(null);
        ((ImageView)(Objects.requireNonNull(this.getView()).findViewById(R.id.photo))).setVisibility(View.GONE);
        (Objects.requireNonNull(this.getView()).findViewById(R.id.deletePhoto)).setVisibility(View.GONE);
        (Objects.requireNonNull(this.getView()).findViewById(R.id.takePhoto)).setVisibility(View.VISIBLE);
    }

    private void takePhotoClick() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        || ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    }, CAMERA_PERMISSION_REQ);        }
        else {
            takePhoto();
        }
    }

    private void sendButtonClick() {
        if (Model.getInstance().getMyOrderInfo() == null) {
            Objects.requireNonNull(getActivity()).finish();
            return;
        }
        int stageId = Objects.requireNonNull(getActivity()).getIntent().getIntExtra("StageID", -1);
        Map<String,String> params = new HashMap<>();
        params.put("stageId", String.valueOf(stageId));
        params.put("tonnage", ((TextView) Objects.requireNonNull(this.getView()).findViewById(R.id.tonnageFact)).getText().toString());
        params.put("comment", ((TextView) Objects.requireNonNull(this.getView()).findViewById(R.id.comment)).getText().toString());
        params.put("waybillNumber", ((TextView) Objects.requireNonNull(this.getView()).findViewById(R.id.waybillNumberEdit)).getText().toString());
        params.put("mileage", ((TextView) Objects.requireNonNull(this.getView()).findViewById(R.id.mileageEdit)).getText().toString());
        viewModel.sendOrderStage(imageUri, params);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CargodealApp dependencyContext = (CargodealApp) Objects.requireNonNull(getActivity()).getApplication();
        context = dependencyContext.getApplicationContext();
    }

    public static final int REQUEST_IMAGE_CAPTURE = 104;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            try {
                (Objects.requireNonNull(this.getView()).findViewById(R.id.takePhoto)).setVisibility(View.GONE);
                (Objects.requireNonNull(this.getView()).findViewById(R.id.deletePhoto)).setVisibility(View.VISIBLE);
                (Objects.requireNonNull(this.getView()).findViewById(R.id.photo)).setVisibility(View.VISIBLE);
                Glide.with(Objects.requireNonNull(getActivity()))
                        .load(imageUri)
                        .into(((ImageView)Objects.requireNonNull(this.getView()).findViewById(R.id.photo)));
            } catch (Exception e) {
                LogWriter.log(e.getMessage());
            }
        }
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String imageFileName = "cargoimage." + System.currentTimeMillis() + ".jpg";
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, imageFileName);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/Cargodeal/");
        imageUri = Objects.requireNonNull(getContext()).getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        if (imageUri == null) {
            values.remove(MediaStore.Images.Media.RELATIVE_PATH);
            imageUri = Objects.requireNonNull(getContext()).getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }
}
