package fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.core.content.ContextCompat;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import common.*;
import common.exceptions.NoTransportUnitException;
import di.DependencyContainer;
import features.log.LogWriter;
import location.LocationManager;
import model.DriverStatus;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import features.main.HomeActivity;
import activities.MainActivity;
import loaders.ChangeStatusLoader;
import loaders.VehiclesLoader;
import map.Driver;
import model.Model;
import model.Vehicle;
import api.ApiHelper;
import api.ServerApiManager;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 31.07.2018.
 * Macsoftex
 */

public class GoogleMapFragment extends Fragment implements LoaderManager.LoaderCallbacks<Object> {
    public static final String TAG = GoogleMapFragment.class.getSimpleName();
    private static final int VEHICLE_LOADER = 100;
    private static final boolean NO_SILENT = false;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private ServerApiManager mServerAPIManager;
    ArrayList<Marker> mMarkers = new ArrayList<>();
    private ClusterManager<Driver> mClusterManager;
    private RecyclerView recyclerView;
    private TextView empty_view;
    private TabLayout tabLayout;
    private MapListAdapter adapter;
    //Периодическая загрузка списка экипажей
    private Handler mHandler = new Handler();
    private final int LIST_UPDATE_PERIOD = 10000;
    private ImageButton changeStatusButton;
    private DriverStatus driverStatus;
    private ArrayList<Vehicle> mVehiclesList;
    private ApiHelper mAPI;
    private DependencyContainer dependencyContext;
    private Drawable endWorkDrawable;
    private Drawable beginWorkDrawable;

    public static GoogleMapFragment newInstance() {
        return new GoogleMapFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(Objects.requireNonNull(getActivity()));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAPI = ApiHelper.getInstance();
        driverStatus = Model.getInstance().getDriverStatus();
        mServerAPIManager = new ServerApiManager((AppCompatActivity) getActivity(), getContext(), NO_SILENT);
        adapter = new MapListAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.map_fragment_title);
        View v = layoutInflater.inflate(R.layout.google_map_fragment, viewGroup, false);
        endWorkDrawable = getResources().getDrawable(R.drawable.stop_trip);
        beginWorkDrawable = getResources().getDrawable(R.drawable.start_trip);
        changeStatusButton = v.findViewById(R.id.changeStatusButton);
        changeStatusButton.setBackground(beginWorkDrawable);
        changeStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!LocationManager.isMockGps()) {
                    if (driverStatus == DriverStatus.CREW_STATUS_NOT_READY) {
                        driverStatus = DriverStatus.CREW_STATUS_READY;
                    } else if (driverStatus == DriverStatus.CREW_STATUS_READY) {
                        driverStatus = DriverStatus.CREW_STATUS_NOT_READY;
                    } else {
                        driverStatus = getChangingStatusByCurrentDrawable();
                    }

                    if(Model.getInstance().getMyOrderInfo() != null) {
                        if(driverStatus == DriverStatus.CREW_STATUS_READY || driverStatus == DriverStatus.CREW_STATUS_NOT_READY){
                            driverStatus = DriverStatus.CREW_STATUS_WORK;
                            Utils.showToast(getString(R.string.cant_set_status_ready), getContext());
                            return;
                        }
                    }
                    mServerAPIManager.setDriverStatus(driverStatus);
                } else {
                    Utils.showToast(getString(R.string.cant_set_status_mock), getContext());
                }
            }

            private DriverStatus getChangingStatusByCurrentDrawable() {
                if (changeStatusButton.getBackground() == endWorkDrawable) {
                    return  DriverStatus.CREW_STATUS_NOT_READY;
                } else if (changeStatusButton.getBackground() == beginWorkDrawable) {
                    return  DriverStatus.CREW_STATUS_READY;
                } else {
                    throw new IllegalStateException("Unknown status drawable");
                }
            }
        });

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        empty_view = v.findViewById(R.id.empty_view);
        tabLayout = (TabLayout) v.findViewById(R.id.tabs);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.map_tab_title));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.map_list_title));
        tabLayout.addOnTabSelectedListener(onTabSelectedListener);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(bundle);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            LogWriter.log(e.getMessage());
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;

                if (checkLocationPermission()) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                }

                mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
                mGoogleMap.getUiSettings().setCompassEnabled(true);
                moveCameraToMyLocation();
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                mGoogleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        if (mGoogleMap != null && mGoogleMap.getMyLocation() == null
                                &&!moveCameraToMyLocation())
                            Utils.showToast(getString(R.string.location_still_checking),getContext());
                        return false;
                    }
                });
            }
        });
        return v;
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.REQUEST_LOCATION_ACCESS);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
            {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {
                }
                return;
            }
        }
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            if(tab.getPosition() == 0){
                recyclerView.setVisibility(View.GONE);
                mMapView.setVisibility(View.VISIBLE);
            } else if(tab.getPosition() == 1){
                mMapView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }
        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }
    };

    public void updateVehicles() {
        if (mVehiclesList == null)
            return;

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.getItems().clear();
                adapter.getItems().addAll(mVehiclesList);
                adapter.notifyDataSetChanged();

                    if (mVehiclesList.size() == 0)
                        empty_view.setVisibility(View.VISIBLE);
                    else
                        empty_view.setVisibility(View.GONE);

            if (mGoogleMap == null)
                return;

            for (Vehicle vehicle : mVehiclesList) {
                if (vehicle.getLatitude() == null || vehicle.getLongitude() == null)
                    continue; //пропускаем машины с неизвестными координатами

                int icon_id = R.drawable.ic_truck_v;
                try {
                    int status = vehicle.getStatus_id();
                    switch (status) {
                        case 1:
                            icon_id = R.drawable.ic_truck_black;
                            break;
                        case 2:
                            icon_id = R.drawable.ic_truck_green;
                            break;
                        case 3:
                            icon_id = R.drawable.ic_truck_red;
                            break;
                        case 4:
                            icon_id = R.drawable.ic_truck_white;
                            break;
                        case 5:
                            icon_id = R.drawable.ic_truck_blue;
                            break;
                    }
                } catch (Exception exc) {
                }


                String trailerNum = vehicle.getTrailerNumber();
                String numberStr = vehicle.getVehicleNumber();
                if (trailerNum != null) {
                    numberStr = numberStr + " - " + trailerNum;
                }
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory
                        .fromResource(icon_id);

                if (mMarkers != null && getMarker(vehicle.getUsername()) == null)
                    mMarkers.add(mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(vehicle.getLatitude(), vehicle.getLongitude()))
                        .icon(bitmapDescriptor)
                        .title(vehicle.getUsername())
                        .snippet(String.format(Locale.getDefault(), "%s %.1f км/ч", numberStr, vehicle.getSpeed()))));
                else
                    getMarker(vehicle.getUsername()).setPosition(new LatLng(vehicle.getLatitude(), vehicle.getLongitude()));
            }
            }
        },1500);
    }

    private Marker getMarker(String title){
        for(int i=0;i<mMarkers.size();i++) {
            if (mMarkers.get(i).getTitle().equals(title))
                return mMarkers.get(i);
        }
        return null;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Loader loader = getLoaderManager().getLoader(100);
            if(loader != null){
                if(!((VehiclesLoader)loader).isActive()) {
                    getLoaderManager().destroyLoader(VEHICLE_LOADER);
                    getLoaderManager().initLoader(VEHICLE_LOADER, null, GoogleMapFragment.this).forceLoad();
                }
            }
            mHandler.postDelayed(this, LIST_UPDATE_PERIOD);
        }
    };

    @Override
    public void onPause() {
        if (mMapView != null)
            mMapView.onPause();
        super.onPause();
        //mGoogleMap = null;
        if (mHandler != null)
            mHandler.removeCallbacks(runnable);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView == null) return;

        mMapView.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMap();
        if (mMapView != null) {
            mMapView.onResume();
            updateStatusButton(false);
        }
        if (mHandler != null)
            mHandler.post(runnable);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        switch (id){
            case 100:
                return new VehiclesLoader(mAPI, getActivity());
            case 1:
                return new ChangeStatusLoader(dependencyContext, args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if(loader instanceof VehiclesLoader){
            mVehiclesList = (ArrayList<Vehicle>) data;
            updateVehicles();
        }

        if(loader instanceof ChangeStatusLoader){
            ChangeStatusLoader changeStatusLoader = (ChangeStatusLoader) loader;
            Fragment prev = getChildFragmentManager().findFragmentByTag("crew_status_progress");
            if (prev != null) {
                DialogFragment df = (DialogFragment) prev;
                df.dismiss();
            }

            if(changeStatusLoader.getException() != null) {
                if (changeStatusLoader.getException() instanceof NoTransportUnitException) {
                    EventBus.getDefault().post(new MessageEvent(changeStatusLoader.getException().getMessage(), MessageEvent.TRANSPORT_UNIT_DISBANDED, getActivity()));
                    Model.getInstance().saveCrewStatus(DriverStatus.CREW_STATUS_NOT_READY);
                    updateStatusButton(false);
                }
                else
                    EventBus.getDefault().post(new MessageEvent(changeStatusLoader.getException().getMessage(), MessageEvent.LOADER_EXCEPTION, getActivity()));
            }
            else  {
                updateStatusButton(true);
                getLoaderManager().restartLoader(1, null, this);
            }

            HomeActivity homeActivity = (HomeActivity) getActivity();
            if(homeActivity != null){
                homeActivity.updateStatus();
            }

        }
    }

    private void updateStatusButton(Boolean showMessage) {
        driverStatus = Model.getInstance().getDriverStatus();
        if (driverStatus == DriverStatus.CREW_STATUS_READY || driverStatus == DriverStatus.CREW_STATUS_WORK || driverStatus == DriverStatus.CREW_STATUS_CRASH) {
            changeStatusButton.setBackground(endWorkDrawable);
            //changeStatusButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.blue_700)));
            if (showMessage)
                Utils.showToast("Статус изменен: "+getString(Model.getInstance().getDriverStatus().titleId()), getActivity());
        }
        else if (driverStatus == DriverStatus.CREW_STATUS_NOT_READY) {
            changeStatusButton.setBackground(beginWorkDrawable);
            //changeStatusButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.my_accent)));
            if (showMessage)
                Utils.showToast("Статус изменен: "+getString(Model.getInstance().getDriverStatus().titleId()), getActivity());
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLoaderManager().initLoader(VEHICLE_LOADER, null, this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void handleStatusChanged(MessageEvent event) {
        switch (event.getEventType()){
            case MessageEvent.DRIVER_STATUS_CHANGED:
                updateStatusButton(false);
                break;
            case MessageEvent.GPS_DATA_CHANGED:
                if (!CargoDealSystem.getInstance().isMyPositionFound()){
                    moveCameraToMyLocation();
                    CargoDealSystem.getInstance().setMyPositionFound(true);
                }
                break;
        }
    }

    private boolean moveCameraToMyLocation() {
        if (mGoogleMap == null)
            return false;

        if (LocationManager.getLastDriverLocation() != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(LocationManager.getLastDriverLocation().getCoordinate().getLatitude(),
                    LocationManager.getLastDriverLocation().getCoordinate().getLongitude()), 14));
            return true;
        } else if (Model.getInstance().getUserInfo() != null && Model.getInstance().getUserInfo().latitude != 0){
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Model.getInstance().getUserInfo().latitude,
                    Model.getInstance().getUserInfo().longitude), 14));
            return true;
        }
        else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(55.753960,
                    37.620393), 5));
        }
        return false;
    }

    public void clearMap() {
        mMarkers.clear();
        if (mGoogleMap != null)
            mGoogleMap.clear();
    }

    public class DriverViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView username, mark, vehicle_number, speed;
        ImageView image;
        Double mLatitude;
        Double mLongitude;
        Marker mMarker;

        private DriverViewHolder(View itemView) {
            super(itemView);
            username = (TextView) itemView.findViewById(R.id.username);
            mark = (TextView) itemView.findViewById(R.id.articleBrand);
            vehicle_number = (TextView) itemView.findViewById(R.id.vehicle_number);
            speed = (TextView) itemView.findViewById(R.id.speed);
            image = (ImageView) itemView.findViewById(R.id.image);
            //image.setImageDrawable(null);
            itemView.setOnClickListener(this);
        }

        private void bind(Vehicle vehicle){
            //ToDo доделать взятие координат для перемещения к маркеру
            mLatitude = vehicle.getLatitude();
            mLongitude = vehicle.getLongitude();
            mMarker = getMarker(vehicle.getUsername());
        }

        @Override
        public void onClick(View view) {
            tabLayout.getTabAt(0).select();
            if (mGoogleMap != null)
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude,
                    mLongitude), 14));
            if (mMarker != null)
                mMarker.showInfoWindow();
        }
    }

    private class MapListAdapter extends RecyclerView.Adapter<DriverViewHolder> {

        private ArrayList<Vehicle> items = new ArrayList<>();
        private Context context;

        @Override
        public DriverViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            this.context = parent.getContext();
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            return new DriverViewHolder(layoutInflater.inflate(R.layout.map_trucks_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(DriverViewHolder holder, int position) {
            Vehicle item = items.get(position);
            holder.bind(item);
            holder.username.setText(item.getUsername());
            holder.mark.setText(item.getMark());
            holder.speed.setText(String.format(Locale.getDefault(), "%.1f км/ч", item.getSpeed()));
            holder.vehicle_number.setText(String.format("%s - %s", item.getVehicleNumber(), item.getTrailerNumber()));
            //Picasso.with(context).load(item.getVehicleNumberImg()).into(holder.image);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        private ArrayList<Vehicle> getItems(){
            return items;
        }


    }

    public void setMapView(MapView mapView) {
        mMapView = mapView;
    }

    public MapView getMapView() {
        return mMapView;
    }

    public void setUpMap() {
        try {
            MapsInitializer.initialize(getActivity());
            updateVehicles();
        } catch (Exception e) {
            LogWriter.log(e.getMessage());
        }
    }
}
