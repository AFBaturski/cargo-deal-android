package fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;

import activities.MainActivity;
import com.macsoftex.cargodeal.driver.R;

public class LoginDialogFragment extends DialogFragment {
    public static final String TAG = AlertDialogFragment.class.getSimpleName();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Light_Dialog);
        builder.setMessage(R.string.user_have_other_session_dialog)
                .setPositiveButton(R.string.okay, (dialog, id) -> ((MainActivity)getActivity()).deleteOtherSessions())
                .setNegativeButton(R.string.cancel, (dialog, id) -> ((MainActivity)getActivity()).cancelDeleteOtherSessions());
        return builder.create();
    }
}
