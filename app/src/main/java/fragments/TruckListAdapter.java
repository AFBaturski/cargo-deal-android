package fragments;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import features.transport_unit.model.Transport;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Игорь on 16.03.2017.
 */

public class TruckListAdapter extends BaseAdapter {
  private final List<Transport> items = new ArrayList<>();
  private final List<Transport> original_items = new ArrayList<>();
  private boolean inSearchMode = false;

  public TruckListAdapter() {
  }

  public void addAll(List<Transport> items) {
    items.stream().forEach((item) -> {
      if (!original_items.contains(item))
        original_items.add(item);
      this.items.add(item);
    });
  }

  public void clear() {
    original_items.clear();
    items.clear();
  }

  @Override
  public int getItemViewType(int position) {
    return ViewTypes.fromString(items.get(position).getGosNumber()).ordinal();
  }

  @Override
  public int getViewTypeCount() {
    return ViewTypes.values().length;
  }

  @NonNull
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View v = convertView;
    if (v == null) {
      LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      v = inflater.inflate(R.layout.truckchooser_spinner_item_frame, null);
    }
    if (items.size() < position || items.get(position) == null) return v;

    Transport item = items.get(position);
    TextView text = v.findViewById(R.id.text1);
    TextView part1Text = v.findViewById(R.id.part1Text);
    TextView part2Text = v.findViewById(R.id.part2Text);
    TextView part3Text = v.findViewById(R.id.part3Text);
    TextView part4Text = v.findViewById(R.id.part4Text);
    ImageView image = v.findViewById(R.id.photo);

    String gosNumber = item.getGosNumber().replace(" ", "");
    if (getItemViewType(position) == ViewTypes.VEHICLE.ordinal()) {
      part1Text.setText(gosNumber.subSequence(0, 1).toString().toUpperCase());
      part2Text.setText(gosNumber.subSequence(1, 4));
      part3Text.setText(gosNumber.subSequence(4, 6).toString().toUpperCase());
      part4Text.setText(gosNumber.subSequence(6, gosNumber.length()));
    } else if (getItemViewType(position) == ViewTypes.TRAILER.ordinal()) {
      part1Text.setText(gosNumber.subSequence(0, 2).toString().toUpperCase());
      part2Text.setText(gosNumber.subSequence(2, 6));
      part3Text.setText("");
      part4Text.setText(gosNumber.subSequence(6, gosNumber.length()));
    } else if (getItemViewType(position) == ViewTypes.UNKNOWN.ordinal()) {
      text.setText(item.getGosNumber());
      text.setVisibility(View.VISIBLE);
      part1Text.setVisibility(View.GONE);
      part2Text.setVisibility(View.GONE);
      part3Text.setVisibility(View.GONE);
      part4Text.setVisibility(View.GONE);
      image.setVisibility(View.GONE);
      image.setImageDrawable(null);
    }
    return v;
  }

  public void search(String query) {
    if (query.length() == 0) {
      items.clear();
      items.addAll(original_items);
      notifyDataSetChanged();
      inSearchMode = false;
      return;
    }
    items.clear();
    notifyDataSetChanged();
    for (Transport item : original_items) {
      if (item.getGosNumber().toLowerCase().contains(query.toLowerCase())) {
        items.add(item);
        notifyDataSetChanged();
      }
    }
    inSearchMode = true;
  }

  @Override
  public int getCount() {
    if (!inSearchMode) {
      return original_items.size();
    } else {
      return items.size();
    }
  }

  @Override
  public Transport getItem(int position) {
    if (!inSearchMode) {
      return original_items.get(position);
    } else {
      return items.get(position);
    }
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  private enum ViewTypes {
    VEHICLE, TRAILER, UNKNOWN;
    private static final Pattern vehiclePattern = Pattern.compile("[АВЕКМНОРСТУХ]\\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\\d{2,3}", Pattern.CASE_INSENSITIVE);
    private static final Pattern trailerPattern = Pattern.compile("[АВЕКМНОРСТУХ]{2}\\d{4}(?<!0000)\\d{2,3}", Pattern.CASE_INSENSITIVE);

    private static boolean isVehicleNumber(String gosNumber) {
      return vehiclePattern.matcher(trimAll(gosNumber)).find();
    }

    private static boolean isTrailerNumber(String gosNumber) {
      return trailerPattern.matcher(trimAll(gosNumber)).find();
    }

    private static String trimAll(String source) {
      return source.replace(" ", "");
    }

    public static ViewTypes fromString(String source) {
      return isVehicleNumber(source) ? VEHICLE : isTrailerNumber(source) ? TRAILER : UNKNOWN;
    }
  }
}
