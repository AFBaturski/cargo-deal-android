package fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import storage.TransportContract;
import features.log.LogWriter;
import features.transport_unit.model.Transport;
import features.transport_unit.model.TransportFactory;
import model.Model;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;


/**
 * Created by barkov00@gmail.com on 24.05.2017.
 */

public class VehicleListDialog extends DialogFragment {

    public static final String TAG = VehicleListDialog.class.getSimpleName();
    private ApiHelper mAPI;

    public static VehicleListDialog newInstance(boolean vehicle){
        VehicleListDialog dialog = new VehicleListDialog();

        Bundle b = new Bundle();
        b.putBoolean("vehicles", vehicle);

        dialog.setArguments(b);

        return dialog;
    }

    private boolean vehicles;
    private TruckListAdapter adapter;
    private SearchView searchView;
    private ListView listView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAPI = ApiHelper.getInstance();
        adapter = new TruckListAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        loadVehiclesList(0, 1000);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        vehicles = getArguments().getBoolean("vehicles");
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View rootView = inflater.inflate(R.layout.searchable_list_dialog_new, null);

        searchView = (SearchView) rootView.findViewById(R.id.search);
        listView = (ListView) rootView.findViewById(R.id.listItems);
        searchView.setOnQueryTextListener(onQueryTextListener);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        swipeRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadVehiclesList(0, 1000);
            }
        });

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(onItemSelectedListener != null){
                    onItemSelectedListener.onItemSelected(adapter.getItem(i));
                }
                dismiss();
            }
        });

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Light_Dialog);
        alertDialog.setView(rootView);
        alertDialog.setPositiveButton(getString(R.string.close), okClick);
        alertDialog.setTitle("");

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            searchView.clearFocus();
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            adapter.search(s);
            return false;
        }
    };

    private Dialog.OnClickListener okClick = new Dialog.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    };


    private boolean requestInProgress = false;

    private Call<JsonObject> vehiclesListRequest;

    private void loadVehiclesList(int offset, int limit){
        if(requestInProgress) return;
        final String type = vehicles ? TransportContract.TYPE_VEHICLE : TransportContract.TYPE_TRAILER;

        //final JsonObject request = Utils.createRPC(method);
        JsonObject request = new JsonObject();
        JsonObject currentUser = new JsonObject();
        request.addProperty("limit", limit);
        request.addProperty("offset", offset);
        Model.UserInfo userInfo = Model.getInstance().getUserInfo();
        String sessionId = userInfo != null ? userInfo.session_id : "";

        LogWriter.log(this.getClass().getSimpleName()+" : listVehicles/Trailers");
        vehiclesListRequest = vehicles ? mAPI.getServerAPI().listVehicles(sessionId, request) : mAPI.getServerAPI().listTrailers(sessionId, request);

        requestInProgress = true;

        vehiclesListRequest.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogWriter.logServerResponse(response);
                if(response.code() == 200 && response.body().has("result")
                        && response.body().get("result").getAsJsonObject().get("result").getAsBoolean()) {
                    adapter.clear();

                    JsonArray array = response.body().get("result").getAsJsonObject().get("objects").getAsJsonArray();
                    List<Transport> trucksList = new ArrayList<>();
                    for (int i = 0; i < array.size(); i++) {
                        JsonObject t = array.get(i).getAsJsonObject();
                        if (vehicles) {
                            trucksList.add(TransportFactory.createVehicle(t));
                        } else {
                            trucksList.add(TransportFactory.createTrailer(t));
                        }
                    }
                    adapter.addAll(trucksList);
                    adapter.notifyDataSetChanged();
                    requestInProgress = false;
                }
                else
                {
                    String errorMessage = response.body() != null ? response.body().toString():"";
                    Toast.makeText(getActivity(),getString(R.string.cant_load_list) + errorMessage, Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.INVISIBLE);
                swipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Context context = getContext();
                if (context == null) return;

                Toast.makeText(context, R.string.cant_load_list, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
                swipeRefresh.setRefreshing(false);
                requestInProgress=false;
            }
        });
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener){
        this.onItemSelectedListener = listener;
    }

    private OnItemSelectedListener onItemSelectedListener;

    public interface OnItemSelectedListener {
        void onItemSelected(Transport item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(vehiclesListRequest != null) {
            vehiclesListRequest.cancel();
        }
    }
}
