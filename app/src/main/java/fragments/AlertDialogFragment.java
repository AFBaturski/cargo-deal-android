package fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;

import features.main.HomeActivity;
import com.macsoftex.cargodeal.driver.BuildConfig;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by barkov00@gmail.com on 30.03.2017.
 */

public class AlertDialogFragment extends DialogFragment {

    private static final String TAG = AlertDialogFragment.class.getSimpleName();

    public static AlertDialogFragment instance(String title, String message, Boolean finishActivity){
        AlertDialogFragment progressDialogFragment = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("eventType", message);
        args.putBoolean("finishActivity", finishActivity);
        progressDialogFragment.setArguments(args);
        return progressDialogFragment;
    }

    public static void showAlert(final String titleMsg, final String bodyMsg, final boolean needFinishActivity, final FragmentManager fragmentManager, final String tag){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                AlertDialogFragment alertDialogFragment = AlertDialogFragment.instance(titleMsg, bodyMsg, needFinishActivity);
                alertDialogFragment.show(fragmentManager, tag);
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Light_Dialog);
        builder.setTitle(getArguments().getString("title"))
                .setMessage(getArguments().getString("eventType"));

        builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!(getActivity() instanceof HomeActivity) && getArguments().getBoolean("finishActivity"))
                    getActivity().finish();
            }
        });

        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        //Убрано за ненадобностью
        /*Intent intent = new Intent();
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, intent);*/
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // safe guard against loss of target fragment
        Fragment targetFragment = getTargetFragment();

        if (targetFragment != null) {
            FragmentManager fragmentManager = getFragmentManager();

            if (!fragmentManager.getFragments().contains(targetFragment)) {
                if (BuildConfig.DEBUG)
                setTargetFragment(null, getTargetRequestCode());
            }
        }

        super.onSaveInstanceState(outState);
    }
}
