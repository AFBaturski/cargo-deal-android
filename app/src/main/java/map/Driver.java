package map;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class Driver implements ClusterItem {
    private final String mName;
    private final LatLng mPosition;
    private final int mImage;

    public Driver(String name, LatLng position, int image) {
        mName = name;
        mPosition = position;
        mImage = image;
    }

    @Override
    public LatLng getPosition() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
