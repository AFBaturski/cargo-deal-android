package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import common.Utils;
import features.order.model.Order;

/**
 * Created by Alex Baturski on 30.08.2018.
 */

public class FinishedOrdersList {
    private static FinishedOrdersList ourInstance = new FinishedOrdersList();
    public static final int ORDER_BY_NUMBER = 0;
    public static final int ORDER_BY_DATE = 1;
    public static final int ASCENDING = 0;
    public static final int DESCENDING = 1;
    private int mOrderBy = ORDER_BY_NUMBER;
    private int mDirection = DESCENDING;
    private List<Order> mFinishedOrders;
    private boolean mIsFiltered;
    private Date mFilteredDate;

    public static FinishedOrdersList getInstance(){
        return ourInstance;
    }

    private FinishedOrdersList(){
        mFinishedOrders = new ArrayList<>();
    }

    public List<Order> getOrders(){
        return mFinishedOrders;
    }

    public void addOrder(Order order){
        mFinishedOrders.add(order);
    }
    public void insertOrder(Order order){
        mFinishedOrders.add(0,order);
    }
    public void removeOrder(Order order){
        mFinishedOrders.remove(order);
    }

    public void сlearOrders(){
        mFinishedOrders.clear();
    }

    public Order get(int index){
        return mFinishedOrders.size() <= index ? null : mFinishedOrders.get(index);
    }

    public void remove(int index){
        if (mFinishedOrders.size() <= index)
            mFinishedOrders.remove(index);
    }

    public Order getOrder(int orderNumber) {
        for (int i = 0; i< mFinishedOrders.size(); i++)
            if (mFinishedOrders.get(i).getNumber().equals(orderNumber)){
                return mFinishedOrders.get(i);
            }
        return null;
    }

    public List<Order> getFinishedOrders() {
        switch (mOrderBy){
            case ORDER_BY_NUMBER:
                Collections.sort(mFinishedOrders, new Comparator<Order>() {
                    @Override
                    public int compare(Order t1, Order t2) {
                        return t1.getNumber().compareTo(t2.getNumber());
                    }
                });
            case ORDER_BY_DATE:
                Collections.sort(mFinishedOrders, new Comparator<Order>() {
                    @Override
                    public int compare(Order t1, Order t2) {
                        return t1.getCreatedAt().compareTo(t2.getCreatedAt());
                    }
                });
        }
        if (mDirection == DESCENDING)
            Collections.reverse(mFinishedOrders);
        if (mIsFiltered)
            return getFilteredOrders(mFilteredDate);

        return mFinishedOrders;
    }

    public List<Order> getFilteredOrders(Date date){
        List<Order> filteredOrders = new ArrayList<>();
        Date filterDate;
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        try {
            filterDate = sdf.parse(Utils.convertUnixTimeToDate(date.getTime()));
            for (Order order : mFinishedOrders) {
                if (sdf.parse(Utils.convertUnixTimeToDate(order.getCreatedAt())).after(filterDate)
                        || sdf.parse(Utils.convertUnixTimeToDate(order.getCreatedAt())).before(filterDate)) {
                    continue;
                }
                filteredOrders.add(order);
            }
        } catch (Exception e){
            filteredOrders = null;
        }
        return filteredOrders;
    }

    public boolean isFiltered() {
        return mIsFiltered;
    }

    public void setFiltered(boolean filtered) {
        mIsFiltered = filtered;
    }

    public Date getFilteredDate() {
        return mFilteredDate;
    }

    public void setFilteredDate(Date filteredDate) {
        mFilteredDate = filteredDate;
    }

    public int getOrderBy() {
        return mOrderBy;
    }

    public void setOrderBy(int orderBy) {
        this.mOrderBy = orderBy;
    }

    public int getDirection() {
        return mDirection;
    }

    public void setDirection(int direction) {
        this.mDirection = direction;
    }
}
