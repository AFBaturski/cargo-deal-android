package model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Игорь on 13.03.2017.
 */

public class Vehicle implements Serializable {

        @SerializedName("crew_status_id")
        @Expose
        private int crew_status_id;
        @SerializedName("crew_id")
        @Expose
        private String crewId;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("articleBrand")
        @Expose
        private String mark;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("speed")
        @Expose
        private Double speed;
        @SerializedName("status_allow_orders")
        @Expose
        private Boolean statusAllowOrders;
        @SerializedName("status_txt")
        @Expose
        private String statusTxt;
        @SerializedName("trailer_number")
        @Expose
        private String trailerNumber;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("vehicle_number")
        @Expose
        private String vehicleNumber;
        @SerializedName("vehicle_number_img")
        @Expose
        private String vehicleNumberImg;

        public Vehicle (JsonObject vehicle){
            username = vehicle.get("driver").getAsString();
            latitude = vehicle.get("latitude").getAsDouble();
            longitude = vehicle.get("longitude").getAsDouble();
            speed = vehicle.get("speed").getAsDouble();
            mark = vehicle.get("vehicle").getAsJsonObject().get("brand").getAsString();
            model = vehicle.get("vehicle").getAsJsonObject().get("model").getAsString();
            vehicleNumber = vehicle.get("vehicle").getAsJsonObject().get("number").getAsString();
            trailerNumber = (vehicle.has("trailer")) ? vehicle.get("trailer").getAsJsonObject().get("number").getAsString():"";

            vehicleNumberImg = vehicle.get("vehicle").getAsJsonObject().get("number").getAsString();
        }

        public String getCrewId() {
            return crewId;
        }

        public void setCrewId(String crewId) {
            this.crewId = crewId;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getMark() {
            return mark;
        }

        public void setMark(String mark) {
            this.mark = mark;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public Double getSpeed() {
            return speed;
        }

        public void setSpeed(Double speed) {
            this.speed = speed;
        }

        public Boolean getStatusAllowOrders() {
            return statusAllowOrders;
        }

        public void setStatusAllowOrders(Boolean statusAllowOrders) {
            this.statusAllowOrders = statusAllowOrders;
        }

        public String getStatusTxt() {
            return statusTxt;
        }

        public void setStatusTxt(String statusTxt) {
            this.statusTxt = statusTxt;
        }

        public String getTrailerNumber() {
            return trailerNumber;
        }

        public void setTrailerNumber(String trailerNumber) {
            this.trailerNumber = trailerNumber;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getVehicleNumber() {
            return vehicleNumber;
        }

        public void setVehicleNumber(String vehicleNumber) {
            this.vehicleNumber = vehicleNumber;
        }

        public String getVehicleNumberImg() {
            return vehicleNumberImg;
        }

        public void setVehicleNumberImg(String vehicleNumberImg) {
            this.vehicleNumberImg = vehicleNumberImg;
        }

        //Пока не требуются различные статусы
    public int getStatus_id() {
        return 2;//crew_status_id;
    }

    public void setStatus_id(int status_id) {
        this.crew_status_id = status_id;
    }
}
