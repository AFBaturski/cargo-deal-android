package model;

import androidx.annotation.StringRes;
import com.macsoftex.cargodeal.driver.R;

public enum DriverStatus {
    CREW_STATUS_NOT_READY(0, R.string.status_not_ready),
    CREW_STATUS_READY(1, R.string.status_ready),
    CREW_STATUS_CRASH(2, R.string.status_crash),
    CREW_STATUS_INVISIBLE(3, R.string.status_invisible),
    CREW_STATUS_WORK(4, R.string.status_work),
    CREW_STATUS_REST(5, R.string.status_rest),
    CREW_STATUS_UNDER_REPAIR(6, R.string.status_under_repair);
    private final int status;
    private final int titleId;

    DriverStatus(int status, @StringRes int titleId) {
        this.status = status;
        this.titleId = titleId;
    }

    public int value() {
        return status;
    }

    public int titleId() {
        return titleId;
    }
}
