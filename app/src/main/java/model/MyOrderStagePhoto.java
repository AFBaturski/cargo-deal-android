package model;

public class MyOrderStagePhoto {
    public static class Entry {
        public static final String TABLE_NAME = "my_order_stage_photo_table";

        public static final String STAGE_INDEX = "stage_index";
        public static final String TRIP_ID = "trip_id";
        public static final String IMAGE_NAME = "image_name";
    }
    private int stageIndex;
    private String tripId;
    private String imageName;
    public MyOrderStagePhoto(int stageIndex, String tripId, String imageName){
        this.stageIndex = stageIndex;
        this.tripId = tripId;
        this.imageName = imageName;
    }
    public String getTripId() {
        return tripId;
    }
    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public int getStageIndex() {
        return stageIndex;
    }

    public String getImageName() {
        return imageName;
    }
}
