package model;

import features.order.model.Order;

public class DealerOrder extends Order {
  public static class Builder extends Order.Builder<Builder> {
    public Builder(Order order) {
      super(order);
    }

    public Builder(Model.MyOrderInfo orderInfo) {
      super(orderInfo);
    }

    @Override
    protected Builder self() {
      return this;
    }

    @Override
    public DealerOrder build() {
      return new DealerOrder(this) ;
    }
  }

  private DealerOrder(Builder builder) {
    super(builder);
  }
}
