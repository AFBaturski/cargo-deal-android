package model;

public
class OrderStatus {
  public int id, lastStageId;
  public String caption;

  public OrderStatus(int id, int lastStageId, String caption) {
    this.id = id;
    this.lastStageId = lastStageId;
    this.caption = caption;
  }
}
