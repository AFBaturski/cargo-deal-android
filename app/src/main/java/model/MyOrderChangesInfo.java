package model;

import android.database.Cursor;
import common.Utils;
import storage.MyOrderChangesContract;
import features.order.model.OrderPropertyTitle;

import java.util.*;
import java.util.stream.Collectors;

public class MyOrderChangesInfo {
    public List<String> getAuthorIds() {
        return changes.values().stream()
                .map(changeData -> changeData.authorId)
                .distinct()
                .collect(Collectors.toList());
    }

    private LinkedHashMap<Integer, ChangeData> changes;
    public LinkedHashMap<Integer, ChangeData> getChanges() {
        return changes;
    }

    public static MyOrderChangesInfo fromCursor(Cursor cursor) {
        LinkedHashMap<Integer, ChangeData> changes = new LinkedHashMap<>();
        while (cursor.moveToNext()) {
            changes.put(cursor.getInt(cursor.getColumnIndex(MyOrderChangesContract.MyOrderChangesEntry.PROPERTY)),
                    new ChangeData(cursor.getString(cursor.getColumnIndex(MyOrderChangesContract.MyOrderChangesEntry.VALUE)),
                            cursor.getString(cursor.getColumnIndex(MyOrderChangesContract.MyOrderChangesEntry.AUTHOR_ID))));
        }
        return new MyOrderChangesInfo(changes);
    }

    public static MyOrderChangesInfo copyFrom(MyOrderChangesInfo changesInfo) {
        return new MyOrderChangesInfo(changesInfo.changes);
    }

    public void applyOrderChanges(Model.MyOrderInfo originalOrder, Model.MyOrderInfo changedOrder, String  authorId) {
        this.changes = getChangedPropertiesWithMerge(changes, originalOrder, changedOrder, authorId);
    }

    private MyOrderChangesInfo(LinkedHashMap<Integer, ChangeData> changes) {
        this.changes = changes;
    }

    private LinkedHashMap<Integer, ChangeData> getChangedPropertiesWithMerge(LinkedHashMap<Integer, ChangeData> initialProperties, Model.MyOrderInfo originalOrder, Model.MyOrderInfo changedOrder, String authorId) {
        if (originalOrder == null || changedOrder == null) {
            return new LinkedHashMap<>();
        }
        LinkedHashMap<Integer, ChangeData> values = new LinkedHashMap<>(initialProperties);

        if (!originalOrder.loadingEntranceAddress.equals(changedOrder.loadingEntranceAddress)) {
            values.put(OrderPropertyTitle.LOADING_ENTRANCE_ADDRESS.value(),
                    new ChangeData(changedOrder.loadingEntranceAddress, authorId));
        }
        if (!originalOrder.unloadingEntranceAddress.equals(changedOrder.unloadingEntranceAddress)) {
            values.put(OrderPropertyTitle.UNLOADING_ENTRANCE_ADDRESS.value(),
                    new ChangeData(changedOrder.unloadingEntranceAddress, authorId));
        }
        if (originalOrder.tonnage != changedOrder.tonnage) {
            values.put(OrderPropertyTitle.TONNAGE.value(),
                    new ChangeData(String.valueOf(changedOrder.tonnage), authorId));
        }
        if (!originalOrder.articleType.equals(changedOrder.articleType)) {
            values.put(OrderPropertyTitle.ARTICLE_TYPE.value(),
                    new ChangeData(changedOrder.articleType, authorId));
        }
        if (!originalOrder.articleBrand.equals(changedOrder.articleBrand)) {
            values.put(OrderPropertyTitle.ARTICLE_BRAND.value(),
                    new ChangeData(changedOrder.articleBrand, authorId));
        }
        if (originalOrder.tariff != changedOrder.tariff) {
            values.put(OrderPropertyTitle.TARIFF.value(),
                    new ChangeData(String.valueOf(changedOrder.tariff), authorId));
        }
        if (!originalOrder.supplier.equals(changedOrder.supplier)) {
            values.put(OrderPropertyTitle.SUPPLIER.value(),
                    new ChangeData(changedOrder.supplier, authorId));
        }
        if (!originalOrder.loadingContactName.equals(changedOrder.loadingContactName)) {
            values.put(OrderPropertyTitle.LOADING_CONTACT_NAME.value(),
                    new ChangeData(changedOrder.loadingContactName, authorId));
        }
        if (!originalOrder.loadingDate.equals(changedOrder.loadingDate)) {
            values.put(OrderPropertyTitle.LOADING_DATE.value(),
                    new ChangeData(Utils.convertUnixTimeToDate(changedOrder.loadingDate), authorId));
        }
        if (!originalOrder.customer.equals(changedOrder.customer)) {
            values.put(OrderPropertyTitle.CUSTOMER.value(),
                    new ChangeData(changedOrder.customer, authorId));
        }
        if (!originalOrder.unloadingContactName.equals(changedOrder.unloadingContactName)) {
            values.put(OrderPropertyTitle.UNLOADING_CONTACT_NAME.value(),
                    new ChangeData(changedOrder.unloadingContactName, authorId));
        }
        if (!originalOrder.unloadingContactPhone.equals(changedOrder.unloadingContactPhone)) {
            values.put(OrderPropertyTitle.UNLOADING_CONTACT_PHONE.value(),
                    new ChangeData(changedOrder.unloadingContactPhone, authorId));
        }
        if (!originalOrder.orderType.equals(changedOrder.orderType)) {
            values.put(OrderPropertyTitle.ORDER_TYPE.value(),
                    new ChangeData(changedOrder.orderType.prettyName(), authorId));
        }
        if (!originalOrder.unloadingBeginDate.equals(changedOrder.unloadingBeginDate)
                || !originalOrder.unloadingEndDate.equals(changedOrder.unloadingEndDate)) {
            values.put(OrderPropertyTitle.UNLOADING_DATE.value(),
                    new ChangeData(Utils.convertUnixTimeToDate(changedOrder.unloadingBeginDate), authorId));
            values.put(OrderPropertyTitle.UNLOADING_TIME.value(),
                    new ChangeData(
                            String.format("c %s до %s",
                            Utils.convertUnixTimeToTimeShort(changedOrder.unloadingBeginDate),
                            Utils.convertUnixTimeToTimeShort(changedOrder.unloadingEndDate)),
                            authorId));
        }
        if (!originalOrder.comment.equals(changedOrder.comment)) {
            values.put(OrderPropertyTitle.COMMENT.value(),
                    new ChangeData(changedOrder.comment, authorId));
        }
        return values;
    }
}
