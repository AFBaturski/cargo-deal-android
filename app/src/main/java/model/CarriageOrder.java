package model;

import com.google.gson.JsonObject;
import features.order.model.Order;

public class CarriageOrder extends Order {
    public static class Builder extends Order.Builder<Builder> {
        public Builder(Order order) {
            super(order);
        }

        public Builder(Model.MyOrderInfo orderInfo) {
            super(orderInfo);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public CarriageOrder build() {
            return new CarriageOrder(this);
        }
    }
    private CarriageOrder(Builder builder) {
        super(builder);
    }
}
