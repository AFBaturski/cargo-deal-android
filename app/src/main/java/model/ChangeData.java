package model;

public class ChangeData {
    public String value;
    public String authorId;

    public ChangeData(String value, String authorId) {
        this.value = value;
        this.authorId = authorId;
    }
}
