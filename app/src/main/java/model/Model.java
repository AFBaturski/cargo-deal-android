package model;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.preference.PreferenceManager;

import api.TaskScheduler;
import com.google.gson.JsonObject;

import java.util.*;

import features.transport_unit.model.Trailer;
import features.transport_unit.model.TransportFactory;
import features.transport_unit.model.Vehicle;
import storage.*;
import di.DependencyContainer;
import features.log.LogWriter;
import features.order.OrderManager;
import features.order.model.Order;
import features.order.stage.model.OrderStage;
import features.order.stage.OrderStageFactory;
import features.order.model.OrderType;
import features.transport_unit.model.Transport;
import location.Location;

/**
 * Created by Eugene on 23.02.2017.
 */

public class Model {

  public static final int ORDER_STAGE_ACCEPTED = 0;
  public static final int ORDER_STAGE_GET_TO_LOADING = 1;
  public static final int ORDER_STAGE_LOADING_COMPLETE = 2;
  public static final int ORDER_STAGE_GET_TO_UNLOADING = 3;
  public static final int ORDER_STAGE_FINAL = 5;

  public static final String TAG = Model.class.getSimpleName();
  private static Transport mTransportUnit;
  private static Model model = null;
  private final TaskScheduler scheduler;
  private final DependencyContainer dependencyContext;

  private boolean inProgress = false;
  private final SharedPreferences sp;
  private final Context context;
  private final SQLiteDatabase storage;

  public Model(DependencyContainer dependencyContext) {
    this.context = dependencyContext.getContext();
    this.storage = dependencyContext.getStorage();
    this.scheduler = dependencyContext.getScheduler();
    this.dependencyContext = dependencyContext;
    sp = PreferenceManager.getDefaultSharedPreferences(context);
  }

  public static Model createInstance(DependencyContainer dependencyContext) {
    if (model == null) {
      model = new Model(dependencyContext);
    }
    return model;
  }

  public static Model getInstance() {
    return model;
  }

  public synchronized void saveUserPosition(Location location) {
    if (!isTransportUnitAvailable()) {
      return;
    }

    final UserPosition userPosition = new UserPosition(
        location.getCoordinate().getLatitude(),
        location.getCoordinate().getLongitude(),
        location.getSpeed(),
        System.currentTimeMillis());

        insertUserPosition(userPosition);
  }

  public static void clearCurrentOrder() {
    Model.getInstance().deleteCurrentOrder();
    OrderManager.getInstance().removeOrder(OrderManager.getInstance().getActiveOrder());
    Model.getInstance().deleteOrderNextStages();
  }

  public void cancelCurrentOrderAndAwaitNewOne() {
    Model.clearCurrentOrder();
    Model.setDriverStatusReady();
    dependencyContext.getLocationManager().changeLocationUpdatesInterval();
  }


  public static void setDriverStatusReady() {
    Model.getInstance().saveCrewStatus(DriverStatus.CREW_STATUS_READY);
    Model.getInstance().saveCrewStatusUpdatedOnServer();
  }

  public boolean isTransportUnitAvailable() {
    if (mTransportUnit == null) {
      mTransportUnit = getSelectedTransport(TransportContract.TYPE_VEHICLE);
    }
    return mTransportUnit != null;
  }

  public static boolean isStageWithPhoto(int stage) {
    return stage == ORDER_STAGE_LOADING_COMPLETE || stage == ORDER_STAGE_FINAL;
  }

  public UserInfo getUserInfo() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    //Cursor cursor = db.query(UserInfoContract.UserInfoEntry.TABLE_NAME, null, null, null, null, null, null);
    Cursor cursor = db.rawQuery("SELECT * FROM " + UserInfoContract.UserInfoEntry.TABLE_NAME, null);
    if (cursor.moveToFirst()) {
      UserInfo info = new UserInfo(
          cursor.getString(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_USERNAME)),
          cursor.getString(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_FIO)),
          cursor.getString(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_TOKEN)),
          cursor.getString(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_USER_ID)),
          cursor.getLong(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_EXPIRE)),
          cursor.getString(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_PASSW)),
          cursor.getInt(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN__ID)),
          cursor.getDouble(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_LAT)),
          cursor.getDouble(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_LON)));
      return info;
    }
    cursor.close();

    return null;
  }

  public void setUserInfo(UserInfo userInfo) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.execSQL("DELETE FROM " + UserInfoContract.UserInfoEntry.TABLE_NAME);

    if (userInfo == null)
      return;

    ContentValues values = new ContentValues();
    values.put(UserInfoContract.UserInfoEntry.COLUMN_PASSW, userInfo.passw);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_USERNAME, userInfo.username);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_FIO, userInfo.fio);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_USER_ID, userInfo.user_id);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_TOKEN, userInfo.session_id);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_EXPIRE, userInfo.expire);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_LAT, userInfo.latitude);
    values.put(UserInfoContract.UserInfoEntry.COLUMN_LON, userInfo.longitude);

    db.insert(UserInfoContract.UserInfoEntry.TABLE_NAME, null, values);
  }

  public void deleteUserInfo() {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.execSQL("DELETE FROM " + UserInfoContract.UserInfoEntry.TABLE_NAME);
  }

  public String getAccessToken() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.query(UserInfoContract.UserInfoEntry.TABLE_NAME, new String[]{UserInfoContract.UserInfoEntry.COLUMN_TOKEN}, null, null, null, null, null);
    String token = null;
    if (cursor.moveToFirst()) {
      token = cursor.getString(cursor.getColumnIndex(UserInfoContract.UserInfoEntry.COLUMN_TOKEN));
    }
    cursor.close();
    return token;
  }

  public Transport getSelectedTransport(String type) {
    Cursor cursor = storage.query(TransportContract.TransportEntry.TABLE_NAME, null, TransportContract.TransportEntry.COLUMN_TYPE + " = ?", new String[]{type}, null, null, null);
    Transport transport = null;
    if (cursor.moveToFirst()) {
      transport = TransportFactory.create(type, cursor);
    }
    cursor.close();
    return transport;
  }

  public void clearTransportTable() {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.execSQL("DELETE FROM " + TransportContract.TransportEntry.TABLE_NAME);
    mTransportUnit = null;
  }

  public void addTransport(Transport... transports) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    ContentValues values = new ContentValues();
    for (Transport t : transports) {
      if (t == null) continue;
      values.put(TransportContract.TransportEntry.COLUMN_TYPE, (t instanceof Vehicle) ? TransportContract.TYPE_VEHICLE : TransportContract.TYPE_TRAILER);
      values.put(TransportContract.TransportEntry.COLUMN_GOS_NOMER, t.getGosNumber());
      values.put(TransportContract.TransportEntry.COLUMN_TRUCK_ID, t.getId());
      values.put(TransportContract.TransportEntry.COLUMN_TONNAGE, t.getTonnage());
      if (t instanceof Vehicle) {
        values.put(TransportContract.TransportEntry.COLUMN_INSPECTION_END_DATE, ((Vehicle)t).getInspectionEndDate());
        values.put(TransportContract.TransportEntry.COLUMN_MTPL_END_DATE, ((Vehicle)t).getMtplEndDate());
      }
      if (t instanceof Trailer) {
        values.put(TransportContract.TransportEntry.COLUMN_INSPECTION_END_DATE, ((Trailer)t).getInspectionEndDate());
      }
      db.insert(TransportContract.TransportEntry.TABLE_NAME, null, values);
    }
  }
  
  public List<Transport> getTrucks(String type) {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.query(TransportContract.TransportEntry.TABLE_NAME, null, TransportContract.TransportEntry.COLUMN_TYPE + " = ?", new String[]{type}, null, null, null);

    List<Transport> trucks = new ArrayList<>();

    int count = cursor.getCount();
    if (cursor.moveToFirst())
      do {
        trucks.add(TransportFactory.create(type, cursor));
      } while (cursor.moveToNext());

    cursor.close();

    return trucks;
  }

  public void insertUserPosition(UserPosition position) {
    final SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    final ContentValues cv = new ContentValues();
    cv.put(UserPositionContract.Entry.COLUMN_LAT, position.lat);
    cv.put(UserPositionContract.Entry.COLUMN_LON, position.lon);
    cv.put(UserPositionContract.Entry.COLUMN_SPEED, position.speed);
    cv.put(UserPositionContract.Entry.COLUMN_TIME, position.timestampSec);

    scheduler.execute(new Runnable() {
      @Override
      public void run() {
        db.insert(UserPositionContract.Entry.COLUMN_TABLE, null, cv);
      }
    });
  }

  public long insertMyOrderStage(OrderStage stage) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    ContentValues cv = stage.getAsContentValues();
    cv.put(OrderStageContract.Entry.orderType, getMyOrderInfo().orderType.toString());
    return db.insert(OrderStageContract.Entry.TABLE_NAME, null, cv);
  }

  public OrderStage getMyOrderStage() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    OrderStage orderStage = null;

    Cursor cursor = db.rawQuery("SELECT * FROM my_order_status_table ORDER BY date ASC LIMIT 1", null);

    if (cursor.moveToFirst()) {
      try {
        orderStage = OrderStageFactory.create(cursor);
      } catch (Exception e) {
        LogWriter.log(e.getMessage());
      }
    }
    cursor.close();
    return orderStage;
  }

  public MyOrderStagePhoto getMyOrderStagePhoto() {
    MyOrderStagePhoto myOrderStagePhoto = null;

    Cursor cursor = storage.rawQuery("SELECT * FROM my_order_stage_photo_table LIMIT 1", null);
    if (cursor.moveToFirst()) {
      try {
        myOrderStagePhoto = new MyOrderStagePhoto(
            cursor.getInt(cursor.getColumnIndex(MyOrderStagePhoto.Entry.STAGE_INDEX)),
            cursor.getString(cursor.getColumnIndex(MyOrderStagePhoto.Entry.TRIP_ID)),
            cursor.getString(cursor.getColumnIndex(MyOrderStagePhoto.Entry.IMAGE_NAME))
        );
      } catch (Exception e) {
        LogWriter.log(e.getMessage());
      }
    }
    cursor.close();
    return myOrderStagePhoto;
  }

  public boolean isMyOrderStatusNeedToSubmit() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    boolean result = false;

    Cursor cursor = db.rawQuery("SELECT * FROM my_order_status_table ORDER BY date ASC LIMIT 1", null);
    if (cursor.moveToFirst())
      result = true;
    cursor.close();
    return result;
  }

  public void deleteMyOrderStage(int stage) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    long result = db.delete(OrderStageContract.Entry.TABLE_NAME, OrderStageContract.Entry.stageId + " = " + stage, null);
    //ToDo Сделать удаление ненужных файлов
  }

  public void deleteMyOrderStage() {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.execSQL("DELETE FROM " + OrderStageContract.Entry.TABLE_NAME);
  }

  public List<UserPosition> getLastUserPosition(int count) {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    List<UserPosition> userPosition = new ArrayList<>();

    Cursor cursor = db.query(UserPositionContract.Entry.COLUMN_TABLE, null, null, null, null, null, UserPositionContract.Entry.COLUMN_TIME + " ASC", Integer.toString(count));
    if (cursor.moveToFirst()) {
      do {
        UserPosition pos = new UserPosition(
            cursor.getDouble(cursor.getColumnIndex(UserPositionContract.Entry.COLUMN_LAT)),
            cursor.getDouble(cursor.getColumnIndex(UserPositionContract.Entry.COLUMN_LON)),
            cursor.getDouble(cursor.getColumnIndex(UserPositionContract.Entry.COLUMN_SPEED)),
            cursor.getLong(cursor.getColumnIndex(UserPositionContract.Entry.COLUMN_TIME))
        );
        pos.id = cursor.getLong(cursor.getColumnIndex(UserPositionContract.Entry.COLUMN_ID));
        userPosition.add(pos);
      } while (cursor.moveToNext());
    }
    cursor.close();

    return userPosition;
  }

  public void deleteUserPositionRange(long time_from, long time_to) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.delete(UserPositionContract.Entry.COLUMN_TABLE, UserPositionContract.Entry.COLUMN_TIME + ">=" + time_from + " AND " + UserPositionContract.Entry.COLUMN_TIME + "<=" + time_to, null);
  }

  public boolean getLogoutState() {
    return sp.getBoolean("user_logout", false);
  }

  public void setLogoutState(boolean state) {
    sp.edit().putBoolean("user_logout", state).commit();
  }

  public void saveMyOrderInfo(MyOrderInfo myOrderInfo) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();

    if (myOrderInfo != null) {
      db.execSQL("DELETE FROM " + MyOrderContract.TABLE_NAME);
      ContentValues values = getAsContentValues(myOrderInfo);
      db.insert(MyOrderContract.TABLE_NAME, null, values);

      if (myOrderInfo.stages != null && myOrderInfo.stages.size() != 0) {
        db.execSQL("DELETE FROM " + MyOrderStagesContract.Entry.TABLE_NAME);

        for (OrderNextStage orderNextStage : myOrderInfo.stages) {
          ContentValues cvalues = new ContentValues();
          cvalues.put(MyOrderStagesContract.Entry.caption, orderNextStage.caption);
          cvalues.put(MyOrderStagesContract.Entry.stage_id, orderNextStage.id);
          db.insert(MyOrderStagesContract.Entry.TABLE_NAME, null, cvalues);
        }
      }
    }
  }

  private ContentValues getAsContentValues(MyOrderInfo myOrderInfo) {
    ContentValues values = new ContentValues();
    values.put(MyOrderContract.MyOrderEntry.createdAt, myOrderInfo.createdAt);
    values.put(MyOrderContract.MyOrderEntry.tariff, myOrderInfo.tariff);
    values.put(MyOrderContract.MyOrderEntry.loadingEntranceAddress, myOrderInfo.loadingEntranceAddress);
    values.put(MyOrderContract.MyOrderEntry.loadingContactName, myOrderInfo.loadingContactName);
    values.put(MyOrderContract.MyOrderEntry.loadingEntranceLatitude, myOrderInfo.loadingEntranceLatitude);
    values.put(MyOrderContract.MyOrderEntry.loadingEntranceLongitude, myOrderInfo.loadingEntranceLongitude);
    values.put(MyOrderContract.MyOrderEntry.loadingDate, myOrderInfo.loadingDate);
    values.put(MyOrderContract.MyOrderEntry.unloadingEntranceAddress, myOrderInfo.unloadingEntranceAddress);
    values.put(MyOrderContract.MyOrderEntry.unloadingContactName, myOrderInfo.unloadingContactName);
    values.put(MyOrderContract.MyOrderEntry.unloadingContactPhone, myOrderInfo.unloadingContactPhone);
    values.put(MyOrderContract.MyOrderEntry.unloadingEntranceLatitude, myOrderInfo.unloadingEntranceLatitude);
    values.put(MyOrderContract.MyOrderEntry.unloadingEntranceLongitude, myOrderInfo.loadingEntranceLongitude);
    values.put(MyOrderContract.MyOrderEntry.unloadingBeginDate, myOrderInfo.unloadingBeginDate);
    values.put(MyOrderContract.MyOrderEntry.unloadingEndDate, myOrderInfo.unloadingEndDate);
    values.put(MyOrderContract.MyOrderEntry.form, myOrderInfo.articleShape);
    values.put(MyOrderContract.MyOrderEntry.mark, myOrderInfo.articleBrand);
    values.put(MyOrderContract.MyOrderEntry.type, myOrderInfo.articleType);
    values.put(MyOrderContract.MyOrderEntry.weight, myOrderInfo.tonnage);
    values.put(MyOrderContract.MyOrderEntry.undistributedWeight, myOrderInfo.undistributedTonnage);
    values.put(MyOrderContract.MyOrderEntry.orderID, myOrderInfo.orderNumber);
    values.put(MyOrderContract.MyOrderEntry.comment, myOrderInfo.comment);
    values.put(MyOrderContract.MyOrderEntry.orderStatusId, myOrderInfo.orderStatusId);
    values.put(MyOrderContract.MyOrderEntry.orderStatusTitle, myOrderInfo.orderStatusTitle);
    values.put(MyOrderContract.MyOrderEntry.orderStatusDateTime, myOrderInfo.orderStatusDateTime);
    values.put(MyOrderContract.MyOrderEntry.supplier, myOrderInfo.supplier);
    values.put(MyOrderContract.MyOrderEntry.customer, myOrderInfo.customer);
    values.put(MyOrderContract.MyOrderEntry.objectId, myOrderInfo.objectId);
    values.put(MyOrderContract.MyOrderEntry.orderType, myOrderInfo.orderType.toString());
    values.put(MyOrderContract.MyOrderEntry.distance, myOrderInfo.distance);

    return values;
  }

  public void saveMyOrderChanges(MyOrderChangesInfo orderChanges, int orderNumber) {
    if (orderChanges == null) {
      return;
    }

    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.execSQL("DELETE FROM " + MyOrderChangesContract.TABLE_NAME +
            " WHERE " + MyOrderChangesContract.MyOrderChangesEntry.ORDER_NUMBER +
            " = " + orderNumber);

    orderChanges.getChanges().entrySet().stream().forEach(entry -> {
      ContentValues values = new ContentValues();
      values.put(MyOrderChangesContract.MyOrderChangesEntry.PROPERTY, entry.getKey());
      values.put(MyOrderChangesContract.MyOrderChangesEntry.VALUE, entry.getValue().value);
      values.put(MyOrderChangesContract.MyOrderChangesEntry.AUTHOR_ID, entry.getValue().authorId);
      values.put(MyOrderChangesContract.MyOrderChangesEntry.ORDER_NUMBER, orderNumber);
      db.insert(MyOrderChangesContract.TABLE_NAME, null, values);
    });
  }

  private ContentValues getPropertyAsContentValues(String value, String loadingEntranceAddress) {
    return null;
  }

  public void deleteCurrentOrder() {
    MyOrderInfo info = model.getMyOrderInfo();
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();

    if (info != null) {
      db.execSQL("DELETE FROM " + MyOrderContract.TABLE_NAME);
      deleteOrderChanges(info.orderNumber);
    }
    db.execSQL("DELETE FROM " + MyOrderStagesContract.Entry.TABLE_NAME);
  }

  private void deleteOrderChanges(int orderNumber) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.execSQL("DELETE FROM " + MyOrderChangesContract.TABLE_NAME +
            " WHERE " + MyOrderChangesContract.MyOrderChangesEntry.ORDER_NUMBER +
            " = " + orderNumber);
  }

  public MyOrderInfo getMyOrderInfo() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.rawQuery("SELECT * FROM " + MyOrderContract.TABLE_NAME, null);
    MyOrderInfo info = null;
    if (cursor.moveToFirst()) {
      info = MyOrderInfo.fromCursor(cursor);
    }
    cursor.close();

    if (info == null) return null;

    cursor = db.rawQuery("SELECT * FROM " + MyOrderStagesContract.Entry.TABLE_NAME, null);

    while (cursor.moveToNext()) {
      OrderNextStage orderNextStage = new OrderNextStage(
          cursor.getString(cursor.getColumnIndex(MyOrderStagesContract.Entry.caption)),
          cursor.getInt(cursor.getColumnIndex(MyOrderStagesContract.Entry.stage_id))
      );
      info.stages.add(orderNextStage);
    }
    cursor.close();

    return info;
  }

  public MyOrderChangesInfo getMyOrderChangesInfo(int orderNumber) {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.query(MyOrderChangesContract.TABLE_NAME, null, MyOrderChangesContract.MyOrderChangesEntry.ORDER_NUMBER + " = ?", new String[]{String.valueOf(orderNumber)}, null, null, null);
    MyOrderChangesInfo info = MyOrderChangesInfo.fromCursor(cursor);
    cursor.close();
    return info;
  }

  public List<OrderNextStage> getOrderNextStages() {
    List<OrderNextStage> stages = new ArrayList<>();
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.rawQuery("SELECT * FROM " + MyOrderStagesContract.Entry.TABLE_NAME, null);
    while (cursor.moveToNext()) {
      OrderNextStage orderNextStage = new OrderNextStage(
          cursor.getString(cursor.getColumnIndex(MyOrderStagesContract.Entry.caption)),
          cursor.getInt(cursor.getColumnIndex(MyOrderStagesContract.Entry.stage_id))
      );
      stages.add(orderNextStage);
    }
    cursor.close();
    return stages;
  }

  public long deleteOrderNextStages() {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    SQLiteStatement statement = db.compileStatement("DELETE FROM " + MyOrderStagesContract.Entry.TABLE_NAME);
    return statement.executeUpdateDelete();
  }

  public long deleteOrderStage(int stageId) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    SQLiteStatement statement = db.compileStatement("DELETE FROM " + MyOrderStagesContract.Entry.TABLE_NAME + " WHERE " + MyOrderStagesContract.Entry.stage_id + " = " + stageId);
    return statement.executeUpdateDelete();
  }

  public long insertOrderNextStage(OrderNextStage orderNextStage) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    ContentValues cvalues = new ContentValues();
    cvalues.put(MyOrderStagesContract.Entry.caption, orderNextStage.caption);
    cvalues.put(MyOrderStagesContract.Entry.stage_id, orderNextStage.id);
    return db.insert(MyOrderStagesContract.Entry.TABLE_NAME, null, cvalues);
  }

  public void saveCrewStatus(DriverStatus driverStatus) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    ContentValues cvalues = new ContentValues();
    cvalues.put(CrewStatusContract.Entry.ID, 0);
    cvalues.put(CrewStatusContract.Entry.STATUS_ID, driverStatus.value());
    db.insertWithOnConflict(CrewStatusContract.TABLE_NAME, null, cvalues, SQLiteDatabase.CONFLICT_REPLACE);
  }

  public DriverStatus getDriverStatus() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.query(CrewStatusContract.TABLE_NAME, null, null, null, null, null, null);
    if (cursor.moveToNext()) {
      return DriverStatus.values()[cursor.getInt(cursor.getColumnIndex(CrewStatusContract.Entry.STATUS_ID))];
    }
    return DriverStatus.CREW_STATUS_NOT_READY;
  }

  public boolean isCrewStatusUpdatedOnServer() {
    SQLiteDatabase db = SQLiteHelper.instance().getReadableDatabase();
    Cursor cursor = db.query(CrewStatusContract.TABLE_NAME, null, null, null, null, null, null);
    if (cursor.moveToNext()) {
      return cursor.getInt(cursor.getColumnIndex(CrewStatusContract.Entry.SERVER_UPDATED)) == 1;
    }
    return false;
  }


  //////////////////////////// Classes ///////////////////////////

  //метод для отложенной записи статуса водителя
  public void saveCrewStatusUpdatedOnServer() {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    ContentValues cvalues = new ContentValues();
    cvalues.put(CrewStatusContract.Entry.SERVER_UPDATED, 1);
    db.update(CrewStatusContract.TABLE_NAME, cvalues, "ID = 0", null);
  }

  public void saveDriverLocation(Location location) {
    UserInfo userInfo = getInstance().getUserInfo();
    if (userInfo == null)
      return;

    userInfo.latitude = location.getCoordinate().getLatitude();
    userInfo.longitude = location.getCoordinate().getLongitude();
    getInstance().setUserInfo(userInfo);
  }

  public boolean getInProgress() {
    return inProgress;
  }

  public void setInProgress(boolean inProgress) {
    this.inProgress = inProgress;
  }

  public void saveMyOrderStagePhoto(MyOrderStagePhoto stagePhoto) {
    ContentValues values = new ContentValues();
    values.put(MyOrderStagePhoto.Entry.STAGE_INDEX, stagePhoto.getStageIndex());
    values.put(MyOrderStagePhoto.Entry.TRIP_ID, stagePhoto.getTripId());
    values.put(MyOrderStagePhoto.Entry.IMAGE_NAME, stagePhoto.getImageName());
    storage.insert(MyOrderStagePhoto.Entry.TABLE_NAME, null, values);
  }

  public void deleteMyOrderStagePhoto(MyOrderStagePhoto stagePhoto) {
    SQLiteDatabase db = SQLiteHelper.instance().getWritableDatabase();
    db.delete(
            MyOrderStagePhoto.Entry.TABLE_NAME,
            MyOrderStagePhoto.Entry.TRIP_ID + " = '" + stagePhoto.getTripId() + "'" +
            " AND " + MyOrderStagePhoto.Entry.STAGE_INDEX + " = " + stagePhoto.getStageIndex(),
            null
    );
  }

  public void releaseTruckAndClearCurrentOrder() {
    clearTransportTable();
    saveCrewStatus(DriverStatus.CREW_STATUS_NOT_READY);
    saveCrewStatusUpdatedOnServer();
    clearCurrentOrder();
    dependencyContext.getLocationManager().changeLocationUpdatesInterval();
  }

  public static class UserInfo {
    public String username, fio, session_id, passw;
    public int id;
    public String user_id;
    public long expire;
    public double latitude;
    public double longitude;

    public UserInfo(String username, String fio, String session_id, String user_id, long expire, String passw, int internal_id, double latitude, double longitude) {
      this.username = username;
      this.fio = fio;
      this.session_id = session_id;
      this.user_id = user_id;
      this.expire = expire;
      this.passw = passw;
      this.id = internal_id;
      this.latitude = latitude;
      this.longitude = longitude;
    }

    public UserInfo() {

    }
  }

  public static class UserPosition {
    public final double lat, lon;
    public final double speed;
    public final long timestampSec;
    public long id;

    public UserPosition(double lat, double lon, double speed, long timestampSec) {
      this.lat = lat;
      this.lon = lon;
      this.speed = speed;
      this.timestampSec = timestampSec;
    }

    public static UserPosition fromLocation(Location location) {
      return new UserPosition(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude(), location.getSpeed(), location.getTime());
    }

    public JsonObject serialize() {
      JsonObject result = new JsonObject();
      result.addProperty("latitude", this.lat);
      result.addProperty("longitude", this.lon);
      result.addProperty("speed", this.speed);
      result.addProperty("sendDelay", System.currentTimeMillis() - this.timestampSec);
      return result;
    }
  }

  public static class MyOrderInfo {
    private static final Integer NO_STATUS = -1;
    public Long orderStatusDateTime;
    public Integer orderStatusId;
    public String orderStatusTitle;
    public String unloadingEntranceAddress;
    public String unloadingContactName;
    public double unloadingEntranceLatitude;
    public double unloadingEntranceLongitude;
    public Long unloadingDate;
    public String loadingEntranceAddress;
    public String loadingContactName;
    public String loadingContactPhone;
    public double loadingEntranceLatitude;
    public double loadingEntranceLongitude;
    public Long loadingDate;
    public String articleShape;
    public String articleBrand;
    public String articleType;
    public double tonnage;
    public double undistributedTonnage;
    public int orderNumber;
    public String comment;
    public int selectedStage;
    public List<OrderNextStage> stages = new ArrayList<>();
    public Long unloadingBeginDate;
    public Long unloadingEndDate;
    public long createdAt;
    public double tariff;
    public String supplier;
    public String customer;
    public String unloadingContactPhone;
    public String objectId;
    public OrderType orderType;
    public int distance;

    private MyOrderInfo() {
    }

    static MyOrderInfo fromCursor(Cursor cursor) {
      MyOrderInfo myOrderInfo = new MyOrderInfo();
      myOrderInfo.createdAt = cursor.getLong(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.createdAt));
      myOrderInfo.tariff = cursor.getDouble(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.tariff));

      myOrderInfo.loadingDate = cursor.getLong(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.loadingDate));
      myOrderInfo.loadingContactName = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.loadingContactName));
      myOrderInfo.loadingContactPhone = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.loadingContactPhone));
      myOrderInfo.loadingEntranceAddress = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.loadingEntranceAddress));
      myOrderInfo.loadingEntranceLatitude = cursor.getDouble(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.loadingEntranceLatitude));
      myOrderInfo.loadingEntranceLongitude = cursor.getDouble(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.loadingEntranceLongitude));

      myOrderInfo.unloadingBeginDate = cursor.getLong(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingBeginDate));
      myOrderInfo.unloadingEndDate = cursor.getLong(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingEndDate));
      myOrderInfo.unloadingContactName = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingContactName));
      myOrderInfo.unloadingContactPhone = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingContactPhone));
      myOrderInfo.unloadingEntranceAddress = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingEntranceAddress));
      myOrderInfo.unloadingEntranceLatitude = cursor.getDouble(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingEntranceLatitude));
      myOrderInfo.unloadingEntranceLongitude = cursor.getDouble(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.unloadingEntranceLongitude));

      myOrderInfo.articleShape = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.form));
      myOrderInfo.articleBrand = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.mark));
      myOrderInfo.articleType = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.type));
      myOrderInfo.tonnage = cursor.getDouble(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.weight));
      myOrderInfo.orderNumber = cursor.getInt(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.orderID));
      myOrderInfo.comment = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.comment));
      myOrderInfo.orderStatusId = cursor.getInt(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.orderStatusId));
      myOrderInfo.orderStatusTitle = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.orderStatusTitle));
      myOrderInfo.orderStatusDateTime = cursor.getLong(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.orderStatusDateTime));
      myOrderInfo.supplier = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.supplier));
      myOrderInfo.customer = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.customer));
      myOrderInfo.objectId = cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.objectId));
      myOrderInfo.orderType = OrderType.fromString(cursor.getString(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.orderType))).orElse(OrderType.DEALER_ORDER);
      myOrderInfo.distance = cursor.getInt(cursor.getColumnIndex(MyOrderContract.MyOrderEntry.distance));
      return myOrderInfo;
    }

    public static MyOrderInfo fromJson(JsonObject jsonObject) {
      MyOrderInfo myOrderInfo = new MyOrderInfo();
      JsonObject loadingPoint = jsonObject.getAsJsonObject("loadingEntrance");
      JsonObject unloadingPoint = jsonObject.getAsJsonObject("unloadingEntrance");
      myOrderInfo.createdAt = (jsonObject.has("createdAt")) ? jsonObject.get("createdAt").getAsLong() : 0;
      myOrderInfo.tariff = (jsonObject.has("tariff")) ? jsonObject.get("tariff").getAsDouble() : 0;
      myOrderInfo.unloadingEntranceAddress = unloadingPoint.get("address").getAsString();
      myOrderInfo.unloadingContactName = (jsonObject.has("unloadingContactName")) ? jsonObject.get("unloadingContactName").getAsString() : "";
      myOrderInfo.unloadingContactPhone = (jsonObject.has("unloadingContactPhone")) ? jsonObject.get("unloadingContactPhone").getAsString() : "";
      myOrderInfo.unloadingEntranceLatitude = unloadingPoint.get("latitude").getAsDouble();
      myOrderInfo.unloadingEntranceLongitude = unloadingPoint.get("longitude").getAsDouble();
      myOrderInfo.unloadingBeginDate = (jsonObject.has("unloadingBeginDate")) ? jsonObject.get("unloadingBeginDate").getAsLong() : 0;
      myOrderInfo.unloadingEndDate = (jsonObject.has("unloadingEndDate")) ? jsonObject.get("unloadingEndDate").getAsLong() : 0;
      myOrderInfo.loadingEntranceAddress = loadingPoint.get("address").getAsString();
      myOrderInfo.loadingContactName = (jsonObject.has("loadingContactName")) ? jsonObject.get("loadingContactName").getAsString() : "";
      myOrderInfo.loadingContactPhone = (jsonObject.has("loadingContactPhone")) ? jsonObject.get("loadingContactPhone").getAsString() : "";
      myOrderInfo.loadingEntranceLatitude = loadingPoint.get("latitude").getAsDouble();
      myOrderInfo.loadingEntranceLongitude = loadingPoint.get("longitude").getAsDouble();
      myOrderInfo.loadingDate = jsonObject.get("loadingDate").getAsLong();
      myOrderInfo.articleShape = jsonObject.get("articleShape").getAsString();
      myOrderInfo.articleBrand = (jsonObject.has("articleBrand")) ? jsonObject.get("articleBrand").getAsString() : "";
      myOrderInfo.articleType = jsonObject.get("articleType").getAsString();
      myOrderInfo.tonnage = jsonObject.get("tonnage").getAsDouble();
      myOrderInfo.orderNumber = jsonObject.get("number").getAsInt();
      myOrderInfo.comment = (jsonObject.has("comment")) ? jsonObject.get("comment").getAsString() : "";
      myOrderInfo.orderStatusId = (jsonObject.has("orderStatusId")) ? jsonObject.get("orderStatusId").getAsInt() : NO_STATUS;
      myOrderInfo.orderStatusTitle = (jsonObject.has("orderStatusTitle")) ? jsonObject.get("orderStatusTitle").getAsString() : "";
      myOrderInfo.orderStatusDateTime = (jsonObject.has("orderStatusDateTime")) ? jsonObject.get("orderStatusDateTime").getAsLong() : 0;
      myOrderInfo.supplier = (jsonObject.has("supplier")) ? jsonObject.get("supplier").getAsString() : "";
      myOrderInfo.customer = (jsonObject.has("customer")) ? jsonObject.get("customer").getAsString() : "";
      myOrderInfo.objectId = (jsonObject.has("objectId")) ? jsonObject.get("objectId").getAsString() : "";
      myOrderInfo.orderType = OrderType.fromJson(jsonObject.get("type")).orElse(OrderType.DEALER_ORDER);
      myOrderInfo.distance = (jsonObject.has("distance")) ? jsonObject.get("distance").getAsInt() : 0;
      return myOrderInfo;
    }

    public Order getAsOrder() {
      Order.Builder<?> result;
      if (orderType == OrderType.CARRIAGE_ORDER) {
        result = new CarriageOrder.Builder(this);
      } else {
        result = new DealerOrder.Builder(this);
      }

      if (orderStatusDateTime != null) {
        result.orderStatusDateTime(orderStatusDateTime);
      }
      result.active(true);
      return result.build();
    }

    public void merge(JsonObject jsonObject) {
      JsonObject loadingPoint = jsonObject.getAsJsonObject("loadingEntrance");
      JsonObject unloadingPoint = jsonObject.getAsJsonObject("unloadingEntrance");
      this.createdAt = (jsonObject.has("createdAt")) ? jsonObject.get("createdAt").getAsLong() : this.createdAt;
      this.tariff = (jsonObject.has("tariff")) ? jsonObject.get("tariff").getAsDouble() : this.tariff;
      this.unloadingEntranceAddress = unloadingPoint.get("address").getAsString();
      this.unloadingContactName = (jsonObject.has("unloadingContactName")) ? jsonObject.get("unloadingContactName").getAsString() : this.unloadingContactName;
      this.unloadingContactPhone = (jsonObject.has("unloadingContactPhone")) ? jsonObject.get("unloadingContactPhone").getAsString() : this.unloadingContactPhone;
      this.unloadingEntranceLatitude = unloadingPoint.get("latitude").getAsDouble();
      this.unloadingEntranceLongitude = unloadingPoint.get("longitude").getAsDouble();
      this.unloadingBeginDate = (jsonObject.has("unloadingBeginDate")) ? jsonObject.get("unloadingBeginDate").getAsLong() : this.unloadingBeginDate;
      this.unloadingEndDate = (jsonObject.has("unloadingEndDate")) ? jsonObject.get("unloadingEndDate").getAsLong() : this.unloadingEndDate;
      this.loadingEntranceAddress = loadingPoint.get("address").getAsString();
      this.loadingContactName = (jsonObject.has("loadingContactName")) ? jsonObject.get("loadingContactName").getAsString() : this.loadingContactName;
      this.loadingContactPhone = (jsonObject.has("loadingContactPhone")) ? jsonObject.get("loadingContactPhone").getAsString() : this.loadingContactPhone;
      this.loadingEntranceLatitude = loadingPoint.get("latitude").getAsDouble();
      this.loadingEntranceLongitude = loadingPoint.get("longitude").getAsDouble();
      this.loadingDate = jsonObject.get("loadingDate").getAsLong();
      this.articleShape = jsonObject.get("articleShape").getAsString();
      this.articleBrand = (jsonObject.has("articleBrand")) ? jsonObject.get("articleBrand").getAsString() : this.articleBrand;
      this.articleType = jsonObject.get("articleType").getAsString();
      this.tonnage = jsonObject.get("tonnage").getAsDouble();
      this.orderNumber = jsonObject.get("number").getAsInt();
      this.comment = (jsonObject.has("comment")) ? jsonObject.get("comment").getAsString() : this.comment;
      this.orderStatusId = (jsonObject.has("orderStatusId")) ? jsonObject.get("orderStatusId").getAsInt() : this.orderStatusId;
      this.orderStatusTitle = (jsonObject.has("orderStatusTitle")) ? jsonObject.get("orderStatusTitle").getAsString() : this.orderStatusTitle;
      this.orderStatusDateTime = (jsonObject.has("orderStatusDateTime")) ? jsonObject.get("orderStatusDateTime").getAsLong() : this.orderStatusDateTime;
      this.supplier = (jsonObject.has("supplier")) ? jsonObject.get("supplier").getAsString() : this.supplier;
      this.customer = (jsonObject.has("customer")) ? jsonObject.get("customer").getAsString() : this.customer;
      this.objectId = (jsonObject.has("objectId")) ? jsonObject.get("objectId").getAsString() : this.objectId;
      this.orderType = (jsonObject.has("type")) ? OrderType.fromJson(jsonObject.get("type")).orElse(OrderType.DEALER_ORDER) : this.orderType;
      this.distance = (jsonObject.has("distance")) ? jsonObject.get("distance").getAsInt() : 0;
    }
  }

  public static class OrderNextStage {
    public String caption;
    public int id;

    public OrderNextStage(String caption, int id) {
      this.caption = caption;
      this.id = id;
    }
  }
}
