package loaders;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;
import api.ApiHelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.macsoftex.cargodeal.driver.R;
import features.log.LogWriter;
import model.FinishedOrdersList;
import model.Model;
import features.order.OrderFactory;
import retrofit2.Response;

/**
 * Created by Alex Baturski on 30.08.2018.
 */

public class FinishedOrdersLoader extends AsyncTaskLoader<Boolean> {
    private Exception exception;
    private Boolean data;
    private ApiHelper mAPI;

    public FinishedOrdersLoader(ApiHelper api, Context context, Bundle args) {
        super(context);
        mAPI = api;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null || exception != null){
            deliverResult(data);
        }
    }

    @Override
    public Boolean loadInBackground() {

        try {
            Response<JsonObject> response = mAPI.getServerAPI().listFinishedOrders(Model.getInstance().getAccessToken()).execute();
            if(response.code() != 200){
                throw new Exception("response code = " + response.code());
            }

            if(response.body().has("result")){
                if(response.body().get("result").getAsJsonObject().get("result").getAsBoolean())
                {
                    JsonArray ordersArray = response.body().getAsJsonObject("result").getAsJsonArray("objects");
                    int ordersCount = response.body().getAsJsonObject("result").get("count").getAsInt();

                    FinishedOrdersList.getInstance().сlearOrders();

                    for (int i=0;i<ordersCount;i++)
                    {
                        JsonObject orderJson = ordersArray.get(i).getAsJsonObject();
                        FinishedOrdersList.getInstance().addOrder(OrderFactory.fromJsonObject(orderJson));
                    }
                    return true;
                } else {
                    switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                        case ApiHelper.INVALID_REQUEST:
                            throw new Exception(getContext().getString(R.string.InvalidRequest));
                    }
                }
            }
        } catch (Exception e) {
            LogWriter.logException(e);
            if (e.getClass().getName().equals("java.net.UnknownHostException"))
                this.exception = new Exception(getContext().getString(R.string.no_server_access));
            else
                this.exception = e;
        }

        return false;
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        data = null;
        exception = null;
    }

    public Exception getException() {
        return exception;
    }
}
