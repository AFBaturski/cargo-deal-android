package loaders;

import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;

import api.TaskScheduler;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.net.UnknownHostException;
import java.util.HashMap;

import di.DependencyContainer;
import features.log.LogWriter;
import features.order.OrderFactory;
import features.order.OrderManager;
import features.transport_unit.model.Transport;
import features.transport_unit.model.TransportFactory;
import model.*;
import retrofit2.Response;
import api.ApiHelper;
import common.CargoDealSystem;
import com.macsoftex.cargodeal.driver.R;
import service.tasks.SendFcmTokenTask;

/**
 * Created by Eugene on 23.04.2017.
 */

public class AuthRequestLoader extends AsyncTaskLoader<Boolean> {
    private static final String LOG_TAG = AuthRequestLoader.class.getSimpleName();
    private static final Integer NO_STAGE = -1;
    private final ApiHelper api;
    private final Model model;
    private final TaskScheduler scheduler;
    private Boolean onlyCheck=null;

    private Exception exception;
    private Boolean data;
    private String username, password;
    private DependencyContainer dependencyContext;

    public AuthRequestLoader(DependencyContainer dependencyContext, Bundle args) {
        super(dependencyContext.getContext());
        this.dependencyContext = dependencyContext;
        api = dependencyContext.getApi();
        model = dependencyContext.getModel();
        scheduler = dependencyContext.getScheduler();
        if(args != null){
            username = args.containsKey("username") ? args.getString("username") : null;
            password = args.containsKey("password") ? args.getString("password") : null;
            onlyCheck = args.containsKey("onlyCheck") ? args.getBoolean("onlyCheck") : null;
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null || exception != null){
            deliverResult(data);
        }
    }

    @Override
    public Boolean loadInBackground() {

        if (onlyCheck == null && username == null && password == null)
            return false;

        try {
            boolean successLogin;
            if (onlyCheck == null) {
                successLogin = checkLogin();
                if (!successLogin)
                    return false;
                if (!api.checkIsAppVersionAllowed())
                    throw new Exception(getContext().getString(R.string.version_deprecated));
            }
        }
        catch (Exception e) {
            handleNoInternetException(e);
            return false;
        }

        try {
            //Загрузка информации о текущей поездке, выбранном грузовике, активных заказах, завершенных заказах
            checkTransportUnit();
            getOrders();
            getFinishedOrders();
        }
        catch (Exception e){
            handleNoInternetException(e);
            if (isForbiddenException(e)) {
                api.parseLogout();
                return false;
            }
        }
        finally {
            checkActiveOrderLocally();
        }

        try {
            checkOtherSessions();
        }
        catch (Exception e){
            handleNoInternetException(e);
            return false;
        }
        return true;
    }

    private boolean isForbiddenException(Exception e) {
        return e instanceof IllegalAccessException;
    }

    private void checkOtherSessions() throws Exception {
        JsonArray sessions = getSessions();

        if (sessions != null && sessions.size() > getMaxAllowedSessions()) {
            throw new Exception(getContext().getString(R.string.user_have_other_session));
        }
    }

    private void handleNoInternetException(Exception e) {
        LogWriter.log(e.getMessage());
        this.exception = (e.getClass().equals(UnknownHostException.class))
                ? new Exception(getContext().getString(R.string.no_server_access)) : e;
    }

    private void checkActiveOrderLocally() {
        Model.MyOrderInfo orderInfo = Model.getInstance().getMyOrderInfo();
        //Если есть активный заказ, то добавляем его в список первым
        if (orderInfo != null )
            OrderManager.getInstance().insertOrder(orderInfo.getAsOrder());
    }

    private boolean checkLogin() throws Exception {
        HashMap<String, String> loginDetails = new HashMap<>();
        loginDetails.put("password",password);
        loginDetails.put("username",username);

        Model.UserInfo info = Model.getInstance().getUserInfo();

        if (info == null)
            info = new Model.UserInfo();

        info.username = username;
        info.passw = password;
        info.session_id = null;
        Model.getInstance().setUserInfo(info);

        Response<JsonObject> response = api.getServerAPI().parseLogin(loginDetails).execute();
        LogWriter.logServerResponse(response);
        checkIncorrectUserCredentials(response);

        JsonObject userInfoFromServer = response.body();
        String sessionId = userInfoFromServer.get("sessionToken").getAsString();
        if (sessionId != null) {
            fetchDriverInfo(userInfoFromServer);
            saveDriverInfo(userInfoFromServer);
            scheduler.execute(new SendFcmTokenTask(getContext(), scheduler, api, model));
            return true;
        }
    return false;
    }

    private void fetchDriverInfo(JsonObject userInfoFromServer) throws Exception {
        String sessionId = userInfoFromServer.get("sessionToken").getAsString();
        if (!userInfoFromServer.has("driver"))
            return;

        JsonObject driverObject = userInfoFromServer.get("driver").getAsJsonObject();
        String driverObjectId = driverObject.get("objectId").getAsString();

        try {
            Response<JsonObject> response = api.getServerAPI().getDriver(sessionId, driverObjectId).execute();
            LogWriter.logServerResponse(response);
            if (response.isSuccessful() && response.body().has("name")){
                String name = response.body().get("name").getAsString();
                userInfoFromServer.addProperty("name", name);
            }
        } catch (Exception e){
            throw new Exception(e);
        }

    }

    private int getMaxAllowedSessions() {
        return CargoDealSystem.MAX_ALLOWED_SESSIONS;
    }

    private void checkIncorrectUserCredentials(Response<JsonObject> response) throws Exception {
        if (!response.isSuccessful()) {
            if (response.code() == 404) {
                throw new Exception(getContext().getString(R.string.incorrect_user_name));
            }
            throw new Exception(response.message());
        } else if (api.isSuccessServerResponse(response)) {//TODO Переработать на функцию isHasErrorFromServer
            api.handleResultError(response);
        }
    }

    private JsonArray getSessions() throws Exception {
        Model.UserInfo userInfo = Model.getInstance().getUserInfo();
        String sessionId = (userInfo != null) ? userInfo.session_id : "";
        Response<JsonObject> response = api.getServerAPI().getSessions(sessionId).execute();
        LogWriter.logServerResponse(response);

        if (!response.isSuccessful())
            throw new Exception(response.message());

        if (!response.body().has("results"))
            throw new Exception(getContext().getString(R.string.have_no_sessions_data));

        JsonArray sessions;
        sessions = response.body().getAsJsonArray("results");
        int indexToRemove = -1;
        for (int i = 0; i<sessions.size(); i++) {
            JsonElement session = sessions.get(i);
            if (session.isJsonObject() && session.getAsJsonObject().has("createdWith")) {
                String createdWith = session.getAsJsonObject().get("createdWith").toString();
                if (createdWith.contains("signup"))
                    indexToRemove = i;
            }
        }

        CargoDealSystem.getInstance().setUserSessions(sessions);

        if (indexToRemove != -1)
            sessions.remove(indexToRemove);

        return sessions;
    }

    private void saveDriverInfo(JsonObject userInfoFromServer) {
        Model.UserInfo info = Model.getInstance().getUserInfo();
        info.session_id = userInfoFromServer.get("sessionToken").getAsString();
        info.user_id = userInfoFromServer.get("objectId").getAsString();

        if (userInfoFromServer.has("name"))
            info.fio = userInfoFromServer.get("name").getAsString();

        //сохраняем информацию о сессии в БД
        Model.getInstance().setUserInfo(info);
    }

    private void checkTransportUnit() throws Exception {
        LogWriter.log(this.getClass().getSimpleName()+" : checkTransportUnit");
        Response<JsonObject> response = api.getServerAPI().checkTransportUnit(Model.getInstance().getAccessToken()).execute();
        LogWriter.logServerResponse(response);

        if (!response.isSuccessful())
            throw new Exception(response.message());

        if (response.body().has("result")) {
            if (response.body().get("result").getAsJsonObject().get("result").getAsBoolean()
                && response.body().get("result").getAsJsonObject().has("exists")
                && response.body().get("result").getAsJsonObject().get("exists").getAsBoolean()) {

                JsonObject vehicle = response.body().getAsJsonObject("result").getAsJsonObject("vehicle");
                JsonObject trailer = response.body().getAsJsonObject("result").getAsJsonObject("trailer");

                Transport selectedVehicle = TransportFactory.createVehicle(vehicle);

                Transport selectedTrailer=null;
                if (trailer != null)
                    selectedTrailer = TransportFactory.createTrailer(trailer);

                Model.getInstance().clearTransportTable();
                Model.getInstance().addTransport(selectedTrailer, selectedVehicle);
                int statusId = (response.body().getAsJsonObject("result").has("status"))?
                        response.body().getAsJsonObject("result").get("status").getAsInt() : DriverStatus.CREW_STATUS_WORK.value();

                //Устанавливаем статус водителя пришедший с сервера
                Model.getInstance().saveCrewStatus(DriverStatus.values()[statusId]);
                Model.getInstance().saveCrewStatusUpdatedOnServer();

                processCurrentTripIfExists(response);
            } else if (response.body().get("result").getAsJsonObject().get("result").getAsBoolean()
                    && response.body().get("result").getAsJsonObject().has("exists")
                    && !response.body().get("result").getAsJsonObject().get("exists").getAsBoolean()) {
                //Нет связки грузовик/трейлер
                model.releaseTruckAndClearCurrentOrder();
            } else if (!response.body().get("result").getAsJsonObject().get("result").getAsBoolean() &&
                    response.body().get("result").getAsJsonObject().has("error")){
                String errorType = response.body().get("result").getAsJsonObject().get("error").getAsString();
                switch (errorType) {
                    case ApiHelper.INVALID_REQUEST:
                        throw new Exception(getContext().getString(R.string.InvalidRequest));
                    case ApiHelper.FORBIDDEN:
                        throw new IllegalAccessException(getContext().getString(R.string.forbidden));
                    default:
                        throw new Exception(errorType);
                }
            }
        }
    }

    private void processCurrentTripIfExists(Response<JsonObject> response) {
        JsonObject trip  = response.body().getAsJsonObject("result").getAsJsonObject("trip");
        if (trip == null) {
            model.cancelCurrentOrderAndAwaitNewOne();
            return;
        }

        JsonObject currentStage = getCurrentStage(response);
        Integer currentStageId = NO_STAGE;
        String currentStageTitle = "";
        long currentStageDateTime = 0;
        String currentStageDateTimeToString = "";
        if (currentStage != null){
            currentStageId = currentStage.has("index") ? currentStage.get("index").getAsInt() : NO_STAGE;
            currentStageTitle = currentStage.has("title") ? currentStage.get("title").getAsString() : "";
            currentStageDateTime = currentStage.has("date") ? currentStage.get("date").getAsLong() : 0;
        }

        String objectId = trip.has("objectId") ? trip.get("objectId").getAsString() : "";
        JsonObject loadingPoint = trip.getAsJsonObject("loadingEntrance");
        JsonObject unloadingPoint = trip.getAsJsonObject("unloadingEntrance");
        JsonArray stages = response.body().getAsJsonObject("result").getAsJsonArray("nextStages");
        trip.addProperty("orderStatusId", currentStageId);
        trip.addProperty("orderStatusTitle", currentStageTitle);
        trip.addProperty("orderStatusDateTime", currentStageDateTime);

        Model.MyOrderInfo myOrderInfo = Model.MyOrderInfo.fromJson(trip);
        Model.MyOrderInfo myOrderInfoLocal = Model.getInstance().getMyOrderInfo();

        if (stages != null && !Model.getInstance().isMyOrderStatusNeedToSubmit()) {
            for (int i = 0; i < stages.size(); i++) {
                JsonObject stage = stages.get(i).getAsJsonObject();
                Model.OrderNextStage orderNextStage = new Model.OrderNextStage(
                        stage.get("title").getAsString(),
                        stage.get("index").getAsInt()
                );
                myOrderInfo.stages.add(orderNextStage);
            }
        } else if (Model.getInstance().isMyOrderStatusNeedToSubmit() && myOrderInfoLocal != null){
            myOrderInfo.stages = myOrderInfoLocal.stages;
        }

        //Заменяем информацию о заказе той, что пришла с сервера
        Model.getInstance().saveMyOrderInfo(myOrderInfo);
    }

    private JsonObject getCurrentStage(Response<JsonObject> getActiveOrderResp) {
        JsonObject result = getActiveOrderResp.body().getAsJsonObject("result");
        JsonObject currentStage = result.has("currentStage") ? result.getAsJsonObject("currentStage") : null;
        return currentStage;
    }

    private void getOrders() throws Exception {
        if (!model.isTransportUnitAvailable())
            return;

        LogWriter.log(this.getClass().getSimpleName()+" : listOrders");
        Response<JsonObject> response = api.getServerAPI().listOrders(Model.getInstance().getAccessToken()).execute();
        LogWriter.logServerResponse(response);

        if(response.code() != 200){
            throw new Exception("response code = " + response.code());
        }

        if(response.body().has("result")){
            if(response.body().get("result").getAsJsonObject().get("result").getAsBoolean())
            {
                JsonArray ordersArray = response.body().getAsJsonObject("result").getAsJsonArray("objects");
                int ordersCount = response.body().getAsJsonObject("result").get("count").getAsInt();

                OrderManager.getInstance().clearOrders();
                for (int i=0;i<ordersCount;i++)
                {
                    JsonObject orderJson = ordersArray.get(i).getAsJsonObject();
                    OrderManager.getInstance().addOrder(OrderFactory.fromJsonObject(orderJson));
                }
            } else {
                switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                    case ApiHelper.INVALID_REQUEST:
                        throw new Exception(getContext().getString(R.string.InvalidRequest));
                    case ApiHelper.NO_TRANSPORT_UNIT: {
                        OrderManager.getInstance().clearOrders();
                        Model.getInstance().clearTransportTable();
                        throw new Exception(getContext().getString(R.string.NoTransportUnit));
                    }
                }
            }
        }
    }

    private void getFinishedOrders() throws Exception {
        LogWriter.log(this.getClass().getSimpleName()+" : listFinishedOrders");
        Response<JsonObject> response = api.getServerAPI().listFinishedOrders(Model.getInstance().getAccessToken()).execute();

        if(response.code() != 200){
            throw new Exception("response code = " + response.code());
        }

        if(response.body().has("result")){
            if(response.body().get("result").getAsJsonObject().get("result").getAsBoolean())
            {
                JsonArray ordersArray = response.body().getAsJsonObject("result").getAsJsonArray("objects");
                int ordersCount = response.body().getAsJsonObject("result").get("count").getAsInt();

                FinishedOrdersList.getInstance().сlearOrders();

                for (int i=0;i<ordersCount;i++)
                {
                    JsonObject orderJson = ordersArray.get(i).getAsJsonObject();
                    FinishedOrdersList.getInstance().addOrder(OrderFactory.fromJsonObject(orderJson));
                }
            } else {
                switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                    case ApiHelper.INVALID_REQUEST:
                        throw new Exception(getContext().getString(R.string.InvalidRequest));
                    case ApiHelper.NO_TRANSPORT_UNIT: {
                        OrderManager.getInstance().clearOrders();
                        Model.getInstance().clearTransportTable();
                        throw new Exception(getContext().getString(R.string.NoTransportUnit));
                    }
                }
            }
        }
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        this.data = null;
        this.exception = null;
    }

    public Exception getException() {
        return exception;
    }
}

