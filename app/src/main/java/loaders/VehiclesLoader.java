package loaders;

import android.content.Context;
import androidx.loader.content.AsyncTaskLoader;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import features.log.LogWriter;
import model.Model;
import model.Vehicle;
import retrofit2.Response;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Eugene on 03.04.2017.
 */

public class VehiclesLoader extends AsyncTaskLoader<List<Vehicle>> {
    private boolean isActive = false;
    private Exception exception;
    private ApiHelper mAPI;

    public VehiclesLoader(ApiHelper api, Context context) {
        super(context);
        mAPI = api;
    }

    @Override
    public List<Vehicle> loadInBackground() {
        /*JsonObject crewListRequest = Utils.createRPC("Crew.list");*/
        JsonObject request = new JsonObject();

        request.addProperty("limit", 1000);
        request.addProperty("offset", 0);
        List<Vehicle> vehicles = new ArrayList<>();

        try {
            LogWriter.log(this.getClass().getSimpleName()+" : listTransportUnits");
            Response<JsonObject> response = mAPI.getServerAPI().listTransportUnits(Model.getInstance().getAccessToken(), request).execute();
            LogWriter.logServerResponse(response);

            if (response.code() != 200) {
                throw new Exception("response code = " + response.code());
            }

            if (response.body().has("result")) {
                if (response.body().get("result").getAsJsonObject().get("result").getAsBoolean()) {
                    JsonArray vehiclesArray = response.body().getAsJsonObject("result").getAsJsonArray("objects");
                    int vehiclesCount = response.body().getAsJsonObject("result").get("count").getAsInt();

                    for (int i = 0; i < vehiclesCount; i++) {
                        JsonObject vehicleJson = vehiclesArray.get(i).getAsJsonObject();
                        vehicles.add(new Vehicle(vehicleJson));
                    }

                }
            } else {
                throw new Exception(response.body().toString());
            }
        } catch (Exception e) {
            LogWriter.log(e.getMessage());
            if (e.getClass().getName().equals("java.net.UnknownHostException"))
                this.exception = new Exception(getContext().getString(R.string.no_server_access));
            else
                this.exception = e;
        }
        return vehicles;
    }

    @Override
    public void deliverResult(List<Vehicle> data) {
        super.deliverResult(data);
        isActive = false;
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        isActive = true;
    }

    protected void onReset() {
        super.onReset();
        exception = null;
    }

    public Exception getException() {
        return exception;
    }

    public boolean isActive() {
        return isActive;
    }
}
