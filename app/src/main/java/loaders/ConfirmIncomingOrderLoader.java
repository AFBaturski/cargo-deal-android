package loaders;


import activities.ViewOrderActivity;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.loader.content.AsyncTaskLoader;

import api.TaskScheduler;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.macsoftex.cargodeal.driver.R;
import common.EmptyCompletion;
import common.exceptions.*;
import storage.TransportContract;
import di.DependencyContainer;
import features.log.LogWriter;
import features.order.model.Order;
import features.order.OrderFactory;
import fragments.AlertDialogFragment;
import model.DriverStatus;
import model.Model;
import features.order.OrderManager;
import notification.NotificationHelper;
import retrofit2.Response;
import api.ApiHelper;
import api.OnResultListener;
import service.tasks.SetOrderStageTask;

/**
 * Created by Eugene on 28.03.2017.
 */

public class ConfirmIncomingOrderLoader extends AsyncTaskLoader<Boolean> {
    private static final String LOG_TAG = ConfirmIncomingOrderLoader.class.getSimpleName();
    private static final String TAG = "confirm_incoming_order";
    private static final Integer NO_STATUS = -1;
    public static final int ID = 1;

    private Boolean data;
    private int orderNumber;
    private double orderTonnage;
    private String orderComment, token;
    private Exception exception;
    private final ApiHelper api;
    private final Model model;
    private final TaskScheduler scheduler;
    private final NotificationHelper notificationHelper;
    private final DependencyContainer dependencyContext;

    public Exception getException(){
        return exception;
    }

    public ConfirmIncomingOrderLoader(DependencyContainer dependencyContext, Bundle args) {
        super(dependencyContext.getContext());
        this.dependencyContext = dependencyContext;
        this.api = dependencyContext.getApi();
        this.model = dependencyContext.getModel();
        this.scheduler = dependencyContext.getScheduler();
        this.notificationHelper = dependencyContext.getNotificationHelper();
        if(args != null) {
            orderNumber = args.getInt("number");
            orderTonnage = args.getDouble("tonnage");
            orderComment = args.getString("comment");
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null){
            deliverResult(data);
        }
    }

    public double getOrderTonnage() {
        return orderTonnage;
    }

    public void setOrderTonnage(double orderTonnage) {
        this.orderTonnage = orderTonnage;
    }

    @Override
    public Boolean loadInBackground() {
        new SetOrderStageTask(dependencyContext, new EmptyCompletion()).run();
        takeOrderRequest();
        return null;
    }

    private void takeOrderRequest() {
        //Функция попытки взятия заказа
        JsonObject request = new JsonObject();
        double allowedTonnage = Model.getInstance().getSelectedTransport(TransportContract.TYPE_VEHICLE).getTonnage();
        if (Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER) != null)
            allowedTonnage += Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER).getTonnage();

        request.addProperty("orderNumber", orderNumber);
        request.addProperty("tonnage", orderTonnage);

        LogWriter.log(this.getClass().getSimpleName()+" : takeOrder("+request+")");
        try {
            Response<JsonObject> response = api.getServerAPI().takeOrder(Model.getInstance().getAccessToken(), request).execute();
            LogWriter.logServerResponse(response);

            api.handleResponse(response, new OnResultListener() {
                @Override
                public void onSuccessResult(Response<JsonObject> response) throws Exception {
                    handleTakeOrderResponse(response);
                    ChangeStatusLoader.changeStatusOnServer(dependencyContext, DriverStatus.CREW_STATUS_WORK);
                }

                @Override
                public void onFailureResult(Response<JsonObject> response) throws Exception {
                    api.handleResultErrorWithThrowException(response);
                }
            });
        } catch (Exception e) {
            Model.getInstance().setInProgress(false);
            LogWriter.log(e.getMessage());
            exception = ExceptionHelper.checkExceptionForNoInternetConnection(getContext(), e);
        }
    }

    private void handleTakeOrderResponse(Response<JsonObject> response) throws Exception {
        JsonObject order = response.body().getAsJsonObject("result").getAsJsonObject("object");
        JsonObject loadingPoint = order.getAsJsonObject("loadingEntrance");
        JsonObject unloadingPoint = order.getAsJsonObject("unloadingEntrance");

        Model.MyOrderInfo myOrderInfo = Model.MyOrderInfo.fromJson(order);

        JsonArray stages = response.body().getAsJsonObject("result").getAsJsonArray("nextStages");
        if(stages != null) {
            for(int i = 0; i < stages.size(); i++) {
                JsonObject stage = stages.get(i).getAsJsonObject();
                Model.OrderNextStage orderNextStage = new Model.OrderNextStage(
                        stage.get("title").getAsString(),
                        stage.get("index").getAsInt()
                );
                myOrderInfo.stages.add(orderNextStage);
            }
        }
        model.saveMyOrderInfo(myOrderInfo);

        PendingIntent pendingIntent = ViewOrderActivity.newPendingIntent(getContext(), myOrderInfo.getAsOrder());
        notificationHelper.updateNotification(pendingIntent);

        //Если нет такого заказа - добавляем
        Order existingOrder = OrderManager.getInstance().getOrder(orderNumber);
        if (existingOrder == null)
            OrderManager.getInstance().insertOrder(myOrderInfo.getAsOrder());
        else {//если есть - делаем его активным и записываем взятый тоннаж
            Order.Builder<?> orderBuilder = OrderFactory.newBuilder(existingOrder)
                .tonnage(order.get("tonnage").getAsDouble())
                .active(true);
            OrderManager.getInstance().replaceOrder(existingOrder, orderBuilder.build());
        }
    }

    public void handleException(Context context, FragmentManager fragmentManager) {
        if (exception instanceof TonnageLessThanRequestedException) {
            AlertDialogFragment.showAlert(context.getString(R.string.incorrect_tonnage), exception.getMessage(), false, fragmentManager, TAG);
        } else if (exception instanceof OrderTimeoutException) {
            AlertDialogFragment.showAlert("Таймаут", exception.getMessage(), true, fragmentManager, TAG);
        } else if (exception instanceof HasActiveTripException) {
            if (Model.getInstance().isMyOrderStatusNeedToSubmit())
                AlertDialogFragment.showAlert(context.getString(R.string.accept_failed), context.getString(R.string.need_to_complete_transfer_previous_order_status), false, fragmentManager, TAG);
            else
                AlertDialogFragment.showAlert(context.getString(R.string.accept_failed), exception.getMessage(), false, fragmentManager, TAG);
        } else
            AlertDialogFragment.showAlert(context.getString(R.string.accept_failed), exception.getMessage(), false, fragmentManager, TAG);
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        this.data = null;
        this.exception = null;
    }
}
