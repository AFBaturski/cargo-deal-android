package loaders;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.loader.content.AsyncTaskLoader;

import com.google.gson.JsonObject;

import common.exceptions.NoOfferException;
import di.DependencyContainer;
import features.log.LogWriter;
import fragments.AlertDialogFragment;
import model.Model;
import retrofit2.Response;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Eugene on 28.03.2017.
 */

public class CancelIncomingOrderLoader extends AsyncTaskLoader<Boolean> {

    private static final String LOG_TAG = CancelIncomingOrderLoader.class.getSimpleName();
    private static final String TAG = "cancel_incoming_order";
    public static final int ID = 2;
    private final ApiHelper api;

    private Boolean data;
    private Integer orderId;
    private boolean mHideOffer;
    private Exception exception;

    public Exception getException(){
        return exception;
    }

    public CancelIncomingOrderLoader(DependencyContainer dependencyContext, Bundle args) {
        super(dependencyContext.getContext());
        api = dependencyContext.getApi();
        if(args != null) {
            orderId = args.getInt("number");
            mHideOffer = args.containsKey("hideOffer") && args.getBoolean("hideOffer");
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null){
            deliverResult(data);
        }
    }

    @Override
    public Boolean loadInBackground() {
        LogWriter.log(this.getClass().getSimpleName()+" : hideOrder");
        JsonObject request = new JsonObject();
        request.addProperty("orderNumber", orderId);
        try {
            LogWriter.log(this.getClass().getSimpleName()+" : hideOffer/declineOffer");
            Response<JsonObject> response = mHideOffer ? api.getServerAPI().hideOrder(Model.getInstance().getAccessToken(), request).execute()
                    : api.getServerAPI().declineOrder(Model.getInstance().getAccessToken(), request).execute();
            LogWriter.logServerResponse(response);

            if (response.code() != 200) {
                throw new Exception("response code = " + response.code());
            }

            if (response.body().has("result")) {
                if (!response.body().get("result").getAsJsonObject().get("result").getAsBoolean()) {
                    switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                        case ApiHelper.INVALID_REQUEST:
                            throw new Exception(getContext().getString(R.string.InvalidRequest));
                        case ApiHelper.NO_TRANSPORT_UNIT:
                            Model.getInstance().clearTransportTable();
                            throw new Exception(getContext().getString(R.string.NoTransportUnit));
                        case ApiHelper.NO_SUCH_ORDER:
                            throw new Exception(getContext().getString(R.string.NoSuchOrder));
                        case ApiHelper.NO_OFFER:
                            throw new NoOfferException(getContext().getString(R.string.NoOffer));
                    }
                }

            }
        }catch (Exception e){
            LogWriter.log(e.getMessage());
            if (e.getClass().getName().equals("java.net.UnknownHostException"))
                this.exception = new Exception(getContext().getString(R.string.no_server_access));
            else
                this.exception = e;
        }
        return true;
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        this.data = null;
        this.exception = null;
    }

    public void handleException(Context context, FragmentManager fragmentManager) {
        AlertDialogFragment.showAlert(context.getString(R.string.cancel_failed), exception.getMessage(), true, fragmentManager, TAG);
    }
}
