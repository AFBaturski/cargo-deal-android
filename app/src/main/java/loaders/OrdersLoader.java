package loaders;

import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import di.DependencyContainer;
import features.log.LogWriter;
import model.Model;
import features.order.OrderFactory;
import features.order.OrderManager;
import retrofit2.Response;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by barkov00@gmail.com on 31.03.2017.
 */

public class OrdersLoader extends AsyncTaskLoader<Boolean> {
    private final Model model;
    private Exception exception;
    private Boolean data;
    private Long timeInterval;
    private final ApiHelper api;

    public OrdersLoader(DependencyContainer dependencyContext, Bundle args) {
        super(dependencyContext.getContext());
        api = dependencyContext.getApi();
        model = dependencyContext.getModel();
        if(args != null){
            timeInterval = args.getLong("timeInterval");
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null || exception != null){
            deliverResult(data);
        }
    }

    @Override
    public Boolean loadInBackground() {
        if (!model.isTransportUnitAvailable())
            return false;

        try {
            LogWriter.log(this.getClass().getSimpleName()+" : listOrders");
            Response<JsonObject> response = api.getServerAPI().listOrders(Model.getInstance().getAccessToken()).execute();
            LogWriter.logServerResponse(response);
            if(response.code() != 200){
                throw new Exception("response code = " + response.code());
            }

            if(response.body().has("result")){
                if(response.body().get("result").getAsJsonObject().get("result").getAsBoolean())
                {
                    JsonArray ordersArray = response.body().getAsJsonObject("result").getAsJsonArray("objects");
                    int ordersCount = response.body().getAsJsonObject("result").get("count").getAsInt();

                    OrderManager.getInstance().clearOrders();
                    //Если есть активный заказ, то добавляем его в список
                    if (Model.getInstance().getMyOrderInfo() != null )
                        OrderManager.getInstance().addOrder(Model.getInstance().getMyOrderInfo().getAsOrder());

                    for (int i=0;i<ordersCount;i++)
                    {
                        JsonObject orderJson = ordersArray.get(i).getAsJsonObject();
                        OrderManager.getInstance().addOrder(OrderFactory.fromJsonObject(orderJson));
                    }
                    return true;
                } else {
                    switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                        case ApiHelper.INVALID_REQUEST:
                            throw new Exception(getContext().getString(R.string.InvalidRequest));
                        case ApiHelper.NO_TRANSPORT_UNIT:
                            Model.getInstance().clearTransportTable();
                            throw new Exception(getContext().getString(R.string.NoTransportUnit));
                    }
                }
            }
        } catch (Exception e) {
            LogWriter.log(e.getMessage());
            if (e.getClass().getName().equals("java.net.UnknownHostException"))
                this.exception = new Exception(getContext().getString(R.string.no_server_access));
            else
                this.exception = e;
        }

        return false;
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        data = null;
        exception = null;
    }

    public Exception getException() {
        return exception;
    }
}
