package loaders;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;

import api.TaskScheduler;
import com.google.gson.JsonObject;

import common.exceptions.NoTransportUnitException;
import common.exceptions.ServerUnavailableException;
import di.DependencyContainer;
import features.log.LogWriter;
import model.DriverStatus;
import notification.NotificationHelper;
import org.greenrobot.eventbus.EventBus;

import model.Model;
import retrofit2.Response;
import api.ApiHelper;
import common.MessageEvent;
import com.macsoftex.cargodeal.driver.R;
import service.tasks.SendCoordsTask;

/**
 * Created by barkov00@gmail.com on 31.03.2017.
 */

public class ChangeStatusLoader extends AsyncTaskLoader<Boolean> {
    private NotificationHelper notificationHelper;
    private Exception exception;
    private Boolean data;
    private DriverStatus statusId;
    private final DependencyContainer dependencyContext;

    public ChangeStatusLoader(DependencyContainer dependencyContext, Bundle args) {
        super(dependencyContext.getContext());
        this.dependencyContext = dependencyContext;
        if(args != null){
            statusId = DriverStatus.values()[args.getInt("StatusID")];
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null || exception != null){
            deliverResult(data);
        }
    }

    @Override
    public Boolean loadInBackground() {
        try {
            LogWriter.log(this.getClass().getSimpleName()+" : setStatus("+statusId+")");
            return changeStatusOnServer(dependencyContext, statusId);
        } catch (Exception e) {
            LogWriter.log(e.getMessage());
            this.exception = (e.getClass().getName().equals("java.net.UnknownHostException"))
                    ? new ServerUnavailableException(getContext().getString(R.string.no_server_access)) : e;
        }
        return false;
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        data = null;
        exception = null;
    }

    public Exception getException() {
        return exception;
    }

    public static boolean changeStatusOnServer(DependencyContainer dependencyContext, DriverStatus driverStatus) throws Exception {
        Context context = dependencyContext.getContext();
        Model model = dependencyContext.getModel();
        ApiHelper api = dependencyContext.getApi();
        NotificationHelper notificationHelper = dependencyContext.getNotificationHelper();
        TaskScheduler scheduler = dependencyContext.getScheduler();

        JsonObject request = new JsonObject();
        request.addProperty("status", driverStatus.value());

        Response<JsonObject> response = api.getServerAPI().setStatus(Model.getInstance().getAccessToken(), request).execute();
        LogWriter.logServerResponse(response);

        if(response.code() != 200){
            throw new Exception("response code = " + response.code());
        }

        if(response.body().has("result")){
            if(response.body().get("result").getAsJsonObject().get("result").getAsBoolean())
            {
                model.saveCrewStatus(driverStatus);
                EventBus.getDefault().post(new MessageEvent(null, MessageEvent.NEED_UPDATE_STATUS, null));
                model.saveCrewStatusUpdatedOnServer();
                scheduler.execute(new SendCoordsTask(dependencyContext));
                notificationHelper.updateNotification(notificationHelper.getForegroundNotificationText());
                return true;
            } else {
                switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                    case ApiHelper.INVALID_REQUEST:
                        throw new Exception(context.getString(R.string.InvalidRequest));
                    case ApiHelper.NO_TRANSPORT_UNIT:
                        model.clearTransportTable();
                        throw new NoTransportUnitException(context.getString(R.string.NoTransportUnit));
                }
            }
        }
        return false;
    }
}
