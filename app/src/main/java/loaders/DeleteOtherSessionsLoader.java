package loaders;

import android.content.Context;
import androidx.loader.content.AsyncTaskLoader;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import features.log.LogWriter;
import model.Model;
import retrofit2.Response;
import api.ApiHelper;
import common.CargoDealSystem;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by barkov00@gmail.com on 31.03.2017.
 */

public class DeleteOtherSessionsLoader extends AsyncTaskLoader<Boolean> {
    private Exception exception;
    private Boolean data;
    private ApiHelper mAPI;

    public DeleteOtherSessionsLoader(ApiHelper api, Context context) {
        super(context);
        mAPI = api;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(data != null || exception != null){
            deliverResult(data);
        }
    }

    @Override
    public Boolean loadInBackground() {

        try {
            Response<JsonObject> response;
            Model.UserInfo info = Model.getInstance().getUserInfo();
            String currentSessionId = (info != null) ? info.session_id : "";
            JsonArray userSessions = CargoDealSystem.getInstance().getUserSessions();

            for (int i = 0; i < userSessions.size(); i++) {
                JsonObject userSession = userSessions.get(i).getAsJsonObject();
                String objectId = userSession.has("objectId") ? userSession.get("objectId").getAsString(): "";
                String userSessionId = userSession.has("sessionToken") ? userSession.get("sessionToken").getAsString(): "";
                if (userSessionId.equals(currentSessionId))
                    continue;

                LogWriter.log(this.getClass().getSimpleName()+" : deleteOtherSessions");
                response = mAPI.getServerAPI().deleteOtherSessions(currentSessionId, objectId).execute();
                LogWriter.logServerResponse(response);
                if(!response.isSuccessful()){
                    throw new Exception("response code = " + response.code());
                }
            }
            return true;
        } catch (Exception e) {
            LogWriter.log(e.getMessage());
            if (e.getClass().getName().equals("java.net.UnknownHostException"))
                this.exception = new Exception(getContext().getString(R.string.no_server_access));
            else
                this.exception = e;
        }
        return false;
    }

    @Override
    public void deliverResult(Boolean data) {
        super.deliverResult(data);
        this.data = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        data = null;
        exception = null;
    }

    public Exception getException() {
        return exception;
    }
}
