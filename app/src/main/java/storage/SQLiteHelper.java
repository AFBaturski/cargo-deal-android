package storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import model.MyOrderStagePhoto;

public class SQLiteHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 58;
    private static final String DB_NAME = "truck_tracker_db";

    private String CREATE_USER_INFO_TABLE = "CREATE TABLE " + UserInfoContract.UserInfoEntry.TABLE_NAME + " (" +
            UserInfoContract.UserInfoEntry.COLUMN__ID + " INTEGER PRIMARY KEY," +
            UserInfoContract.UserInfoEntry.COLUMN_USER_ID + " TEXT," +
            UserInfoContract.UserInfoEntry.COLUMN_EXPIRE + " INTEGER," +
            UserInfoContract.UserInfoEntry.COLUMN_FIO + " TEXT," +
            UserInfoContract.UserInfoEntry.COLUMN_USERNAME + " TEXT," +
            UserInfoContract.UserInfoEntry.COLUMN_TOKEN + " TEXT," +
            UserInfoContract.UserInfoEntry.COLUMN_PASSW + " TEXT," +
            UserInfoContract.UserInfoEntry.COLUMN_LAT + " REAL," +
            UserInfoContract.UserInfoEntry.COLUMN_LON + " REAL" +" )";

    private String DROP_USERINFO_TABLE = "DROP TABLE IF EXISTS " + UserInfoContract.UserInfoEntry.TABLE_NAME;

    private String CREATE_TRUCKS_TABLE = "CREATE TABLE " + TransportContract.TransportEntry.TABLE_NAME + " (" +
            TransportContract.TransportEntry.COLUMN_ID + " INTEGER PRIMARY KEY," +
            TransportContract.TransportEntry.COLUMN_GOS_NOMER + " TEXT," +
            TransportContract.TransportEntry.COLUMN_GOS_NOMER_URL + " TEXT," +
            TransportContract.TransportEntry.COLUMN_TRUCK_ID + " INTEGER," +
            TransportContract.TransportEntry.COLUMN_TYPE + " TEXT," +
            TransportContract.TransportEntry.COLUMN_TONNAGE + " REAL, " +
            TransportContract.TransportEntry.COLUMN_INSPECTION_END_DATE + " TEXT," +
            TransportContract.TransportEntry.COLUMN_MTPL_END_DATE + " TEXT)";

    private String DROP_TRUCKS_TABLE = "DROP TABLE IF EXISTS " + TransportContract.TransportEntry.TABLE_NAME;

    private String CREATE_USER_POS_TABLE = "CREATE TABLE " + UserPositionContract.Entry.COLUMN_TABLE + " (" +
            UserPositionContract.Entry.COLUMN_ID + " INTEGER PRIMARY KEY," +
            UserPositionContract.Entry.COLUMN_LAT + " REAL," +
            UserPositionContract.Entry.COLUMN_LON + " REAL," +
            UserPositionContract.Entry.COLUMN_SPEED + " REAL," +
            UserPositionContract.Entry.COLUMN_TIME + " INTEGER )";

    private String DROP_USER_POS_TABLE = "DROP TABLE IF EXISTS " + UserPositionContract.Entry.COLUMN_TABLE;

    private String CREATE_MY_ORDER_STATUS_TABLE = "CREATE TABLE " + OrderStageContract.Entry.TABLE_NAME + " (" +
        OrderStageContract.Entry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            OrderStageContract.Entry.stageId + " INTEGER," +
            OrderStageContract.Entry.date + " BIGINT," +
            OrderStageContract.Entry.comment + " TEXT," +
            OrderStageContract.Entry.tonnage + " REAL," +
            OrderStageContract.Entry.photo + " TEXT," +
            OrderStageContract.Entry.latitude + " REAL," +
            OrderStageContract.Entry.longitude + " REAL," +
            OrderStageContract.Entry.mileage + " INTEGER," +
            OrderStageContract.Entry.waybillNumber + " TEXT," +
            OrderStageContract.Entry.orderType + " TEXT" +
            ")";

    private String DROP_MY_ORDER_STATUS_TABLE = "DROP TABLE IF EXISTS " + OrderStageContract.Entry.TABLE_NAME;

    private String CREATE_MY_ORDER_TABLE = "CREATE TABLE " + MyOrderContract.TABLE_NAME + " (" +
            MyOrderContract.MyOrderEntry.COLUMN_ID + " INTEGER PRIMARY KEY," +
            MyOrderContract.MyOrderEntry.createdAt + " BIGINT," +
            MyOrderContract.MyOrderEntry.tariff + " REAL," +
            MyOrderContract.MyOrderEntry.unloadingEntranceAddress + " TEXT," +
            MyOrderContract.MyOrderEntry.unloadingContactName + " TEXT," +
            MyOrderContract.MyOrderEntry.unloadingContactPhone + " TEXT," +
            MyOrderContract.MyOrderEntry.unloadingEntranceLatitude + " REAL," +
            MyOrderContract.MyOrderEntry.unloadingEntranceLongitude + " REAL," +
            MyOrderContract.MyOrderEntry.unloadingDate + " BIGINT," +
            MyOrderContract.MyOrderEntry.unloadingBeginDate + " BIGINT," +
            MyOrderContract.MyOrderEntry.unloadingEndDate + " BIGINT," +
            MyOrderContract.MyOrderEntry.loadingEntranceAddress + " TEXT," +
            MyOrderContract.MyOrderEntry.loadingContactName + " TEXT," +
            MyOrderContract.MyOrderEntry.loadingContactPhone + " TEXT," +
            MyOrderContract.MyOrderEntry.loadingEntranceLatitude + " REAL," +
            MyOrderContract.MyOrderEntry.loadingEntranceLongitude + " REAL," +
            MyOrderContract.MyOrderEntry.loadingDate + " BIGINT," +
            MyOrderContract.MyOrderEntry.form + " TEXT," +
            MyOrderContract.MyOrderEntry.mark + " TEXT," +
            MyOrderContract.MyOrderEntry.type + " TEXT," +
            MyOrderContract.MyOrderEntry.weight + " REAL," +
            MyOrderContract.MyOrderEntry.undistributedWeight + " REAL," +
            MyOrderContract.MyOrderEntry.orderID + " INTEGER," +
            MyOrderContract.MyOrderEntry.comment + " TEXT," +
            MyOrderContract.MyOrderEntry.supplier + " TEXT," +
            MyOrderContract.MyOrderEntry.customer + " TEXT," +
            MyOrderContract.MyOrderEntry.objectId + " TEXT," +
            MyOrderContract.MyOrderEntry.orderStatusId + " INTEGER," +
            MyOrderContract.MyOrderEntry.orderStatusTitle + " TEXT," +
            MyOrderContract.MyOrderEntry.orderStatusDateTime + " BIGINT," +
            MyOrderContract.MyOrderEntry.orderType + " TEXT," +
            MyOrderContract.MyOrderEntry.distance + " INTEGER" +
            " )";

    private String DROP_MY_ORDER_TABLE = "DROP TABLE IF EXISTS " + MyOrderContract.TABLE_NAME;

    private String CREATE_MY_ORDER_CHANGES_TABLE = "CREATE TABLE " + MyOrderChangesContract.TABLE_NAME + " (" +
            MyOrderChangesContract.MyOrderChangesEntry.PROPERTY + " INTEGER, " +
            MyOrderChangesContract.MyOrderChangesEntry.VALUE + " TEXT, " +
            MyOrderChangesContract.MyOrderChangesEntry.AUTHOR_ID + " TEXT, " +
            MyOrderChangesContract.MyOrderChangesEntry.ORDER_NUMBER + " INTEGER" + " )";

    private String DROP_MY_ORDER_CHANGES_TABLE = "DROP TABLE IF EXISTS " + MyOrderChangesContract.TABLE_NAME;

    private String CREATE_MY_ORDER_STAGES_TABLE = "CREATE TABLE " + MyOrderStagesContract.Entry.TABLE_NAME + " (" +
            MyOrderStagesContract.Entry.ID + " INTEGER PRIMARY KEY," +
            MyOrderStagesContract.Entry.caption + " TEXT," +
            MyOrderStagesContract.Entry.stage_id + " INTEGER" + " )";

    private String DROP_MY_ORDER_STAGES_TABLE = "DROP TABLE IF EXISTS " + MyOrderStagesContract.Entry.TABLE_NAME;

    private String CREATE_MY_ORDER_STAGE_PHOTO_TABLE = "CREATE TABLE " + MyOrderStagePhoto.Entry.TABLE_NAME + " (" +
            MyOrderStagePhoto.Entry.STAGE_INDEX + " INTEGER," +
            MyOrderStagePhoto.Entry.IMAGE_NAME + " TEXT," +
            MyOrderStagePhoto.Entry.TRIP_ID + " TEXT)";

    private String DROP_MY_ORDER_STAGE_PHOTO_TABLE = "DROP TABLE IF EXISTS " + MyOrderStagePhoto.Entry.TABLE_NAME;

    private String CREATE_CREW_STATUS_TABLE = "CREATE TABLE " + CrewStatusContract.TABLE_NAME + " (" +
            CrewStatusContract.Entry.ID + " INTEGER PRIMARY KEY," +
            CrewStatusContract.Entry.STATUS_ID + " INTEGER DEFAULT 1," +
            CrewStatusContract.Entry.SERVER_UPDATED + " INTEGER DEFAULT 0" + " )";

    private static final String DROP_CREW_STATUS_TABLE = "DROP TABLE IF EXISTS " + CrewStatusContract.TABLE_NAME;

    private static SQLiteOpenHelper helper = null;
    public static SQLiteOpenHelper instance(){
        return helper;
    }

    public static void createInstance(Context context){
        helper = new SQLiteHelper(context.getApplicationContext());
    }

    private SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_USER_INFO_TABLE);
        sqLiteDatabase.execSQL(CREATE_TRUCKS_TABLE);
        sqLiteDatabase.execSQL(CREATE_USER_POS_TABLE);
        sqLiteDatabase.execSQL(CREATE_MY_ORDER_TABLE);
        sqLiteDatabase.execSQL(CREATE_MY_ORDER_CHANGES_TABLE);
        sqLiteDatabase.execSQL(CREATE_MY_ORDER_STAGES_TABLE);
        sqLiteDatabase.execSQL(CREATE_MY_ORDER_STAGE_PHOTO_TABLE);
        sqLiteDatabase.execSQL(CREATE_CREW_STATUS_TABLE);
        sqLiteDatabase.execSQL(CREATE_MY_ORDER_STATUS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_USERINFO_TABLE);
        sqLiteDatabase.execSQL(DROP_TRUCKS_TABLE);
        sqLiteDatabase.execSQL(DROP_USER_POS_TABLE);
        sqLiteDatabase.execSQL(DROP_MY_ORDER_TABLE);
        sqLiteDatabase.execSQL(DROP_MY_ORDER_CHANGES_TABLE);
        sqLiteDatabase.execSQL(DROP_MY_ORDER_STAGES_TABLE);
        sqLiteDatabase.execSQL(DROP_MY_ORDER_STAGE_PHOTO_TABLE);
        sqLiteDatabase.execSQL(DROP_CREW_STATUS_TABLE);
        sqLiteDatabase.execSQL(DROP_MY_ORDER_STATUS_TABLE);
        onCreate(sqLiteDatabase);
    }
}
