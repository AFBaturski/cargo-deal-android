package storage;

/**
 * Created by barkov00@gmail.com on 29.03.2017.
 */

public class MyOrderStagesContract {
    public static abstract class Entry {
        public static final String TABLE_NAME = "my_order_stages_table";
        public static final String ID = "id";
        public static final String caption = "caption";
        public static final String stage_id = "stage_id";
    }
}
