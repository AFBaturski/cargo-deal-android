package storage;

public class MyOrderContract {
    public static final String TABLE_NAME = "order_status_table";

    public abstract static class MyOrderEntry {
        public static final String COLUMN_ID = "id";
        public static final String unloadingEntranceAddress = "unloadingPointAddress";
        public static final String unloadingContactName = "unloadingPointContact";
        public static final String unloadingContactPhone = "unloadingPointContactPhone";
        public static final String unloadingEntranceLatitude = "unloadingPointLatitude";
        public static final String unloadingEntranceLongitude = "unloadingPointLongitude";
        public static final String unloadingDate = "unloadingPointDate";
        public static final String loadingEntranceAddress = "loadingPointAddress";
        public static final String loadingContactName = "loadingPointContact";
        public static final String loadingContactPhone = "loadingContactPhone";
        public static final String loadingEntranceLatitude = "loadingPointLatitude";
        public static final String loadingEntranceLongitude = "loadingPointLongitude";
        public static final String loadingDate = "loadingPointDate";
        public static final String form = "articleShape";
        public static final String mark = "articleBrand";
        public static final String type = "articleType";
        public static final String weight = "tonnage";
        public static final String undistributedWeight = "undistributedTonnage";
        public static final String orderID = "order_id";
        public static final String comment = "comment";
        public static final String createdAt = "createdAt";
        public static final String tariff = "tariff";
        public static String orderStatusId = "order_status_id";
        public static String orderStatusTitle = "order_status_caption";
        public static String orderStatusDateTime = "order_status_datetime";
        public static final String unloadingBeginDate = "unloadingBeginDate";
        public static final String unloadingEndDate = "unloadingEndDate";
        public static final String supplier = "supplier";
        public static final String customer = "customer";
        public static String objectId = "trip_id";
        public static String orderType = "order_type";
        public static String distance = "distance";
    }
}
