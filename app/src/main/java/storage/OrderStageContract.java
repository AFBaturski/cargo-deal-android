package storage;

/**
 * Created by Alex Baturski on 21.08.2018.
 */

public class OrderStageContract {
  public static abstract class Entry {
    public static final String TABLE_NAME = "my_order_status_table";
    public static final String ID = "id";
    public static final String stageId = "stage";
    public static final String date = "date";
    public static final String comment = "comment";
    public static final String tonnage = "tonnage";
    public static final String photo = "photo";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String mileage = "mileage";
    public static final String waybillNumber = "waybill_number";
    public static final String orderType = "order_type";
  }
}
