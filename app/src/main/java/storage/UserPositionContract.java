package storage;

/**
 * Created by Eugene on 24.02.2017.
 */

public class UserPositionContract {
    public abstract static class Entry {
        public static final String COLUMN_TABLE = "user_position";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_LAT = "lat";
        public static final String COLUMN_LON = "lon";
        public static final String COLUMN_SPEED = "speed";
        public static final String COLUMN_TIME = "time";
    }
}
