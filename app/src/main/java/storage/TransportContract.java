package storage;

/**
 * Created by Eugene on 23.02.2017.
 */

public class TransportContract {
    public static abstract class TransportEntry {
        public static final String TABLE_NAME = "truck_number_entry";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TRUCK_ID = "truck_id";
        public static final String COLUMN_GOS_NOMER = "gos_nomer";
        public static final String COLUMN_GOS_NOMER_URL = "gos_nomer_url";
        public static final String COLUMN_TYPE = "type"; //0 - head, 1 - tail
        public static final String COLUMN_TONNAGE = "tonnage";
        public static final String COLUMN_INSPECTION_END_DATE = "inspection_end_date";
        public static final String COLUMN_MTPL_END_DATE = "mtpl_end_date";
    }
    public static final String TYPE_VEHICLE = "venicle";
    public static final String TYPE_TRAILER = "trailer";
}
