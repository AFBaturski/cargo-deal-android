package storage;

/**
 * Created by Eugene on 23.02.2017.
 */

public class UserInfoContract {
    public static abstract class UserInfoEntry {

        // To prevent someone from accidentally instantiating the contract class,
        // give it an empty constructor.
        public UserInfoEntry() {}

        public static final String TABLE_NAME = "user_info";
        public static final String COLUMN__ID = "id";
        public static final String COLUMN_FIO = "fio";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_EXPIRE = "expire";
        public static final String COLUMN_TOKEN = "session_id";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_PASSW = "passw";
        public static final String COLUMN_LAT = "latitude";
        public static final String COLUMN_LON = "longitude";
    }
}
