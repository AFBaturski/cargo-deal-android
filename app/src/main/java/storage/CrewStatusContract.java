package storage;

/**
 * Created by barkov00@gmail.com on 31.03.2017.
 */

public class CrewStatusContract {
    public static final String TABLE_NAME = "crew_status_table";
    public abstract static class Entry {
        public static final String ID = "id";
        public static final String STATUS_ID = "status_id";
        public static final String SERVER_UPDATED = "server_updated";
    }
}
