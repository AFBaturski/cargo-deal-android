package storage;

public class MyOrderChangesContract {
    public static final String TABLE_NAME = "order_changes_table";

    public abstract static class MyOrderChangesEntry {
        public static final String PROPERTY = "orderProperty";
        public static final String VALUE = "orderPropertyValue";
        public static final String AUTHOR_ID = "orderChangesAuthorId";
        public static final String ORDER_NUMBER = "orderNumber";
    }
}
