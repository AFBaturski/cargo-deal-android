package api.requests;

import android.content.Context;
import api.ApiHelper;
import com.google.gson.JsonObject;
import common.*;
import features.log.LogWriter;
import model.Model;
import model.MyOrderStagePhoto;
import retrofit2.Response;
import java.io.File;

public class OrderStagePhotoRequest {
  private final ApiHelper api;
  private final OnResult callback;
  private final Model model;
  private final Context context;

  public OrderStagePhotoRequest(Context context, ApiHelper api, Model model, OnResult callback) {
    this.callback = callback;
    this.api = api;
    this.model = model;
    this.context = context;
  }

  public void submit() {
    MyOrderStagePhoto stagePhoto = model.getMyOrderStagePhoto();
    if (stagePhoto == null || model.isMyOrderStatusNeedToSubmit()) {
      callback.onResult(Result.failure());
      return;
    }

    String imageName = stagePhoto.getImageName();
    File imageFile = new File(context.getFilesDir(), imageName);
    if (!imageFile.exists()) {
      model.deleteMyOrderStagePhoto(stagePhoto);
      callback.onResult(Result.success());
      return;
    }

    File compressedFile = Utils.compressImage(context, imageName);
    if (compressedFile == null) {
      callback.onResult(Result.failure());
      return;
    }
    LogWriter.log(this.getClass().getSimpleName() + " begin convert to base64 string");
    String base64Photo = Utils.getFileAsBase64(context, compressedFile);
    LogWriter.log(this.getClass().getSimpleName() + " end convert to base64 string");

    if (base64Photo == null) {
      LogWriter.log(this.getClass().getSimpleName() + "base64Photo is null, aborting. imageName: "+imageName);
      model.deleteMyOrderStagePhoto(stagePhoto);
      callback.onResult(Result.failure());
      return;
    }

    boolean success = setStagePhoto(stagePhoto, base64Photo);
    imageFile.delete();
    compressedFile.delete();
    if (success) {
      callback.onResult(Result.success());
    } else {
      callback.onResult(Result.failure());
    }
  }

  private boolean setStagePhoto(MyOrderStagePhoto stagePhoto, String base64Photo) {
    boolean result = false;
    if (base64Photo == null || stagePhoto.getTripId() == null)
      return true;

    String stageId = stagePhoto.getTripId();
    JsonObject request = new JsonObject();
    request.addProperty("tripId", stagePhoto.getTripId());
    request.addProperty("stage", stagePhoto.getStageIndex());
    request.addProperty("photo", base64Photo);

    try {
      LogWriter.log(this.getClass().getSimpleName() + " : setStagePhoto");
      Response<JsonObject> response = api.getServerAPI().setStagePhoto(Model.getInstance().getAccessToken(), request).execute();
      LogWriter.logServerResponse(response);

      if (response.code() == 200 && response.body().has("result") && response.body().getAsJsonObject("result").get("result").getAsBoolean()) {
        result = true;
        Model.getInstance().deleteMyOrderStagePhoto(stagePhoto);
      } else if (response.body().has("result") && response.body().getAsJsonObject("result").has("error")) {
        switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
          case ApiHelper.OBJECT_NOT_FOUND:
            model.deleteMyOrderStagePhoto(stagePhoto);
            break;
          case ApiHelper.FORBIDDEN:
            MessageEvent.postNeedReloginEvent();
            break;
          case ApiHelper.INVALID_REQUEST:
            MessageEvent.postInvalidRequestEvent();
            break;
        }
      }
    } catch (Exception e) {
      result = false;
      LogWriter.log(e.getMessage());
    }
    return result;
  }
}
