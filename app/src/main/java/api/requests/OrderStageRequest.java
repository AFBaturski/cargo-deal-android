package api.requests;

import android.content.Context;
import api.ApiHelper;
import api.TaskScheduler;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import common.*;
import common.exceptions.*;
import di.DependencyContainer;
import features.log.LogWriter;
import features.order.OrderManager;
import features.order.stage.model.OrderStage;
import features.common.serialize.OrderStageSerializer;
import model.*;
import org.greenrobot.eventbus.EventBus;

import loaders.ChangeStatusLoader;
import retrofit2.Response;
import service.tasks.SetOrderStagePhotoTask;

public class OrderStageRequest {
  public class Task implements Runnable{
    @Override
    public void run() {
      OrderStageRequest.this.submit();
    }
  }
  private final ApiHelper api;
  private final OnResult callback;
  private final Context context;
  private final TaskScheduler scheduler;
  private final Model model;
  private final DependencyContainer dependencyContext;
  private final OrderStage orderStage;

  public OrderStageRequest(DependencyContainer dependencyContext, OrderStage orderStage, OnResult callback) {
    this.dependencyContext = dependencyContext;
    this.orderStage = orderStage;
    this.callback = callback;
    this.api = dependencyContext.getApi();
    this.context = dependencyContext.getContext();
    this.scheduler = dependencyContext.getScheduler();
    this.model = dependencyContext.getModel();
  }

  public void submit() {
    if (orderStage == null) {
      callback.onResult(Result.failure(new IllegalArgumentException()));
      return;
    }
    Result result = setStage(orderStage);
    callback.onResult(result);
  }

  private Result setStage(OrderStage orderStage) {
    Result result = Result.failure();
    int stageIndex = orderStage.getStage();
    JsonObject request = OrderStageSerializer.serialize(orderStage).getAsJsonObject();

    LogWriter.log(this.getClass().getSimpleName() + " : setStage(" + request + ")");
    try {
      Response<JsonObject> response = api.getServerAPI().setStage(Model.getInstance().getAccessToken(), request).execute();
      LogWriter.logServerResponse(response);

      if (response.code() == 200 && response.body().has("result") && response.body().getAsJsonObject("result").get("result").getAsBoolean()) {
        result = Result.success();
        model.deleteMyOrderStage(stageIndex);
        if (Model.isStageWithPhoto(orderStage.getStage())) {
          scheduler.execute(new SetOrderStagePhotoTask(dependencyContext));
        }

        JsonArray nextStages = response.body().getAsJsonObject("result").get("nextStages").getAsJsonArray();
        if (nextStages.size() != 0) {
          Model.getInstance().deleteOrderNextStages();
          for (int i = 0; i < nextStages.size(); i++) {
            JsonObject stageObject = nextStages.get(i).getAsJsonObject();
            OrderStatus orderStatus = new OrderStatus(stageObject.get("index").getAsInt(), orderStage.getStage(), stageObject.get("title").getAsString());
            Model.getInstance().insertOrderNextStage(new Model.OrderNextStage(orderStatus.caption, orderStatus.id));
          }
        } else {
          if (orderStage.getStage() == Model.ORDER_STAGE_FINAL) { //5 - финальный статус
            Model.getInstance().deleteCurrentOrder(); //удаляем инфу о текущем заказе
            OrderManager.getInstance().removeOrder(OrderManager.getInstance().getActiveOrder());
            ChangeStatusLoader.changeStatusOnServer(dependencyContext, DriverStatus.CREW_STATUS_READY);
            EventBus.getDefault().post(new MessageEvent(null, MessageEvent.ORDER_COMPLETE, null));
          }
        }
      } else if (response.body().has("result") && response.body().getAsJsonObject("result").has("error")) {
        switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
          case ApiHelper.INVALID_REQUEST:
            result = Result.failure(new InvalidRequestException(context));
            break;
          case ApiHelper.NO_ACTIVE_TRIP:
            Model.getInstance().deleteMyOrderStage();
            result = Result.failure(new NoActiveTripException(context));
            break;
          case ApiHelper.INTERNAL_ERROR:
            result = Result.failure(new InternalErrorException(context));
            break;
          case ApiHelper.NO_TRANSPORT_UNIT:
            Model.getInstance().deleteMyOrderStage();
            Model.getInstance().clearTransportTable();
            result = Result.failure(new NoTransportUnitException(context));
            break;
          case ApiHelper.INVALID_TRIP_STAGE:
            if (response.body().getAsJsonObject("result").has("errorData")
                    && response.body().getAsJsonObject("result").get("errorData").getAsJsonObject().has("currentStage")) {
              int stage = response.body().getAsJsonObject("result").get("errorData").getAsJsonObject().get("currentStage").getAsInt();
              if (Model.isStageWithPhoto(stage)) {
                scheduler.execute(new SetOrderStagePhotoTask(dependencyContext));
              }

            }
            model.deleteMyOrderStage(stageIndex);
            result = Result.failure(new InvalidTripStageException(context));
            break;
        }
      }
    } catch (Exception e) {
      result = Result.failure(e);
      LogWriter.log(e.getMessage());
    }
    return result;
  }
}
