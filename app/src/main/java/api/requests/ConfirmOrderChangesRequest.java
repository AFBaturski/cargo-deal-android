package api.requests;

import android.content.Context;
import api.ApiHelper;
import com.google.gson.JsonObject;
import com.macsoftex.cargodeal.driver.R;
import common.*;
import common.exceptions.NoActiveTripException;
import common.exceptions.NoOfferException;
import common.exceptions.NoTransportUnitException;
import common.exceptions.ObjectNotFoundException;
import di.CargodealApp;
import di.DependencyContainer;
import features.log.LogWriter;
import model.Model;
import retrofit2.Response;

public class ConfirmOrderChangesRequest {
  public class Task implements Runnable {
    public static final long CONFIRM_ORDER_CHANGES_RETRY_PERIOD = 10000;
    @Override
    public void run() {
      ConfirmOrderChangesRequest.this.submit();
    }
  }
  private final ApiHelper api;
  private final OnCompletion callback;
  private final Context context;
  private final int orderNumber;
  private final String authorId;

  public ConfirmOrderChangesRequest(DependencyContainer dependencyContext, int orderNumber, String authorId, OnCompletion callback) {
    this.callback = callback;
    this.orderNumber = orderNumber;
    this.authorId = authorId;
    this.api = dependencyContext.getApi();
    this.context = dependencyContext.getContext();
  }

  public void submit() {
    boolean success = confirmOrderChanges();
    callback.onComplete(success);
  }

  private boolean confirmOrderChanges() {
    boolean result = false;
    JsonObject request = new JsonObject();
    request.addProperty("orderNumber", orderNumber);
    request.addProperty("authorId", authorId);

    try {
      LogWriter.log(this.getClass().getSimpleName() + " : confirmOrderChanges");
      Response<JsonObject> response = api.getServerAPI().confirmOrderChanges(Model.getInstance().getAccessToken(), request).execute();
      LogWriter.logServerResponse(response);

      if (response.isSuccessful() && response.body().has("result") && response.body().getAsJsonObject("result").get("result").getAsBoolean()) {
        result = true;
      } else if (response.body().has("result") && response.body().getAsJsonObject("result").has("error")) {
        switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
          case ApiHelper.OBJECT_NOT_FOUND:
            throw new ObjectNotFoundException(context.getString(R.string.ObjectNotFound));
          case ApiHelper.NO_TRANSPORT_UNIT:
            throw new NoTransportUnitException(context.getString(R.string.NoTransportUnit));
          case ApiHelper.NO_ACTIVE_TRIP:
            throw new NoActiveTripException(context.getString(R.string.NoActiveTrip));
          case ApiHelper.NO_SUCH_ORDER:
            throw new NoActiveTripException(context.getString(R.string.NoSuchOrder));
          case ApiHelper.NO_OFFER:
            throw new NoOfferException(context.getString(R.string.NoOffer));
          case ApiHelper.FORBIDDEN:
            MessageEvent.postNeedReloginEvent();
            break;
          case ApiHelper.INVALID_REQUEST:
            MessageEvent.postInvalidRequestEvent();
            break;
        }
      }
    } catch (Exception e) {
      result = false;
      LogWriter.log(e.getMessage());
    }
    return result;
  }
}
