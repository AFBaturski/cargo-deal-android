package api.requests;

import api.ApiHelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Collection;

import common.OnCompletion;
import model.Model;
import retrofit2.Response;
import features.log.LogWriter;

/**
 * Update by Alex on 29.04.2019.
 */

public class UserPositionRequest {
    private final OnCompletion callback;
    private final ApiHelper api;

    public UserPositionRequest(ApiHelper api, OnCompletion callback){
        this.api = api;
        this.callback = callback;
    }

    public synchronized void submit(Collection<Model.UserPosition> positions) {
        try {
            Response<JsonObject> response = api.getServerAPI().addWaypoints(api.getAccessToken(), createRequest(positions)).execute();
            LogWriter.logServerResponse(response);

            if (api.isSuccessServerResponse(response)) {
                callback.onComplete(true);
            } else {
                api.handleResultError(response);
                callback.onComplete(false);
            }
        } catch (Exception e){
            LogWriter.log("SendUserPositionTask Exception: "+e.getMessage());
            callback.onComplete(false);
        }
    }

    private JsonObject createRequest(Collection<Model.UserPosition> positions) {
        JsonObject result = new JsonObject();
        JsonArray waypoints = new JsonArray();

        for(Model.UserPosition position : positions)
            waypoints.add(position.serialize());

        result.add("waypoints", waypoints);
        return result;
    }
}