package api;


import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Eugene on 22.02.2017.
 */

public interface ServerApi {
    @POST("/")
    Call<JsonObject> requestStr(@Body RequestBody request);

    @POST("/")
    Call<JsonObject> request(@Body JsonObject request);

    @GET("login")
    Call<JsonObject> parseLogin(@QueryMap Map<String, String> loginDetails);

    @GET("config")
    Call<JsonObject> fetchConfig(@Header("X-Parse-Session-Token") String session_token);

    @GET("classes/Driver/{objectId}")
    Call<JsonObject> getDriver(@Header("X-Parse-Session-Token") String session_token, @Path("objectId") String objectId);

    @DELETE("sessions/{objectId}")
    Call<JsonObject> deleteOtherSessions(@Header("X-Parse-Session-Token") String session_token, @Path("objectId") String objectId);

    @GET("sessions")
    Call<JsonObject> getSessions(@Header("X-Parse-Session-Token") String session_token);

    @POST("logout")
    Call<JsonObject> parseLogout(@Header("X-Parse-Session-Token") String session_token);

    @Headers("Content-Type: application/json")
    @POST("installations")
    Call<JsonObject> createInstallation(@Body JsonObject request);

    @Headers("Content-Type: application/json")
    @POST("functions/Driver_listVehicles")
    Call<JsonObject> listVehicles(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @Headers("Content-Type: application/json")
    @POST("functions/Driver_listTrailers")
    Call<JsonObject> listTrailers(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_makeTransportUnit")
    Call<JsonObject> makeTransportUnit(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("classes/VehicleType")
    Call<JsonObject> createVehicle(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_disbandTransportUnit")
    Call<JsonObject> disbandTransportUnit(@Header("X-Parse-Session-Token") String session_token);

    @POST("functions/Driver_setTransportUnitStatus")
    Call<JsonObject> setStatus(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_addWaypoints")
    Call<JsonObject> addWaypoints(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_orderReceived")
    Call<JsonObject> orderReceived(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_confirmOrderChanges")
    Call<JsonObject> confirmOrderChanges(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_takeOrder")
    Call<JsonObject> takeOrder(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_checkTransportUnit")
    Call<JsonObject> checkTransportUnit(@Header("X-Parse-Session-Token") String session_token);

    @POST("functions/Driver_setTripStage")
    Call<JsonObject> setStage(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_setTripStagePhoto")
    Call<JsonObject> setStagePhoto(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_declineOrder")
    Call<JsonObject> declineOrder(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_hideOffer")
    Call<JsonObject> hideOrder(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_listOrders")
    Call<JsonObject> listOrders(@Header("X-Parse-Session-Token") String session_token);

    @POST("functions/Driver_listTransportUnits")
    Call<JsonObject> listTransportUnits(@Header("X-Parse-Session-Token") String session_token, @Body JsonObject request);

    @POST("functions/Driver_listFinishedTrips")
    Call<JsonObject> listFinishedOrders(@Header("X-Parse-Session-Token") String session_token);
}
