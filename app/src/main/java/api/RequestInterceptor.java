package api;



import com.google.gson.JsonParser;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import common.MessageEvent;


/**
 * Created by Eugene on 22.02.2017.
 */

public class RequestInterceptor implements Interceptor {

    public static final String TAG = RequestInterceptor.class.getSimpleName();
    private final ApiHelper mAPI;
    private JsonParser parser = new JsonParser();

    RequestInterceptor(ApiHelper api){
        mAPI = api;
    }

    private Request appendTokenToRequest(Request request){
        Request.Builder builder = request.newBuilder();
        builder.removeHeader("Authorization");
        builder.removeHeader("User-Agent");

        builder.addHeader("X-Parse-Application-Id", mAPI.getServerConfiguration().getAppId());
        builder.addHeader("X-Parse-Client-Key", mAPI.getServerConfiguration().getClientKey());
        builder.addHeader("X-Parse-Revocable-Session", "1");
        builder.addHeader("Content-Type", "application/json");
        request = builder.build();
        return request;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        request = appendTokenToRequest(request);
        Response response = chain.proceed(request); //выполняем запрос с токеном
        String bodyString = response.body().string();

        if (!response.isSuccessful() && checkResponseForAuthErrors(bodyString)) {
            MessageEvent.postNeedReloginEvent();
        }

        Response newResponse =  response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), bodyString))
                .build();
        return newResponse;
    }

    private boolean checkResponseForAuthErrors(String bodyString) {
        return bodyString.toLowerCase().contains(ApiHelper.INVALID_SESSION) ||
                bodyString.contains(ApiHelper.FORBIDDEN) ||
                bodyString.contains(ApiHelper.NO_DRIVER);
    }
}
