package api;

import com.macsoftex.cargodeal.driver.BuildConfig;

/**
 * Created by Alex Baturski on 15.08.2019.
 * Macsoftex
 */
class ServerConfiguration {
    private final String mBaseUrl;
    private final String mAppId;
    private final String mClientKey;

    public static ServerConfiguration defaultConfiguration () {
        return new ServerConfiguration();
    }

    private ServerConfiguration() {
        mBaseUrl = generateBaseUrl();
        mAppId = generateAppId();
        mClientKey = generateClientKey();
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }

    public String getAppId() {
        return mAppId;
    }

    public String getClientKey() {
        return mClientKey;
    }

    private static String generateBaseUrl() {
        if (BuildConfig.DEBUG)
            return "https://198307.simplecloud.ru/cargodeal-dev/";
        else
            return "https://dash.cargodeal.ru/cargodeal/";
    }


    private static String generateAppId() {
        if (BuildConfig.DEBUG)
            return "X3nvnkeAubR8L23s8BpzWELQ9uCRAXw0HfHxG9uU";
        else
            return "gtn7enhk9goEASSL7eWqq8VaUGFLg2dE9WUBRIqR";
    }

    private static String generateClientKey() {
        if (BuildConfig.DEBUG)
            return "u2ylnpgBkJ52a6q7Ge4VyDJ9OFbUp53luKnzs0NF";
        else
            return "D67YFaFbzGWoYo3LqiBEkNHuTDezr7XktWRFjGYu";
    }

}
