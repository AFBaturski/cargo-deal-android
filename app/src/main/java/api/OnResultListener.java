package api;

import com.google.gson.JsonObject;
import retrofit2.Response;

public interface OnResultListener {
    void onSuccessResult(Response<JsonObject> response) throws Exception;
    void onFailureResult(Response<JsonObject> response) throws Exception;
}
