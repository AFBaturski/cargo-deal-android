package api;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import com.google.gson.JsonObject;

import com.macsoftex.cargodeal.driver.BuildConfig;
import common.CargoDealSystem;
import common.OnCompletion;
import common.exceptions.*;
import common.MessageEvent;
import features.log.LogWriter;
import features.order.model.OrderType;
import model.Model;
import features.order.model.Order;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.macsoftex.cargodeal.driver.R;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ApiHelper {
    public enum ApiError {
        INVALID_REQUEST("InvalidRequest", R.string.InvalidRequest),
        CARRIER_MISMATCH("CarrierMismatch", R.string.CarrierMismatch),
        OBJECT_NOT_FOUND("ObjectNotFound", R.string.ObjectNotFound),
        VEHICLE_ALREADY_IN_USE("VehicleAlreadyInUse", R.string.VehicleAlreadyInUse),
        TRAILER_ALREADY_IN_USE("TrailerAlreadyInUse", R.string.TrailerAlreadyInUse),
        NO_TRANSPORT_UNIT("NoTransportUnit", R.string.NoTransportUnit),
        NO_SUCH_ORDER("NoSuchOrder", R.string.NoSuchOrder),
        HAS_ACTIVE_TRIP("HasActiveTrip", R.string.HasActiveTrip),
        NO_ACTIVE_TRIP("NoActiveTrip", R.string.NoActiveTrip),
        ORDER_ALREADY_DISTRIBUTED("OrderAlreadyDistributed", R.string.OrderAlreadyDistributed),
        NO_OFFER("NoOffer", R.string.NoOffer),
        TONNAGE_LESS_THAN_REQUESTED("TonnageLessThanRequested", R.string.TonnageLessThanRequested),
        ALREADY_HAVE_TRANSPORT_UNIT("AlreadyHasTransportUnit", R.string.AlreadyHaveTransportUnit),
        INTERNAL_ERROR("InternalError", R.string.InternalError),
        ORDER_TIMEOUT("OrderTimeout", R.string.OrderTimeout),
        INVALID_TRIP_STAGE("InvalidTripStage", R.string.InvalidTripStage),
        FORBIDDEN("Forbidden", R.string.forbidden),
        NO_DRIVER("NoDriver", R.string.NoDriver);
        private static final Map<String, ApiError> stringToEnum =
                Stream.of(values()).collect(
                        Collectors.toMap(Object::toString, v -> v)
                );

        private final String errorCode;
        private final @StringRes int resId;
        ApiError(String errorCode, int resId) {
            this.errorCode = errorCode;
            this.resId = resId;
        }

        public int getResId() {
            return resId;
        }

        @NonNull
        @Override
        public String toString() {
            return errorCode;
        }

        public static Optional<ApiError> of(String errorCode) {
            return Optional.ofNullable(stringToEnum.get(errorCode));
        }
    }

    public static final String TAG = ApiHelper.class.getSimpleName();
    private static ApiHelper sInstance;
    private final Model mModel;
    private final ServerConfiguration mServerConfiguration;
    private Retrofit client;
    private ServerApi serverAPI;
    private OkHttpClient httpClient;
    private Context mContext;

    //Коды ошибок возвращаемые API

    public static final String INVALID_REQUEST = "InvalidRequest";
    public static final String CARRIER_MISMATCH = "CarrierMismatch";
    public static final String OBJECT_NOT_FOUND = "ObjectNotFound";
    public static final String VEHICLE_ALREADY_IN_USE = "VehicleAlreadyInUse";
    public static final String TRAILER_ALREADY_IN_USE = "TrailerAlreadyInUse";
    public static final String NO_TRANSPORT_UNIT = "NoTransportUnit";
    public static final String NO_SUCH_ORDER = "NoSuchOrder";
    public static final String HAS_ACTIVE_TRIP = "HasActiveTrip";
    public static final String NO_ACTIVE_TRIP = "NoActiveTrip";
    public static final String ORDER_ALREADY_DISTRIBUTED = "OrderAlreadyDistributed";
    public static final String NO_OFFER = "NoOffer";
    public static final String TONNAGE_LESS_THAN_REQUESTED = "TonnageLessThanRequested";
    public static final String ALREADY_HAVE_TRANSPORT_UNIT = "AlreadyHasTransportUnit";
    public static final String INTERNAL_ERROR = "InternalError";
    public static final String ORDER_TIMEOUT = "OrderTimeout";
    public static final String INVALID_TRIP_STAGE = "InvalidTripStage";
    public static final String FORBIDDEN = "Forbidden";
    static final String NO_DRIVER = "NoDriver";

    public static final int MAX_POSITION_REQUEST_COUNT = 100;
    public static final int NEED_NEXT_POSITION_REQUEST = MAX_POSITION_REQUEST_COUNT;

    //Коды ошибок Parse сервера
    static final String INVALID_SESSION = "invalid session token";

    public static final long ORDERS_LIST_PERIOD = 120*60*60*1000;

    public ApiHelper(Model model, Context applicationContext) {
        mModel = model;
        mContext = applicationContext;
        mServerConfiguration = ServerConfiguration.defaultConfiguration();
    }


    public static ApiHelper createInstance(Model model, Context applicationContext) {
        if(sInstance == null){
            sInstance = new ApiHelper(model, applicationContext);
        }
        return sInstance;
    }

    public String getAccessToken() {
        return mModel.getAccessToken();
    }

    public static ApiHelper getInstance(){
        return sInstance;
    }

    public void handleResultError(Response<JsonObject> response) {
        String resultError = response.body().get("result").getAsJsonObject().get("error").getAsString();
        LogWriter.log(resultError);
        switch (resultError) {
            case ApiHelper.INVALID_REQUEST:
                MessageEvent.postInvalidRequestEvent();
            case ApiHelper.NO_TRANSPORT_UNIT:
                postTransportUnitDisbandedEvent();
                break;
            case ApiHelper.FORBIDDEN:
            case ApiHelper.NO_DRIVER:
                MessageEvent.postNeedReloginEvent();
                break;
        }
    }

    public void handleResponse(Response<JsonObject> response, OnResultListener listener) throws Exception {
        if(response.body().has("result")){
            if(response.body().get("result").getAsJsonObject().get("result").getAsBoolean()){
                listener.onSuccessResult(response);
            } else {
                listener.onFailureResult(response);
            }
        }
    }

    public void handleResultErrorWithThrowException(Response<JsonObject> response) throws Exception {
        if (!response.isSuccessful())
            throw new Exception(response.message());

        if (!response.body().has("result"))
            return;
        if (!response.body().get("result").getAsJsonObject().has("error"))
            return;

        String resultError = response.body().get("result").getAsJsonObject().get("error").getAsString();
        LogWriter.log(resultError);
        switch (resultError) {
            case ApiHelper.HAS_ACTIVE_TRIP:
                throw new HasActiveTripException(mContext.getString(R.string.HasActiveTrip));
            case ApiHelper.ORDER_ALREADY_DISTRIBUTED:
                throw new OrderAlreadyDistributedException(mContext.getString(R.string.OrderAlreadyDistributed));
            case ApiHelper.NO_OFFER:
                throw new NoOfferException(mContext.getString(R.string.NoOffer));
            case ApiHelper.ORDER_TIMEOUT:
                throw new OrderTimeoutException(mContext.getString(R.string.OrderTimeout));
            case ApiHelper.TONNAGE_LESS_THAN_REQUESTED:
                throw new TonnageLessThanRequestedException(mContext.getString(R.string.TonnageLessThanRequested));
            case ApiHelper.INVALID_REQUEST:
                throw new InvalidRequestException(mContext.getString(R.string.InvalidRequest));
            case ApiHelper.NO_TRANSPORT_UNIT:
                Model.getInstance().clearTransportTable();
                throw new NoTransportUnitException(mContext.getString(R.string.NoTransportUnit));
            case ApiHelper.NO_SUCH_ORDER:
                throw new NoSuchOrderException(mContext.getString(R.string.NoSuchOrder));
            default:
                throw new Exception(resultError);
        }
    }

    public ServerConfiguration getServerConfiguration() {
        return mServerConfiguration;
    }

    public boolean isSuccessServerResponse(Response<JsonObject> response) {
        boolean hasResult = response.body().has("result");
        return hasResult && response.body().get("result").getAsJsonObject().get("result").getAsBoolean();
    }

    private void postTransportUnitDisbandedEvent() {
        Model.getInstance().clearTransportTable();
        MessageEvent transportUnitDisbandedEvent = new MessageEvent(null, MessageEvent.TRANSPORT_UNIT_DISBANDED, null);
        EventBus.getDefault().post(transportUnitDisbandedEvent);
    }

    private Retrofit getClient(){
        if(client == null){
            httpClient = getHttpClient();

            client = new Retrofit.Builder()
                    .baseUrl(mServerConfiguration.getBaseUrl())
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return client;
    }

    private OkHttpClient getHttpClient(){
        if(httpClient == null){

            //Раскомментируйте, чтобы включить логи в файл

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(
                    new HttpLoggingInterceptor.Logger() {
                        @Override
                        public void log(String message) {
                            //Log.appendLog(eventType);
                        }
                    }
            );
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


            httpClient  = new OkHttpClient.Builder()
                    .addInterceptor(new RequestInterceptor(sInstance))
                    .addInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .callTimeout(60, TimeUnit.SECONDS)
                    .build();
        }
        return httpClient;
    }

    public ServerApi getServerAPI(){
        if(serverAPI == null) {
            serverAPI = getClient().create(ServerApi.class);
        }
        return serverAPI;
    }

    public void confirmOrderChanges(Order order, String authorId){
            JsonObject request = new JsonObject();
            request.addProperty("orderNumber", order.getNumber());
            request.addProperty("authorId", authorId);

        LogWriter.log(" API : confirmOrderChanges");
            Call<JsonObject> call = getServerAPI().confirmOrderChanges(Model.getInstance().getAccessToken(), request);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    LogWriter.logServerResponse(response);
                    try {
                        handleResultErrorWithThrowException(response);
                    } catch (Exception e) {
                        LogWriter.log(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
    }

    public void confirmOrderReceived(Order order){
        JsonObject request = new JsonObject();
        request.addProperty("orderNumber", order.getNumber());

        LogWriter.log(" API : orderReceived");
            Call<JsonObject> call = getServerAPI().orderReceived(Model.getInstance().getAccessToken(), request);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    LogWriter.logServerResponse(response);
                    try {
                        handleResultErrorWithThrowException(response);
                    } catch (Exception e) {
                        LogWriter.log(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
    }

    public void parseLogout() {
        try {
            Response response = getServerAPI().parseLogout(Model.getInstance().getAccessToken()).execute();
            CargoDealSystem.getInstance().clearSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void checkIsAppVersionAllowed(final Consumer<Boolean> callback) {
        Model.UserInfo info = Model.getInstance().getUserInfo();
        try {
            getServerAPI().fetchConfig(info.session_id).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    boolean result = handleCheckAppVersionResponse(response);
                    callback.accept(result);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    callback.accept(false);
                }
            });
        } catch (Exception e) {
            LogWriter.log(e.getLocalizedMessage());
            callback.accept(false);
        }
    }

    public boolean checkIsAppVersionAllowed() {
        Model.UserInfo info = Model.getInstance().getUserInfo();
        try {
            Response<JsonObject> response = getServerAPI().fetchConfig(info.session_id).execute();
                LogWriter.logServerResponse(response);
                return handleCheckAppVersionResponse(response);
        } catch (Exception e) {
            LogWriter.log(e.getLocalizedMessage());
            return false;
        }
    }

    private boolean handleCheckAppVersionResponse(Response<JsonObject> response) {
        LogWriter.logServerResponse(response);
        if (response.isSuccessful() && response.body() != null && response.body().has("params")) {
            JsonObject params = response.body().get("params").getAsJsonObject();
            if (params.has("driverAppMinVersionCode")) {
                int minVersionCodeAllowed = params.get("driverAppMinVersionCode").getAsInt();
                return BuildConfig.VERSION_CODE >= minVersionCodeAllowed;
            }
        }
        return true;
    }
}
