package api;

import android.content.Context;

import di.CargodealApp;
import common.OnCompletion;
import di.DependencyContainer;
import loaders.ChangeStatusLoader;
import model.DriverStatus;
import model.Model;
import features.log.LogWriter;

/**
 * Created by Alex Baturski on 23.08.2018.
 */

public class DriverStatusTask {
    private final ApiHelper api;
    private final OnCompletion callback;
    private final Context context;
    private final Model model;
    private final DependencyContainer dependencyContext;
    private boolean success;

    public DriverStatusTask(DependencyContainer dependencyContext, OnCompletion callback) {
        this.dependencyContext = dependencyContext;
        this.callback = callback;
        this.context = dependencyContext.getContext();
        this.api = dependencyContext.getApi();
        this.model = dependencyContext.getModel();
    }

    public void submit() {
        DriverStatus driverStatus = model.getDriverStatus();
        try {
            ChangeStatusLoader.changeStatusOnServer(dependencyContext, driverStatus);
            success = true;
        } catch (Exception e) {
            success = false;
            LogWriter.log(e.getMessage());
        } finally {
            if (callback != null) {
                callback.onComplete(success);
            }
        }
    }
}
