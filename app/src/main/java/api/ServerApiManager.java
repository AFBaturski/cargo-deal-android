package api;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatActivity;

import di.CargodealApp;
import common.exceptions.NoTransportUnitException;
import di.DependencyContainer;
import location.LocationManager;
import model.DriverStatus;
import org.greenrobot.eventbus.EventBus;

import common.Utils;
import loaders.AuthRequestLoader;
import loaders.ChangeStatusLoader;
import model.Model;
import common.MessageEvent;
import com.macsoftex.cargodeal.driver.R;
import service.tasks.SetDriverStatusTask;

/**
 * Created by Alex Baturski on 16.08.2018.
 */

public class ServerApiManager implements LoaderManager.LoaderCallbacks {
    private static final int CHANGE_STATUS_LOADER = 0;
    private static final int AUTH_LOADER = 1;
    public static final String BAD_REQUEST = "Bad Request";
    public static final String INTERNAL_ERROR = "InternalError";
    private final boolean mSilent;
    private final ApiHelper api;
    private final DependencyContainer dependencyContext;
    private final Model model;
    private final TaskScheduler scheduler;
    private final AppCompatActivity mActivity;
    private final Context mContext;

    public ServerApiManager(AppCompatActivity activity, Context context, boolean silent) {
        dependencyContext = DependencyContainer.getInstance(activity);
        this.api = dependencyContext.getApi();
        this.model = dependencyContext.getModel();
        this.scheduler = dependencyContext.getScheduler();
        mActivity = activity;
        mContext = context;
        mSilent = silent;
    }

    public void setDriverStatus(DriverStatus driverStatus) {
        if(!LocationManager.isMockGps()) {

            if(driverStatus == DriverStatus.CREW_STATUS_READY && Model.getInstance().getMyOrderInfo() != null) {
                if (!mSilent)
                    EventBus.getDefault().post(new MessageEvent(mContext.getString(R.string.cant_set_status_ready), MessageEvent.CANT_SET_STATUS_READY, mActivity));
                return;
            } else if (driverStatus == DriverStatus.CREW_STATUS_WORK && Model.getInstance().getMyOrderInfo() == null) {
                if (!mSilent)
                    EventBus.getDefault().post(new MessageEvent(mContext.getString(R.string.cant_set_status_work), MessageEvent.CANT_SET_STATUS_READY, mActivity));
                return;
            }

            Bundle args = new Bundle();
            args.putInt("StatusID", driverStatus.value());
            if (!mSilent)
                EventBus.getDefault().post(new MessageEvent(mActivity.getString(R.string.status_setting), MessageEvent.DRIVER_STATUS_SETTING, mActivity));
            mActivity.getSupportLoaderManager().destroyLoader(CHANGE_STATUS_LOADER);
            mActivity.getSupportLoaderManager().initLoader(CHANGE_STATUS_LOADER, args, this).forceLoad();
        } else {
            if (!mSilent)
                EventBus.getDefault().post(new MessageEvent(mContext.getString(R.string.cant_set_status_mock), MessageEvent.CANT_SET_STATUS_BECAUSE_MOCK, mActivity));
        }
    }

    public void checkData(){
        Bundle args = new Bundle();
        args.putBoolean("onlyCheck", true);
        if (!mSilent)
            EventBus.getDefault().post(new MessageEvent(mActivity.getString(R.string.info_is_updating), MessageEvent.CHECK_TRANSPORT_UNIT_STARTED, mActivity));
        mActivity.getSupportLoaderManager().destroyLoader(AUTH_LOADER);
        mActivity.getSupportLoaderManager().initLoader(AUTH_LOADER, args, this).forceLoad();
    }



    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if(id == CHANGE_STATUS_LOADER){
            return new ChangeStatusLoader(dependencyContext, args);
        } else if (id == AUTH_LOADER){
            return new AuthRequestLoader(dependencyContext, args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if (loader instanceof ChangeStatusLoader) {
            ChangeStatusLoader changeStatusLoader = (ChangeStatusLoader) loader;

            String successMessage =  mContext.getString(R.string.my_status_success_msg)+" : "+mContext.getString(Model.getInstance().getDriverStatus().titleId());
            if (mSilent)
                successMessage = null;
            if (changeStatusLoader.getException() == null) {
                EventBus.getDefault().post(new MessageEvent(successMessage, MessageEvent.DRIVER_STATUS_CHANGED, mActivity));
            } else {
                Utils.dismissSnackBar();
                if (changeStatusLoader.getException() instanceof NoTransportUnitException)
                    EventBus.getDefault().post(new MessageEvent(null, MessageEvent.TRANSPORT_UNIT_DISBANDED, mActivity));
                else {
                    scheduler.execute(new SetDriverStatusTask(dependencyContext));
                }
            }
            mActivity.getSupportLoaderManager().restartLoader(CHANGE_STATUS_LOADER, null, this);
        } else if (loader instanceof AuthRequestLoader) {
            AuthRequestLoader authRequestLoader = (AuthRequestLoader) loader;

            String message = (authRequestLoader.getException() != null)? authRequestLoader.getException().getMessage(): null;
            if (authRequestLoader.getException() == null) {
                EventBus.getDefault().post(new MessageEvent(message, MessageEvent.CHECK_TRANSPORT_UNIT_FINISHED, mActivity));
            } else if (authRequestLoader.getException().getMessage().equals(BAD_REQUEST))
                EventBus.getDefault().post(new MessageEvent(message, MessageEvent.NEED_RELOGIN, mActivity));
            else {
                EventBus.getDefault().post(new MessageEvent(message, MessageEvent.LOADER_EXCEPTION, mActivity));
            }
            mActivity.getSupportLoaderManager().restartLoader(AUTH_LOADER, null, this);
        }
    }


    @Override
    public void onLoaderReset(Loader loader) {

    }

}
