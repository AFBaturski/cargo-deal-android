package api;

import common.schedule.LowPriorityThreadFactory;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class TaskScheduler extends ScheduledThreadPoolExecutor {
    private static TaskScheduler instance;
    private final Deque<Map.Entry<Class<?>, ScheduledFuture<?>>> tasks = new ConcurrentLinkedDeque<>();
    public TaskScheduler() {
        super(1, new LowPriorityThreadFactory());
    }

    public static TaskScheduler createInstance() {
        if (instance == null ){
            instance = new TaskScheduler();
        }
        return instance;
    }

    public void schedule(Runnable r, long delay){
        removeTasks(r);
        tasks.offer(new AbstractMap.SimpleImmutableEntry<>(r.getClass(), schedule(r, delay, TimeUnit.MILLISECONDS)));
    }

    public void schedule(Runnable r, long delay, long period){
        removeTasks(r);
        tasks.offer(new AbstractMap.SimpleImmutableEntry<>(r.getClass(), scheduleAtFixedRate(r, delay, period, TimeUnit.MILLISECONDS)));
    }

    public void execute(Runnable r){
        removeTasks(r);
        tasks.offer(new AbstractMap.SimpleImmutableEntry<>(r.getClass(), schedule(r, 0, TimeUnit.MILLISECONDS)));
    }

    private <T> void removeTasks(final T obj) {
        Collection<Map.Entry<Class<?>, ScheduledFuture<?>>> listToRemove = tasks.stream()
                .filter(item -> item.getKey().equals(obj.getClass()))
                .collect(Collectors.toList());

        listToRemove.stream().forEach(item -> {
            item.getValue().cancel(false);
        });
        tasks.removeAll(listToRemove);
    }
}
