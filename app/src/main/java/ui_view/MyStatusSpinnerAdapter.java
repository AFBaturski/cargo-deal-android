package ui_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.macsoftex.cargodeal.driver.R;

import java.util.List;


/**
 * Created by barkov00@gmail.com on 05.05.2017.
 */

public class MyStatusSpinnerAdapter extends ArrayAdapter<MyStatusSpinnerAdapter.SpinnerEntry> {

    private List<SpinnerEntry> items = null;

    public MyStatusSpinnerAdapter(Context context, List<SpinnerEntry> items){
        super(context, R.layout.spinner_item_with_radial_box, items);
        this.items = items;
    }

    public void setItems(List<SpinnerEntry> items){
        this.items = items;
    }

    public List<SpinnerEntry> getItems(){
        return items;
    }

    public void add(SpinnerEntry entry){
        items.add(entry);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public SpinnerEntry getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void clear(){
        items.clear();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            convertView = layoutInflater.inflate(R.layout.spinner_item_with_radial_box, parent, false);
        }
        TextView text1 = (TextView) convertView.findViewById(android.R.id.text1);
        text1.setText(items.get(i).caption);
        return convertView;
    }

    @Override
    public View getDropDownView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            convertView = layoutInflater.inflate(R.layout.spinner_item_with_radial_box_dropdown, parent, false);
        }
        TextView text1 = (TextView) convertView.findViewById(android.R.id.text1);
        RadioButton radioButton = (RadioButton) convertView.findViewById(R.id.radio);
        text1.setText(items.get(position).caption);
        radioButton.setChecked(items.get(position).checked);
        radioButton.setVisibility(items.get(position).showRadioButton ? View.VISIBLE : View.GONE);
        return convertView;
    }

    public static class SpinnerEntry {
        public String caption;
        public int id;
        public boolean checked, showRadioButton;

        public SpinnerEntry(int id, String caption) {
            this.id = id;
            this.caption = caption;
            showRadioButton = true;
        }

        public SpinnerEntry(int id, String caption, boolean showRadioButton) {
            this.id = id;
            this.caption = caption;
            this.showRadioButton = showRadioButton;
        }

        @Override
        public String toString() {
            return caption;
        }
    }
}
