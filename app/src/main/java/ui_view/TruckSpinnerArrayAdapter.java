package ui_view;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import com.bumptech.glide.Glide;
import features.transport_unit.model.Transport;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Игорь on 16.03.2017.
 */

public class TruckSpinnerArrayAdapter extends ArrayAdapter<Transport> {
    private List<Transport> items = new ArrayList<>();
    private List<Transport> original_items;


    public TruckSpinnerArrayAdapter(Context context, int resource, List<Transport> objects) {
        super(context, resource, objects);
        this.original_items = objects;
        items.addAll(objects);
    }

    @Override
    public void add(Transport object) {
        if(!original_items.contains(object))
            original_items.add(object);
        items.add(object);
    }

    @Override
    public void clear() {
        super.clear();
        original_items.clear();
        items.clear();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.truckchooser_spinner_item_frame, null);
        }
        if(items.size() > position) {
            Transport item = items.get(position);
            if (item != null) {
                TextView text = (TextView) v.findViewById(R.id.text1);
                ImageView image = (ImageView) v.findViewById(R.id.image);
                text.setText(item.getGosNumber());
                if (item.getNumberUrl() != null) {
                    image.setVisibility(View.VISIBLE);
                    Glide.with(getContext()).load(item.getNumberUrl()).into(image);
                    //text.setVisibility(View.GONE);
                } else {
                    //text.setVisibility(View.VISIBLE);
                    image.setVisibility(View.GONE);
                    image.setImageDrawable(null);
                }
            }
        }
        return v;
    }


    private boolean inSearchMode = false;
    public void search(String query){
        if(query.length() == 0){
            items.clear();
            items.addAll(original_items);
            notifyDataSetChanged();
            inSearchMode = false;
            return;
        }
        items.clear();
        notifyDataSetChanged();
        for(Transport item : original_items){
            if(item.getGosNumber().toLowerCase().contains(query.toLowerCase())){
                items.add(item);
                notifyDataSetChanged();
            }
        }
        inSearchMode  = true;
    }

    @Override
    public int getCount() {
        if(!inSearchMode) {
            return original_items.size();
        } else {
            return items.size();
        }
    }

    @Override
    public Transport getItem(int position) {
        if(!inSearchMode) {
            return original_items.get(position);
        } else {
            return items.get(position);
        }
    }



}
