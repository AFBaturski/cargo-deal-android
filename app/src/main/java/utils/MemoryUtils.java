package utils;

import android.app.ActivityManager;
import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.ACTIVITY_SERVICE;

public class MemoryUtils {

    public static ActivityManager.MemoryInfo getAvailableMemory(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo;
    }

    public static int getAppMemoryLimit(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        return activityManager.getMemoryClass();
    }

    public static Map<String, Long> getRuntimeMemoryInfo(Context context) {
        Runtime runtime = Runtime.getRuntime();
        Map<String, Long> result = new HashMap<>();
        result.put("Total memory", runtime.totalMemory()/1024/1024);
        result.put("Free memory", runtime.freeMemory()/1024/1024);
        result.put("Used memory", (runtime.totalMemory() - runtime.freeMemory())/1024/1024);
        result.put("Max memory", runtime.maxMemory()/1024/1024);
        result.put("Available memory", (runtime.maxMemory() - (runtime.totalMemory() - runtime.freeMemory()))/1024/1024);
        return result;
    }
}
