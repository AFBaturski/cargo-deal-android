package activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Button;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.macsoftex.cargodeal.driver.R;
import common.Utils;
import model.ChangeData;
import viewmodels.ActivityOrderChangesViewModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ActivityOrderChanges extends AppCompatActivity {
    private ActivityOrderChangesViewModel viewModel;
    private RecyclerView changesList;
    private Button acceptButton;
    private Button closeButton;
    private NestedScrollView scrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_changes);
        this.setFinishOnTouchOutside(false);
        changesList = findViewById(R.id.changesList);
        acceptButton = findViewById(R.id.acceptButton);
        closeButton = findViewById(R.id.closeButton);
        scrollView = findViewById(R.id.scrollView);
        viewModel = new ViewModelProvider(this).get(ActivityOrderChangesViewModel.class);
        viewModel.init();
        initObservers();
        initEventListeners();
    }

    private void initEventListeners() {
        acceptButton.setOnClickListener(v -> {
           viewModel.confirmChanges(callback -> {
               hideLoadingProgress();
               finish();
           });
        });
    }

    private void hideLoadingProgress() {
        //TODO add infinite ProgressBar to layout
    }

    private void initObservers() {
        viewModel.changedProperties.observe(this, this::updateChangesList);
    }

    private void updateChangesList(LinkedHashMap<Integer, ChangeData> orderChanges) {
        changesList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        List<Map.Entry<Integer, ChangeData>> changes = new ArrayList<>(orderChanges.entrySet());
        changesList.setAdapter(new OrderChangesAdapter(getResources(), changes));
    }

    @Override
    protected void onStart() {
        super.onStart();
        animateScrollDownAndUp();
    }

    private void animateScrollDownAndUp() {
        Utils.showToast(getString(R.string.check_changes_and_confirm), this);
        Handler handler = new Handler(Looper.getMainLooper());
        int topY = scrollView.getScrollY();
        int topX = scrollView.getScrollX();
        handler.postDelayed(() -> {
            smoothScrollDown();
            handler.postDelayed(() -> {
                scrollView.smoothScrollTo(topX, topY, 500);
            }, 500);
        }, 1000);
    }

    private void smoothScrollDown() {
        int bottomY = scrollView.computeVerticalScrollRange();
        int bottomX = scrollView.computeHorizontalScrollRange();
        scrollView.smoothScrollTo(bottomX, bottomY, 500);
    }

    @Override
    public void onBackPressed() {
        Utils.showToast(getString(R.string.need_accept_for_close_activity), this);
        smoothScrollDown();
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ActivityOrderChanges.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }
}
