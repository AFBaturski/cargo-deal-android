package activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import features.main.HomeActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import common.OrderActionsListener;
import fragments.MyOrderFragment;
import features.order.model.Order;
import features.order.OrderManager;
import common.MessageEvent;
import com.macsoftex.cargodeal.driver.R;
import common.Utils;

/**
 * Created by Alex Baturski on 27.07.2018.
 */

public class OrdersActivity extends AppCompatActivity implements OrderActionsListener {
    private static final String EXTRA_ORDER_NUMBER = "com.macsoftex.cargodeal.driver.order_number";
    private static final String EXTRA_ORDER_LIST_TYPE = "com.macsoftex.cargodeal.driver.order_list_type";
    private static final String EXTRA_ORDERS = "com.macsoftex.cargodeal.driver.orders";
    private ViewPager mViewPager;
    private PagerTabStrip pagerTabStrip;
    private List<Order> mOrders;
    public PagerAdapter mOrderPagerAdapter;

    private int ordersCount;
    private int mOrderListType;
    private boolean justCreate;

    public static Intent newIntent(Context context, @NonNull List<Order> orders, int selectedOrderNumber, int orderListType){
        Intent intent = new Intent(context, OrdersActivity.class);
        intent.putExtra(EXTRA_ORDER_NUMBER, selectedOrderNumber);
        intent.putExtra(EXTRA_ORDER_LIST_TYPE, orderListType);
        intent.putExtra(EXTRA_ORDERS, (ArrayList<Order>)orders);
        return intent;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        getSupportActionBar().setTitle(R.string.menu_orders);

        if (getIntent() == null || !getIntent().hasExtra(EXTRA_ORDER_NUMBER)) {
            finish();
            return;
        }

        int orderNumber = (int) getIntent().getSerializableExtra(EXTRA_ORDER_NUMBER);
        mOrderListType = (int) getIntent().getSerializableExtra(EXTRA_ORDER_LIST_TYPE);
        mOrders = (List<Order>) getIntent().getSerializableExtra(EXTRA_ORDERS);

        mViewPager = findViewById(R.id.ordersPager);
        //TODO сделать инъекцию списка заказов

        ordersCount = 0;//mOrders.size();
        justCreate = true;
        pagerTabStrip = (PagerTabStrip) findViewById(R.id.ordersPagerTabStrip);
        pagerTabStrip.setDrawFullUnderline(false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        mOrderPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mOrderPagerAdapter);

        for (int i=0;i<mOrders.size();i++){
            if (mOrders.get(i).getNumber().equals(orderNumber)){
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Если изменился источник данных, обновляем список заказов
        if (mOrderPagerAdapter != null && mOrders.size() != ordersCount) {
            ordersCount = mOrders.size();
            mOrderPagerAdapter.notifyDataSetChanged();

            //Если нет активных заказов, закрываем активность
            if (ordersCount == 0)
                finish();

            //При открытии через список заказов, не нужно выделять активный заказ
            if (justCreate) {
                justCreate = false;
                return;
            }

            for (int i=0;i<mOrders.size();i++){
                if (mOrders.get(i).equals(OrderManager.getInstance().getActiveOrder())){
                    mViewPager.setCurrentItem(i);
                    break;
                }
            }
        }
    }

    public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        public long baseId = 0;

        MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Order order = mOrders.get(position);
            return MyOrderFragment.newInstance(order, mOrderListType);
        }

        @Override
        public long getItemId(int position) {
            return baseId + position;
        }

        @Override
        public int getCount() {
            return mOrders.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public void notifyDataSetChanged() {
            baseId += getCount() + 1;
            super.notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position > mOrders.size() )
                return getString(R.string.orderTitle);

            if (mOrders.get(position).equals(OrderManager.getInstance().getActiveOrder()))
                return getString(R.string.activeOrderTitle)+" № "+mOrders.get(position).getNumber();
            else
                return getString(R.string.orderTitle)+" № "+mOrders.get(position).getNumber();
        }
    }

    public void setOrdersCount(int ordersCount) {
        this.ordersCount = ordersCount;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void declineOrder(Order order) {
        Utils.showToast(String.format(Locale.getDefault(),"Заказ № %d отклонен", order.getNumber()), this);
        if (mOrders.size() == 1) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("fragmentName", "map");
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
            return;
        }
        mOrders.remove(mViewPager.getCurrentItem());
        mOrderPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Subscribe
    public void handleStatusChanged(MessageEvent event) {
        if (event.getEventType() == MessageEvent.ORDER_COMPLETE)
            finish();
    }
}
