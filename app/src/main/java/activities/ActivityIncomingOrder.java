package activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import common.Utils;
import fragments.IncomingOrderFragment;
import model.Model;
import features.order.model.Order;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Eugene on 28.03.2017.
 */

public class ActivityIncomingOrder extends AppCompatActivity{

    ViewPager pager;
    PagerAdapter pagerAdapter;
    PagerTabStrip pagerTabStrip;
    int ordersCount = 0;
    Bundle ordersBundle;
    Bundle bundleForPager=null;
    ArrayList<Integer> orderNumbers = new ArrayList<>();
    ArrayList<Integer> orderBundleKeys = new ArrayList<>();

    public PagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }

    public int getOrdersCount() {
        return orderNumbers.size();
    }

    public static Intent newIntent(Context context, List<Order> ordersList) {
        Bundle ordersBundle = new Bundle();
        for (int i = 0; i < ordersList.size(); i++)
            ordersBundle.putBundle(Integer.toString(i), ordersList.get(i).getAsBundle());

        Intent intent = new Intent(context, ActivityIncomingOrder.class);
        intent.putExtra("ordersBundle",ordersBundle);
        intent.putExtra("ordersCount",ordersList.size());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onResume() {
        super.onResume();
    checkActiveOrder();
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_order);

        checkActiveOrder();

        if (savedInstanceState != null) {
            ordersBundle = savedInstanceState.getBundle("ordersBundle");
            ordersCount = savedInstanceState.getInt("ordersCount");
        } else {
            ordersBundle = getIntent().getBundleExtra("ordersBundle");
            ordersCount = getIntent().getIntExtra("ordersCount", 0);
        }

        bundleForPager = new Bundle(ordersBundle);

        //Добавляем номера заказов в массив, для отображения на вкладках и удаления непринятых заказов из ViewPager
        for (Integer i=0;i<ordersCount;i++) {
            Bundle bundle = ordersBundle.getBundle(i.toString());
            orderNumbers.add(bundle.getInt("number"));
            orderBundleKeys.add(i);
        }

        pager = (ViewPager) findViewById(R.id.pager);
        pagerTabStrip = (PagerTabStrip) findViewById(R.id.pagerTabStrip);
        pagerTabStrip.setDrawFullUnderline(false);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (savedInstanceState != null)
            pagerAdapter.notifyDataSetChanged();
    }

    private void checkActiveOrder() {
        Model.MyOrderInfo alreadyHaveActiveOrder = Model.getInstance().getMyOrderInfo();
        if (alreadyHaveActiveOrder != null){
            finish();
            Intent intent = ViewOrderActivity.newIntent(getBaseContext(), alreadyHaveActiveOrder.getAsOrder());
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Model.getInstance().setInProgress(false);
    }

    public void removeCurrentItem() {
        int declinedOrderNumber = orderNumbers.get(pager.getCurrentItem());
        Integer keyToRemove = orderBundleKeys.get(pager.getCurrentItem());
        //Integer position = pager.getCurrentItem();
        orderNumbers.remove(pager.getCurrentItem());
        orderBundleKeys.remove(pager.getCurrentItem());
        ordersBundle.remove(keyToRemove.toString());
        bundleForPager.clear();
        //Из-за возможного удаления страницы в середине, смещаем все заказы, заполняя освободившиеся места
        Integer j=0;
        for (Integer i=0;i<ordersCount;i++) {
            Bundle bundle = ordersBundle.getBundle(i.toString());
            if (bundle != null) {
                bundleForPager.putBundle(j.toString(), bundle);
                j++;
            }
        }
                pagerAdapter.notifyDataSetChanged();
        Utils.showToast(String.format(Locale.getDefault(),"Заказ № %d отклонен",declinedOrderNumber), this);
    }

    public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        public long baseId = 0;

        MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return IncomingOrderFragment.newInstance(bundleForPager, position, orderNumbers.size(), -1);
        }

        @Override
        public long getItemId(int position) {
            return baseId + position;
        }

        @Override
        public int getCount() {
            return orderNumbers.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public void notifyDataSetChanged() {
            baseId += getCount() + 1;
            super.notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return getString(R.string.new_order_num) +" "+ orderNumbers[position];
            return getString(R.string.orderTitle)+" "+(position+1);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        ordersBundle = getIntent().getBundleExtra("ordersBundle");
        ordersCount = ordersCount +ordersBundle.size();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Model.getInstance().getInProgress())
            Model.getInstance().setInProgress(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("ordersBundle", bundleForPager);
        outState.putInt("ordersCount",orderNumbers.size());
    }

    public ViewPager getPager() {
        return pager;
    }

}
