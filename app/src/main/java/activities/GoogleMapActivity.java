package activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 30.08.2018.
 * Macsoftex
 */

public class GoogleMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double mLatitude;
    private double mLongitude;
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setContentView(R.layout.activity_googlemap);
        mLongitude = getIntent().getFloatExtra("longitude",0);
        mLatitude = getIntent().getFloatExtra("latitude",0);
        mTitle = getIntent().getStringExtra("title");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng point = new LatLng(mLatitude, mLongitude);
        mMap.addMarker(new MarkerOptions().position(point).title(mTitle)).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point,14));
    }
}