package activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Objects;

import fragments.PhotoCommentFragment;
import com.macsoftex.cargodeal.driver.R;


/**
 * Created by barkov00@gmail.com on 30.03.2017.
 */

public class PhotoCommentActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.photo_comment_activity);
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, PhotoCommentFragment.newInstance(), PhotoCommentFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PhotoCommentFragment.CAMERA_PERMISSION_REQ:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PhotoCommentFragment fragment = (PhotoCommentFragment)getSupportFragmentManager().findFragmentByTag(PhotoCommentFragment.TAG);
                    Objects.requireNonNull(fragment).takePhoto();
                } else {
                    Toast.makeText(this, R.string.please_grant_camera_permission, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
