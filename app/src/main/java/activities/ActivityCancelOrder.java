package activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.util.Locale;

import features.main.HomeActivity;
import features.order.model.Order;
import com.macsoftex.cargodeal.driver.R;

/**
 * Created by Alex Baturski on 25.08.2018.
 */

public class ActivityCancelOrder extends AppCompatActivity {
    private Integer orderNumber;
    private String loadingPointAddress;
    private String unloadingPointAddress;

    public static Intent newIntent(Context context, Order order) {
        Intent intent = new Intent(context, ActivityCancelOrder.class);
        intent.putExtra("orderNumber",order.getNumber());
        intent.putExtra("loadingPointAddress",order.getLoadingPointAddress());
        intent.putExtra("unloadingPointAddress",order.getUnloadingPointAddress());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        saveInstanceData(outState);
        super.onSaveInstanceState(outState);
    }

    private void saveInstanceData(Bundle outState) {
        outState.putInt("orderNumber", orderNumber);
        outState.putString("loadingPointAddress", loadingPointAddress);
        outState.putString("unloadingPointAddress", unloadingPointAddress);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cancel_order);
        loadInstanceData(savedInstanceState);
        initUI();
    }

    private void initUI() {
        TextView cancelOrderText = findViewById(R.id.cancelOrderText);
        cancelOrderText.setText(String.format(Locale.getDefault(),"Ваш заказ № %d отменен", orderNumber));

        TextView loadingPointText = findViewById(R.id.loadingPointText);
        loadingPointText.setText(loadingPointAddress);

        TextView unloadingPointText = findViewById(R.id.unloadingPointText);
        unloadingPointText.setText(unloadingPointAddress);

        TextView cancelOrderButton = findViewById(R.id.cancelOrderButton);
        cancelOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = HomeActivity.newIntent(getBaseContext());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void loadInstanceData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            orderNumber = savedInstanceState.getInt("orderNumber");
            loadingPointAddress = savedInstanceState.getString("loadingPointAddress");
            unloadingPointAddress = savedInstanceState.getString("unloadingPointAddress");
        }
        else {
            orderNumber = getIntent().getIntExtra("orderNumber", 0);
            loadingPointAddress = getIntent().getStringExtra("loadingPointAddress");
            unloadingPointAddress = getIntent().getStringExtra("unloadingPointAddress");
        }
    }
}
