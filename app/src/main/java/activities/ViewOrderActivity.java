package activities;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import com.macsoftex.cargodeal.driver.R;
import common.*;
import di.DependencyContainer;
import features.main.HomeActivity;
import fragments.MyOrderFragment;
import location.LocationManager;
import features.order.model.Order;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;
import java.util.Objects;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

/**
 * Created by Alex Baturski on 27.07.2018.
 */

public class ViewOrderActivity extends AppCompatActivity implements OrderActionsListener {
    private static final String EXTRA_ORDER = "com.macsoftex.cargodeal.driver.order_number";
    private DependencyContainer dependencyContext;
    private LocationManager locationManager;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationManager.REQUEST_CHECK_SETTINGS && resultCode == RESULT_CANCELED) {
            locationManager.checkLocationSettings(ViewOrderActivity.this);
        }
    }

    public static PendingIntent newPendingIntent(Context context, Order order) {
        Intent intent = new Intent(context, ViewOrderActivity.class);
        intent.putExtra(EXTRA_ORDER, order);
        return TaskStackBuilder.create(context)
                .addNextIntent(new Intent(context, HomeActivity.class))
                .addNextIntent(intent)
                .getPendingIntent(0, FLAG_UPDATE_CURRENT);
    }

    public static Intent newIntent(Context context, Order activeOrder) {
        Intent intent = new Intent(context, ViewOrderActivity.class);
        intent.putExtra(EXTRA_ORDER, activeOrder);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(this);
        locationManager = dependencyContext.getLocationManager();
        locationManager.checkLocationSettings(ViewOrderActivity.this);
        setContentView(R.layout.activity_order);
        CargoDealSystem.getInstance().setNeedShowMyOrder(false);
        Order activeOrder = (Order) getIntent().getSerializableExtra(EXTRA_ORDER);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.activeOrderTitle) + " № " + activeOrder.getNumber());

        MyOrderFragment myOrderFragment = MyOrderFragment.newInstance(activeOrder, CargoDealSystem.ORDERS_LIST);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, myOrderFragment)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Subscribe
    public void handleStatusChanged(MessageEvent event) {
        if (event.getEventType() == MessageEvent.ORDER_COMPLETE)
            finish();
    }

    @Override
    public void declineOrder(Order order) {
        finish();
        Utils.showToast(String.format(Locale.getDefault(), "Заказ № %d отклонен", order.getNumber()), this);
    }
}
