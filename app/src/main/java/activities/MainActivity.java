package activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.app.LoaderManager;
import androidx.core.content.ContextCompat;
import androidx.loader.content.Loader;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;

import di.DependencyContainer;
import features.log.LogWriter;
import features.main.HomeActivity;
import features.transport_unit.ui.TransportChooserActivity;
import fragments.LoginDialogFragment;
import loaders.AuthRequestLoader;
import loaders.OrdersLoader;
import loaders.DeleteOtherSessionsLoader;
import model.Model;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import api.ApiHelper;
import common.CargoDealSystem;
import com.macsoftex.cargodeal.driver.R;
import common.Utils;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks {

    private final static String TAG = MainActivity.class.getSimpleName();

    public static final String LOGOUT = "account logout";
    public static final int REQUEST_LOCATION_ACCESS = 1;

    private EditText login, password;
    private Button loginButton;

    private Call<JsonObject> disbandTransportUnitRequest;

    private Call<JsonObject> checkAppVersionRequest;
    private static final int AUTH_LOADER = 1;
    private static final int ORDERS_LOADER = 2;
    private static final int DELETE_OTHER_SESSIONS_LOADER = 3;
    Call<JsonObject> logoutRequest;
    private ApiHelper api;
    private Model model;
    private DependencyContainer dependencyContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(this);
        api = dependencyContext.getApi();
        model = dependencyContext.getModel();
        setContentView(R.layout.activity_login);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);

        login = (EditText) findViewById(R.id.input_login);
        password = (EditText) findViewById(R.id.input_password);
        loginButton = (Button) findViewById(R.id.btn_login);

        loginButton.setOnClickListener(buttonClickListener);
    }

    private void checkAuth() {
        if (!(getIntent() != null && LOGOUT.equals(getIntent().getAction()))) {
            Model.UserInfo userInfo = Model.getInstance().getUserInfo();
            //Если токен еще не истек и юзер выбрал номер головы - переходим на HomeActivity. Если не выбрал - на TruckChooser.
            if (userInfo != null && userInfo.session_id != null) {
                if (model.isTransportUnitAvailable()) {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    overridePendingTransition(0, 0);
                    finish();
                    return;
                } else {
                    startActivity(new Intent(getApplicationContext(), TransportChooserActivity.class));
                    overridePendingTransition(0, 0);
                    finish();
                    return;
                }
            }
        }
        checkAppVersion();
    }

    private void checkAppVersion() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent() != null && LOGOUT.equals(getIntent().getAction())) {
            model.setLogoutState(true);
            getIntent().setAction(null);
        }
        if (model.getLogoutState()) {

            blockUi(true);
            showProgressDialog(getString(R.string.logout_in_progress));

            if (Model.getInstance().getMyOrderInfo() == null && model.isTransportUnitAvailable()) {
                LogWriter.log(this.getClass().getSimpleName()+" : disbandTransportUnit");
                disbandTransportUnitRequest = api.getServerAPI().disbandTransportUnit(Model.getInstance().getAccessToken());
                disbandTransportUnitRequest.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        LogWriter.logServerResponse(response);
                        try {
                            if (response.code() != 200)
                                throw new Exception(response.message());

                            if (response.body().has("result")) {
                                if (response.body().get("result").getAsJsonObject().get("result").getAsBoolean()){
                                    Model.getInstance().clearTransportTable();
                                }
                                else
                                    switch (response.body().get("result").getAsJsonObject().get("error").getAsString()) {
                                        case ApiHelper.INVALID_REQUEST:
                                            throw new Exception(getString(R.string.InvalidRequest));
                                        case ApiHelper.NO_TRANSPORT_UNIT:
                                            Model.getInstance().clearTransportTable();
                                            throw new Exception(getString(R.string.NoTransportUnit));
                                    }
                            }
                        } catch (Exception e) {
                            LogWriter.log(e.getMessage());
                        } finally {
                            destroySession();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        destroySession();
                    }
                });
            }
            else {
                destroySession();
            }
        }
    }

    public void destroySession() {
        LogWriter.log(this.getClass().getSimpleName()+" : parseLogout");
        logoutRequest = api.getServerAPI().parseLogout(Model.getInstance().getAccessToken());
        logoutRequest.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogWriter.logServerResponse(response);
                clearSession();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                clearSession();
            }
        });
    }

    public void clearSession() {
        CargoDealSystem.getInstance().clearSession();
        blockUi(false);
        hideProgressDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkAuth();
        login.requestFocus();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION_ACCESS);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_ACCESS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    login();
                } else {
                    Utils.showToast(getString(R.string.need_location_access_msg), getApplicationContext());
                }
                break;
        }
    }

    public void login() {
        if (!checkLocationPermission()){
            return;
        }

        if (!validate()) {
            onLoginFailed(getString(R.string.invalid_login_passw));
            return;
        }

        blockUi(true);
        showProgressDialog(getString(R.string.auth_in_progress));

        Bundle args = new Bundle();
        args.putString("username", login.getText().toString());
        args.putString("password", password.getText().toString());
        getSupportLoaderManager().destroyLoader(AUTH_LOADER);
        getSupportLoaderManager().initLoader(AUTH_LOADER, args, this).forceLoad();

        closeKeyboard();
    }

    public boolean validate() {
        boolean valid = true;

        String _login = login.getText().toString();
        String _password = password.getText().toString();

        if (_login.isEmpty()) {
            login.setError(getString(R.string.invalid_login_hint));
            valid = false;
        } else {
            password.setError(null);
        }

        if (_password.isEmpty() || _password.length() < 3 || _password.length() > 16) {
            password.setError(getString(R.string.invalid_password_hint));
            valid = false;
        } else {
            password.setError(null);
        }

        return valid;
    }

    public void onLoginSuccess() {
        blockUi(true);
        hideProgressDialog();
        if (!model.isTransportUnitAvailable())
            startActivity(new Intent(getApplicationContext(), TransportChooserActivity.class));
        else
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        finish();
    }

    public void onLoginFailed(String errorMsg) {
        if(errorMsg != null) Utils.showToast(errorMsg, getBaseContext());
        hideProgressDialog();
        blockUi(false);
    }

    public void closeKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private View.OnClickListener buttonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_login:
                    login();
                    break;
            }
        }
    };

    private void blockUi(boolean blocked){
        blocked = !blocked;
        loginButton.setEnabled(blocked);
        login.setEnabled(blocked);
        password.setEnabled(blocked);
    }

    private  ProgressDialog progressDialog;
    public void showProgressDialog(String message){
        if(progressDialog != null && progressDialog.isShowing()) return;
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void hideProgressDialog(){
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
        cancelLogoutRequest();
        if(checkAppVersionRequest != null){
            checkAppVersionRequest.cancel();
        }
    }

    private void cancelLogoutRequest() {
        if(logoutRequest != null) {
            logoutRequest.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }


    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if(id == AUTH_LOADER){
            return new AuthRequestLoader(dependencyContext, args);
        }
        if(id == ORDERS_LOADER){
            return new OrdersLoader(dependencyContext, args);
        }
        if(id == DELETE_OTHER_SESSIONS_LOADER){
            return new DeleteOtherSessionsLoader(api, this);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        checkAuthRequestLoaderIsFinished(loader, data);
        checkOrdersLoaderIsFinished(loader, data);
        checkDeleteOtherSessionsLoaderIsFinished(loader, data);
    }

    private void checkAuthRequestLoaderIsFinished(Loader loader, Object data) {
        if(!(loader instanceof AuthRequestLoader))
            return;

        AuthRequestLoader authLoader = (AuthRequestLoader) loader;
        Boolean success = (Boolean) data;
        if (success) {
            onLoginSuccess();
        } else {
            Exception loaderException = authLoader.getException();
            String message = authLoader.getException() != null ? authLoader.getException().getMessage() : "";
            if (!checkOtherSession(loaderException))
                onLoginFailed(getString(R.string.auth_failed) + "\n" + message);
        }
    }

    private void checkOrdersLoaderIsFinished(Loader loader, Object data) {
        if(!(loader instanceof OrdersLoader))
            return;

        OrdersLoader ordersLoader = (OrdersLoader) loader;
        Boolean success = (Boolean) data;
        if(!success && ordersLoader.getException() != null){
            String message = ordersLoader.getException().getMessage();
            Utils.showToast(message, getBaseContext());
        }
    }

    private void checkDeleteOtherSessionsLoaderIsFinished(Loader loader, Object data) {
        if(!(loader instanceof DeleteOtherSessionsLoader))
            return;

        blockUi(false);
        hideProgressDialog();
        DeleteOtherSessionsLoader deleteOtherSessionsLoader = (DeleteOtherSessionsLoader) loader;
        Boolean success = (Boolean) data;
        if(success){
            onLoginSuccess();
        }
        else {
            String message = deleteOtherSessionsLoader.getException() != null ? deleteOtherSessionsLoader.getException().getMessage() : "";
            Utils.showToast(message, getBaseContext());
            destroySession();
        }
    }

    private boolean checkOtherSession(Exception exception) {
        if (exception == null)
            return false;

        if (exception.getMessage().equals(getString(R.string.user_have_other_session))){
            blockUi(false);
            hideProgressDialog();
            final LoginDialogFragment loginDialogFragment = new LoginDialogFragment();
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    loginDialogFragment.show(getSupportFragmentManager(), LoginDialogFragment.TAG);
                }
            });
            return true;
        }
        return false;
    }

    public void deleteOtherSessions() {
        blockUi(true);
        showProgressDialog(getString(R.string.destroying_other_sessions));
        getSupportLoaderManager().destroyLoader(DELETE_OTHER_SESSIONS_LOADER);
        getSupportLoaderManager().initLoader(DELETE_OTHER_SESSIONS_LOADER, null, this).forceLoad();
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    public void cancelDeleteOtherSessions() {
        destroySession();
    }
}
