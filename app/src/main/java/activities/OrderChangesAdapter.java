package activities;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.IntegerRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.macsoftex.cargodeal.driver.R;
import model.ChangeData;

import java.util.List;
import java.util.Map;

public class OrderChangesAdapter extends RecyclerView.Adapter<OrderChangesAdapter.OrderChangesViewHolder> {
    private final List<Map.Entry<Integer, ChangeData>> orderChanges;
    private final Resources resources;

    public OrderChangesAdapter(Resources resources, List<Map.Entry<Integer, ChangeData>> orderChanges) {
        this.resources = resources;
        this.orderChanges = orderChanges;
    }

    @NonNull
    @Override
    public OrderChangesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderchanges, parent, false);
        return new OrderChangesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderChangesViewHolder holder, int position) {
        if (orderChanges != null && orderChanges.size() != 0) {
            holder.bind(resources, orderChanges.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return orderChanges.size();
    }

    protected static class OrderChangesViewHolder extends RecyclerView.ViewHolder {
        private TextView propertyTitle;
        private TextView propertyValue;

        public OrderChangesViewHolder(@NonNull View itemView) {
            super(itemView);
        propertyTitle = itemView.findViewById(R.id.propertyTitle);
        propertyValue = itemView.findViewById(R.id.propertyValue);
        }

        private void bind(Resources res, Map.Entry<Integer, ChangeData> data) {
            propertyTitle.setText(res.getString(data.getKey()));
            propertyValue.setText(data.getValue().value);
        }
    }
}
