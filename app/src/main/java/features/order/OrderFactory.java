package features.order;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import features.order.model.Order;
import features.order.model.OrderType;
import model.CarriageOrder;
import model.DealerOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderFactory {
  public static Order.Builder<?> newBuilder(Order order) {
    if (order instanceof DealerOrder) {
      return new DealerOrder.Builder(order);
    } else if (order instanceof CarriageOrder) {
      return new CarriageOrder.Builder(order);
    }
    throw new IllegalArgumentException("Unknown order type");
  }

  public static Order fromJsonObject(JsonObject jsonObject) {
    Order result = null;
    Optional<OrderType> orderType = OrderType.fromJson(jsonObject.get("type"));

    switch (orderType.orElse(OrderType.DEALER_ORDER)) {
      case DEALER_ORDER:
        result = Order.create(jsonObject, DealerOrder.class);
        break;
      case CARRIAGE_ORDER:
        result = Order.create(jsonObject, CarriageOrder.class);
        break;
    }
    return result;
  }

  public static List<Order> fromJsonArray(JsonArray ordersArray) {
    List<Order> orders = new ArrayList<>();
    for (JsonElement item : ordersArray) {
      JsonObject object = item.getAsJsonObject();
      orders.add(fromJsonObject(object));
    }
    return orders;
  }
}
