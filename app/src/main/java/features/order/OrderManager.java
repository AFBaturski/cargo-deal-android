package features.order;

import features.order.model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex Baturski on 25.07.2018.
 */

public class OrderManager {
    public static int ACTIVE_ORDER_REQUEST_PERIOD = 3  * 1000;
    public static int NO_ORDER_REQUEST_PERIOD = 15 * 60 * 1000;
    private static OrderManager ourInstance = new OrderManager();
    private List<Order> mOrders;

    public static OrderManager getInstance(){
        return ourInstance;
    }

    private OrderManager(){
        mOrders = new ArrayList<>();
    }

    public List<Order> getOrders(){
        return mOrders;
    }

    public void addOrder(Order order){
        mOrders.add(order);
    }
    public void insertOrder(Order order){
        mOrders.add(0,order);
    }

    public boolean replaceOrder(Order oldOrder, Order newOrder){
        int index = mOrders.indexOf(oldOrder);
        if (index != -1) mOrders.set(index, newOrder);
        return index != -1;
    }

    public void removeOrder(Order order){
        mOrders.remove(order);
    }

    public void removeOrderByNumber(int orderNumber){
        Order orderToRemove = null;
        for (Order order : mOrders) {
            if (order.getNumber() == orderNumber)
                orderToRemove = order;
        }
        mOrders.remove(orderToRemove);
    }

    public void clearOrders(){
        mOrders.clear();
    }

    public Order getActiveOrder(){
        for (int i = 0; i< mOrders.size(); i++)
            if (mOrders.get(i).isActive()){
                return mOrders.get(i);
            }
        return null;
    }

    public static long getRequestPeriodDependsOnOrderStatus() {
        if (getInstance().getActiveOrder() != null)
            return ACTIVE_ORDER_REQUEST_PERIOD;
        else
            return NO_ORDER_REQUEST_PERIOD;
    }

    public Order get(int index){
        return mOrders.size() <= index ? null : mOrders.get(index);
    }

    public void remove(int index){
        if (mOrders.size() <= index)
            mOrders.remove(index);
    }

    public Order getOrder(int orderNumber) {
        for (int i = 0; i< mOrders.size(); i++)
            if (mOrders.get(i).getNumber().equals(orderNumber)){
            return mOrders.get(i);
            }
        return null;
    }

    public Order applyChanges(int orderNumber) {
        for (int i = 0; i< mOrders.size(); i++)
            if (mOrders.get(i).getNumber().equals(orderNumber)){
                return mOrders.get(i);
            }
        return null;
    }

    public void removeActiveOrder() {
        removeOrder(getActiveOrder());
    }
}
