package features.order.model;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import common.Utils;
import model.Model;

import java.io.Serializable;
import java.util.Objects;

public abstract class Order implements Comparable<Order>, Serializable {
    private final long unloadingEndDate;
    private final double loadedTonnage;
    private final double unloadedTonnage;
    private final long createdAt;
    private final Integer number;
    private final String articleType;
    private final String articleBrand;
    private final String articleShape;
    private final double tonnage;
    private final double undistributedTonnage;
    private final String comment;
    private final Entrance loadingEntrance;
    private final Entrance unloadingEntrance;
    private final long loadingDate;
    private final String loadingContactName;
    private final String loadingContactPhone;
    private final double tariff;
    private final long unloadingBeginDate;
    private final String unloadingContactName;
    private final String unloadingContactPhone;
    private final String supplier;
    private final String customer;

    private final String objectId;
    private final boolean active;
    private final Integer orderStatusId;
    private final String orderStatusCaption;
    private final long orderStatusDateTime;
    private final int distance;


    public abstract static class Builder <T extends Builder<T>> {
        private final long unloadingEndDate;
        private final long createdAt;
        private final Integer number;
        private final String articleType;
        private final String articleBrand;
        private final String articleShape;
        private final String comment;
        private final Entrance loadingEntrance;
        private final Entrance unloadingEntrance;
        private final long loadingDate;
        private final String loadingContactName;
        private final String loadingContactPhone;
        private final double tariff;
        private final long unloadingBeginDate;
        private final String unloadingContactName;
        private final String unloadingContactPhone;
        private final String supplier;
        private final String customer;
        private final int distance;

        private double tonnage = 0;
        private double undistributedTonnage = 0;
        private double loadedTonnage = 0;
        private double unloadedTonnage = 0;
        private String objectId = "";
        private boolean active = false;
        private Integer orderStatusId = null;
        private String orderStatusCaption = "";
        private long orderStatusDateTime = 0;

        public Builder(Order order) {
            createdAt = order.createdAt;
            unloadingEndDate = order.unloadingEndDate;
            number = order.number;
            articleType = order.articleType;
            articleBrand = order.articleBrand;
            articleShape = order.articleShape;
            tonnage = order.tonnage;
            comment = order.comment;
            loadingEntrance = order.loadingEntrance;
            unloadingEntrance = order.unloadingEntrance;
            loadingDate = order.loadingDate;
            loadingContactName = order.loadingContactName;
            loadingContactPhone = order.loadingContactPhone;
            tariff = order.tariff;
            unloadingBeginDate = order.unloadingBeginDate;
            unloadingContactName = order.unloadingContactName;
            unloadingContactPhone = order.unloadingContactPhone;
            supplier = order.supplier;
            customer = order.customer;
            objectId = order.objectId;
            active = order.active;
            orderStatusId = order.orderStatusId;
            orderStatusCaption = order.orderStatusCaption;
            orderStatusDateTime = order.orderStatusDateTime;
            distance = order.distance;
        }

        public Builder(Model.MyOrderInfo orderInfo) {
            createdAt = orderInfo.createdAt;
            unloadingEndDate = orderInfo.unloadingEndDate;
            number = orderInfo.orderNumber;
            articleType = orderInfo.articleType;
            articleBrand = orderInfo.articleBrand;
            articleShape = orderInfo.articleShape;
            tonnage = orderInfo.tonnage;
            comment = orderInfo.comment;
            loadingEntrance = Entrance.create("", orderInfo.loadingEntranceAddress, orderInfo.loadingEntranceLatitude, orderInfo.loadingEntranceLongitude);
            unloadingEntrance = Entrance.create("", orderInfo.unloadingEntranceAddress, orderInfo.unloadingEntranceLatitude, orderInfo.unloadingEntranceLongitude);
            loadingDate = orderInfo.loadingDate;
            loadingContactName = orderInfo.loadingContactName;
            loadingContactPhone = orderInfo.loadingContactPhone;
            tariff = orderInfo.tariff;
            unloadingBeginDate = orderInfo.unloadingBeginDate;
            unloadingContactName = orderInfo.unloadingContactName;
            unloadingContactPhone = orderInfo.unloadingContactPhone;
            supplier = orderInfo.supplier;
            customer = orderInfo.customer;
            orderStatusId = orderInfo.orderStatusId;
            orderStatusCaption = orderInfo.orderStatusTitle;
            objectId = orderInfo.objectId;
            distance = orderInfo.distance;
        }

        public Builder(Bundle bundle) {
            createdAt = bundle.containsKey("createdAt") ? bundle.getLong("createdAt") : 0;
            number = bundle.getInt("number");
            articleType = bundle.getString("articleType");
            articleBrand = bundle.getString("articleBrand");
            articleShape = bundle.getString("articleShape");
            tonnage = bundle.getDouble("tonnage");
            comment = bundle.getString("comment");
            loadingEntrance = Entrance.create(
                bundle.getString("loadingPoint.name"),
                bundle.getString("loadingPoint.address"),
                bundle.getDouble("loadingPoint.latitude"),
                bundle.getDouble("loadingPoint.longitude")
                );
            loadingDate = bundle.getLong("loadingDate");
            loadingContactName = bundle.getString("loadingContactName");
            loadingContactPhone = bundle.getString("loadingContactPhone");
            unloadingEntrance = Entrance.create(
                bundle.getString("unloadingPoint.name"),
                bundle.getString("unloadingPoint.address"),
                bundle.getDouble("unloadingPoint.latitude"),
                bundle.getDouble("unloadingPoint.longitude")
            );
            unloadingBeginDate = bundle.getLong("unloadingBeginDate");
            unloadingEndDate = bundle.getLong("mUnloadingEndDate");
            unloadingContactName = bundle.getString("unloadingContactName");
            unloadingContactPhone = bundle.getString("unloadingContactPhone");
            tariff = bundle.containsKey("tariff") ? bundle.getDouble("tariff"):0;
            supplier = bundle.containsKey("supplier") ? bundle.getString("supplier"): "";
            customer = bundle.containsKey("customer") ? bundle.getString("customer"): "";
            objectId = bundle.containsKey("objectId") ? bundle.getString("objectId"): "";
            distance = bundle.containsKey("distance") ? bundle.getInt("distance"): 0;
        }

        protected abstract T self();
        public abstract Order build();

        public T undistributedTonnage(double undistributedTonnage) {
            this.undistributedTonnage = undistributedTonnage;
            return self();
        }

        public T tonnage(double tonnage) {
            this.tonnage = tonnage;
            return self();
        }

        public T loadedTonnage(double loadedTonnage) {
            this.loadedTonnage = loadedTonnage;
            return self();
        }

        public T unloadedTonnage(double unloadedTonnage) {
            this.unloadedTonnage = unloadedTonnage;
            return self();
        }

        public T objectId(String objectId) {
            this.objectId = objectId;
            return self();
        }

        public T active(boolean active) {
            this.active = active;
            return self();
        }

        public T orderStatusId(Integer orderStatusId) {
            this.orderStatusId = orderStatusId;
            return self();
        }

        public T orderStatusCaption(String orderStatusCaption) {
            this.orderStatusCaption = orderStatusCaption;
            return self();
        }

        public T orderStatusDateTime(long orderStatusDateTime) {
            this.orderStatusDateTime = orderStatusDateTime;
            return self();
        }
    }

    protected Order(Builder<?> builder) {
        createdAt = builder.createdAt;
        unloadingEndDate = builder.unloadingEndDate;
        number = builder.number;
        articleType = builder.articleType;
        articleBrand = builder.articleBrand;
        articleShape = builder.articleShape;
        tonnage = builder.tonnage;
        comment = builder.comment;
        loadingEntrance = builder.loadingEntrance;
        unloadingEntrance = builder.unloadingEntrance;
        loadingDate = builder.loadingDate;
        loadingContactName = builder.loadingContactName;
        loadingContactPhone = builder.loadingContactPhone;
        tariff = builder.tariff;
        unloadingBeginDate = builder.unloadingBeginDate;
        unloadingContactName = builder.unloadingContactName;
        unloadingContactPhone = builder.unloadingContactPhone;
        supplier = builder.supplier;
        customer = builder.customer;
        undistributedTonnage = builder.undistributedTonnage;
        loadedTonnage = builder.loadedTonnage;
        unloadedTonnage = builder.unloadedTonnage;
        objectId = builder.objectId;
        active = builder.active;
        orderStatusId = builder.orderStatusId;
        orderStatusCaption = builder.orderStatusCaption;
        orderStatusDateTime = builder.orderStatusDateTime;
        distance = builder.distance;
    }
    
    public static <T> T create(JsonElement json, Class<T> cls) {
        return new Gson().fromJson(json, cls);
    }
    

    public Bundle getAsBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("number", number);
        bundle.putString("articleType", articleType);
        bundle.putString("articleBrand", articleBrand);
        bundle.putString("articleShape", articleShape);
        if (tonnage > undistributedTonnage
                && undistributedTonnage != 0)
            bundle.putDouble("tonnage", undistributedTonnage);
        else
            bundle.putDouble("tonnage", tonnage);
        bundle.putDouble("undistributedTonnage", undistributedTonnage);
        bundle.putString("comment", comment);
        bundle.putDouble("tariff", tariff);

        bundle.putString("loadingPoint.name", loadingEntrance.getName());
        bundle.putString("loadingPoint.address", loadingEntrance.getAddress());
        bundle.putDouble("loadingPoint.latitude", loadingEntrance.getLatitude());
        bundle.putDouble("loadingPoint.longitude", loadingEntrance.getLongitude());
        bundle.putLong("loadingDate", loadingDate);
        bundle.putString("loadingContactName", loadingContactName);
        bundle.putString("loadingContactPhone", loadingContactPhone);
        bundle.putString("unloadingPoint.name", unloadingEntrance.getName());
        bundle.putString("unloadingPoint.address", unloadingEntrance.getAddress());
        bundle.putDouble("unloadingPoint.latitude", unloadingEntrance.getLatitude());
        bundle.putDouble("unloadingPoint.longitude", unloadingEntrance.getLongitude());
        bundle.putLong("unloadingBeginDate", unloadingBeginDate);
        bundle.putLong("unloadingEndDate", unloadingEndDate);
        bundle.putString("unloadingContactName", unloadingContactName);
        bundle.putString("unloadingContactPhone", unloadingContactPhone);
        bundle.putString("supplier", supplier);
        bundle.putString("customer", customer);
        bundle.putString("objectId", objectId);
        bundle.putInt("distance", distance);
        return bundle;
    }

    public String getLoadingPointName() {
        return loadingEntrance.getName();
    }

    public String getLoadingPointAddress() {
        return loadingEntrance.getAddress();
    }

    public double getLoadingPointLat() {
        return loadingEntrance.getLatitude();
    }

    public double getLoadingPointLon() {
        return loadingEntrance.getLongitude();
    }

    public String getUnloadingPointName() {
        return unloadingEntrance.getName();
    }

    public String getUnloadingPointAddress() {
        return unloadingEntrance.getAddress();
    }

    public double getUnloadingPointLat() {
        return unloadingEntrance.getLatitude();
    }

    public double getUnloadingPointLon() {
        return unloadingEntrance.getLongitude();
    }

    public Integer getNumber() {
        return this.number;
    }

    public String getArticleType() {
        return this.articleType;
    }

    public String getArticleBrand() {
        return this.articleBrand;
    }

    public String getArticleShape() {
        return this.articleShape;
    }

    public double getTonnage() {
        return this.tonnage;
    }

    public String getComment() {
        return this.comment;
    }

    public Long getLoadingDate() {
        return this.loadingDate;
    }

    public String getLoadingContactName() {
        return this.loadingContactName;
    }

     public Long getUnloadingBeginDate() {
        return this.unloadingBeginDate;
    }

    @NonNull
    @Override
    public String toString() {
        return "Order No: " + this.number + ". Order date: " + Utils.convertUnixTimeToDateTime(this.createdAt);
    }

    public String getUnloadingContactName() {
        return this.unloadingContactName;
    }

    public boolean isActive() {
        return active;
    }

    public double getUndistributedTonnage() {
        return undistributedTonnage;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    @Override
    public int compareTo(@NonNull Order o) {
        return 0;
    }

    public double getTariff() {
        return tariff;
    }

    public Integer getOrderStatusId() {
        return orderStatusId;
    }

    public double getUnloadedTonnage() {
        return unloadedTonnage;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getCustomer() {
        return customer;
    }

    public Long getUnloadingEndDate() {
        return this.unloadingEndDate;
    }

    public String getUnloadingContactPhone() {
        return unloadingContactPhone;
    }

    public String getLoadingContactPhone() {
        return loadingContactPhone;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return number.equals(order.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}