package features.order.model;

import androidx.annotation.StringRes;
import com.macsoftex.cargodeal.driver.R;

public enum OrderPropertyTitle {
    ARTICLE_TYPE(R.string.articleTypeTitle),
    ARTICLE_BRAND(R.string.articleBrandTitle),
    TONNAGE(R.string.tonnageTitle),
    TARIFF(R.string.tariffTitle),
    LOADING_ENTRANCE_ADDRESS(R.string.loadingEntranceAddressTitle),
    LOADING_CONTACT_NAME(R.string.loadingContactNameTitle),
    LOADING_DATE(R.string.loadingDateTitle),
    UNLOADING_ENTRANCE_ADDRESS(R.string.unloadingEntranceAddressTitle),
    UNLOADING_CONTACT_NAME(R.string.unloadingContactNameTitle),
    UNLOADING_CONTACT_PHONE(R.string.unloadingContactPhoneTitle),
    UNLOADING_DATE(R.string.unloadingDateTitle),
    UNLOADING_TIME(R.string.unloadingTimeTitle),
    SUPPLIER(R.string.supplierTitle),
    CUSTOMER(R.string.customerTitle),
    COMMENT(R.string.commentTitle),
    ORDER_TYPE(R.string.orderTypeTitle);

    private final int resId;
    OrderPropertyTitle(@StringRes int resId) {
        this.resId = resId;
    }

    @StringRes
    public int value() {
        return resId;
    }
}
