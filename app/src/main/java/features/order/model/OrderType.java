package features.order.model;

import androidx.annotation.NonNull;
import com.google.gson.JsonElement;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum OrderType {
    DEALER_ORDER("", "Обычный"),
    CARRIAGE_ORDER("carriage", "Перевозка");
    private static final Map<String, OrderType> stringToEnum =
            Stream.of(values()).collect(
                    Collectors.toMap(Object::toString, v -> v)
            );
    private final String orderType;
    private final String prettyName;

    OrderType(String orderType, String prettyName) {
        this.orderType = orderType;
        this.prettyName = prettyName;
    }

    public static Optional<OrderType> fromJson(JsonElement orderType) {
        if (orderType == null) return Optional.empty();
        return Optional.ofNullable(stringToEnum.get(orderType.getAsString()));
    }

    public static Optional<OrderType> fromString(String orderType) {
        return Optional.ofNullable(stringToEnum.get(orderType));
    }

    @NonNull
    @Override
    public String toString() {
        return orderType;
    }

    public String prettyName() {
        return prettyName;
    }
}
