package features.order.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.Serializable;

public final class Entrance implements Serializable {
  private final String name;
  private final String address;
  private final double latitude;
  private final double longitude;

  public static Entrance create(JsonElement json) {
    return new Gson().fromJson(json, Entrance.class);
  }

  public static Entrance create(String name, String address, double latitude, double longitude) {
    return new Entrance(name, address, latitude, longitude);
  }

  private Entrance(String name, String address, double latitude, double longitude) {
    this.name = name;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }
}
