package features.order.stage;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import features.common.JsonSerializable;

import java.util.Objects;

public class OrderStagePhoto implements JsonSerializable {
  private final OrderStages stageId;
  private final String tripId;
  private final transient String waybillPhotoFile;

  public static OrderStagePhoto createLoadingCompletePhotoStage(String tripId, String waybillPhotoFile) {
    return new OrderStagePhoto(OrderStages.ORDER_STAGE_LOADING_COMPLETE, tripId, waybillPhotoFile);
  }

  public static OrderStagePhoto createUnloadingCompletePhotoStage(String tripId, String waybillPhotoFile) {
    return new OrderStagePhoto(OrderStages.ORDER_STAGE_UNLOADING_COMPLETE, tripId, waybillPhotoFile);
  }

  private OrderStagePhoto(OrderStages stageId, String tripId, String waybillPhotoFile) {
    this.stageId = stageId;
    this.tripId = tripId;
    this.waybillPhotoFile = waybillPhotoFile;
    validate(tripId, waybillPhotoFile);
  }

  private void validate(String tripId, String waybillPhoto) {
    Objects.requireNonNull(waybillPhoto, "Waybill photo must be not null");
    Objects.requireNonNull(tripId, "Trip id must be not null");
    if (tripId.isEmpty())
      throw new IllegalArgumentException("Trip id must be not empty");
  }

  @Override
  public JsonElement serialize() {
    return new Gson().toJsonTree(this);
  }
}
