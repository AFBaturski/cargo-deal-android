package features.order.stage;

import android.database.Cursor;
import storage.OrderStageContract;
import features.order.model.OrderType;
import features.order.stage.model.*;

public class OrderStageFactory {
  public static OrderStage create(Cursor cursor) {
    int stageId = cursor.getInt(cursor.getColumnIndex(OrderStageContract.Entry.stageId));
    OrderType orderType = OrderType.fromString(
        cursor.getString(cursor.getColumnIndex(OrderStageContract.Entry.orderType)))
        .orElse(OrderType.DEALER_ORDER);
    long date = cursor.getLong(cursor.getColumnIndex(OrderStageContract.Entry.date));
    double tonnage = cursor.getDouble(cursor.getColumnIndex(OrderStageContract.Entry.tonnage));
    double latitude = cursor.getDouble(cursor.getColumnIndex(OrderStageContract.Entry.latitude));
    double longitude = cursor.getDouble(cursor.getColumnIndex(OrderStageContract.Entry.longitude));
    String comment = cursor.getString(cursor.getColumnIndex(OrderStageContract.Entry.comment));
    String waybillNumber = cursor.getString(cursor.getColumnIndex(OrderStageContract.Entry.waybillNumber));
    int mileage = cursor.getInt(cursor.getColumnIndex(OrderStageContract.Entry.mileage));
    return create(orderType, stageId, date, comment, latitude, longitude, tonnage, waybillNumber, mileage);
  }

  public static OrderStage create(OrderType orderType, int stageId, long date, String comment, double latitude, double longitude, double tonnage, String waybillNumber, int mileage) {
    OrderStages stage = OrderStages.values()[stageId];
    OrderStage.Builder builder;
    switch (stage) {
      case ORDER_STAGE_GET_TO_LOADING:
      case ORDER_STAGE_ARRIVED_TO_LOADING:
      case ORDER_STAGE_GET_TO_UNLOADING:
      case ORDER_STAGE_ARRIVED_TO_UNLOADING:
        builder = new OrderStage.Builder(orderType, stage, date);
        break;
      case ORDER_STAGE_LOADING_COMPLETE:
        builder = new OrderStage.Builder(orderType, stage, date)
            .tonnage(tonnage);

        if (orderType == OrderType.CARRIAGE_ORDER) {
          builder
              .additionalData(new AdditionalData(waybillNumber, mileage));
        }
        break;
      case ORDER_STAGE_UNLOADING_COMPLETE:
        builder = new OrderStage.Builder(orderType, stage, date)
            .tonnage(tonnage);
        break;
      default:
        throw new IllegalArgumentException("Unknown order stage");
    }
    builder
        .latitude(latitude)
        .longitude(longitude)
        .comment(comment)
        .build();

    return builder.build();
  }
}
