package features.order.stage;

import androidx.annotation.NonNull;
import com.google.gson.JsonElement;
import features.order.model.Order;
import features.order.model.OrderType;
import features.order.stage.model.AdditionalData;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum OrderStages {
    ORDER_STAGE_GET_TO_LOADING,
    ORDER_STAGE_ARRIVED_TO_LOADING,
    ORDER_STAGE_LOADING_COMPLETE(Validator.WITH_TONNAGE),
    ORDER_STAGE_GET_TO_UNLOADING,
    ORDER_STAGE_ARRIVED_TO_UNLOADING,
    ORDER_STAGE_UNLOADING_COMPLETE(Validator.WITH_TONNAGE);

    OrderStages(Validator validator) {
        this.validator = validator;
    }

    OrderStages() {
        this.validator = Validator.DEFAULT;
    }

    private final Validator validator;

    private static final Map<String, OrderStages> stagesMap =
        Stream.of(values()).collect(
            Collectors.toMap(Object::toString, v -> v)
        );

    public static Optional<OrderStages> fromJson(JsonElement stage) {
        if (stage == null) return Optional.empty();
        return Optional.ofNullable(stagesMap.get(stage.getAsString()));
    }

    public void validate(OrderType orderType, long date, @NonNull String comment, double tonnage, AdditionalData additionalData) {
        validator.validate(orderType, date, comment, tonnage, additionalData);
    }

    private enum Validator{
        DEFAULT {
            @Override
            void validate(OrderType orderType, long date, @NonNull String comment, double tonnage, AdditionalData additionalData) {
                if (date == 0)
                    throw new IllegalStateException("Provided date is invalid: "+ date);
                Objects.requireNonNull(comment, "Comment must be not null");
            }
        }, WITH_TONNAGE {
            @Override
            void validate(OrderType orderType, long date, @NonNull String comment, double tonnage, AdditionalData additionalData) {
                DEFAULT.validate(orderType, date, comment, tonnage, additionalData);
                if (tonnage <= 0)
                    throw new IllegalArgumentException("Tonnage must be greater than zero");
            }
        }, WITH_ADDITIONAL_DATA {
            @Override
            void validate(OrderType orderType, long date, @NonNull String comment, double tonnage, AdditionalData additionalData) {
                DEFAULT.validate(orderType, date, comment, tonnage, additionalData);
                Objects.requireNonNull(additionalData, "Additional data must be not null");
            }
        };
        abstract void validate(OrderType orderType, long date, @NonNull String comment, double tonnage, AdditionalData additionalData);
    }
}
