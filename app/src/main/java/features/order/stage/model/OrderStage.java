package features.order.stage.model;

import android.content.ContentValues;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.JsonObject;
import storage.OrderStageContract;
import features.order.model.OrderType;
import features.order.stage.OrderStages;

public class OrderStage {

  public static class Builder {
    private final OrderType orderType;
    private final OrderStages stageId;
    private final long date;

    private double latitude = 0;
    private double longitude = 0;
    private double tonnage = 0f;
    private String waybillNumber = "";
    private int mileage = 0;

    private @NonNull String comment = "";
    private @Nullable AdditionalData additionalData = null;

    public Builder(OrderType orderType, OrderStages stageId, long date) {
      this.orderType = orderType;
      this.stageId = stageId;
      this.date = date;
    }

    public Builder latitude(double latitude) {
      this.latitude = latitude;
      return this;
    }

    public Builder longitude(double longitude) {
      this.longitude = longitude;
      return this;
    }

    public Builder tonnage(double tonnage) {
      this.tonnage = tonnage;
      return this;
    }

    public Builder additionalData(AdditionalData additionalData) {
      this.additionalData = additionalData;
      return this;
    }

    public Builder comment(@NonNull String comment) {
      this.comment = comment;
      return this;
    }
    public OrderStage build() {
      return new OrderStage(this);
    }
  }

  private final OrderType orderType;
  private final OrderStages stageId;
  private final long date;
  private final double latitude;
  private final double longitude;
  private final double tonnage;
  private final AdditionalData additionalData;
  private final @NonNull String comment;

  protected OrderStage(Builder builder) {
    this.orderType = builder.orderType;
    this.stageId = builder.stageId;
    this.date = builder.date;
    this.latitude = builder.latitude;
    this.longitude = builder.longitude;
    this.tonnage = builder.tonnage;
    this.additionalData = builder.additionalData;
    this.comment = builder.comment;
    stageId.validate(orderType, date, comment, tonnage, additionalData);
  }

  public JsonObject serialize() {
    JsonObject result = new JsonObject();
    result.addProperty("stage", stageId.ordinal());
    result.addProperty("sendDelay", System.currentTimeMillis() - this.date);
    if (latitude != 0)
      result.addProperty("latitude", latitude);
    if (longitude != 0)
      result.addProperty("longitude", longitude);
    if (!comment.isEmpty())
      result.addProperty("comment", comment);
    return result;
  }

  public ContentValues getAsContentValues() {
    ContentValues cv = new ContentValues();
    cv.put(OrderStageContract.Entry.stageId, stageId.ordinal());
    cv.put(OrderStageContract.Entry.date, date);
    cv.put(OrderStageContract.Entry.tonnage, tonnage);
    cv.put(OrderStageContract.Entry.comment, comment);
    cv.put(OrderStageContract.Entry.latitude, latitude);
    cv.put(OrderStageContract.Entry.longitude, longitude);
    if (additionalData != null) {
      cv.put(OrderStageContract.Entry.waybillNumber, additionalData.getWaybillNumber());
      cv.put(OrderStageContract.Entry.mileage, additionalData.getMileage());
    }
    return cv;
  }

  public OrderStages getStageId() {
    return stageId;
  }

  public int getStage() {
    return stageId.ordinal();
  }

  public long getDate() {
    return date;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  @NonNull
  public String getComment() {
    return comment;
  }

  public double getTonnage() {
    return tonnage;
  }

  public AdditionalData getAdditionalData() {
    return additionalData;
  }

  @NonNull
  @Override
  public String toString() {
    return serialize().toString();
  }
}