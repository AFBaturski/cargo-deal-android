package features.order.stage.model;

import androidx.annotation.NonNull;
import com.google.gson.JsonObject;
import features.common.JsonSerializable;

import java.util.Objects;

public class AdditionalData implements JsonSerializable {
  private final String waybillNumber;
  private final int mileage;

  public AdditionalData(@NonNull String waybillNumber, int mileage) {
    validate(waybillNumber, mileage);
    this.waybillNumber = waybillNumber;
    this.mileage = mileage;
  }

  public AdditionalData(JsonObject jsonObject) {
    waybillNumber = (jsonObject.has("waybillNumber")) ? jsonObject.get("waybillNumber").getAsString() : "";
    mileage = (jsonObject.has("mileage")) ? jsonObject.get("mileage").getAsInt() : 0;
    validate(waybillNumber, mileage);
  }

  private void validate(@NonNull String waybillNumber, int mileage) {
    Objects.requireNonNull(waybillNumber, "Waybill number must be not null");
    if (waybillNumber.length() == 0)
      throw new IllegalArgumentException("Waybill number must be not empty");
    if (mileage < 0)
      throw new IllegalArgumentException("Mileage must be equal or greater than zero");
  }

  @Override
  public JsonObject serialize() {
    JsonObject result = new JsonObject();
    result.addProperty("waybillNumber", waybillNumber);
    result.addProperty("kilometers", mileage);
    return result;
  }

  public String getWaybillNumber() {
    return waybillNumber;
  }

  public int getMileage() {
    return mileage;
  }
}
