package features.log;

import android.content.Context;
import common.schedule.ScheduledExecutor;
import org.zeroturnaround.zip.ZipUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by BAFF on 13.01.2020.
 * Macsoftex
 */
public class LogStorageManager {
    private static final long STORE_TIME_PERIOD = 5000;
    private static final String STORE_LOG_ZIPPED_FILE_NAME = "app_Logs.zip";
    private static final String LOG_DIR = "Logs";
    private final String logFileName;
    private final LogWriter logWriter;
    private final Context context;
    private int previousLogSize;

    LogStorageManager(Context context, LogWriter logWriter) {
        this.logWriter = logWriter;
        this.context = context;
        logFileName = createLogFileName();
        ScheduledExecutor.schedule(this::checkNeedToStoreLog, 0, STORE_TIME_PERIOD);
    }

    private String createLogFileName(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy_HH:mm:ss", Locale.getDefault());
        return String.format("%s.log", sdf.format(new Date()));
    }

    public void checkNeedToStoreLog() {
        List<String> currentLog = logWriter.getCurrentLog();
        if (currentLog.size() == previousLogSize)
            return;
        storeLogToFile(currentLog);
    }

    private synchronized void storeLogToFile(List<String> currentLog) {
        List<String> log = new ArrayList<>(currentLog);
        File logsDirectory = context.getDir(LOG_DIR, Context.MODE_PRIVATE);
        File logFile = new File(logsDirectory, logFileName);
        StringBuilder logBuilder = new StringBuilder();
        previousLogSize = currentLog.size();

        for (String logItem: log) {
            logBuilder.append(logItem);
            logBuilder.append("\n");
        }
        saveTextFile(logFile, logBuilder.toString());
    }

    Optional<File> getLogsDirectoryZipped() {
        checkNeedToStoreLog();
        File logsDirectory = context.getDir(LOG_DIR, Context.MODE_PRIVATE);
        File zipFileName = new File(context.getFilesDir(), STORE_LOG_ZIPPED_FILE_NAME);
        if (Objects.requireNonNull(logsDirectory.listFiles()).length == 0)
            return Optional.empty();
        ZipUtil.pack(logsDirectory, zipFileName);
        return Optional.of(zipFileName);
    }

    public void purgeLogsAsync(){
        LogWriter.log(this.getClass().getSimpleName()+ " purgeLogsAsync: submit");
        ScheduledExecutor.submit(this::purgeLogs);
    }

    private void purgeLogs(){
        File logsDirectory = context.getDir(LOG_DIR, Context.MODE_PRIVATE);
        File[] logFiles = logsDirectory.listFiles();
        for(File logFile : logFiles) {
            logFile.delete();
        }
    }

    private void saveTextFile(File file, String text) {
        if (file==null || text==null)
            return;

        BufferedOutputStream outputStream = null;

        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(file));
            outputStream.write(text.getBytes());
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
