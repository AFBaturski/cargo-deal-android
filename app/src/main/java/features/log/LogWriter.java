package features.log;

import android.content.Context;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import com.google.gson.JsonObject;
import common.Utils;
import retrofit2.Response;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class LogWriter {
    private final LogStorageManager logStorageManager;
    private static LogWriter instance;
    private final List<String> log = new ArrayList<>();
    private final Object lock = new Object();
    private final Consumer<Throwable> logExceptionHandler;
    private final Consumer<String> logHandler;

    public String getLogAsString() {
        return log.stream().collect(Collectors.joining("\n"));
    }

    public interface LogHandler {
        void onLog(String log);
    }

    public static void logServerResponse(Response<JsonObject> response) {
        try {
            if (response.errorBody() != null) {
                log(response.code() + " " + response.errorBody().string());
            }
            else if (response.body().has("result")){
                log(response.code() + " " + response.body().get("result"));
            } else {
                log(String.valueOf(response.code()));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private LogWriter(Context context, @Nullable Consumer<Throwable> logExceptionHandler, @Nullable Consumer<String> logHandler) {
        logStorageManager = new LogStorageManager(context, this);
        this.logExceptionHandler = logExceptionHandler;
        this.logHandler = logHandler;
    }

    public static LogStorageManager getLogStorageManager() {
        return getInstance().logStorageManager;
    }

    public static LogWriter getInstance() {
        if (instance == null)
            throw new IllegalStateException("Must create instance LogWriter with newInstance() before using getInstance()");
        else
            return instance;
    }

    public static LogWriter createInstance(Context context, Consumer<Throwable> logExceptionHandler, Consumer<String> logHandler) {
        if (instance == null) {
            instance = new LogWriter(context, logExceptionHandler, logHandler);
        }
        return instance;
    }

    public static void log(String logMessage) {
            getInstance().logEvent(logMessage);
    }

    public static void logException(Throwable throwable) {
        getInstance().logEvent(throwable.getClass() + ": " +throwable.getMessage() + "\n"+ Arrays.toString(throwable.getStackTrace()));
        if (getInstance().logExceptionHandler != null)
            getInstance().logExceptionHandler.accept(throwable);
    }

    private void logEvent(String eventToLog) {
        Date dateTime = new java.util.Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String log = sdf.format(dateTime) +
                " : " +
                Utils.getThreadSignature() +
                " : " +
                eventToLog;
        synchronized (lock) {
            this.log.add(log);
        }
        if (getInstance().logHandler != null)
            getInstance().logHandler.accept(log);
    }

    List<String> getCurrentLog() {
        return log;
    }

    public void clearCurrentLog() {
        log.clear();
    }
}
