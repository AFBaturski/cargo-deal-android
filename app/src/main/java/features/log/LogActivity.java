package features.log;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.File;

import com.macsoftex.cargodeal.driver.R;

public class LogActivity extends AppCompatActivity {
  private TextView logTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_log);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    logTextView = findViewById(R.id.logTextView);
  }

  @Override
  protected void onStart() {
    super.onStart();
    LogPresenter.displayCurrentLog(logTextView);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    menu.add(0, MenuItems.RELOAD.ordinal(), MenuItems.RELOAD.ordinal(), MenuItems.RELOAD.toString());
    menu.add(0, MenuItems.SEND.ordinal(), MenuItems.SEND.ordinal(), MenuItems.SEND.toString());
    menu.add(0, MenuItems.CLEAR.ordinal(), MenuItems.CLEAR.ordinal(), MenuItems.CLEAR.toString());
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == MenuItems.CLEAR.ordinal()) {
      LogWriter.getInstance().clearCurrentLog();
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          logTextView.setText(null);
        }
      });
    }
    if (id == MenuItems.RELOAD.ordinal()) {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          LogPresenter.displayCurrentLog(logTextView);
        }
      });
    }

    if (id == MenuItems.SEND.ordinal()) {
      LogPresenter.shareLogFilesZipped(this);
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onSupportNavigateUp() {
    finish();
    return true;
  }

  private enum MenuItems {
    RELOAD("Обновить"), SEND("Отправить"), CLEAR("Очистить");
    private final String title;
    MenuItems(String title) {
      this.title = title;
    }

    @NonNull
    @Override
    public String toString() {
      return title;
    }
  }
}
