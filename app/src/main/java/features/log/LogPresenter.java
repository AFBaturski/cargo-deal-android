package features.log;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import com.macsoftex.cargodeal.driver.R;
import common.Utils;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Created by BAFF on 10.01.2020.
 * Macsoftex
 */

public class LogPresenter {
    public static void displayCurrentLog(TextView destination) {
        List<String> currentLog = LogWriter.getInstance().getCurrentLog();
        StringBuilder log = new StringBuilder();
        for (String logElement : currentLog) {
            log.append(logElement).append("\n");
        }
        destination.setText(log);
    }

    public static void shareLogFilesZipped(Context context){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Optional<File> logZipped = LogWriter.getLogStorageManager().getLogsDirectoryZipped();
        if (!logZipped.isPresent()){
            Utils.showToast("Лог файлы отсутствуют", context);
            return;
        }
        Uri sharedFile = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".LogProvider", logZipped.get());
        sharingIntent.setType("application/zip");
        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"Antiradar M Log");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, sharedFile);
        context.startActivity(Intent.createChooser(sharingIntent, "Отправить"));
    }

}
