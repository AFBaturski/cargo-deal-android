package features.transport_unit.storage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import api.ApiHelper;
import com.google.gson.JsonObject;
import common.Result;
import features.log.LogWriter;
import features.transport_unit.model.Trailer;
import features.transport_unit.model.Vehicle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.function.Consumer;

public class TransportRepository {
    private final ApiHelper api;

    public TransportRepository(ApiHelper api) {
        this.api = api;
    }

    public void makeTransportUnit(
            @NonNull String sessionId,
            @Nullable Vehicle selectedVehicle,
            @Nullable Trailer selectedTrailer,
            Consumer<Result> callback
    ) {
        JsonObject request = new JsonObject();
        if (selectedVehicle != null) {
            request.addProperty("vehicleId", selectedVehicle.getId());
        }
        if (selectedTrailer != null) {
            request.addProperty("trailerId", selectedTrailer.getId());
        }

        LogWriter.log(this.getClass().getSimpleName() + " : makeTransportUnit");
        api.getServerAPI().makeTransportUnit(sessionId, request).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogWriter.logServerResponse(response);
                try {
                    if (response.code() != 200) {
                        throw new Exception("response code = " + response.code());
                    }
                    if (response.body().has("result")) {
                        if (response.body().get("result").getAsJsonObject().get("result").getAsBoolean()) {
                            callback.accept(Result.success());
                        } else {
                            String errorCode = response.body().get("result").getAsJsonObject().get("error").getAsString();
                            throw new Exception(errorCode);
                        }
                    }
                } catch (Exception e) {
                    callback.accept(Result.failure(e));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t != null) {
                    callback.accept(Result.failure(t));
                } else {
                    callback.accept(Result.failure());
                }
            }
        });
    }
}
