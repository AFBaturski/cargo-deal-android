package features.transport_unit.model;

import java.util.Objects;

public abstract class Transport {
    public String getGosNumber() {
        return gosNumber;
    }

    public String getId() {
        return id;
    }

    public String getNumberUrl() {
        return numberUrl;
    }

    public double getTonnage() {
        return tonnage;
    }

    public static abstract class Builder <T extends Builder<T>> {
        private final String gosNumber;
        private final String id;
        private String numberUrl = "";
        private final double tonnage;

        protected Builder(String gosNumber, String id, double tonnage) {
            this.gosNumber = gosNumber;
            this.id = id;
            this.tonnage = tonnage;
        }

        public Builder numberUrl(String numberUrl) {
            this.numberUrl = numberUrl;
            return self();
        }

        protected abstract T self();
        protected abstract Transport build();
    }

    private final String gosNumber;
    private final String id;
    private final String numberUrl;
    private final double tonnage;

    public Transport(Builder builder) {
        this.gosNumber = builder.gosNumber;
        this.id = builder.id;
        this.numberUrl = builder.numberUrl;
        this.tonnage = builder.tonnage;
    }

    @Override
    public String toString() {
        return getGosNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transport that = (Transport) o;
        return Double.compare(that.getTonnage(), getTonnage()) == 0 && Objects.equals(getGosNumber(), that.getGosNumber()) && Objects.equals(getId(), that.getId()) && Objects.equals(getNumberUrl(), that.getNumberUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGosNumber(), getId(), getNumberUrl(), getTonnage());
    }
}
