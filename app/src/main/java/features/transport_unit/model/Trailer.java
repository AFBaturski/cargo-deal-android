package features.transport_unit.model;

public class Trailer extends Transport {
    public static class Builder extends Transport.Builder<Builder> {
        private String inspectionEndDate = "";

        Builder(String gosNumber, String id, double tonnage) {
            super(gosNumber, id, tonnage);
        }

        public Builder inspectionEndDate(String inspectionEndDate) {
            this.inspectionEndDate = inspectionEndDate;
            return this;
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        protected Transport build() {
            return new Trailer(this);
        }
    }

    private final String inspectionEndDate;

    public Trailer(Builder builder) {
        super(builder);
        this.inspectionEndDate = builder.inspectionEndDate;
    }

    public String getInspectionEndDate() {
        return inspectionEndDate;
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder()
                .append("Прицеп ")
                .append(getGosNumber())
                .append("\n");

        if (!inspectionEndDate.isEmpty()) {
            info.append("Техосмотр: по ")
                .append(inspectionEndDate)
                .append("\n");
        }
        return info.toString();
    }
}