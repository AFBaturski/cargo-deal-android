package features.transport_unit.model;

import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.JsonObject;
import storage.TransportContract;

public class TransportFactory {
    public static Transport createVehicle(@NonNull JsonObject vehicle) {
        return new Vehicle.Builder(
                vehicle.get("number").getAsString(),
                vehicle.get("objectId").getAsString(),
                vehicle.get("tonnage").getAsDouble())
                .inspectionEndDate(getInspectionEndDate(vehicle))
                .mtplEndDate(getMtplEndDate(vehicle)).build();
    }

    public static Transport createTrailer(@NonNull JsonObject trailer) {
        return new Trailer.Builder(
                trailer.get("number").getAsString(),
                trailer.get("objectId").getAsString(),
                trailer.get("tonnage").getAsDouble())
                .inspectionEndDate(getInspectionEndDate(trailer)).build();
    }

    private static @NonNull String getInspectionEndDate(JsonObject json) {
        if (!json.has("admissionData")) return "";
        if (!json.get("admissionData").getAsJsonObject().has("inspectionEndDate")) return "";
        return json.get("admissionData").getAsJsonObject().get("inspectionEndDate").getAsString();
    }

    private static @NonNull String getMtplEndDate(JsonObject json) {
        if (!json.has("admissionData")) return "";
        if (!json.get("admissionData").getAsJsonObject().has("mtplEndDate")) return "";
        return json.get("admissionData").getAsJsonObject().get("mtplEndDate").getAsString();
    }

    public static Transport create(String type, Cursor cursor) {
        if (type.equals(TransportContract.TYPE_VEHICLE)) {
            return new Vehicle.Builder(
                    cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_GOS_NOMER)),
                    cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_TRUCK_ID)),
                    cursor.getDouble(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_TONNAGE)))
                    .inspectionEndDate(cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_INSPECTION_END_DATE)))
                    .mtplEndDate(cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_MTPL_END_DATE)))
                    .build();
        } else if (type.equals(TransportContract.TYPE_TRAILER)) {
            return new Trailer.Builder(
                    cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_GOS_NOMER)),
                    cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_TRUCK_ID)),
                    cursor.getDouble(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_TONNAGE)))
                    .inspectionEndDate(cursor.getString(cursor.getColumnIndex(TransportContract.TransportEntry.COLUMN_INSPECTION_END_DATE)))
                    .build();
        }
        throw new IllegalArgumentException("Undefined tranport type");
    }
}
