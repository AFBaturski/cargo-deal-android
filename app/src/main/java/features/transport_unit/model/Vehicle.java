package features.transport_unit.model;

public class Vehicle extends Transport {
    public static class Builder extends Transport.Builder<Builder> {
        private String inspectionEndDate = "";
        private String mtplEndDate = "";

        Builder(String gosNumber, String id, double tonnage) {
            super(gosNumber, id, tonnage);
        }

        public Builder inspectionEndDate(String inspectionEndDate) {
            this.inspectionEndDate = inspectionEndDate;
            return this;
        }

        public Builder mtplEndDate(String mtplEndDate) {
            this.mtplEndDate = mtplEndDate;
            return this;
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        protected Transport build() {
            return new Vehicle(this);
        }
    }

    private final String inspectionEndDate;
    private final String mtplEndDate;

    public Vehicle(Builder builder) {
        super(builder);
        this.inspectionEndDate = builder.inspectionEndDate;
        this.mtplEndDate = builder.mtplEndDate;
    }

    public String getInspectionEndDate() {
        return inspectionEndDate;
    }

    public String getMtplEndDate() {
        return mtplEndDate;
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder()
                .append("Автомобиль ")
                .append(getGosNumber())
                .append("\n");

        if (!inspectionEndDate.isEmpty()) {
            info.append("Техосмотр: по ")
                .append(inspectionEndDate)
                .append("\n");
        }
        if (!mtplEndDate.isEmpty()) {
            info.append("ОСАГО: по ")
                .append(mtplEndDate)
                .append("\n");
        }
        return info.toString();
    }
}