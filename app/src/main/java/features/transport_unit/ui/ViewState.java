package features.transport_unit.ui;

import androidx.annotation.Nullable;
import common.Result;
import features.transport_unit.model.Trailer;
import features.transport_unit.model.Vehicle;

import java.util.Optional;

public class ViewState {
    private final Vehicle vehicle;
    private final Trailer trailer;
    private final Result result;
    public ViewState(
            @Nullable Vehicle vehicle,
            @Nullable Trailer trailer,
           Result result
    ) {
        this.vehicle = vehicle;
        this.trailer = trailer;
        this.result = result;
    }

    public Optional<Vehicle> getVehicle() {
        return Optional.ofNullable(vehicle);
    }

    public Optional<Trailer> getTrailer() {
        return Optional.ofNullable(trailer);
    }

    public Result getOperationState() {
        return result;
    }
}
