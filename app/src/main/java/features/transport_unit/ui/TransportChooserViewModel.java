package features.transport_unit.ui;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import api.TaskScheduler;
import di.CargodealApp;
import di.DependencyContainer;
import features.transport_unit.model.Trailer;
import features.transport_unit.model.Transport;
import features.transport_unit.model.Vehicle;
import features.transport_unit.storage.TransportRepository;
import model.Model;
import service.tasks.SendCoordsTask;

public class TransportChooserViewModel extends AndroidViewModel {
    private Vehicle selectedVehicle;
    private Trailer selectedTrailer;
    private final MutableLiveData<ViewState> _viewState = new MutableLiveData<>();
    private final DependencyContainer dependencyContext;
    private final Model model;
    private final TransportRepository repository;
    private final TaskScheduler scheduler;
    public LiveData<ViewState> viewState = _viewState;

    public TransportChooserViewModel(@NonNull Application application) {
        super(application);
        dependencyContext = ((CargodealApp) application).getDc();
        model = dependencyContext.getModel();
        repository = new TransportRepository(dependencyContext.getApi());
        scheduler = dependencyContext.getScheduler();
    }

    public void makeTransportUnit() {
        if (selectedVehicle == null) throw new IllegalStateException("Vehicle must be not null");
        if (selectedTrailer == null) throw new IllegalStateException("Trailer must be not null");

        String sessionId = model.getUserInfo().session_id;
        repository.makeTransportUnit(sessionId, selectedVehicle, selectedTrailer, result -> {
            if (result.isSuccess()) {
                scheduler.execute(new SendCoordsTask(dependencyContext));

                model.clearTransportTable();
                model.addTransport(selectedTrailer, selectedVehicle);
            }
            _viewState.setValue(new ViewState(selectedVehicle, selectedTrailer, result));
        });
    }

    public boolean isTrailerSelected() {
        return selectedTrailer != null;
    }

    public boolean isVehicleSelected() {
        return selectedVehicle != null;
    }

    public void selectTrailer(Transport transport) {
        this.selectedTrailer = (Trailer) transport;
        _viewState.setValue(new ViewState(selectedVehicle, selectedTrailer, null));
    }

    public void selectVehicle(Transport transport) {
        this.selectedVehicle = (Vehicle) transport;
        _viewState.setValue(new ViewState(selectedVehicle, selectedTrailer, null));
    }
}
