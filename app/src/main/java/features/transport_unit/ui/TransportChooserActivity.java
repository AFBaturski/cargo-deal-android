package features.transport_unit.ui;

import features.main.HomeActivity;
import activities.MainActivity;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import api.TaskScheduler;

import common.*;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;
import di.DependencyContainer;
import features.common.ApiErrorHelper;
import features.transport_unit.model.Trailer;
import features.transport_unit.model.Transport;
import features.transport_unit.model.Vehicle;
import fragments.VehicleListDialog;

/**
 * Created by Eugene on 23.02.2017.
 */

public class TransportChooserActivity extends AppCompatActivity {

    public static final String TAG = TransportChooserActivity.class.getSimpleName();
    public static final String APP_PREFERENCES = "cargo_settings";
    public static final String APP_PREFERENCES_TRAILER_ID = "trailer_id";
    public static final String APP_PREFERENCES_TRAILER_GOSNOMER = "trailer_gosnomer";
    public static final String APP_PREFERENCES_VEHICLE_ID = "vehicle_id";
    public static final String APP_PREFERENCES_VEHICLE_GOSNOMER = "vehicle_gosnomer";
    private static final String APP_PREFERENCES_VEHICLE_TONNAGE = "vehicle_tonnage";
    private static final String APP_PREFERENCES_TRAILER_TONNAGE = "trailer_tonnage";

    private Button submitButton;
    private Button openSettingsButton;
    private Button changeUserButton;
    private Button vehiclesButton;
    private Button trailersButton;
    private View permissionBlock;

    private final int _PERMISSION_REQUEST = 123;
    private String[] permissions = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private ApiHelper api;
    private DependencyContainer dependencyContext;
    private TaskScheduler scheduler;
    private TransportChooserViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dependencyContext = DependencyContainer.getInstance(this);
        viewModel = new ViewModelProvider(this).get(TransportChooserViewModel.class);
        observeViewState();
        api = dependencyContext.getApi();
        scheduler = dependencyContext.getScheduler();
        setContentView(R.layout.transport_chooser_activity);

        setTitle(getString(R.string.truck_chooser_title));

        findViewById(R.id.changeUser).setOnClickListener(buttonsListener);
        permissionBlock = findViewById(R.id.permission_block);
        submitButton = (Button) findViewById(R.id.button);
        findViewById(R.id.openSettingsButton).setOnClickListener(buttonsListener);

        vehiclesButton = (Button) findViewById(R.id.selectVehicleButton);
        trailersButton = (Button) findViewById(R.id.selectTrailerButton);
        vehiclesButton.setOnClickListener(buttonsListener);
        trailersButton.setOnClickListener(buttonsListener);

        submitButton.setOnClickListener(buttonsListener);
        submitButton.setEnabled(false);

        permissionBlock.setVisibility(View.GONE);
    }

    private void observeViewState() {
        viewModel.viewState.observe(this, viewState -> {
            viewState.getVehicle().ifPresent(this::updateVehicleInfo);
            viewState.getTrailer().ifPresent(this::updateTrailerInfo);

            if (viewState.getOperationState() == null) return;
            if (viewState.getOperationState().isSuccess()) {
                block_ui(false);
                hideProgressDialog();
                startActivity(new Intent(TransportChooserActivity.this, HomeActivity.class));
                finish();
            }
            if (viewState.getOperationState().isFailure()) {
                block_ui(false);
                failure(viewState.getOperationState().errorMessageOrNull());
                hideProgressDialog();
            }
        });
    }

    private void updateVehicleInfo(Vehicle vehicle) {
        vehiclesButton.setText(vehicle.getGosNumber());
        TextView inspectionEndDate = findViewById(R.id.vehicle_inspection_end_date);
        if (!vehicle.getInspectionEndDate().isEmpty()) {
            inspectionEndDate.setText(String.format("Техосмотр: по %s", vehicle.getInspectionEndDate()));
            inspectionEndDate.setVisibility(View.VISIBLE);
        } else {
            inspectionEndDate.setVisibility(View.GONE);
        }

        TextView mtplEndDate = findViewById(R.id.vehicle_mtpl_end_date);
        if (!vehicle.getMtplEndDate().isEmpty()) {
            mtplEndDate.setText(String.format("ОСАГО: по %s", vehicle.getMtplEndDate()));
            mtplEndDate.setVisibility(View.VISIBLE);
        } else {
            mtplEndDate.setVisibility(View.GONE);
        }
        submitButton.setEnabled(true);
    }

    private void updateTrailerInfo(Trailer trailer) {
        trailersButton.setText(trailer.getGosNumber());
        TextView inspectionEndDate = findViewById(R.id.trailer_inspection_end_date);
        if (!trailer.getInspectionEndDate().isEmpty()) {
            inspectionEndDate.setText(String.format("Техосмотр: по %s", trailer.getInspectionEndDate()));
            inspectionEndDate.setVisibility(View.VISIBLE);
        } else {
            inspectionEndDate.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener buttonsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.openSettingsButton:
                    startInstalledAppDetailsActivity();
                    break;
                case R.id.button:
                    submitUserChoice();
                    break;
                case R.id.changeUser:
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setAction(MainActivity.LOGOUT);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.selectVehicleButton:
                    VehicleListDialog dialog = VehicleListDialog.newInstance(true);
                    dialog.setOnItemSelectedListener(vehiclesSelect);
                    dialog.show(getSupportFragmentManager(), "select_vehicle");
                    break;
                case R.id.selectTrailerButton:
                    VehicleListDialog dialog1 = VehicleListDialog.newInstance(false);
                    dialog1.setOnItemSelectedListener(trailersSelect);
                    dialog1.show(getSupportFragmentManager(), "select_trailer");
                    break;
            }
        }
    };

    private VehicleListDialog.OnItemSelectedListener vehiclesSelect = new VehicleListDialog.OnItemSelectedListener() {
        @Override
        public void onItemSelected(Transport item) {
            viewModel.selectVehicle(item);

        }
    };

    private VehicleListDialog.OnItemSelectedListener trailersSelect = new VehicleListDialog.OnItemSelectedListener() {
        @Override
        public void onItemSelected(Transport item) {
            viewModel.selectTrailer(item);
        }
    };

    private void submitUserChoice(){
        if (!viewModel.isTrailerSelected()) {
            Utils.showToast(getString(R.string.select_trailer_message), getApplicationContext());
            return;
        }
        api.checkIsAppVersionAllowed(result -> {
            if (result) {
                block_ui(true);
                showProgressDialog(getString(R.string.data_exchange));
                viewModel.makeTransportUnit();
            } else {
                Utils.showToast(getString(R.string.version_deprecated), getApplicationContext());
            }
        });
    }

    private void block_ui(boolean enabled){
        enabled = !enabled;
        vehiclesButton.setEnabled(enabled);
        trailersButton.setEnabled(enabled);
        submitButton.setEnabled(enabled);
    }

    private void failure(String errorCode) {
        hideProgressDialog();
        String errorMessage = ApiErrorHelper.getErrorMessage(getResources(), errorCode, getString(R.string.choose_truck_failed));
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        block_ui(false);
    }

    private void requestPermissions(){
        boolean ok = true;
        for(String p : permissions){
            if(ContextCompat.checkSelfPermission(this, p) != PackageManager.PERMISSION_GRANTED){
                ok = false;
                break;
            }
        }
        if (!ok) {
            ActivityCompat.requestPermissions(this, permissions, _PERMISSION_REQUEST);
        } else {
            permissionBlock.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestPermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case _PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Ok!
                    permissionBlock.setVisibility(View.GONE);
                } else {
                    //Toast.makeText(this, R.string.please_grant_camera_permission, Toast.LENGTH_SHORT).show();
                    permissionBlock.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    public void startInstalledAppDetailsActivity() {
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(i);
    }

    private ProgressDialog progressDialog;
    public void showProgressDialog(String message){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void hideProgressDialog(){
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }
}
