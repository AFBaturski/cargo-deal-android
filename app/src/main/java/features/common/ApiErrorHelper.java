package features.common;

import android.content.res.Resources;
import api.ApiHelper;
import com.macsoftex.cargodeal.driver.R;

import java.util.Optional;

public class ApiErrorHelper {
    public static String getErrorMessage(Resources resources, String errorCode, String defaultMessage) {
        Optional<ApiHelper.ApiError> error = ApiHelper.ApiError.of(errorCode);
        if (!error.isPresent()) return defaultMessage;
        return resources.getString(error.get().getResId());
    }
}
