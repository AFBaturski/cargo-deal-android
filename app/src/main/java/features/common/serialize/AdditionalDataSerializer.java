package features.common.serialize;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import features.order.stage.model.AdditionalData;

public class AdditionalDataSerializer {
  public static JsonElement serialize(AdditionalData additionalData) {
    JsonObject result = new JsonObject();
    result.addProperty("waybillNumber", additionalData.getWaybillNumber());
    result.addProperty("kilometers", additionalData.getMileage());
    return result;
  }
}
