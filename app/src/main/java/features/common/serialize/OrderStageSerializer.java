package features.common.serialize;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import features.order.stage.model.OrderStage;

public class OrderStageSerializer {
  public static JsonElement serialize(OrderStage stage) {
    JsonObject result = new JsonObject();
    result.addProperty("stage", stage.getStageId().ordinal());
    result.addProperty("sendDelay", System.currentTimeMillis() - stage.getDate());
    if (stage.getLatitude() != 0)
      result.addProperty("latitude", stage.getLatitude());
    if (stage.getLongitude() != 0)
      result.addProperty("longitude", stage.getLongitude());
    if (stage.getTonnage() != 0)
      result.addProperty("tonnage", stage.getTonnage());
    if (stage.getAdditionalData() != null) {
      result.add("additionalData", AdditionalDataSerializer.serialize(stage.getAdditionalData()));
    }
    if (!stage.getComment().isEmpty())
      result.addProperty("comment", stage.getComment());

    return result;
  }
}
