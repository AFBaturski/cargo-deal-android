package features.common.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import features.log.LogWriter;

public class Dialogs {
    public static void showDialog(Context context, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(true);
            builder.setMessage(message);
            builder.setPositiveButton(android.R.string.ok, null);
            builder.show();
        } catch (Exception e) {
            LogWriter.logException(e);
        }
    }
}
