package features.common.crash;

import androidx.annotation.NonNull;
import features.log.LogWriter;

public class CrashHandler implements Thread.UncaughtExceptionHandler {
  private Thread.UncaughtExceptionHandler defaultHandler;

  public CrashHandler(Thread.UncaughtExceptionHandler defaultHandler) {
    this.defaultHandler = defaultHandler;
  }

  @Override
  public void uncaughtException(@NonNull Thread t, @NonNull Throwable e) {
    LogWriter.logException(e);
    LogWriter.getLogStorageManager().checkNeedToStoreLog();
    defaultHandler.uncaughtException(t, e);
  }
}
