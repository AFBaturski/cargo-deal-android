package features.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public interface JsonSerializable {
  JsonElement serialize();
}
