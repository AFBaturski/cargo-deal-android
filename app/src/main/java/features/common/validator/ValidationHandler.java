package features.common.validator;

import androidx.annotation.StringRes;

public interface ValidationHandler {
    void onInvalidData(@StringRes int helperTextId);
}
