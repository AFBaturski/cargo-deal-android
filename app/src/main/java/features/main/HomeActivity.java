package features.main;

import activities.MainActivity;
import activities.OrdersActivity;
import activities.ViewOrderActivity;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import api.TaskScheduler;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;

import common.*;
import di.DependencyContainer;
import features.common.dialog.Dialogs;
import features.log.LogActivity;
import features.order.model.Order;
import features.order.OrderManager;
import features.transport_unit.model.Trailer;
import features.transport_unit.model.Vehicle;
import location.LocationManager;
import location.LocationReceiver;
import model.*;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import fragments.GoogleMapFragment;
import fragments.OrdersFragment;
import storage.TransportContract;
import api.ServerApiManager;
import service.CargoDealService;
import com.macsoftex.cargodeal.driver.R;
import service.tasks.SendCoordsTask;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = HomeActivity.class.getSimpleName();
    private static final boolean NO_SILENT = false;
    GoogleMap mGoogleMap;

    private TextView fio_view, gosnomer_view, my_status;
    private TextView nav_orders;
    private NavigationView navigationView;
    private ImageView arrowImage;
    private AlertDialog confirmShutdownDialog;
    private ServerApiManager mServerAPIManager;
    private CargoDealService mService;
    private TaskScheduler scheduler;
    boolean mBound = false;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            CargoDealService.LocalBinder binder = (CargoDealService.LocalBinder) iBinder;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };
    private DependencyContainer dependencyContext;
    private LocationManager locationManager;
    private LocationReceiver locationReceiver;
    private TextView mtplEndDateView;
    private TextView inspectionEndDateView;
    private Model model;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationManager.REQUEST_CHECK_SETTINGS && resultCode == RESULT_CANCELED) {
            locationManager.checkLocationSettings(HomeActivity.this);
        }
    }

    public static PendingIntent newPendingIntent(Context context) {
        return TaskStackBuilder.create(context)
                .addNextIntent(new Intent(context, HomeActivity.class))
                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    private int headerClickCount;
    private static final int DEVELOPER_MODE_CLICK_COUNT = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dependencyContext = DependencyContainer.getInstance(this);
        scheduler = dependencyContext.getScheduler();
        model = dependencyContext.getModel();
        locationManager = dependencyContext.getLocationManager();
        locationManager.checkLocationSettings(HomeActivity.this);
        mServerAPIManager = new ServerApiManager(HomeActivity.this, getApplicationContext(), NO_SILENT);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //Если меню закрылось и были видно меню статусов - убираем его
                if (navigationView.getMenu().findItem(R.id.nav_map) == null) {
                    arrowImage.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp);
                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.activity_home_drawer);
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        nav_orders = (TextView) navigationView.getMenu().findItem(R.id.nav_orders).getActionView();
        nav_orders.setGravity(Gravity.CENTER_VERTICAL);
        nav_orders.setTypeface(null, Typeface.BOLD);
        nav_orders.setTextColor(getResources().getColor(R.color.blue_700));

        initNavigationViewHeader(navigationView);

        if(savedInstanceState == null){
            mServerAPIManager.checkData();//загрузка актуальной информации о заказах, занятом грузовике
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_home, GoogleMapFragment.newInstance(), GoogleMapFragment.TAG)
                    .commit();
            getSupportActionBar().setTitle(R.string.map_fragment_title);
        } else {
            getSupportActionBar().setTitle(savedInstanceState.getString("title", ""));
        }

        routeLogic(); //обработка перехода на конкнретный фрагмент при запуске с параметрами

        if(savedInstanceState != null)
            if(savedInstanceState.getBoolean("shutdown_dialog_visible", false))
                showAlertDialog(savedInstanceState.getString("shutdown_dialog_title"), savedInstanceState.getString("shutdown_dialog_body"), savedInstanceState.getInt("dialogType"));
    }

    private void initNavigationViewHeader(NavigationView navigationView) {
        View headerLayout = navigationView.getHeaderView(0);
        headerLayout.setOnClickListener(v -> {
            headerClickCount++;
            if (headerClickCount == DEVELOPER_MODE_CLICK_COUNT) {
                Utils.showToast(getString(R.string.developer_mode_activated), getApplicationContext());
                navigationView.getMenu().findItem(R.id.nav_log).setVisible(true);
            }
        });
        fio_view = (TextView) headerLayout.findViewById(R.id.fio_view);
        gosnomer_view = (TextView) headerLayout.findViewById(R.id.gosnomer_view);
        arrowImage = headerLayout.findViewById(R.id.arrowImage);
        my_status = (TextView) headerLayout.findViewById(R.id.my_status);

        RelativeLayout layout = headerLayout.findViewById(R.id.relativeLayout);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int menuId;
                //Смена меню в боковой шторке
                if (navigationView.getMenu().findItem(R.id.nav_map) != null) {
                    menuId = R.menu.activity_home_drawer_status;
                    arrowImage.setImageResource(R.drawable.ic_arrow_drop_up_white_24dp);
                }
                else {
                    menuId = R.menu.activity_home_drawer;
                    arrowImage.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp);
                }
                navigationView.getMenu().clear();
                navigationView.inflateMenu(menuId);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        routeLogic(); //обработка перехода на конкнретный фрагмент при запуске с параметрами
    }

    private void routeLogic(){
        List<Order> orders = OrderManager.getInstance().getOrders();
        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
            if(extras.containsKey("fragmentName"))
            {
                if("my_orders".equals(extras.getString("fragmentName")) && orders.size() > 0) {
                    Intent intent = OrdersActivity.newIntent(getBaseContext(), orders, orders.get(0).getNumber(), CargoDealSystem.ORDERS_LIST);
                    startActivity(intent);
                    navigationView.setCheckedItem(R.id.nav_orders);
                } else if ("map".equals(extras.getString("fragmentName"))){
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                            .replace(R.id.content_home, GoogleMapFragment.newInstance(), GoogleMapFragment.TAG)
                            .addToBackStack(GoogleMapFragment.TAG)
                            .commit();
                    navigationView.setCheckedItem(R.id.nav_map);
                }
                extras.remove("fragmentName");
            }
        }
    }

    private void showMyOrderIfExist() {
        if (CargoDealSystem.getInstance().isNeedShowMyOrder()) {
            Model.MyOrderInfo alreadyHaveActiveOrder = Model.getInstance().getMyOrderInfo();
            if (alreadyHaveActiveOrder != null) {
                CargoDealSystem.getInstance().setNeedShowMyOrder(false);
                Intent intent = ViewOrderActivity.newIntent(getBaseContext(), alreadyHaveActiveOrder.getAsOrder());
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        CargoDealService.startService(getApplicationContext());
        scheduler.execute(new SendCoordsTask(dependencyContext));

        Intent intent = new Intent(this, CargoDealService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

        updateStatus();

        Model.UserInfo info = Model.getInstance().getUserInfo();
        if(info != null){
            fio_view.setText(info.fio);
        }

        Vehicle vehicle = (Vehicle) Model.getInstance().getSelectedTransport(TransportContract.TYPE_VEHICLE);
        Trailer trailer = (Trailer) Model.getInstance().getSelectedTransport(TransportContract.TYPE_TRAILER);

        String transportUnitGosNumber = "";
        if(vehicle != null) {
            if (trailer == null)
                transportUnitGosNumber = vehicle.getGosNumber();
            else
                transportUnitGosNumber = vehicle.getGosNumber() + " - " + trailer.getGosNumber();
        }
        gosnomer_view.setText(transportUnitGosNumber);

        if (XiaomiDialog.isNeedToShow(this)) {
            XiaomiDialog.show(this, new OnCompletion() {
                @Override
                public void onComplete(boolean result) {
                    if (! result)
                        XiaomiDialog.show(getParent(), this);
                }
            });
        }
        showMyOrderIfExist();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            /*
                Последний добавленный фрагмент - в конце
             */
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if(count > 1) {
                String name = getSupportFragmentManager().getBackStackEntryAt(count-2).getName();
                if (GoogleMapFragment.TAG.equals(name)) {
                    navigationView.setCheckedItem(R.id.nav_map);
                    updateGoogleMap();
                } else if (OrdersFragment.TAG.equals(name)) {
                    navigationView.setCheckedItem(R.id.nav_orders);
                }
            } else if (count == 1){
                updateGoogleMap();
            }

            super.onBackPressed();
        }
    }

    private void updateGoogleMap() {
        GoogleMapFragment googleMapFragment = (GoogleMapFragment)getSupportFragmentManager().findFragmentByTag(GoogleMapFragment.TAG);
        if (googleMapFragment != null) {
            googleMapFragment.clearMap();
            googleMapFragment.updateVehicles();
        }
    }

    private boolean isFragmentAlreadyDisplayed(String fragmentTag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (fragment != null && fragment.isVisible()) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        List<Order> orders;
        // Handle navigation view item clicks here.
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_home_drawer);
        arrowImage.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp);

        int id = item.getItemId();

        switch (id) {
            case R.id.nav_log:
                navigationView.setCheckedItem(R.id.nav_log);
                Intent intent = new Intent(getApplicationContext(), LogActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_map:
                navigationView.setCheckedItem(R.id.nav_map);
                    getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                        .replace(R.id.content_home, GoogleMapFragment.newInstance(), GoogleMapFragment.TAG)
                        .addToBackStack(GoogleMapFragment.TAG)
                        .commit();
                break;

            case R.id.nav_orders:
                navigationView.setCheckedItem(R.id.nav_orders);
                orders =  OrderManager.getInstance().getOrders();
                //Если только один заказ, то не выводим список заказов
                if (orders.size() == 1) {
                    Intent logIntent = OrdersActivity.newIntent(getBaseContext(), orders, orders.get(0).getNumber(), CargoDealSystem.ORDERS_LIST);
                    startActivity(logIntent);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                } else {
                        getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                            .replace(R.id.content_home, OrdersFragment.newInstance(orders, CargoDealSystem.ORDERS_LIST), OrdersFragment.TAG)
                            .addToBackStack(OrdersFragment.TAG)
                            .commit();
                }
                break;

            case R.id.nav_finished_orders:
                navigationView.setCheckedItem(R.id.nav_finished_orders);
                orders = FinishedOrdersList.getInstance().getOrders();
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                            .replace(R.id.content_home, OrdersFragment.newInstance(orders, CargoDealSystem.FINISHED_ORDERS_LIST), OrdersFragment.TAG)
                            .addToBackStack(null)
                            .commit();
                break;

            case R.id.nav_turnoff:
                exitApp();
                break;

            case R.id.nav_relogin:
                logout();
                break;

            case R.id.nav_tu_info:
                showTransportUnitInfo();
                break;

            case R.id.nav_crew_status_not_ready:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_NOT_READY);
                break;

            case R.id.nav_crew_status_ready:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_READY);
                break;

            case R.id.nav_crew_status_work:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_WORK);
                break;

            case R.id.nav_crew_status_rest:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_REST);
                break;

            case R.id.nav_crew_status_crash:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_CRASH);
                break;

            case R.id.nav_crew_status_invisible:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_INVISIBLE);
                break;

            case R.id.nav_crew_status_under_repair:
                mServerAPIManager.setDriverStatus(DriverStatus.CREW_STATUS_UNDER_REPAIR);
                break;
            }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void showTransportUnitInfo() {
        Vehicle vehicle = (Vehicle) model.getSelectedTransport(TransportContract.TYPE_VEHICLE);
        Trailer trailer = (Trailer) model.getSelectedTransport(TransportContract.TYPE_TRAILER);
        String info = vehicle.toString() + "\n" + trailer.toString();
        Dialogs.showDialog(this, info);
    }

    private void logout() {
        CargoDealSystem.getInstance().logout();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateStatus();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MainActivity.REQUEST_LOCATION_ACCESS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                            .replace(R.id.content_home, GoogleMapFragment.newInstance(), GoogleMapFragment.TAG)
                            .commit();
                } else {
                    Utils.showToast(getString(R.string.need_location_access_msg), getApplicationContext());
                }
                break;
        }
    }
    private void exitApp(){
        mService.killApp();
    }

    public void updateStatus(){
        my_status.setText(getString(Model.getInstance().getDriverStatus().titleId()));

        int ordersCount = OrderManager.getInstance().getOrders().size();
        if (OrderManager.getInstance().getActiveOrder() != null)
            ordersCount--;

        if (ordersCount > 0)
            nav_orders.setText(String.format("+%s", String.valueOf(ordersCount)));
        else
            nav_orders.setText("");
    }

    public void showAlertDialog(String title, String message, final int dialogType){
        if(confirmShutdownDialog != null && confirmShutdownDialog.isShowing()){
            confirmShutdownDialog.dismiss();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Light_Dialog);
        confirmShutdownDialog = builder
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //В обоих случая- и при выходе и при закрытии приложения удаляем всю инфу о заказе
                        /*Model.getInstance().saveCrewStatus(CREW_STATUS_NOT_READY);
                        Model.getInstance().deleteCurrentOrder();
                        if(dialogType == DIALOG_SHUTDOWN_CONFIRM) {
                            exitApp();
                            finish();
                        } else {
                            exitApp();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setAction(MainActivity.LOGOUT);
                            startActivity(intent);
                            finish();
                        }
                        dialogInterface.dismiss();*/
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        confirmShutdownDialog.show();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if (mBound){
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(confirmShutdownDialog != null && confirmShutdownDialog.isShowing())
            confirmShutdownDialog.dismiss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleStatusChanged(MessageEvent event) {
        if (event.getEventType() == MessageEvent.NEED_UPDATE_STATUS)
            updateStatus();
        if (event.getEventType() == MessageEvent.DRIVER_STATUS_CHANGED)
            updateStatus();
        else if (event.getEventType() == MessageEvent.NEED_RELOGIN)
            logout();
        else if (event.getEventType() == MessageEvent.ORDER_STATUS_CHANGED)
            Utils.showSnackBar(getString(R.string.order_status_set_success), this);
    }
}
