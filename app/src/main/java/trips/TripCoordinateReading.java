package trips;

import java.util.Date;

import common.Coord;

public class TripCoordinateReading {
    private Coord coordinate;
    private long elapsedRealTimeNanos;
    private double averageSpeed;

    public TripCoordinateReading(Coord coordinate, long elapsedRealTimeNanos, double averageSpeed) {
        this.coordinate = coordinate;
        this.elapsedRealTimeNanos = elapsedRealTimeNanos;
        this.averageSpeed = averageSpeed;
    }

    public Coord getCoordinate() {
        return coordinate;
    }

    public long getElapsedRealTimeNanos() {
        return elapsedRealTimeNanos;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }
}
