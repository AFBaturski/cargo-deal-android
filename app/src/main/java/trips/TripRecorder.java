package trips;

import android.os.SystemClock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import common.Coord;
import location.Location;
import model.Model;
import trips.filters.MultipleTripCoordinateFilter;
import trips.filters.TripCoordinateFilter;

public class TripRecorder {
  private static final double CHECK_POINT_STEP = 5.0;
  private final Model model;
  private final double checkpointStep;
  private final TripCoordinateFilter coordinateFilter;
  private final ArrayList<Coord> tripCheckpoints;

    public TripRecorder(Model model) {
    this.model = model;
    coordinateFilter = MultipleTripCoordinateFilter.defaultFilter();
    checkpointStep = CHECK_POINT_STEP;
    tripCheckpoints = new ArrayList<>();
  }

  synchronized public List<Coord> tripCheckpoints() {
    if (tripCheckpoints == null)
      return new ArrayList<>();

    return Collections.synchronizedList(new ArrayList<Coord>(tripCheckpoints));
  }

  public double getCheckpointStep() {
    return checkpointStep;
  }

  public TripCoordinateFilter getCoordinateFilter() {
    return coordinateFilter;
  }

  private void appendTripCheckpoint(Coord checkpoint) {
    tripCheckpoints.add(checkpoint);
  }

  synchronized public void processUserPosition(Location location) {
    if (location == null)
      return;

    TripCoordinateReading coordinateReading = new TripCoordinateReading(location.getCoordinate(), SystemClock.elapsedRealtimeNanos(), location.getSpeed());
    boolean coordinateCheckResult = getCoordinateFilter().checkCoordinateWithReading(coordinateReading);

    if (tripCheckpoints.size() > 0) {
      if (coordinateCheckResult) {
        double distance = location.getCoordinate().distanceTo(tripCheckpoints.get(tripCheckpoints.size() - 1));

        if (distance > getCheckpointStep()) {
          appendTripCheckpoint(location.getCoordinate());
          model.saveUserPosition(location);
        }
      }
    } else {
      appendTripCheckpoint(location.getCoordinate());
      model.saveUserPosition(location);
    }

  }
}
