package trips.filters;


import trips.TripCoordinateReading;

public interface TripCoordinateFilter {
    boolean checkCoordinateWithReading(TripCoordinateReading reading);
    void clearState();
}
