package trips.filters;

import features.log.LogWriter;
import trips.TripCoordinateReading;

public class DistanceTripCoordinateFilter implements TripCoordinateFilter {
    private double maximumDistanceFromValidReading, maximumDistanceFromInvalidReading;
    private TripCoordinateReading lastReading, lastValidReading;

    public DistanceTripCoordinateFilter() {
        maximumDistanceFromValidReading = 200;
        maximumDistanceFromInvalidReading = 100;
    }

    public double getMaximumDistanceFromValidReading() {
        return maximumDistanceFromValidReading;
    }

    public void setMaximumDistanceFromValidReading(double maximumDistanceFromValidReading) {
        this.maximumDistanceFromValidReading = maximumDistanceFromValidReading;
    }

    public double getMaximumDistanceFromInvalidReading() {
        return maximumDistanceFromInvalidReading;
    }

    public void setMaximumDistanceFromInvalidReading(double maximumDistanceFromInvalidReading) {
        this.maximumDistanceFromInvalidReading = maximumDistanceFromInvalidReading;
    }

    @Override
    public boolean checkCoordinateWithReading(TripCoordinateReading reading) {
        boolean validCoordinate = true;

        if (lastReading != null) {
            double distanceFromInvalidReading = lastReading.getCoordinate().distanceTo(reading.getCoordinate());
            double distanceFromValidReading = lastValidReading.getCoordinate().distanceTo(reading.getCoordinate());
            validCoordinate = distanceFromInvalidReading <= getMaximumDistanceFromInvalidReading() || distanceFromValidReading <= getMaximumDistanceFromValidReading();
            if (!validCoordinate)
                LogWriter.log("INCORRECT DATA. distanceFromInvalidReading:" + distanceFromInvalidReading +" distanceFromValidReading:" + distanceFromValidReading);
        }

        lastReading = reading;

        if (validCoordinate)
            lastValidReading = reading;

        return validCoordinate;
    }

    @Override
    public void clearState() {
        lastReading = null;
        lastValidReading = null;
    }
}
