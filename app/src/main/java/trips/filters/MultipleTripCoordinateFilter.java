package trips.filters;

import java.util.ArrayList;
import java.util.List;

import trips.TripCoordinateReading;

public class MultipleTripCoordinateFilter implements TripCoordinateFilter {
    private List<TripCoordinateFilter> filters;

    public static MultipleTripCoordinateFilter nullFilter() {
        return new MultipleTripCoordinateFilter();
    }

    public static MultipleTripCoordinateFilter defaultFilter() {
        DistanceTripCoordinateFilter distanceTripCoordinateFilter = new DistanceTripCoordinateFilter();
        TimeTripCoordinateFilter timeTripCoordinateFilter = new TimeTripCoordinateFilter();

        ArrayList<TripCoordinateFilter> filterList = new ArrayList<>();
        filterList.add(distanceTripCoordinateFilter);
        filterList.add(timeTripCoordinateFilter);

        return new MultipleTripCoordinateFilter(filterList);
    }

    public MultipleTripCoordinateFilter() {
        this.filters = new ArrayList<>();
    }

    public MultipleTripCoordinateFilter(List<TripCoordinateFilter> filters) {
        this.filters = filters;
    }

    public List<TripCoordinateFilter> getFilters() {
        return filters;
    }

    @Override
    public boolean checkCoordinateWithReading(TripCoordinateReading reading) {
        boolean validCoordinate = true;

        for (TripCoordinateFilter filter : getFilters()) {
            boolean checkResult = filter.checkCoordinateWithReading(reading);
            validCoordinate = validCoordinate && checkResult;
        }

        return validCoordinate;
    }

    @Override
    public void clearState() {
        for (TripCoordinateFilter filter : getFilters())
            filter.clearState();
    }
}
