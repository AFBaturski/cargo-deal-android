package trips.filters;

import features.log.LogWriter;
import trips.TripCoordinateReading;

public class TimeTripCoordinateFilter implements TripCoordinateFilter {
    private long maximumTimeInterval;
    private
    TripCoordinateReading lastReading;

    public TimeTripCoordinateFilter() {
        maximumTimeInterval = 120 * 1000;
    }

    public long getMaximumTimeInterval() {
        return maximumTimeInterval;
    }

    public void setMaximumTimeInterval(long maximumTimeInterval) {
        this.maximumTimeInterval = maximumTimeInterval;
    }

    @Override
    public boolean checkCoordinateWithReading(TripCoordinateReading reading) {
        boolean validCoordinate = true;

        if (lastReading != null) {
            long timeInterval = Math.abs((reading.getElapsedRealTimeNanos() - lastReading.getElapsedRealTimeNanos())/1000000);
            validCoordinate = timeInterval <= getMaximumTimeInterval();
            if (!validCoordinate)
                LogWriter.log("INCORRECT DATA. TIME INTERVAL:" + timeInterval  );
        }

        lastReading = reading;
        return validCoordinate;
    }

    @Override
    public void clearState() {
        lastReading = null;
    }
}
