package viewmodels;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import api.TaskScheduler;
import api.requests.OrderStageRequest;
import com.macsoftex.cargodeal.driver.R;
import common.*;
import common.exceptions.InternalErrorException;
import di.CargodealApp;
import di.DependencyContainer;
import features.log.LogWriter;
import features.order.OrderManager;
import features.order.stage.model.OrderStage;
import features.order.stage.OrderStageFactory;
import location.LocationManager;
import model.*;
import org.greenrobot.eventbus.EventBus;
import service.tasks.SetOrderStagePhotoTask;
import service.tasks.SetOrderStageTask;

import java.io.IOException;
import java.util.Map;

public class PhotoCommentViewModel extends AndroidViewModel {
    public MutableLiveData<Event<String>> error = new MutableLiveData<>();
    public MutableLiveData<Event<StageOperationState>> state = new MutableLiveData<>();
    private DependencyContainer dependencyContext;
    private Model model;
    private Context context;
    private OrderStage orderStage;
    private TaskScheduler scheduler;
    private LocationManager locationManager;

    public PhotoCommentViewModel(@NonNull Application application) {
        super(application);
        dependencyContext = ((CargodealApp) application).getDc();
        context = dependencyContext.getContext();
        model = dependencyContext.getModel();
        scheduler = dependencyContext.getScheduler();
        locationManager = dependencyContext.getLocationManager();
    }

    public void sendOrderStage(Uri imageUri, Map<String, String> params) {
        Model.MyOrderInfo orderInfo = model.getMyOrderInfo();
        int stageId = Integer.parseInt(params.get("stageId"));
        String tonnage = params.get("tonnage");
        String comment = params.get("comment");
        String waybillNumber = params.getOrDefault("waybillNumber","");
        String mileage = params.getOrDefault("mileage","-1");

        if (orderInfo.getAsOrder() instanceof CarriageOrder) {
            if (!validateCarriageOrderStage(imageUri, tonnage, stageId, mileage, waybillNumber))
                return;
        } else if (orderInfo.getAsOrder() instanceof DealerOrder) {
            if (!validateDealerOrderStage(imageUri, tonnage, stageId))
                return;
        }

        double longitude, latitude;
        longitude = (LocationManager.getLastDriverLocation() != null)
            ? LocationManager.getLastDriverLocation().getCoordinate().getLongitude()
            : model.getUserInfo().longitude;

        latitude = (LocationManager.getLastDriverLocation() != null)
            ? LocationManager.getLastDriverLocation().getCoordinate().getLatitude()
            : model.getUserInfo().latitude;

        double tonnageValue = parseTonnage(tonnage);
        int mileageValue = parseMileage(mileage);

        orderStage = OrderStageFactory.create(
            orderInfo.orderType,
            stageId,
            System.currentTimeMillis(),
            comment,
            latitude,
            longitude,
            tonnageValue,
            waybillNumber,
            mileageValue
            );

        state.postValue(new Event<>(StageOperationState.LOADING));

        checkNeedToStoreStagePhoto(imageUri, stageId);
        OrderStageRequest request = new OrderStageRequest(dependencyContext, orderStage, result -> {
            if (result.isSuccess()) {
                successChangeOrderStatusHandler();
            } else {
                failChangeOrderStatusHandler(result.exceptionOrNull());
            }
        });
        scheduler.execute(request.new Task());
    }

    private void successChangeOrderStatusHandler() {
        if (Model.isStageWithPhoto(orderStage.getStage()))
            scheduler.execute(new SetOrderStagePhotoTask(dependencyContext));

        if (orderStage.getStage() == Model.ORDER_STAGE_FINAL) {
            OrderManager.getInstance().removeOrder(OrderManager.getInstance().getActiveOrder());
            locationManager.changeLocationUpdatesInterval();
            state.postValue(new Event<>(StageOperationState.ORDER_COMPLETED));
        } else {
            saveCurrentOrderStatus();
            state.postValue(new Event<>(StageOperationState.STAGE_SET));
        }
    }

    private void failChangeOrderStatusHandler(Throwable exception) {
        LogWriter.log(exception.getLocalizedMessage());
        state.postValue(new Event<>(StageOperationState.ERROR));
        if (exception instanceof InternalErrorException) {
            error.postValue(new Event<>(exception.getLocalizedMessage()));
        } else {
            error.postValue(new Event<>(context.getString(R.string.status_will_set_later)));
            sendOrderStatusToQueue();
        }
    }

    private void sendOrderStatusToQueue() {
        if (orderStage != null) {
            saveCurrentOrderStatus();
            putOrderStatusToDelayedSubmit();

            if (orderStage.getStage() == Model.ORDER_STAGE_FINAL) {
                setOrderCompletedLocally();
            } else {
                setNextOrderStageLocally();
            }
        }
    }

    private void setNextOrderStageLocally() {
        model.deleteOrderNextStages();
        int nextStage = orderStage.getStage() + 1;
        String[] stages = context.getResources().getStringArray(R.array.orderNextStages);
        model.insertOrderNextStage(new Model.OrderNextStage(stages[nextStage], nextStage));
    }

    private void setOrderCompletedLocally() {
        model.deleteCurrentOrder();
        OrderManager.getInstance().removeOrder(OrderManager.getInstance().getActiveOrder());
        model.saveCrewStatus(DriverStatus.CREW_STATUS_READY);
        Handler handler = new Handler();
        handler.postDelayed(() -> EventBus.getDefault().post(new MessageEvent(null, MessageEvent.ORDER_COMPLETE, null)),1000);
        locationManager.changeLocationUpdatesInterval();
    }

    private void putOrderStatusToDelayedSubmit() {
        model.insertMyOrderStage(orderStage);
        scheduler.execute(new SetOrderStageTask(dependencyContext, new EmptyCompletion()));
    }

    private void saveCurrentOrderStatus() {
        Model.MyOrderInfo orderInfo = model.getMyOrderInfo();
        if (orderStage == null || orderInfo == null)
            return;

        orderInfo.orderStatusId = orderStage.getStage();
        orderInfo.orderStatusDateTime = orderStage.getDate();
        String[] stages = context.getResources().getStringArray(R.array.orderNextStages);
        orderInfo.orderStatusTitle = stages[orderStage.getStage()];
        model.saveMyOrderInfo(orderInfo);
    }

    private boolean validateDealerOrderStage(Uri imageUri, String tonnage, int stageId) {
        if (imageUri == null && (stageId == Model.ORDER_STAGE_LOADING_COMPLETE || stageId == Model.ORDER_STAGE_FINAL)) {
            error.postValue(new Event<>(context.getString(R.string.waybillPhotoHelperText)));
            return false;
        }

        if (tonnage.length() == 0 ){
            switch (stageId){
                case Model.ORDER_STAGE_LOADING_COMPLETE:
                    error.postValue(new Event<>(context.getString(R.string.tonnageLoadingHelperText)));

                    return false;
                case Model.ORDER_STAGE_FINAL:
                    error.postValue(new Event<>(context.getString(R.string.tonnageUnloadingHelperText)));
                    Utils.showToast(context.getString(R.string.tonnageUnloadingHelperText), context);
                    return false;
            }
        }

        return true;
    }

    private boolean validateCarriageOrderStage(Uri imageUri, String tonnage, int stageId, String mileage, String waybillNumber) {
        if (imageUri == null && (stageId == Model.ORDER_STAGE_LOADING_COMPLETE || stageId == Model.ORDER_STAGE_FINAL)) {
            error.postValue(new Event<>("Снимок документа обязателен. Нажмите кнопку Прикрепить фотографию и сделайте снимок"));
            return false;
        }

        if (tonnage.length() == 0 ){
            switch (stageId){
                case Model.ORDER_STAGE_LOADING_COMPLETE:
                    error.postValue(new Event<>(context.getString(R.string.tonnageLoadingHelperText)));

                    return false;
                case Model.ORDER_STAGE_FINAL:
                    error.postValue(new Event<>(context.getString(R.string.tonnageUnloadingHelperText)));
                    Utils.showToast(context.getString(R.string.tonnageUnloadingHelperText), context);
                    return false;
            }
        }

        if (waybillNumber.length() == 0 ){
            if (stageId == Model.ORDER_STAGE_LOADING_COMPLETE) {
                error.postValue(new Event<>(context.getString(R.string.wayBillNumberHelper)));
                return false;
            }
        }

        if (mileage.length() == 0 ){
            if (stageId == Model.ORDER_STAGE_LOADING_COMPLETE) {
                error.postValue(new Event<>(context.getString(R.string.mileageHelper)));
                return false;
            }
        }

        return true;
    }

    private void checkNeedToStoreStagePhoto(Uri imageUri, int stage) {
        MyOrderStagePhoto stagePhoto;

        if (Model.isStageWithPhoto(stage) && imageUri != null) {
            String tripId = model.getMyOrderInfo().objectId;
            String destinationImagePath = tripId + "_" + stage + ".jpg";
            try {
                Utils.copy(context, imageUri, destinationImagePath);
            } catch (IOException e) {
                LogWriter.logException(e);
            }
            stagePhoto = new MyOrderStagePhoto(stage, tripId, destinationImagePath);
            model.saveMyOrderStagePhoto(stagePhoto);
        }
    }

    private double parseTonnage(String tonnage) {
        if (tonnage.isEmpty())
            return 0;

        double tonnageValue = Double.parseDouble(tonnage);
        if (tonnageValue > 999.99){
            return tonnageValue/1000;
        }
        return tonnageValue;
    }

    private int parseMileage(String mileage) {
        if (mileage.isEmpty())
            return 0;
        return Integer.parseInt(mileage);
    }
}
