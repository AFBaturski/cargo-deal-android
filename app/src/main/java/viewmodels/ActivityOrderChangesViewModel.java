package viewmodels;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import api.ApiHelper;
import api.TaskScheduler;
import api.requests.ConfirmOrderChangesRequest;
import di.CargodealApp;
import common.EmptyCompletion;
import common.OnCompletion;
import common.Utils;
import di.DependencyContainer;
import model.ChangeData;
import model.Model;
import model.MyOrderChangesInfo;

import java.util.LinkedHashMap;
import java.util.List;

public class ActivityOrderChangesViewModel extends AndroidViewModel {
    private final ApiHelper apiHelper;
    DependencyContainer dependencyContext;
    public MutableLiveData<LinkedHashMap<Integer, ChangeData>> changedProperties = new MutableLiveData<>();
    private MyOrderChangesInfo orderChanges;
    private Model model;
    private TaskScheduler scheduler;

    public ActivityOrderChangesViewModel(@NonNull Application application) {
        super(application);
        dependencyContext = ((CargodealApp) application).getDc();
        model = dependencyContext.getModel();
        apiHelper = dependencyContext.getApi();
        scheduler = dependencyContext.getScheduler();
    }

    private int orderNumber;
    public void init() {
        Model.MyOrderInfo activeOrderInfo = model.getMyOrderInfo();
        if (activeOrderInfo == null)
            throw new IllegalStateException("No active order found");
        orderNumber = activeOrderInfo.orderNumber;
        orderChanges = model.getMyOrderChangesInfo(orderNumber);
        changedProperties.setValue(orderChanges.getChanges());
    }

    public void confirmChanges(OnCompletion callback) {
        //TODO make ConfirmOrderChangesTask instead of new executeConfirmOrderChangesRequest
        List<String> authorIds = orderChanges.getAuthorIds();
        authorIds.stream().forEach(authorId -> {
            executeConfirmOrderChangesRequest(dependencyContext, orderNumber, authorId, new EmptyCompletion());
        });
        callback.onComplete(true);
    }

    private void executeConfirmOrderChangesRequest(DependencyContainer dependencyContext, int orderNumber, String authorId, OnCompletion callback) {
        ConfirmOrderChangesRequest request = new ConfirmOrderChangesRequest(dependencyContext, orderNumber, authorId, callback);
        if (Utils.checkInternetConnection(dependencyContext.getContext())) {
            scheduler.execute(request.new Task());
        }
        else {
            scheduler.schedule(request.new Task(), ConfirmOrderChangesRequest.Task.CONFIRM_ORDER_CHANGES_RETRY_PERIOD);
        }
    }
}
