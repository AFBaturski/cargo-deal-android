package viewmodels;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import di.CargodealApp;
import di.DependencyContainer;
import model.ChangeData;
import model.Model;

import java.util.Map;

public class MyOrderFragmentViewModel extends AndroidViewModel {
    private DependencyContainer dependencyContext;
    private Model model;
    public MutableLiveData<Map<Integer, ChangeData>> orderChanges = new MutableLiveData<>();

    public MyOrderFragmentViewModel(@NonNull Application application) {
        super(application);
         dependencyContext = ((CargodealApp) application).getDc();
         model = dependencyContext.getModel();
    }

    public void checkForOrderChanges() {
        Model.MyOrderInfo orderInfo = model.getMyOrderInfo();
        if (orderInfo == null) {
            return;
        }
        orderChanges.setValue(model.getMyOrderChangesInfo(orderInfo.orderNumber).getChanges());
    }
}
