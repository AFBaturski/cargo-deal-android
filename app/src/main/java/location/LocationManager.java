package location;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.IntentSender;
import android.os.Looper;
import android.os.SystemClock;
import android.widget.Toast;
import androidx.annotation.NonNull;
import api.TaskScheduler;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import common.*;
import di.CargodealApp;
import di.DependencyContainer;
import features.log.LogWriter;
import model.Model;
import org.greenrobot.eventbus.EventBus;
import trips.TripRecorder;

import java.util.Locale;

import static location.Location.createLocation;

public class LocationManager {
  public static final int CHECK_GPS_PERIOD = 30 * 1000;
  public static final int REQUEST_CHECK_SETTINGS = 101;
  private static final long UPDATE_INTERVAL_WITH_ACTIVE_ORDER = 1000;
  private static final long ORDER_FASTEST_UPDATE_INTERVAL = 1000;
  private static final long UPDATE_INTERVAL_NO_ACTIVE_ORDER = 5 * 60 * 1000;
  private static final long NO_ORDER_FASTEST_UPDATE_INTERVAL = 60 * 1000;
  private static final long CHECKING_REQUEST_LOCATION_INTERVAL = 60 * 1000;
  private static location.Location sLastDriverLocation = null;
  private Coord previousCoord;
  private long previousTime = -1;
  private double previousSpeed;
  private location.Location location;
  private double lastCourse;
  private TripRecorder tripRecorder;

  private final Context context;
  private final TaskScheduler scheduler;
  private final Model model;
  private FusedLocationProviderClient mFusedLocationClient;
  private LocationRequest mLocationRequest;
  private LocationCallback mLocationCallback;

  public LocationManager(DependencyContainer dependencyContext) {
    context = dependencyContext.getContext();
    scheduler = dependencyContext.getScheduler();
    model = dependencyContext.getModel();
    tripRecorder = dependencyContext.getTripRecorder();
  }

  public void init(Service service) {
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(service);
    mLocationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        processLocation(locationResult.getLastLocation());
      }
    };

    createLocationRequest();
    getLastLocation();
    requestLocationUpdates();
    startCheckingRequestLocationTask();
  }

  private void deInit() {
    removeLocationUpdates();
  }

  private void createLocationRequest() {
    if (Model.getInstance().getMyOrderInfo() != null)
      createActiveOrderLocationRequest();
    else
      createNoOrderLocationRequest();
  }

  private void createActiveOrderLocationRequest() {
    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(UPDATE_INTERVAL_WITH_ACTIVE_ORDER);
    mLocationRequest.setFastestInterval(ORDER_FASTEST_UPDATE_INTERVAL);
    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
  }

  private void createNoOrderLocationRequest() {
    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(UPDATE_INTERVAL_NO_ACTIVE_ORDER);
    mLocationRequest.setFastestInterval(NO_ORDER_FASTEST_UPDATE_INTERVAL);
    mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
  }

  private void recreateLocationRequest() {
    removeLocationUpdates();
    createLocationRequest();
    requestLocationUpdates();
  }

  public void checkLocationSettings(Activity activity) {
    requestLocationSettings(mLocationRequest, activity);
  }

  private void requestLocationUpdates() {
    try {
      if (mFusedLocationClient != null && mLocationCallback != null) {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.getMainLooper());
      }
    } catch (SecurityException e) {
      LogWriter.log("Lost location permission. Could not request updates. " + e);
    }
  }

  private void requestLocationSettings(LocationRequest locationRequest, final Activity activity) {
    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
        .addLocationRequest(locationRequest);

    Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(context).checkLocationSettings(builder.build())
        .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
          @Override
          public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
          }
        })
        .addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            int statusCode = ((ApiException) e).getStatusCode();
            if (statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
              handleLocationResolutionRequired(e, activity);
            }
          }
        });
  }

  private void removeLocationUpdates() {
    try {
      if (mFusedLocationClient != null && mLocationCallback != null)
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    } catch (SecurityException e) {
      LogWriter.log("Lost location permission. Could not remove updates. " + e);
    }
  }

  public void getLastLocation() {
    try {
      mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<android.location.Location>() {
        @Override
        public void onComplete(@NonNull Task<android.location.Location> task) {
          if (task.isSuccessful() && task.getResult() != null) {
            location.Location location = createLocation(task.getResult());
            Model.getInstance().saveDriverLocation(location);
          }
        }
      });
    } catch (SecurityException e) {
      LogWriter.log(e.getLocalizedMessage());
    }
  }

  public void handleLocationResolutionRequired(Exception e, Activity activity) {
    ResolvableApiException resolvable = (ResolvableApiException) e;
    try {
      resolvable.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
    } catch (IntentSender.SendIntentException exception) {
      LogWriter.logException(exception);
    }
  }

  private void startCheckingRequestLocationTask() {
    scheduler.schedule(new Runnable() {
      @Override
      public void run() {
        if (model.getMyOrderInfo() != null
            && mLocationRequest.getInterval() != UPDATE_INTERVAL_WITH_ACTIVE_ORDER) {
          LogWriter.log("INCORRECT REQUEST LOCATION INTERVAL: " + mLocationRequest.getInterval());
          recreateLocationRequest();
        }
      }
    }, 0, CHECKING_REQUEST_LOCATION_INTERVAL);
  }

  public void processLocation(android.location.Location androidLocation) {
    if (checkIsMock(androidLocation))
      return;

    final location.Location location = createLocation(androidLocation);
    if (previousCoord != null) {
      float distance = location.getCoordinate().distanceTo(previousCoord);
      if (previousTime == -1)
        previousTime = SystemClock.elapsedRealtimeNanos();
      long deltaTime = (location.getElapsedRealtimeNanos() - previousTime) / 1000000 / 1000;
      double accelerate = deltaTime != 0 ? (location.getSpeed() / 3.6 - previousSpeed / 3.6) / deltaTime : 0;

      if (location.getSpeed() < 20 && distance > 10) {
        LogWriter.log(String.format(Locale.getDefault(), "CHECKING ACCELERATE. accelerate: %.1f speed: %.1f deltatime: %d distance: %.1f", accelerate, location.getSpeed(), deltaTime, distance));
      }

      //Проверка на скачки GPS
      boolean correctData = (LocationChecker.isCorrectLocationData(previousSpeed, accelerate));
      if (this.location != null && !correctData) {
        if (distance > 10) {
          String logString = String.format(Locale.getDefault(), "INCORRECT DATA: time: %d acc: %.1f speed: %.1f dist: %.1f", deltaTime, accelerate, location.getSpeed(), distance);
          LogWriter.log(logString);
        }
        updatePreviousLocation(location);
        return;
      }
    }

    location.setSpeed((this.location == null && previousSpeed == -1) ? -1 : location.getSpeed());

    if (location.getCourse() >= 0 && location.getSpeed() >= CargoDealSystem.MIN_SPEED_FOR_COURSE_DETECTION)
      lastCourse = location.getCourse();

    this.location = location;
    this.location.setCourse(lastCourse);

    if (location.getElapsedRealtimeNanos() < previousTime) {
      LogWriter.log(String.format(Locale.getDefault(), "NEGATIVE TIME DELTA: %d, testLocation: %.1f", (location.getElapsedRealtimeNanos() - previousTime) / 1000, location.getCoordinate().distanceTo(previousCoord)));
      return;
    }

    if (location.getElapsedRealtimeNanos() == previousTime) {
      LogWriter.log(String.format(Locale.getDefault(), "ZERO TIME DELTA: %d, testLocation: %.1f", (location.getElapsedRealtimeNanos() - previousTime) / 1000, location.getCoordinate().distanceTo(previousCoord)));
    }

    if (location.getSpeed() != -1 && previousCoord != null) {
      sLastDriverLocation = location;
      tripRecorder.processUserPosition(location);
      EventBus.getDefault().post(new MessageEvent(null, MessageEvent.GPS_DATA_CHANGED, null));
    }

    updatePreviousLocation(location);
  }

  private void updatePreviousLocation(location.Location location) {
    previousSpeed = location.getSpeed();
    previousTime = location.getElapsedRealtimeNanos();
    previousCoord = location.getCoordinate();
  }

  private boolean checkIsMock(android.location.Location androidLocation) {
    isMock = androidLocation.isFromMockProvider();
    if (isMock) {
      Toast.makeText(context, "Подставные координаты!", Toast.LENGTH_SHORT).show();
      return true;
    }
    return false;
  }

  private static boolean isMock = false;
  public static boolean isMockGps() {
    return isMock;
  }

  public static location.Location getLastDriverLocation() {
    return sLastDriverLocation;
  }

  public void changeLocationUpdatesInterval() {
    recreateLocationRequest();
  }
}
