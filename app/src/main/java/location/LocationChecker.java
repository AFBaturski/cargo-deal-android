package location;

public class LocationChecker {
    private static final double MIN_SPEED_FOR_FIX_LOCATION = 0;
    private static final double MAX_ALLOWED_ACCELERATE = 1.8;

    public static boolean isCorrectLocationData(double previousSpeed, double accelerate) {
        return (accelerate < MAX_ALLOWED_ACCELERATE) &&  previousSpeed > LocationChecker.MIN_SPEED_FOR_FIX_LOCATION; //&& accelerate > (Config.MAX_ALLOWED_ACCELERATE*-1));
    }
}
