package location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import di.CargodealApp;
import di.DependencyContainer;
import features.log.LogWriter;
import notification.NotificationHelper;

public class LocationReceiver extends BroadcastReceiver {
    private static LocationReceiver sInstance;
    private final NotificationHelper notificationHelper;

    public LocationReceiver(DependencyContainer dependencyContext) {
        this.notificationHelper = dependencyContext.getNotificationHelper();
    }

    public static LocationReceiver createInstance(DependencyContainer dependencyContext) {
        if (sInstance == null){
            sInstance = new LocationReceiver(dependencyContext);
        }
        return sInstance;
    }

    public static LocationReceiver getInstance() {
        return sInstance;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null)
            return;

        final String action = intent.getAction();
        LogWriter.log( "GpsLocationReceiver: " + action);
        LocationManager locationManager = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!(gps && network)){
            notificationHelper.updateNotification("Неточное местоположение!");
        } else {
            notificationHelper.updateNotification(notificationHelper.getForegroundNotificationText());
        }
    }
}
