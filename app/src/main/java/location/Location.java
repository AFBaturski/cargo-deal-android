package location;

import common.Coord;

import java.io.Serializable;


public class Location implements Serializable {
    private Coord mCoordinate;
    private double mSpeed = 0;
    private double mCourse = 0;
    private long mElapsedRealtimeNanos = 0;
    private long mTime = 0;
    private float mAccuracy;

    public static Location createLocation(android.location.Location location) {
        return new Location(location);
    }

    private Location(android.location.Location location) {
        mCoordinate = new Coord(location.getLatitude(), location.getLongitude());
        mSpeed = toKilometersPerHour(location.getSpeed());
        mCourse = location.getBearing();
        mTime = location.getTime();
        mElapsedRealtimeNanos = location.getElapsedRealtimeNanos();
        mAccuracy = getAccuracy();
    }

    private double toKilometersPerHour(float speed) {
        return speed*3.6;
    }

    public Coord getCoordinate() {
        return mCoordinate;
    }

    public double getSpeed() {
        return mSpeed;
    }

    public double getCourse() {
        return mCourse;
    }


    public void setCoordinate(Coord coordinate) {
        this.mCoordinate = coordinate;
    }

    public void setSpeed(double speed) {
        this.mSpeed = speed;
    }

    public void setCourse(double course) {
        this.mCourse = course;
    }

    public long getElapsedRealtimeNanos() {
        return mElapsedRealtimeNanos;
    }

    public void setElapsedRealtimeNanos(long elapsedRealtimeNanos) {
        this.mElapsedRealtimeNanos = elapsedRealtimeNanos;
    }

    public float getAccuracy() {
        return mAccuracy;
    }

    public void setAccuracy(float accuracy) {
        mAccuracy = accuracy;
    }

    public long getTime() {
        return mTime;
    }
}
